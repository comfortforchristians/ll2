---
date:
publishDate:
title: "NAME: A Biographical Sketch"
slug: "SLUG"
categories: ["Biographical Sketches"]
tags: ["LAST", "Lutheran Ministers", "Evangelical Review"]
authors: ["LAST, FIRST", "Krauth, Charles Porterfield"]
synods: ["SYNOD"]
---

{{% toc %}}

…content here…


# Publication Information

- _Author_: __"Krauth, Charles P."__, Editor.
- _Journal_: __"The Evangelical Review."__
- _Originally Published_: 1854.
- _Lutheran Library Edition_: 2019
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

{{% alert note %}}
{{% biographical-sketch-note %}}
{{% request-pdf %}}
{{% /alert %}}
