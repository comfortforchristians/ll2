---
date: 
publishDate: 
title: "NAME by AUTHOR"
slug: "SLUG"
categories: ["Lutheran Library Publications"]
tags: ["LASTNAME"]
authors: ["LAST, FIRST MIDDLE"]
titles: ["TITLE"]
origpublishers: ["Evangelical Review"]
origdates: ["1915"]
synods: ["SYNOD"]
---
<brief intro>


{{% toc %}}

# Book Contents

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/SLUG-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/SLUG.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request SLUG&body=Please send SLUG.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request SLUG&body=Please send SLUG.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 
- _Version 4 update_:  
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
