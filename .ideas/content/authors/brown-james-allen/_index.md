---
name: James Allen Brown
---
Ursula Kroeber Le Guin (October 21, 1929 – January 22, 2018) was an American novelist. The New York Times described her as “America’s greatest  science fiction writer”, although she said that she would prefer to be known as an “American novelist”.