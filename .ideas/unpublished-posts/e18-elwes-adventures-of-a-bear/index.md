---
date: 2017-08-07 12:00:00
title: 'The Adventures of a Bear by Alfred Elwes'
slug: "e18-elwes-adventures-of-a-bear"
categories: ["Lutheran Library Publications"]
tags: ["Elwes", "Extras", "Animals", "Children", "Fiction"]
authors: ["Elwes, Alfred"]
titles: ["The Adventures of a Bear"]
origpublishers: ["Addey and Co."]
origdates: ["1857"]
synods: ["N/A"]

---

"Yes, it is an "at home" to which I am going to introduce you; but not the at-home that many of you--I hope _all_ of you--have learnt to love, but the at-home of a bear. No carpeted rooms, no warm curtains, no glowing fireside, no pictures, no sofas, no tables, no chairs; no music, no books; no agreeable, cosy chat...
 
{{% toc %}}

## A Lutheran Library "Extra"

["Extras"](/tags/extras/) are entertaining or otherwise valuable books which don't fit the regular publishing guidelines for the Lutheran Library. The editors hope you will find them worthwhile.

## Chapters

- The Adventures Of A Bear And A Great Bear Too
- At Home.
- Upon His Travels.
- Town Life.
- Prosperity.
- Reverses.
- Progress.
- Down Hill.
- At Rest.

## Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download) 

![book cover](/img/posts/e18-elwes-adventures-of-a-bear-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/e18-elwes-adventures-of-a-bear.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request e18&body=Please send e18-elwes-adventures-of-a-bear.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request e18&body=Please send e18-elwes-adventures-of-a-bear.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}
