---
date: 2017-10-13 12:00:00
title: "The Story of the Jews by James K. Hosmer"
slug: "125ln-hosmer-story-of-the-jews"
categories: ["Lutheran Library Publications"]
tags: ["Hosmer", "Jewish Christians", "Missions"]
authors: ["Hosmer, James Kendell"]
titles: ["The Story of the Jews"]
origpublishers: ["G. P. Putnam's Sons"]
origdates: ["1889"]
synods: ["N/A"]

---
There has always been hatred of Jewish people.  Saddest is when it has come from professing Christians.  Today hatred of Jews raises its head in Christendom under the banner of Christian Palestinianism.  The warning from Scripture is clear.  Taking the side of those who would seek to harm the Jewish people is to place one's self against the Holy One of Israel.  "Vengeance is mine.  I will repay," saith the Lord.

Writes Hosmer:

>In many periods in almost all lands, whoever sinned or suffered, the Jew was accused, and the occasion straightway made use of for attacks in which hundreds or thousands might perish. When the "Hep, Hep!" was heard, he might well raise his hands in despair.
 
{{% toc %}}

# "Pogrom Survivors"

The cover of the book is a 1918 painting by the Hungarian artist Alfred Lakos. It reflects the pogroms which took place throughout Ukraine and Russia after the Russian Revolution.  The Lwów Pogrom (November 21 to 23, 1918) carried out by uniformed Polish soldiers, was particularly brutal.  

The word *pogrom* comes from the Yiddish, meaning devastation or destruction.  In English, Websters Third Unabridged defines it as, "an organized massacre and looting of helpless people usually with the connivance of officials; specifically: such a massacre of Jews".

# Sections of the Book

- Part I – Ancient Pride
    - Chapter 1 – Why The Story Of The Jews Is So Strikingly Vivid
    - Chapter 2 – The Morning-time In Palestine
    - Chapter 3 – Israel At Nineveh
    - Chapter 4 – The Destruction Of Sennacherib
    - Chapter 5 – Judas Maccabeus, The Hebrew William Tell
    - Chapter 6 – The Beauty Of Holiness
    - Chapter 7 – Vespasian And Josephus
    - Chapter 8 – Titus On The Ruins Of Zion
- Part II – Medieval Humiliation
    - Chapter 9 – How The Rabbis Wrought The Talmud
    - Chapter 10 – The Holocausts In Spain
    - Chapter 11 – The Bloody Hand In Germany
    - Chapter 12 – The Frown And The Curse In England, Italy, And France
    - Chapter 13 – Shylock – The Wandering Jew
    - Chapter 14 – The Casting Out Of A Prophet
- Part III – The Breaking of the Chair
    - Chapter 15 – Israel’s New Moses
    - Conclusion

# The Recurring Story

"The (mediaeval) Hebrew story is everywhere the same substantially – a constant moan as it were, with variations indeed, but seldom a note in which we miss the quality of agony. In their best estate, the Jews were but chattels of the sovereign, who sometimes followed his interest in protecting them. The king kept his Jews as the farmer keeps his bees, creatures whose power for mischief is to be feared, but tolerated for their marvelous faculty of storing up something held to be of value. As the price of his protection, the prince helped himself from the Jew's hoard, sometimes leaving the Jew enough for a livelihood, – enough sometimes, indeed, to maintain a rich state. 

- If they increased, however, the potentate did not scruple to sell them, as the farmer sells his superfluous swarms; and 
- If fanaticism drove out in the royal mind the sense of greed, as in the case of Richard Coeur de Lion, St. Louis, and Isabella, the Jew had no defense against a world in arms before him. 
- If sickness prevailed, it was because the Jews had poisoned the wells; 
- If a Christian child were lost, it had been crucified at a Jewish ceremony; 
- If a church sacristan was careless, it was the Jews who had stolen the Host from the altar, to stab it with knives at the time of the Passover. 

"In many periods in almost all lands, whoever sinned or suffered, the Jew was accused, and the occasion straightway made use of for attacks in which hundreds or thousands might perish. The wild cry of the rabble, "Hep! hep!" said to be derived from the Latin formula, "*Hierosolyma est perdita*"[^abV] might break out at any time. 

"The Jew was made conspicuous, sometimes by a badge in the shape of a wheel, red, yellow, or parti-colored, fixed upon the breast. In some lands the mark was square and placed upon the shoulder or hat. At Avignon the sign was a pointed yellow cap; at Prague, a sleeve of the same color; in Italy and Germany, a horn-shaped head-dress, red or green. This distinguishing mark or dress the Jew was forced to wear, and when the "Hep, Hep!" was heard, he might well raise his hands in despair. 

"He might indeed flee to the Turk [Muslim]; but the tender mercies of the Turk, tolerant as he was as compared with the Christian, were often very cruel. – From Chapter 11

[^abV]: "Jerusalem is lost."

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download) 

![book cover](/img/posts/125ln-hosmer-story-of-the-jews-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/125ln-hosmer-story-of-the-jews.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 125ln&body=Please send 125ln-hosmer-story-of-the-jews.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 125ln&body=Please send 125ln-hosmer-story-of-the-jews.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}
