---
date: 2017-08-01 12:00:00
title: "The Adventures of a Cat by Alfred Elwes"
slug: "e16-elwes-adventures-of-a-cat"
categories: ["Lutheran Library Publications"]
tags: ["Elwes", "Extras", "Cats", "Animals", "Children", "Fiction"]
authors: ["Elwes, Alfred"]
titles: ["The Adventures of a Cat"]
origpublishers: ["Addey and Co."]
origdates: ["1857"]
synods: ["N/A"]

---
'Being now a Cat of some years’ standing (I do not much like remembering how many), I was of course a Kitten on making my entry into life, – my first appearance being in company with a brother and three sisters...'

{{% toc %}}

# A Cruel Wrong Done to the Race of Cats

"It is a cruel wrong done to our race to exclaim, as many do, that “Cats have no attachments, no tenderness; that there is always a lurking fierceness in their hearts, which makes them forget, with the first mark of roughness or ill treatment, a thousand benefits which they may have formerly received.” I deny it wholly. I, a Cat, affirm that, with few exceptions, there are no animals more loving or more tender...How would you, dear reader, act if your tail were wantonly pulled, or if your house were to be entered by an ugly stranger without invitation? – Miss Minette Gattina, from the introduction.

 

# What are Lutheran Library "Extras"?

["Extras"](/tags/extras/) are entertaining or otherwise valuable books which don't fit the regular publishing guidelines for the Lutheran Library. The editors hope you will find them worthwhile.

# Chapters

- Preface.
- Introduction.
- Kittenhood.
- Dangers.
- A New Leaf.
- Love And War.
- Reflections And Resolutions.
- Life Abroad.
- The Wheel Of Fortune.

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download) 

![book cover](/img/posts/e16-elwes-adventures-of-a-cat-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/e16-elwes-adventures-of-a-cat.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request e16&body=Please send e16-elwes-adventures-of-a-cat.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request e16&body=Please send e16-elwes-adventures-of-a-cat.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}
