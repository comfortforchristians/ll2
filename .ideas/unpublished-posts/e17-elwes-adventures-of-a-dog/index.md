---
date: 2017-08-01 12:00:00
title: "The Adventures Of A Dog by Alfred Elwes"
slug: "e17-elwes-adventures-of-a-dog"
categories: ["Lutheran Library Publications"]
tags: ["Elwes", "Extras", "Dogs", "Animals", "Children", "Fiction"]
authors: ["Elwes, Alfred"]
titles: ["The Adventures of a Dog"]
origpublishers: ["George Routledge And Co."]
origdates: ["1857"]
synods: ["N/A"]

---
Alfred Elwes describes the source of these adventures as follows:

"From the archives of the city of Caneville, I lately drew the materials of a Bear’s Biography. From the same source I now derive my “Adventures of a Dog.” My task has been less that of a composer than a translator, for a feline editoress, a Miss Minette Gattina, had already performed her part. This latter animal appears, however, to have been so learned a cat–one may say so deep a puss–that she had furnished more notes than there was original matter. Another peculiarity which distinguished her labors was the obscurity of her style; I call it a peculiarity, and not a defect, because I am not quite certain whether the difficulty of getting at her meaning lay in her mode of expressing herself or my deficiency in the delicacies of her language. I think myself a tolerable linguist, yet have too great a respect for puss to say that any fault is attributable to her.

Minette Gattina closes her introduction to the story with these words:

"Such was the Dog whose autobiography I have great pleasure in presenting to the world. Many may object to the unpolished style in which his memoirs are clothed, but all who knew him will easily pardon every want of elegance in his language; and those who had not the honor of his acquaintance, will learn to appreciate his character from the plain spirit of truth which breathes in every line he wrote. I again affirm that I need make no apology for attaching my name to that of one so worthy the esteem of his co-dogs, ay, and co-cats too; for in spite of the differences which have so often raised up a barrier between the members of his race and ours, not even the noblest among us could be degraded by raising a “mew” to the honor of such a thoroughly honest dog.

 
{{% toc %}}

## What are Lutheran Library "Extras"?

["Extras"](/tags/extras/) are entertaining or otherwise valuable books which don't fit the regular publishing guidelines for the Lutheran Library. The editors hope you will find them worthwhile.

## Chapters

- Preface.
- Introduction By Miss Minette Gattina.
- Early Days.
- Changes.
- Ups And Downs.
- The Inundation.
- Pains And Pleasures.
- Duty.

## Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download) 

![book cover](/img/posts/e17-elwes-adventures-of-a-dog-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/e17-elwes-adventures-of-a-dog.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request e17&body=Please send e17-elwes-adventures-of-a-dog.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request e17&body=Please send e17-elwes-adventures-of-a-dog.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}
