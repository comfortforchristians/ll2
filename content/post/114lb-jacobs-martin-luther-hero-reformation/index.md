---
date: 2017-09-09 12:00:00
title: 'Martin Luther: The Hero of the Reformation by Henry Eyster Jacobs'
slug: "114lb-jacobs-martin-luther-hero-reformation"
categories: ["Lutheran Library Publications"]
tags: ["Luther", "Biography", "Jacobs", "The Reformation"]
authors: ["Jacobs, Henry Eyster"]
titles: ["Martin Luther: The Hero of the Reformation"]
origpublishers: ["G.P. Putnam's Sons"]
origdates: ["1898"]
synods: ["General Council"]

---
Henry Eyster Jacobs' biography of Luther was originally published in 1898 as part of Putnam's *Heroes of the Reformation* series for self-learners. Professor Samuel Macauley Jackson, says this about it an early Putnam catalog:

>A series of biographies of the leaders in the Protestant Reformation.
>
>The Literary skill and the standing as scholars of the writers who have agreed to prepare these biographies will, it is believed, ensure for them a wide acceptance on the part not only of special students of the period but of the general reader.  Full use will be made in them of the correspondence of their several subjects and of any other autobiographical material that may be available.  The general reader will be pleased to find all these citations translated into English. – Backmatter.  Samuel Jackson. "Huldreich Zwingli: The Reformer of German Switzerland 1484-1531" (Future release by the Lutheran Library)

Jacobs' prose is a pleasure to read.  The illustrations add to the enjoyment of this Lutheran Library title.

[About Henry Eyster Jacobs](/henry-eyster-jacobs/)

{{% toc %}}
 

# Book 1. The Monk (1483-1517)

1. Birth And Childhood
2. Student Life
3. In The Cloister
4. The Professor

# Book 2. The Protestant (1517-1522)

1. The Sale Of Indulgences; And The 95 Theses
2. The Reception Of The Theses And The Heidelberg Conference
3. Eck, Prierias, and the Pope
4. Before Cajetan at Augsburg
5. Miltitz and the Leipzig Disputation
6. Political Complications; New Allies; The Three Great Treatises Of 1520
7. The Bull
8. The Diet Of Worms
9. At The Wartburg

# Book 3. The Reformer (1522-1546)

1. Carlstadt And The Zwickau Prophets
2. Rebuilding
3. The Lines Drawn
4. The Peasants’ War
5. Marriage
6. Visitation Of Churches And The Catechisms
7. Zwingli And The Marburg Colloquy
8. Coburg And Augsburg
9. The Schmalkald League And The Struggles With Rome And Fanaticism
10. Vergerius; The Wittenberg Concord; And The Schmalkald Articles
11. New Triumphs And Trials
12. The Landgrave Of Hesse
13. Diet Of Ratisbon; Controversies With The Jurists, Emperor, And Pope
14. Luther’s Theology
15. Home Life And Last Days

# Extras 

- Appendix I – Bull Of Leo X. Against The Errors Of Martin Luther And His Followers
- Appendix II – Luther’s Confession
- A Selection from the Catalogue of G. P. PUTNAM’S SONS

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download) 

![book cover](/img/posts/114lb-jacobs-martin-luther-hero-reformation-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/114lb-jacobs-martin-luther-hero-reformation.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 114lb&body=Please send 114lb-jacobs-martin-luther-hero-reformation.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 114lb&body=Please send 114lb-jacobs-martin-luther-hero-reformation.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}
