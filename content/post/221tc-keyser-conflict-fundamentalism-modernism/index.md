---
date: 2018-03-06 12:00:00
title: "The Conflict Of Fundamentalism And Modernism by Leander Sylvester Keyser"
slug: "221tc-keyser-conflict-fundamentalism-modernism"
categories: ["Lutheran Library Publications"]
tags: ["Keyser", "Apostasy"]
authors: ["Keyser, Leander Sylvester"]
titles: ["The Conflict Of Fundamentalism And Modernism"]
origpublishers: ["The Lutheran Literary Board"]
origdates: ["1926"]
synods: ["Iowa Synod"]

---
"What's happening in 21st Century "Evangelical Christianity" echoes a battle that took place 100 years ago.  According to Dr. Keyser, modernists can be identified by these tendencies:

1. They place high importance on "being strictly up-to-date".
2. They boast of "scholarship" above faithfulness to the Scriptures.
3. They take a rationalistic approach to the Word.
4. They believe in evolution.
5. They tend to reject the supernatural events in the Bible such as the flood.
6. They reject some or all of these specific doctrines of historic Christianity:

 - The plenary inspiration of the Bible
 - The Virgin Birth of our Lord
 - The real Godhood of our Lord
 - The vicarious or substitutionary atonement wrought by our Lord through His sufferings and death
 - The bodily resurrection of Christ
 - The apocalyptic or visible second coming of Christ to raise the dead and judge the world. 

7. They show respect for "ethnic religions".

{{% toc %}}

# Contents (48 pages)
- About the Author – Leander Sylvester Keyser
- Copyright Information
- Publishers’ Note
- Just A Brief Foreword
- Chapter 1 – The Crucial Nature Of The Conflict
- Chapter 2 – The Main Features Of Modernism
    1. Some Tenets To Be Commended
        1. Theism
        2. The Modernists often display a high regard for the Bible that is, as they have “expurgated” it.
        3. In general the Modernists have a high regard for Christ.
    2. Modernistic Holdings Of The Wrong Kind
        1. As its name implies, it professes to be very “modern.”
        2. Another hall-mark of the Modernists is their boast of “scholarship.”
        3. A third insignia of Modernism is its rationalistic attitude toward the Bible.
        4. Evolution.
        5. Arm in arm with the acceptance of evolution goes, to a large degree, the rejection of the supernatural.
        6. There are at least six specific doctrines of historic Christianity that the Modernists cannot accept.
        7. Praise of “Ethnic Religions”.
- Chapter 3 – The Position Of The Fundamentalists
    1. It may be frankly admitted that some of them are more earnest than gentle.
    2. The Fundamentalists stand firmly, unalterably for the orthodox doctrines.
    3. Taking a firm and stalwart position on the Bible, they logically accept, ex animo, whatever they believe to be the clear teaching of the Bible.
    4. The Fundamentalists also hold it to be a Christian duty to defend the faith, and not to sit idly by and let the enemies beset and capture the citadel of truth.
    5. Fundamentalism is not Premillennialism.
    6. The Fundamentalists are sometimes accused of being opposed to science.
- Chapter 4 – What Is The Duty Of The Evangelical Churches? 
    1. They surely cannot remain placid and indifferent in this crucial contingency.
    2. Many strong books have been published in recent years on the positive side of this controversy.
    3. Summary of These Works.
- Dr. Keyser’s Book List

# About the Author – Leander S. Keyser

Leander S. Keyser was born March 13, 1856 in Tuscarawas County, Ohio.  He was educated at Wittenberg College Seminary, Springfield, Ohio, and served pastorates at La Grange, Indiana (1879-1881), Elkhart, Indiana (1883-1889), Springfield, Ohio (1889-1895), Atchison, Kansas (1897-1903), and Dover, Ohio (1903-1911). He was appointed professor of Systematic Theology at Hamma Divinity School in 1911, and was considered one of the leading theologians of the General Synod.

Dr. Keyser's works include _The Conflict Between Fundamentalism and Modernism, The Rational Test, A System of Christian Evidence (Apologetics), A System of General Ethics, A System of Natural Theism, and In The Redeemer's Footsteps_.

Professor Keyser went to be with his Lord on October 18, 1937.[^aD3]

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)

![book cover](/img/posts/221tc-keyser-conflict-fundamentalism-modernism-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/221tc-keyser-conflict-fundamentalism-modernism.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 221tc&body=Please send 221tc-keyser-conflict-fundamentalism-modernism.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 221tc&body=Please send 221tc-keyser-conflict-fundamentalism-modernism.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}

[^aD3]: Retrieved 2018-03-04 from cyclopedia.lcms.org "Leander Sylvester Keyser"