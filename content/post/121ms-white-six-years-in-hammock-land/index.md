---
date: 2017-10-08 12:00:00
title: 'Six Years in Hammock Land by Ralph Jerome White'
slug: "121ms-white-six-years-in-hammock-land"
categories: ["Lutheran Library Publications"]
tags: ["White", "Missions"]
authors: ["White, Ralph Jerome"]
titles: ["Six Years in Hammock Land"]
origpublishers: ["United Lutheran Publication House"]
origdates: ["1922"]
synods: ["United Lutheran Church"]
---
"As one travels up and down the Berbice River there are two things that grow upon him. The first is an ever increasing appreciation of the beauty of that tropical stream, while the second is a knowledge of the vileness of degraded man. Neither the beauty nor the vileness are at first so evident. Both are revealed only upon close acquaintance."

{{% toc %}}

# British Guiana

White subtitles his book, "An historical sketch of the Lutheran Church in British Guiana, with observations and experiences of the Missionary of the United Lutheran Church in that land."

This fascinating little book is part travelogue, part history, and part reflections on a missionary life.  Lutheran Pastor Ralph Jerome White's writing style is fresh and unadorned.  Here he writes of an unusual service:

>It was here at St. Lust that the missionary and his wife had a rather interesting experience about five years ago with tarantulas.
>
>We arrived at St. Lust from St. Paul’s Mission on a Sunday afternoon to hold an evening service. The little chapel was packed with people. We had a few oil lamps and the parson’s lantern was placed on the pulpit so that he could see to read the Scripture lessons. While he was preaching there was something about the size of his hand and black and hairy, that dropped from the thatched roof and struck the lantern. One of the members knocked it to the floor and dispatched it with his foot. This was our first sight of a full-sized tarantula.
>
>After the service we killed about half a dozen of those large black fellows that appeared between the roof leaves. After this we got out our folding cots with their mosquito nets, and putting them up in the open chapel, retired. Under the loose floor pigs and goats grunted and snored, and fleas left pig and goat for a cannibalistic diet. However, after some time, we managed to get to sleep.

# Contents

- 1 – Scenes And Experiences En route To Hammock Land
- 2 – A Brief History And Description Of The Country
- 3 – Planting And Development Of The Churches Of British Guiana
- 4 – The History Of The Lutheran Church In Berbice
- 5 – Our Church In Berbice As It Is Today
- 6 – The River Churches As A Mission Field
- 7 – Mount Hermon Mission And The Practice Of Obeah
- 8 – The Work Among The Arrawak Indians
- 9 – The Native Indians
- 10 – Snakes Insects, Animals And Birds
- 11 – Some Experiences In Visiting Our River Missions

>...At first all the people seemed normal. They came to Church properly dressed. They listened to the sermons with attention. They were respectful in their attitude. They seemed to be sincerely anxious to lead truly Christian lives. Many of them were found upon close acquaintance to be truly sincere and noble people of God. But so many still believe in and fear the obeah (voodoo) man; so many live lives of immorality unashamed; so many children are illegitimate ; so many acts of brutality are committed unrebuked that the old time Mission Hymn still applies to this country with force and truth, and sadly we must say that for many it is: 

    "In vain with lavish kindness 
     The gifts of God are strown."

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download) 

![book cover](/img/posts/121ms-white-six-years-in-hammock-land-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/121ms-white-six-years-in-hammock-land.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 121ms&body=Please send 121ms-white-six-years-in-hammock-land.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 121ms&body=Please send 121ms-white-six-years-in-hammock-land.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}
