---
date: 2019-05-05 09:41:01-04:00
publishDate: 2019-05-05 09:41:01-04:00
title: "The Purple Robe: A Novel by Joseph Hocking"
slug: "320-hocking-the-purple-robe"
categories: ["Lutheran Library Publications"]
tags: ["Hocking", "Roman Catholicism", "Fiction"]
authors: ["Hocking, Joseph"]
titles: ["The Purple Robe"]
origpublishers: ["Ward, Lock & Company"]
origdates: ["1909"]
synods: ["Methodist"]

--- 

"It is exceedingly clever, and excites the reader's interest and brings out the powerful nature of the clever young minister. This most engrossing book challenges comparison with the brilliance of Lothair. Mr. Hocking has one main fact always before him in writing his books––to interest his readers; and he certainly succeeds admirably in doing so."  ––The Queen.

---

"All of us long for forbidden fruit. This may explain why Alison Neville, of Neville Priory, desired to enter a Nonconformist Church. Had she been free to do as she liked about the matter, the possibilities are that she would not have had the faintest inclination in this direction; but being a Roman Catholic, as her forefathers had been for many generations, she had been led to regard worship within unconsecrated walls as a species of blasphemy. Hence the desire to take part in a Protestant service."

# Joseph Hocking

Joseph Hocking was a faithful Nonconformist minister in Wales.  Rev. Hocking considered the novel an ideal platform for exploring the deeper aspects of life.  His books deal with trials, difficulties and issues of faith.

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)

![book cover](/img/posts/320-hocking-the-purple-robe-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/320-hocking-the-purple-robe.pdf)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 320&body=Please send 320-hocking-the-purple-robe.mobi)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 320&body=Please send 320-hocking-the-purple-robe.epub)


{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2018-12-12
- _Version 4 update_: 2019-05-05 
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
