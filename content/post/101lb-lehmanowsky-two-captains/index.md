---
date: 2019-04-17 10:12:28-04:00
publishDate: 2019-04-17 10:12:28-04:00

title: Under Two Captains – The Autobiography of John Jacob Lehmanowsky 
slug: "101lb-lehmanowsky-two-captains"
categories: ["Lutheran Library Publications"]
tags: ["Biography", "Lehmanowsky", "Napoleon", "French Revolution", "War", "Jewish Christians", "Soldiers", "Inquisition"]
authors: ["Lehmanowsky, John Jacob", "Sandt, William Augustus"]
titles: ["Under Two Captains: The Autobiography of John Jacob Lehmanowsky"]
origpublishers: ["United Lutheran Publication House"]
origdates: ["1902"]
synods: ["General Council"]
---

John Lehmanowsky was born in Warsaw in 1773 to a Jewish family, and as a young man he converted to Christianity.  Through a series of events he became known to Napoleon, and took part in many campaigns including the destruction of the Inquisition at Madrid.  He escaped from Austerlitz, and later prison.  As an immigrant to the US, he served the church in many ways, and was a friend to Lafayette, Henry Clay, and others.  Lehmanowsky explains:

>"I write my tale of a life not from vanity or love of this world: but to set forth the fact of the guiding hand of Almighty God in my life and in the life of the world. In one life among the untold billions as well as in the conduct of the vast universe, God is present."

Some have the mistaken idea that Christian men are wimpy.  The Centurion who believed Christ and courageous military officers like Col. Lehmanowsky squelch that notion.

May our brother's memory as captured in this book live on in the hearts and minds of a new generation.  Thank you dear Lord, for heroes.  Amen.


# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![Lehmanowsky](/img/posts/101lb-lehmanowsky-two-captains-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/101lb-lehmanowsky-two-captains.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 101lb&body=Please send 101lb-lehmanowsky-under-two-captains.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 101lb&body=Please send 101lb-lehmanowsky-under-two-captain.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}

# Publication Information

- _Lutheran Library edition first published_: 2017-10-01
- _Version 4 update_: 2019-04-17 
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)


[^BX]: "Preface to the Original 1902 Edition"
