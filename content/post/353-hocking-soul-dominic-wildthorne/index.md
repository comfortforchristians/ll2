---
date: 2019-05-16 15:13:29-04:00
publishDate: 2019-05-16 15:13:29-04:00

title: "The Soul of Dominic Wildthorne by Joseph Hocking"
slug: "353-hocking-soul-dominic-wildthorne"
categories: ["Lutheran Library Publications"]
tags: ["Hocking", "Fiction", "Oxford Movement", "Roman Catholicism", "Jesuits"]
authors: ["Hocking, Joseph"]
titles: ["The Soul of Dominic Wildthorne"]
origpublishers: ["Hodder and Stoughton"]
origdates: ["1908"]
synods: ["Methodist"]
---
"Mr, Wildthorne," said Maggie, "have you fulfilled the promise you made me the last time we met?" 

"What promise?" 

"You promised me that you would read an authoritative life of Luther, an authoritative history of the Reformation."

"Did I? Oh, yes, I remember. But why should' I? I have read a great deal of Church history."

"Yes; but you admitted that you had not read an authoritative life of Luther; that you only read such books as spoke of him as a Philistine and a clown, and which regarded the Reformation as the result of an appeal to the mob. Besides, you promised."


# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/353-hocking-soul-dominic-wildthorne-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/353-hocking-soul-dominic-wildthorne.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 353-hocking-soul-dominic-wildthorne&body=Please send 353-hocking-soul-dominic-wildthorne.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 353-hocking-soul-dominic-wildthorne&body=Please send 353-hocking-soul-dominic-wildthorne.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2019-05-16    
- _Version 4 update_: 2019-05-16 
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
