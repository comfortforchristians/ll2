---
date: 2019-04-25 05:21:56-04:00 
publishDate: 2019-04-25 05:21:56-04:00 
title: "Tales from Catland for Little Kittens by An Old Tabby by Tabitha Grimalkin"
slug: "e15-grimalkin-tales-from-catland"
categories: ["Lutheran Library Publications"]
tags: ["Fiction", "Animals", "Children", "Cats"]
authors: ["Grimalkin, Tabitha"]
titles: ["Tales from Catland for Little Kittens by An Old Tabby"]
origpublishers: ["Ticknor, Reed, and Fields"]
origdates: ["1852"]
synods: ["N/A"]
---

"Once upon a time I can’t say exactly when it was there stood a neat, tidy little hut on the borders of a wild forest. A poor old woman dwelt in this hut. She lived on the whole pretty comfortably; for, though she was poor, she was able to keep a few goats, that supplied her with milk, and a flock of chickens, that gave her fresh eggs every morning: and then she had a small garden, which she cultivated with her own hands, and that supplied her with cabbages and other vegetables, besides gooseberries and apples for dumplings. Her goats browsed upon the short grass just outside the garden, and her chickens ran about everywhere, and picked up everything they could find. There were some fine old trees which defended the cottage on three sides from the cold winds, and the front was to the south; so it was very snug and sheltered. The forest afforded her sticks and young logs for fuel, so that she never was in want of a fire; and, altogether, she managed to make out a pretty comfortable life of it.

– From "The Discontented Cat"


# Stories in "Tales From Catland For Little Kittens"

1. The Three Cats.
2. The Discontented Cat.
3. The Wishing-day.



# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download) 

![book cover](/img/posts/e15-grimalkin-tales-from-catland-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/e15-grimalkin-tales-from-catland.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request e15&body=Please send e15-grimalkin-tales-from-catland.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request e15&body=Please send e15-grimalkin-tales-from-catland.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2017-08-01 
- _Version 4 update_: 2019-04-25
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
