---
date: 2018-10-29 12:00:00
title: "Martin Luther's Large Catechism translated by Henry Eyster Jacobs"
slug: "194tc-jacobs-luther-large-catechism"
categories: ["Lutheran Library Publications"]
tags: ["Luther", "Confessions", "Book of Concord"]
authors: ["Jacobs, Henry Eyster"]
titles: ["Martin Luther's Large Catechism"]
origpublishers: ["United Lutheran Publication Society"]
origdates: ["1911"]
synods: ["General Council"]

--- 
Perhaps the best introduction to Luther's Work.  The essence of Biblical Christianity is here.  Read Luther!

{{% toc %}}

# From a Pastor

"For Reformation Sunday I talked a little about the Large Catechism, which I owned as a separate paperback (Augsburg Press, ALC). I read it all the time for years, because it also serves as another way to read Luther's Sermons, but using the Catechism as an outline.  When I wanted to show how the efficacy of the Word was basic to the Bible, Luther, and Lutheran doctrine, I used the Large Catechism."

– Pastor Greg Jackson, Bethany Lutheran Church, from the post [Martin Luther's Large Catechism - A Great Theology Book for Pastors and Laity](http://ichabodthegloryhasdeparted.blogspot.com/2018/10/martin-luthers-large-catechism-great.html)

# From the Translator:

"The popular edition, here offered, fulfills the hope of the editor from the very beginning, to have the Confessions published at such price that they may he scattered broadcast throughout all English-speaking lands. . . Such edition will serve an important office in deepening and strengthening the faith of our people in drawing them together in the bonds of a common fellowship, and in enabling them to appreciate all the more highly their heritage. 

"While in many other religious bodies confessional lines have vanished and confessional obligations weakened, a standard is here raised around which millions in this western world will rally. __The attentive reader . . . will see that the matters here treated are not antiquated or obsolescent, but enter most deeply into the issues of the hour__.

"We send forth this volume with gratitude for the privilege of having been called to edit it . . . and in the full confidence that it will be a blessing to our Church in America, and, through it, in advancing the kingdom of our Lord Jesus Christ, in whose name these confessions were written. 

– [Henry Eyster Jacobs](/henry-eyster-jacobs/), From the Preface.

# Luther from the Large Catechism

"Oh, what mad, senseless fools are we, that while we must ever live and dwell among such mighty enemies as devils, we nevertheless despise our armor and defense, and are too indolent to look for, or think of them! 

"And what else are such supercilious, presumptuous saints, who are unwilling to read and study the Catechism daily, doing, but esteeming themselves much more learned than God himself with all his saints, angels, patriarchs, prophets, apostles, and all Christians? 

"For inasmuch as God himself is not ashamed to teach the same daily, since he knows nothing better to teach, and always keeps teaching the same thing, and does not take up anything new or different, and all the saints know nothing better to learn, or different, and cannot learn this perfectly, are we not wonderful men to imagine, if we have once read or heard it, that we know it all, and have no farther need to read and learn, but can learn perfectly in one hour what God himself cannot finish teaching, since he continues teaching it from the beginning to the end of the world, and all prophets, together with all saints, have been occupied with learning it but in part, and are still pupils, and must remain such? 

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)

![book cover](/img/posts/194tc-jacobs-luther-large-catechism-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/194tc-jacobs-luther-large-catechism.pdf)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 194tc&body=Please send 194tc-jacobs-luther-large-catechism.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}
[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 194tc&body=Please send 194tc-jacobs-luther-large-catechism.epub)


