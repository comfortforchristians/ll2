---
publishDate: 2019-02-21 05:01:00-05:00
date: 2019-02-21 05:01:00-05:00
title: "Eli Huber: A Biographical Sketch"
slug: "eli-huber"
categories: ["Biographical Sketches"]
tags: ["Huber", "Lutheran Ministers"]
authors: ["Huber, Eli"]
synods: ["Pennsylvania Synod"]
---


Dr. Eli Huber was born Jan. 14, 1884, in Pinegrove, Schuylkill Co., Pa., and belonged to that class of people known as Pennsylvania Germans, who are the descendants of the emigrants who came to this country at an early period from the southern part of Germany. Jacob and Sarah Huber are the names of his parents. His father's ancestors are reported to have come from Switzerland. Both parents possessed good natural abilities though deprived by force of circumstances and the times of a good education. His father used to say laughingly, that he completed the usual course of that day — from the alphabet to the Psalter — in three months. But though themselves deprived of the advantages of even a good common school education, they valued it all the more highly, and used all diligence and practiced self-denial even to secure better opportunities for their children. 

Both his father and mother were persons of superior moral character and earnest, warm-hearted Christians. His father was at first a member of the German Reformed church but subsequently united with the Lutheran church to which his mother belonged. He received his preliminary education in the public schools of his native place, and was prepared in chief part for college by Hon. James L. Nutting, a graduate of Bowdoin College, Maine. He entered the Freshman class of Pennsylvania College, Gettysburg, in the fall of 1851, and graduated with honor from the same institution in 1855. 

After leaving college he taught an academy at Green Castle, Franklin Co., for several years. He also held the position of tutor in Pennsylvania College during the summer of 1857. Whilst teaching at Green Castle during the winter of 1855-6, he felt the necessity of beginning a decided Christian life. Lying awake one night from pain caused by toothache, he made up his mind to postpone the work of repentance no longer, and accordingly resolved within himself that from that hour he would yield obedience to all God's will, doing what he commanded, and abstaining from everything that was displeasing in His eyes. Instantly a voice within seemed to say: Tomorrow morning when you go to breakfast you ought to say grace, thanking God for his goodness in supplying all your needs. He had to admit that he ought to do this, but the time was so near at hand when it was to be done, and he imagined that the family with whom he was boarding would think it strange on his part, and the result was he immediately recalled the promise made but a moment before and decided to postpone further consideration of the matter until the morning. Morning came and he awoke and took up his little Bible to read; he happened to get hold of the tenth chapter of St. Matthew, and in that met the passage concerning confessing Christ before men. This pointed declaration by Christ himself had sufficient influence to bring him to the determination to go to the table and do his duty. This simple act of asking a blessing before meals was his first open and decided step in his religious course, and it has had its influence over his thinking and teaching during his whole subsequent ministry. 

Acting on this same principle of doing what God wished, he made up his mind to join church. He accordingly entered the class of catechumens, which Rev. E. Breidenbaugh, pastor of the Lutheran church at Green Castle, was at the time preparing for confirmation. Soon after uniting with the church, he also abandoned his purpose of studying law, and decided to fit himself for the Christian ministry. A few words spoken by two Christian women, — Mrs. E. Breidenbaugh and Mrs. John Kitzmuller — had much to do in bringing about the decision to study theology. After studying privately for several years under the direction of Mr. Breidenbaugh, he entered the Theological Seminary at Gettysburg and prepared himself for licensure at the meeting of the Synod of East Pennsylvania, held in Bloomsburg, September, 1858. He was, after due examination, admitted to the Christian ministry as a licentiate. He was ordained one year later at the meeting of this same Synod at Harrisburg, and is at this date a member of the Synod that licensed and ordained him. 

He was married in the spring of 1860 to Miss Ellen Dubert, of Schuylkill Haven, Pa. 

The degree of Doctor of Divinity was conferred upon him in 1884 by Pennsylvania College, Gettysburg, his Alma Mater. 

His first field of labor was at Schuylkill Haven, having been directed there by prominent members of the East Pennsylvania Synod to start an English Lutheran church. An organization, consisting of twenty-nine members, was soon effected, and in a year thereafter a small brick church was erected. The fruit of this planting is the present prosperous St. Matthew's Lutheran church building and parsonage, surpassed by none in the place. 

This work was accomplished without any aid from the Board of Home Missions. His salary the first year amounted to $300 and he boarded at the best hotel in the place at that. At the end of two years he accepted a call to one of the Lutheran churches at Danville, but remained there only nine months. In the fall of 1861 he took charge of the church in Hummelstown, with which were connected two smaller congregations near by. He labored here for five years, till September, 1866, at which time he was appointed as a home missionary to Nebraska City, Neb., beginning his work there in the latter part of October, 1866. He established a church here, and also one in the country, about ten miles from the city. 

On the 1st of March, 1876, he began his labors as pastor of Messiah Lutheran Church of Philadelphia, which is his present charge. 

Whilst Dr. Huber's success in the ministry cannot be regarded as unusual, those who know him and his work look upon him as an efficient workman. 

His method of preaching as to matter is expository, and in this he greatly delights and indulges in perhaps to excess. His manner of speaking is extemporaneous, but with careful preparation. He talks rapidly — too much so till people become accustomed to him — and with perhaps more animation than is generally agreeable. But though fast he is distinct, his ideas being clearly conceived and well arranged, and his language plain. He is easy to understand, even though his mode of thought is somewhat abstract. 

After quitting his work in Nebraska he accepted a call to the Messiah Lutheran Church, of Philadelphia, March 1, 1876. The congregation at that time numbered about 125 members, and were worshiping in the basement of their present fine edifice. The church has been completed at an additional cost of $30,000, which has been paid off by degrees, together with part of a mortgage previously on the property. The present membership is between three and four hundred, and that of the Sunday School two hundred and fifty to three hundred. 

He is at present a member of the Board of Directors of the Theological Seminary at Gettysburg; was for three years president of the East Pennsylvania Synod, and also a member of the Board of Publication and of the Examining Committee. 

He was appointed to deliver the Holman Lecture for 1885, on the Second Article of the Augsburg Confession. This lecture is delivered annually to the students of the seminary, and the lecturer is appointed by the seminary's Board of Directors. He also prepared the little book intended to be put into the hands of persons after confirmation, called ["Food for the Heavenly Way"](/212wd-huber-food-for-the-heavenly-way/).

{{% alert note %}}
This biography is taken from Jens Jensson's _American Lutheran Biographies_, published 1890.
{{% /alert %}}

First published online 2019 at [LutheranLibrary.org](.).