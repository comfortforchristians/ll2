---
date: 2019-03-11 08:47:05-04:00
title: "The Victory of Faith – Lutheran Meditations by Rev. John Henry Harms"
slug: "207wd-harms-victory-faith-lenten-season"
categories: ["Lutheran Library Publications"]
tags: ["Harms"]
authors: ["Harms, John Henry"]
titles: ["The Victory of Faith: Lutheran Meditations"]
origpublishers: ["United Lutheran Publication House"]
origdates: ["1946"]
synods: ["United Lutheran Church"]


---
"We might get the idea that our Master was always in retreat, passively enduring His hostilities, always on the defense, never driving onward to the attack. Yet all the time He was sweeping on in victory, reaching for His crown, which was a cross. 

{{% toc %}}

# The Lenten Season

The Lenten season begins 46 days before Easter Sunday.  This little book of Lutheran devotions is _light but meaty_.  It includes short meditations for each day.  Consider downloading Rev. Harms' book and using it yourself or with your family as a means to get closer to our Lord, the Christ revealed in his Word.

# About the Prodigal

"We do not need to go as far as the Prodigal went to do our sinning or to get into all the trouble he got into. It is possible to do our sinning nearer home than that. The "far country" is not a matter of mileage so much as it is a matter of morals. We run away from God with our affections, not our feet. The elder brother was as bad a sinner, with his anger, as the prodigal, although he never left his father's house." – Rev. John Henry Harms

# Sample devotion – Thursday­ – 45 Days Before Easter

Read: 1 Timothy 1:1-15 

"Timothy 1:15 'This is a faithful saying and worthy of all acceptation that Christ Jesus came into the world to save sinners." 

Here is a saying to be celebrated, worthy of all acceptation, praise. And it is a saying that cannot be disputed, or subjected to any question as to its veracity. It is a faithful saying, sure as God Himself, that Christ Jesus came to save sinners. 

Think of a sinning world against the background of this assertion. Where is the world going now, whither is it headed." Such a world as ours." What is the hope of getting things straight again? Where is there salvation for such a world?

Is it in science? Will economics save us? Will secular education pull us up out of the awful mire of strife and hate? It looks as if there is nothing left but the spirit of revenge; as if the only thing the nations have in common is things to fight with. 

It is to our world and all its people, to us and our fellowmen, this saying sounds today. The only Saviour is Jesus Christ. Nothing is ever settled right unless He settles it. Nobody is ever saved unless He saves them. 

There must be some of us, right now, celebrating Him, as the sinner's Saviour and the Saviour of a sinning world. 

_O Creator and Ruler of mankind, look in mercy on us and help us in our distress. Deliver us from our present sorrows and lead all nations to the love of Him whom Thou hast sent; ever Jesus Christ, our Lord. Amen._ 

# About Rev. John Henry Harms

John Henry Harms graduated from Newberry College in 1893 and from the Gettysburg Lutheran Theological Seminary in 1897. His brother, John Edward also graduated from the Gettysburg Seminary and was a Lutheran preacher.

His first call was to Trinity Lutheran Church in Chambersburg, Pennsylvania, serving there until 1900, when he accepted a call from the Newport (Perry County, Pennsylvania) Lutheran Charge. After a short stay of eighteen months, he was called to Bethlehem Lutheran Church in Harrisburg.

In the summer of 1908, he was notified by telegram of his election to the presidency of Newberry College. He accepted the call and served until 1918. During this period, in 1912, he received his D. D. from Erskine College, Due West, South Carolina.

In 1918, he began his work at the Lutheran Church of the Holy Communion in Philadelphia. He was to continue in this call for 27 years, resigning in 1945.

During this pastorate he also served as the president of the Board of Publications of the United Lutheran Church. And, in 1945, received a Litt. D. from Newberry College.

He was the author of at least two books: The Victory of Faith: Devotions for the Lenten Season" and "From Day to Day: Book of Daily Devotions.

Rev. Harms passed away at his home in Philadelphia after a long illness.[^aBY]

# Contents of the Book

- The Victory of Faith – Lutheran Meditations for the Lenten Season
- Copyright Information
- About the Author
- Ash Wednesday​ – 46 Days Before Easter
- Thursday​ – 45 Days Before Easter
- . . . Wed - Sat . . .
- First Sunday In Lent​ – 42 Days Before Easter
- . . . Mon - Sat . . .
- Second Sunday In Lent​ – 35 Days Before Easter
- . . . Mon - Sat . . .
- Third Sunday In Lent​ – 28 Days Before Easter
- . . . Mon - Sat . . .
- Fourth Sunday In Lent​ – 21 Days Before Easter
- . . . Mon - Sat . . .
- Passion Sunday​ – 14 Days Before Easter
- . . . Mon - Sat . . .
- Palm Sunday​ – 7 Days Before Easter
- Monday​ – 6 Days Before Easter
- Tuesday​ – 5 Days Before Easter
- Wednesday​ – 4 Days Before Easter
- Thursday​ – 3 Days Before Easter
- Friday​ – 2 Days Before Easter
- Saturday​ – Day Prior To Easter
- Easter Sunday


# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)

![book cover](/img/posts/207wd-harms-victory-faith-lenten-season-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/207wd-harms-victory-faith-lenten-season.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 207wd&body=Please send 207wd-harms-victory-faith-lenten-season.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 207wd&body=Please send 207wd-harms-victory-faith-lenten-season.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}

[^aBY]: [source](https://www.findagrave.com/memorial/108507163/john-henry-harms)