---
date: 2017-11-15 12:00:00
title: The Foreign Mission Work of Pastor Louis Harms and the Church at Hermansburg by Emanuel Greenwald
slug: "149ms-greenwald-louis-harms-and-hermansburg"
categories: ["Lutheran Library Publications"]
tags: ["Missions", "Greenwald", "Harms", "Short Books", "Biography"]
authors: ["Greenwald, Emanuel"]
titles: ["The Foreign Mission Work of Pastor Louis Harms and the Church at Hermansburg"]
origpublishers: ["Lutheran Board of Publication"]
origdates: ["1867"]
synods: ["Ohio Synod"]

---
"A single congregation can sustain an entire mission. There is no grander spectacle in the history of the whole Church, than this noble work of that one congregation of plain peasant Lutherans at Hermansburg...
 
{{% toc %}}

# The Foreign Mission Work of Pastor Louis Harms and the Congregation at Hermansburg

"Louis Harms was the beloved pastor of a plain, but pious congregation. Before his connection with it, it had little practical religious life. He was a Lutheran of the old sort, like those of the earlier days of our Church in Germany. He believed firmly the doctrines of Luther, and held tenaciously to the old forms and customs of the Church. He was plain and unassuming in his manners; a man of the people, with strong sympathies for the poor and outcast, and he lived among his people as a father. He was at the same time highly educated, refined in his intercourse with cultivated society, and very genial, and even jocund in his feelings. He was self-denying and whole-souled, with strong faith and great activity in doing good. He was a godly and earnest man, and by his teachings, his influence, and his example, a wonderful improvement was effected in his congregation, of which he was the honored and useful pastor...
"
"One congregation, single-handed, organize and support a mission thousands of miles away from home, consisting of more than a hundred missionaries, and an equal number of mechanics and farmers sent to constitute a Christian settlement. It is almost incredible. Judging by the unwillingness and want of large liberality displayed on such subjects everywhere about us, we would almost refuse to believe the accounts, if the facts were not so well supported. This one congregation of Lutherans at Hermansburg does more for Foreign Missions, single-handed, than is done by the whole Lutheran Church in the United States, consisting of forty-one Synods, one thousand six hundred and fifty ministers, three thousand congregations, and more than three hundred thousand communicants.[^aki]

# The Work of Missions

"The work of missions must go on. It dare not stop. It is the only method known, or that can be devised, to accomplish the result desired. The whole world must be evangelized, and the only agency to do it, is the Bible, with the living preacher, sent forth and sustained by the church. It is a noble work. It is the peaceful messenger bearing with him light, civilization, Christianity, and everlasting salvation, to the benighted, degraded, barbarous, and lost tribes of men. What a blessed work![^akj]

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download) 

![book cover](/img/posts/149ms-greenwald-louis-harms-and-hermansburg-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/149ms-greenwald-louis-harms-and-hermansburg.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 149ms&body=Please send 149ms-greenwald-louis-harms-and-hermansburg.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 149ms&body=Please send 149ms-greenwald-louis-harms-and-hermansburg.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}
[^aki]: Emanuel Greenwald. *Pastor Louis Harms and the Church at Hermansburg.*

[^akj]: Emanuel Greenwald. *Ibid.* 