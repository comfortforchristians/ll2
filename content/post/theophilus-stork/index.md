---
date: 2019-02-04 00:18:00
publishDate: 2019-02-04 00:18:00-05:00
title: "Theophilus Stork: A Biographical Sketch"
slug: "theophilus-stork"
categories: ["Biographical Sketches"]
tags: ["Stork"]
authors: ["Stork, Theophilus"]
synods: ["Unknown"]
---

[![Theophilus Stork](/img/posts/stork-theophilus.jpg)](https://www.lutheranlibrary.org/authors/stork-theophilus/)

# Rev. Theophilus Stork, D.D.

As to its external facts and changes, Dr. Stork's life may be easily told. Eternity alone can unfold the full extent of the work he did. The most useful and influential life is not always marked by the greatest changes or crowded with the most exciting incidents. He was born in North Carolina, where his father preached the gospel with marked and blessed results. He was early brought to Christ, and became at once an open and pronounced Christian. His education was secured in the institutions of the church at Gettysburg, in which he took advanced grade, and where his memory is still fondly cherished. Entering the ministry about the year 1837, his first labors were given to Winchester, Va. Of the character and results of his first ministry I feel authorized to speak with confidence, as it was my good fortune, in after years, to occupy the same pulpit. Even to this day he is there remembered with undiminished confidence and affection, and his efforts spoken of in terms of highest praise. For long years his friends of that congregation maintained frequent communication with him, and consulted him freely upon questions of mutual interest. 

During his ministry in that place the present church edifice was projected, and many of the most active and most zealous members of the congregation, who have ever since adorned the doctrine of God their Savior in all things, were brought into the church. From Winchester he came to Philadelphia as pastor of St. Matthew's, and thus, for the second time, I have become his successor, entering into his labors. In this city his public ministerial life is known and read of all men. He was faithful in all things, and successful, in more than an ordinary degree, in leading sinners to the Saviour. The additions to the membership, and the strong, undying attachments of those received, give full and satisfactory evidence of his power over men and of his fidelity to Christ. 

"The memory of the just is blessed," and we listen today, with a sad and chastened interest, to the unstinted praises and strong utterances of undying attachment from many sorrowing hearts of those who, in long years past, called Dr. Stork their pastor, and received the word of the Lord from his lips. "He being dead yet speaketh" in the works and words of a large and loving spiritual family. 

Realizing then already the need of enlarged and advanced church accommodations, Dr. Stork proposed the enterprise of a new congregation in the rapidly increasing northwest section of the city. Leading the movement in person, he succeeded beyond expectation in building St. Mark's congregation. As in each field of labor, so in this; his works do follow him. In all that St. Mark's has been and may yet become, the merit of its founding, as well as the wisdom of its timely inception, will be given to Dr. Stork. In this, as in all his movements, he showed genuine progressive courage and unfaltering childlike faith. He had faith in God and in God's word. He saw there was need and the ability to supply that need, and unhesitatingly assumed the responsibility of guiding and consummating the movement; and, by God's grace and man's help, he succeeded. Let his success inspire many similar movements in this and in all the cities of our land, until all who need are supplied as advantageously as are those who followed him so readily in founding that new monument to his memory. 

From the pulpit of St. Mark's Dr. Stork was called to the presidency of Newberry College, S. C. To this new field of labor he gave his maturist efforts, and very soon gave promise of being as acceptable as a teacher as he had been as a preacher. The hope of improving his impaired health exercised no little influence in deciding the question of his change of occupation and location. The milder winters of the south and the change of surroundings it was hoped would affect him beneficially. Not without hesitation, yet with much characteristic enthusiasm, he entered this new and untried work, hoping, if possibly in wider sphere, by educating the future educators of the Church, to serve the cause he loved so well. But, ere he could become fairly engaged and interested, and his aptness or success become apparent, the disturbed condition of the country so far interfered with the conduct of the institution, and the prospect of the early adjustment of our civil difficulties was so unsatisfactory, that Dr. Stork very soon resigned and retired from the college, and once more held himself in readiness to serve his day and generation in the pastoral office. 

Nor did he long wait for an engagement. St, Mark's Church, Baltimore, thankfully seized the opportunity, and urged his acceptance of their call to become pastor of their newly organized congregation. To this he readily acceded, and at once became a favorite within and without his charge. Under his faithful and affectionate care the church grew in every element of congregational strength, and now, under the charge of his son, is one of the most active and liberal congregations of our Church. Serving this people until his own son was prepared to assume charge thereof, Dr. Stork once more returned to Philadelphia, so dear to him by the most cherished associations. He was welcomed anew by hosts of devoted friends, who rejoiced in the prospect of long continued association with him. 

Nor was he long disengaged. In this immediate locality he saw the need of a church, and began the unpromising work of establishing it. Circumstances interfered with the consummation of his original purpose, and the gradual failure of his health, attended at times with alarming symptoms, compelled a new line of engagement and a change of labor. 

Gifted as a ready, ornate and acceptable writer, with strong literary tastes and long culture, he prepared his own productions for the press, all of which met with a flattering reception, and served with untiring interest in the work of the Publication Society of our Church. 

The last issue of the _Lutheran Home Monthly_, appearing about the same day with his death, contains his last literary labor, and, by a most singular and touchingly interesting coincidence, its leading editorial has the striking title, "I am now ready." 

We believe he was ready, awaiting Christ's coming. He was an humble Christian, that highest style of man, called, redeemed, pardoned, and accepted in Jesus Christ. 

He was a faithful Christian minister, devoted to his calling and "steadfast in the faith." He was an exemplary servant of Jesus Christ, doing his Master's will with all alacrity, and seeking ever to advance his Master's interests. Thus did he live and labor, and, in dying, could with all meekness, say, "I am ready to be offered, and the time of my departure is at hand." — Morris. 


{{% alert note %}}
This biography is taken from Jens Jensson's _American Lutheran Biographies_, published 1890.
{{% /alert %}}

First published online 2019 at [LutheranLibrary.org](.).