---
date: 2019-07-26 05:06:21-04:00
publishDate: 2019-07-26 05:06:21-04:00
title: "The Pomp of Yesterday: A Novel by Joseph Hocking"
slug: "341-hocking-the-pomp-of-yesterday"
categories: ["Lutheran Library Publications"]
tags: ["Hocking", "fiction", "war"]
authors: ["Hocking, Joseph"]
titles: ["The Pomp of Yesterday"]
origpublishers: ["Hodder and Stoughton"]
origdates: ["1915"]
synods: ["Methodist"]
---
The Pomp of Yesterday is a novel inspired by Rudyard Kipling's poem _Recessional_. Its message of England at the height of her glory has meaning for America today.

{{% toc %}}

# _Recessional_ by Rudyard Kipling
>God of our fathers, known of old, \
>&emsp;Lord of our far-flung battle line, \
>Beneath whose awful hand we hold \
>&emsp;Dominion over palm and pine— \
>Lord God of Hosts, be with us yet, \
>Lest we forget—lest we forget! \

>The tumult and the shouting dies; \
>&emsp;The Captains and the Kings depart: \
>Still stands Thine ancient sacrifice, \
>&emsp;An humble and a contrite heart. \
>Lord God of Hosts, be with us yet, \
>Lest we forget—lest we forget! \

>Far-called our navies melt away; \
>&emsp;On dune and headland sinks the fire: \
>Lo, all our pomp of yesterday \
>&emsp;Is one with Nineveh and Tyre! \
>Judge of the Nations, spare us yet, \
>Lest we forget—lest we forget! \

>If, drunk with sight of power, we loose \
>&emsp;Wild tongues that have not Thee in awe, \
>Such boastings as the Gentiles use, \
>&emsp;Or lesser breeds without the Law— \
>Lord God of Hosts, be with us yet, \
>Lest we forget—lest we forget! \

>For heathen heart that puts her trust \
>&emsp;In reeking tube and iron shard, \
>All valiant dust that builds on dust, \
>&emsp;And guarding calls not Thee to guard, \
>For frantic boast and foolish word— \
>Thy Mercy on Thy People, Lord!

# Book Contents

- “Recessional” by Rudyard Kipling
- Foreword
- 1. The Man Without A Past
- 2. Sir Roger Granville’s Suggestion
- 3. The Strange Behavior Of George St. Mabyn
- 4. I Meet Captain Springfield
- 5. How A Man Worked A Miracle
- 6. Paul Edgecumbe’s Memory
- 7. A Cause Of Failure
- 8. I Become An Eavesdropper
- 9. Edgecumbe Is Missing
- 10. The Struggle In The Trenches
- 11. Edgecumbe’s Story
- 12. The Struggle On The Somme
- 13. Edgecumbe’s Madness
- 14. Edgecumbe’s Logic
- 15. Devonshire
- 16. Lorna Bolivick’s Home
- 17. A New Development
- 18. A Tragic Happening
- 19. A Mysterious Illness
- 20. A Strange Night
- 21. Colonel Mcclure’s Verdict
- 22. Edgecumbe’s Resolve
- 23. Springfield’s Progress
- 24. A Strange Love-making
- 25. Why Is Victory Delayed?
- 26. Where Does God Come In?
- 27. Seeing London
- 28. Sunshine And Shadow
- 29. Cross Currents
- 30. The March Of Events
- 31. Edgecumbe’s Return
- 32. The Great Meeting
- 33. The Lifted Curtain
- 34. Memory
- 35. Afterwards
- 36. Edgecumbe’s Resolution
- 37. Maurice St. Mabyn
- 38. A Bombshell
- 39. Springfield At Bay
- 40. Maurice St. Mabyn’s Generosity
- 41. The New Hope
- 42. An Unfinished Story

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/341-hocking-the-pomp-of-yesterday-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/341-hocking-the-pomp-of-yesterday.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 341-hocking-the-pomp-of-yesterday&body=Please send 341-hocking-the-pomp-of-yesterday.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 341-hocking-the-pomp-of-yesterday&body=Please send 341-hocking-the-pomp-of-yesterday.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2019-07-26
- _Version 4 update_: 2019-07-26 
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
