---
date: 2019-08-28 05:49:45-04:00
publishDate: 2019-08-28 05:49:45-04:00
title: "[A11] The Christian Family (The Small Catechism)"
categories: ["This Week With The Small Catechism"]
tags: ["Weekly Catechism", "The Ten Commandments", "Golladay", "Catechism", "Second Table"]
authors: ["Golladay, Robert Emory"]
titles: ["The Ten Commandments"]
origdates: ["1915"]
---

We are now ready to take up the study of the Second Table of God's holy Law. This Second Table, beginning, according to our division, with the Fourth Commandment, deals with man's relation to his fellowman. But let us not forget that, though these commandments treat of human relations, they are still God's laws. And equally well let us remember that the violation of these commands, while a sin against man, is, primarily, a sin against God Himself.

{{% toc %}}

# 11. The Christian Family

>"The Lord God said, it is not good that the man should be alone; I will make him an helpmeet for him… Therefore shall a man leave his father and his mother, and shall cleave unto his wife; and they shall be one flesh." — Gen. 2:18, 24.

>"Ye stand this day all of you before the Lord your God… your little ones, your wives, and the stranger that is in thy camp, from the hewer of wood unto the drawer of water; that thou shouldst enter into covenant with the Lord thy God, and into His oath, which the Lord thy God maketh with thee this day; that He may establish thee today for a people unto Himself, and that He may be unto thee a God, as He hath sworn unto thy fathers… lest there should be among you a man, or woman, or family, or tribe, whose heart turneth away from the Lord our God, to go and serve the gods of these nations; lest there should be among you a root that beareth gall and wormwood." — Deut. 29:10-13, 18.


In taking a general survey of this Second Table of the Law, even the most cursory student could scarcely fail to notice that there is one institution  which lies at the very foundation of more than half these commandments, and is not excluded from the others. The Fourth, the Sixth, the Ninth and Tenth Commandments presuppose directly the family relation. And we shall never get the bearing, or the real significance, of many things spoken of, or referred to, in the Second Table of the Law until we have a fair idea of this fundamental human relation, — the family. As an introduction, then, to this second part of the Law let us consider —

>__The Christian Family.__

## 1. The Family Is A Divine Institution.

God Himself established it. The family existed in Paradise before ever sin made its appearance on earth. These facts give us sufficient evidence of the holy nature of the estate, and of the sacred and important ends God intended to have accomplished through it.

The family is not only the first institution of earth as to time, but it is the first also in importance. It is the fundamental institution for the perpetuation and development of the race. It is the radiating point of all the influences which affect the children of men. As the family is, so, largely, the State is going to be, and the Church. The ideals which prevail in the family circle, and are imbibed there, become the actuating motives of those who grow into manhood and womanhood there, and will dominate them when they go out into the wider circles of business and social life.

Reformatory efforts, whether they look to improvements in the Church, the State, or the general social conditions of mankind, shall fail, and fail miserably, if they do not look, first of all, at the correction of the family life. The family is the first unit in all human relations. It is the fountainhead of human existence. And no stream can rise higher than its source.

No man can build a house which will stand, especially when the storms rage and the floods surge, on a foundation of sand. And a flourishing Church, a sane and prosperous State, a wholesome, brotherly social and business life, cannot be built upon a family life which is not sane, and wholesome, and spiritually minded. We do not have healthful, flourishing, fruit-bearing trees where the roots are cramped, or rotten. The tree surgeon may trim such a tree so that it will look very symmetrical, and fill up the decayed places with medicated cement; but if the soil conditions are not good, if the roots are slowly dying, other branches will continue to die, and other decayed places will continue to appear in the trunk, and soon there will be a dead tree. But if the soil is such as the tree needs, if the roots are sound, and perform their functions, there is good prospect of warding off the enemies which attack the trunk and branches; of continuing the life of the tree, and keeping it fruitful. 

This is a fair illustration of the relation of the family to other human institutions. It is the root, they are the branches. And the health of the branches depends on the health of the root. This is a truth which is being more and more recognized, and emphasized, by those who are not teachers of religion. Let us who are not only members of families, but members of the Church of God, not fail to learn, or having learned, not fail to profit by this lesson.

### Marriage An Ordinance of God's Own Establishment

As the basis of the family relation we have marriage, an ordinance of God's own establishment. It was He who said in the words of our text: "It is not good that the man should be alone, I will make him an help meet for him." 

It was God who brought the first lonely man a companion. 

It was God who united them, one man and one woman, in the indissoluble bonds of love, of unity of purpose, and of hearty cooperation, for life. And God is still the maker of marriages when people, in the fear and love of God, seek His good guidance. But when parents have no other thought than to be matchmakers, in order to get their children off their hands, and well settled in life; when young people, who have never had a serious thought in all their lives, rush into marriage with never a thought of its real responsibilities, with never a thought about the decisive bearing this step cannot fail to have on their own lives for time and eternity; with never a thought about the bearing this union will have on the Church, the State, the race; under such conditions marriages are not made in heaven, but in the other place. And only a miracle of grace can prevent them from being failures in every sense of the word.

Marriage! Marriage an institution of God's ordaining, sacred in all its relations! Marriage an institution through which God is to be continually glorified, and humanity to be blessed! What thoughts these terms provoke! What a noble relationship! But would right thinking people want to apply these terms, or any one of them, to a union which is the result of a thoughtless, foolish escapade? Would we want to call that a Divine union where a man only wanted a housekeeper, and the woman only wanted to escape what is sometimes foolishly spoken of as the stigma of spinsterhood? I glory in the woman who, rather than sell herself for a name, or a home, or surrender herself to a man whom she can neither respect nor love, will courageously shoulder the responsibility of carving out her own career, and live and die unmated.

And my advice to the young man is that, if he cannot find something better than an animated clothes-rack, or one of those empty-minded giggling nonentities who has never had a higher thought than eating chocolate bonbons, having a neverending good time, escaping every form of work and responsibility, and keeping a man eternally in debt, then he had better follow suit, and live and die unmated; or hunt up one of the sensible, unspoiled bachelor maids and see whether he cannot get her to change her mind.

I am giving such advice, not because I disparage or would discourage marriage, but because I have such an exalted opinion of marriage and its fundamental relation to every human institution that I would a hundred times rather see no marriages than bad ones, which wreck and ruin human lives and human institutions.

### The Home.

The center around which the family life radiates is the home. Home, one of the sweetest words in any language. If we had hours instead of minutes in which to dwell on this thought we could come nearer to doing it justice. The word home is one of those which strikes deepest into the heart of the average man or woman. Aside from Divine overruling grace there is no influence on earth which does so much to make or mar humanity as the home.

A house is not necessarily a home. A palace, with costly tapestries, statuary, and every product of art, may still be no real home. To have a home there must be the contact of loving hearts, there must be fellowship of those of kindred spirit, there must be that which draws out and strengthens the best of which human nature is capable.

If we could only get people to think more of being home-makers! It is a great thing to be a landowner, and produce live stock, and grain, with which to feed hungry people. It is a great thing to be a big business man, producing for men, or conveying to them, the necessities of life. It is a great thing to be an honest toiler of any kind, and to know that thereby one is contributing to the good of humanity. But the greatest work of all is that which has as its conscious object true home-making. Here, as nowhere else, minds are trained, characters developed, and destinies of men and nations fixed.

## 2. Christian Family Life Is The Only Hope Of The World.

Because of these undeniable facts, I affirm that Christian Family Life is the only Hope of the World.

In our second text, which is a part of the account of God's covenant made with his people on the occasion of the giving of His Law, God Himself tells us what the consequences are when men, whether as individuals, or families, or nations, depart from the Lord. He tells us that it means the introduction of "a root that beareth gall and wormwood." Such people bring on themselves, justly and inevitably, the wrath and punishment of Almighty God. And, sooner or later, it means the downfall of that man, or family, or nation. And this fall comes because men themselves despise and neglect the elements which make for safety and perpetuity, and allow the introduction of the elements which can ultimately end only in decay and dissolution.

The family circle is the primary training-school for the cultivation of all the virtues. 

>"Finally, brethren, whatsoever things are true, whatsoever things are honest, whatsoever things are just, what soever things are pure, whatsoever things are lovely, whatsoever things are of good report; if there be any virtue, if there be any praise, think on these things" (Phil. 4:8). 

I ask you where is the first place, and the most effective place, for teaching these things? There is but one answer, in the home. And they will not be taught, they cannot be effectually taught there, unless the fear and love of God rules in the home, unless the spirit of Christ reigns there. It is the Church's exalted duty to teach these things, but one reason why the Church's teaching does not produce any greater results is that they are not practiced in the home as they ought to be. None of us are perfect, we all have enough faults; but it is certain that if these things are not taught and practiced in the home, they will not be practiced out in the world.

It is in the home that human beings are brought into the closest, the dearest, the most responsible relationships in life. The Divine ideal is that humanity is to be a great brotherhood, mutually considerate and helpful. "None of us liveth to himself," saith the Lord. Now the very center of this great complex of human relationship is the family circle. Here is where the real heart-beats of humanity are felt. If here there is a true spirit of mutual love and helpfulness, if here there is willingness to make sacrifices one for another, if here the spirit of self-control is exercised, if here the spirit of selfishness is banished more and more, then it may be reasonably expected that, with proper encouragement, this spirit will be carried out into the larger relationships, into the community and the Church. But if parents are themselves supremely selfish, if they are not willing to make any sacrifices for the common good, if they constantly seek to escape responsibilities, if they are full of impatience with one another, if their children are always in the way, and everything done for them is done under protest, what is to be expected when such people get out into the world? Is it any wonder that they regard the world only as an orange to be squeezed for their own pleasure? And if children learn none of these primary virtues in the home circle, if each one lives only for himself or herself, if each one is an Ishmael with his hand against everyone else, if each one lives only to get and be served, but never to give or to serve, then there is but little hope that they will ever effectually learn these lessons. By the grace of God there will be exceptions. Just as the water lily grows up in beauty and fragrance out of the mud and miasm of a stagnant lake, so an occasional child, reared in the godless and chilly atmosphere of an unchristian home, will develop into beautiful manhood or womanhood, because the warm rays of Divine grace beam upon it from other sources than the home. But the odds are greatly against this.

How shall the one picture we have drawn be escaped, and the other be realized? How shall the root which bears gall and wormwood be destroyed? How shall the curse which means the blotting out of our names from under heaven be avoided? Only in one way, by turning to the Lord our God, by having the fear of God before our eyes, by making the Law of God the rule of our lives, by opening our hearts to be filled and ruled by the Spirit of Jesus Christ; and by making the home the first and greatest school for the teaching and practice of these heavenly virtues.

Blessed are the men and women who came out from homes where unassumed and unassuming Christian piety reigned; from homes where the only law was the law of righteousness and love; from homes where the grief of one was the grief of all, and the joy of one the joy of all; from homes where the richest heritage was a godly father's counsel, and a sainted mother's prayers. Such a home is earth's vestibule to heaven. In such a home love is a law of life, and kindly deeds are love's legitimate fruitage. If, because of human frailty, there is offense given, there is also ready reparation and forgiveness. To such a home as this the inmates will flee, as to a haven of rest, from the wearying, vexing battles which are waged in the world by warring human passions. And when to the members of such a home inevitable sorrow comes, bowed and burdened hearts are comforted and cheered by love both human and Divine. From such homes come nearly all the men and women who bless the earth with their presence and their work. The names of such homes are kept sacred in the records of heaven. And their influence shall never perish from the earth.

Brethren, on bended knees, and with hearts filled with gratitude, let us thank God if we have the memory of such homes. Their sweetening, corrective influence on our lives will never be entirely obliterated. But the only safe thing to do, for ourselves and others, is to perpetuate such homes.  Let us make our homes Christian homes in the full, rich meaning of that term. Let us make God a party to our home life. Let His Word be the daily manna of our life. Let us endeavor to put its principles into practice in the little everyday things of life as well as the great. Without this it is meaningless prattle to talk of a regenerated society, of a noble citizenship, of self-effacing, self-sacrificing officials, or of a spirit of brotherhood manifesting itself in the marts of trade. Without this it is useless to talk of a Church filled with power which shall conquer the world for Christ. 

The family and the home are threatened in our day by an ever-growing and powerful list of enemies. The growing congestion of city life, the crowding of families into apartments where real family life is next to impossible, the rearing of children in the midst of the most unfavorable environment, the increasing stress of competition which breadwinners have to face, and the multiplication of temptations for children and adults, have led students of economic and social conditions to ask the question: Can the family, the home, survive?

It behooves those who love humanity, and are interested in the coming of the Kingdom of God, to bestir themselves to the end that it shall survive; not alone in the sense of substantial dwellings, but in the sense of men and women of God mated in love, Divine and human, who will live their religion in their homes, and beget and rear children who will follow in their footsteps. Then shall the root which beareth gall and bitterness find no soil for growth in our homes. Our land will be safe and prosper. And the Kingdom of God take on new vigor. As of old the called and professing children of God, with the nation at large, are standing at a critical point in our history. What shall our answer be to God's proposals? Let the answer of each be that of the man of God of old:

"As for me and my house, we will serve the Lord."

# Publication Information

- _Author_: __"Golladay, Robert Emory"__
- _Title_: __"The Ten Commandments"__
- _Originally Published_: 1915 by Lutheran Book Concern, Columbus, Ohio.
- _Lutheran Library Edition_: 2019
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

{{% alert note %}}
{{% weekly-catechism-note %}}
{{% request-pdf %}}
{{% /alert %}}