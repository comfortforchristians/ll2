---
date: 2018-04-04 12:00:00
title: "Theosophy and New Thought by Henry Clay Sheldon"
slug: "236tc-sheldon-theosophy-and-new-thought"
categories: ["Lutheran Library Publications"]
tags: ["Apostasy", "Sheldon", "Blavatsky", "Emerson"]
authors: ["Sheldon, Henry Clay"]
titles: ["Theosophy and New Thought"]
origpublishers: ["The Abingdon Press"]
origdates: ["1916"]
synods: ["Methodist"]

---
Much of Evangelical Christianity is now what used to be called _New Thought_. In this small, easy to digest book, Rev. Sheldon makes the important connections between the ideas of Madame Blavatsky's _Theosophy_, Ralph Waldo Emerson's New Thought and what passes as mainstream Christianity today. 

{{% toc %}}

# Humanistic rather than Biblical

Rev. Sheldon writes,

"The conception of Christ characteristic of New Thought is purely humanitarian...He may be characterized as a God-man, but not as the God-man...There is no ground whatever for believing that his personality differed from that of other men. He stands before us as the moral ideal, and fulfills the office of Saviour by example. Even in his miracles he is not apart from us. The so-called miracles were perfectly conformable to law, and indicate the kind of equipment any man might use if he would but enter upon his full inheritance."


# Contents (91 pages)

- Preface
- Part I – Theosophy
  - Chapter 1. Historical Outlines
  - Chapter 2. Appraisal Of Theosophy By Theosophists
  - Chapter 3. The Attitude Assumed Toward Competing Faiths
  - Chapter 4. The Basis Of Authority
  - Chapter 5. The Doctrine Of God
  - Chapter 6. Cosmological Theories
  - Chapter 7. Conceptions Of Man And His Destiny
  - Chapter 8. The Theosophic Principle Of Authority Tested
    - 1 Madame Blavatsky falsified her History with Spiritualism
    - 2 Madame Blavatsky Played the Role of a Charlatan and Trickster
    - 3 Theosophy Was Drawn From Modern Writings, not the Mahatmas
    - 4 Tibet not evidence of exceptionally endowed instructors
    - 5 Where are the Benefits of the Supposed Mahatmas?
    - 6 Skepticism of Theosophists
  - Chapter 9. Comments On Prominent Features Of The Theosophical System
- Part II – New Thought
  - Chapter 1. General Sketch
  - Chapter 2. The Doctrine Of Man
  - Chapter 3. The Conception Of God And Of Man’s Relation To Him
  - Chapter 4. The Therapeutic Scheme
  - Chapter 5. Some Grounds Of Criticism

# About the Author – Henry Clay Sheldon

"Henry Clay Sheldon was elected to the Boston University School of Theology Faculty in 1875, where he served as processor of Systematic Theology. He held his chair for forty-six years until his retirement in 1921.

"After receiving a Bachelors and Masters at Yale in 1867 and 1870 respectively, he entered the ministry, serving a church in St. Johnsbury, Vermont (1871-73) and Brunswick, Maine (1872-75).

"Sheldon was a prolific writer; his texts include History of Christian Doctrine (in two volumes), History of the Christian Church (in five volumes), System of Christian Doctrine, Unbelief in the Nineteenth Century, Sacerdotalism in the Nineteenth Century, and New Testament Theology. Sheldon died in 1928.

"According to Sheldon, 'The art of preaching consists in seeing the truth clearly, grasping it firmly, holding it in due perspective, and proclaiming it with the whole man to the whole man. The ideal preacher is logician, prophet and poet in one.'[^aN6]

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)

![book cover](/img/posts/236tc-sheldon-theosophy-and-new-thought-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/236tc-sheldon-theosophy-and-new-thought.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 236tc&body=Please send 236tc-sheldon-theosophy-and-new-thought.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 236tc&body=Please send 236tc-sheldon-theosophy-and-new-thought.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}

[^aN6]: "Henry C. Sheldon" retrieved 2018-04-04 from https://www.bu.edu/sth-history/graduates/henry-c-sheldon-1871/