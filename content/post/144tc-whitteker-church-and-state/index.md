---
date: 2018-10-20 12:00:00
title: Church and State by John Edwin Whitteker
slug: "144tc-whitteker-church-and-state"
aliases: 
  - /144tc-whitteker-church-and-state-a-thanksgiving-sermon/
categories: ["Lutheran Library Publications"]
tags: ["Whitteker", "Two Kingdoms", "Sermons", "Short Books"]
authors: ["Whitteker, John Edwin"]
titles: ["Church and State"]
origpublishers: ["Trinity Lutheran Church"]
origdates: ["1908"]
synods: ["General Council"]

---
"There is nothing more difficult, these times, than to keep the Church out of politics. And this difficulty is intensified where a Christian principle is at stake. 

"The Church, in some of its branches, has been knocking at the door of State and clamoring for the name of God in the Constitution. This is not the Church's work: it is the province of the Church to knock at men's hearts and get the name of God written there – written there by the blood of the New Covenant; and the Constitution will take care of the Church's interests. Imagine Christ calling upon Caesar to change his coin and put God's name on it. Imagine Him calling upon Caesar to tear down the heathen temples and set up an altar to the God who dwells in the circle of the skies. He did not touch a single heathen rite; He did not interfere with a single heathen sacrifice; He planted principles. And with what result? The whole host of Roman deities have gone down to the dust, and Jesus Christ reigns supreme. 

{{% toc %}}

# Contents

This small pamphlet consists of one sermon, *Church and State*, delivered by Rev. John Edwin Whitteker at Trinity Lutheran Church, Lancaster, Pa., on November 26, 1908. 

# About Rev. John Edwin Whitteker

John Edwin Whitteker was born April 20, 1851, in Ontario, Canada. He graduated Valedictorian of Thiel College, Greenville, PA in 1875.  He was ordained in 1877, and continued work in the college until 1888, having meantime been promoted to the position of Adjunct Professor of Latin, and later to the chair of Latin Professor.

In the summer of 1888 Dr. Whitteker became pastor of the Church of the Reformation at Rochester, N. Y. During his pastorate of five years there he established three missions and built two mission churches, both of which became self-sustaining before he left that field of labor. In 1893 Mr. Whitteker was called to Easton, Pa., where he became the pastor of the old historic church of St. John's, serving two years in a very acceptable manner, at the end of which time he was called to the superintendency of the English Home Missions of the General Council, with headquarters in Philadelphia, although still retaining his residence in Easton. For the following three years he remained in that work, and in the fall of 1898 accepted a call to Grace Lutheran Church. at Rochester, Pa., which congregation under his pastoral care became one of the most prosperous in that section. Having accepted a call to the Church of the Holy Trinity, in Lancaster, he entered upon this work Feb. 1, 1901, and was installed as pastor seventeen days later. At the commencement following this event his Alma Mater honored him with the title of Doctor of Divinity.  Dr. Whitteker died in 1925.[^ahU]

[^ahU]: Source: Biographical Annals of Lancaster County, Pa., Beers, 1903, pp. 205-6.

# Some Excerpts

>The rule of the Church is a spiritual rule: the weapons of the Church are spiritual weapons: the work of the Church is a spiritual work. The Church's activities must center upon the pure preaching of the Word and the proper administration of the Sacraments. 

>Let the Church give its loftiest effort, not to little outside policies, but to the great essential principles; and Christian manhood will be the result. 

>This is no theory of mine: it is involved in the principles of Christ and His Apostles, the principles of the best men of every best age. It should, therefore, command our thoughtful respect: it should lead us to inquire where we stand on this great question of Church and State; and if we have been wrong-set, to change our course and get in the true path of the pure principles of Christ. 

>The Church must not be in politics: politics must not be in the Church. But manhood in the Church means manhood in the State; and manhood in the Church comes from the proper use of Word and Sacrament. 

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download) 

![book cover](/img/posts/144tc-whitteker-church-and-state-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/144tc-whitteker-church-and-state.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 144tc&body=Please send 144tc-whitteker-church-and-state.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 144tc&body=Please send 144tc-whitteker-church-and-state.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}
[^acK]: Jacobs, Henry Eyster, ed. (1899) *The Lutheran Cyclopedia*. New York: Charles Scribner's Sons 