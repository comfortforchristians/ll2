---
date: 2018-05-01 12:00:00
title: "Selection of Sermons by Samuel Laird"
slug: "156wd-laird-selection-of-sermons"
categories: ["Lutheran Library Publications"]
tags: ["Laird", "Sermons"]
authors: ["Laird, Samuel"]
titles: ["Selection of Sermons"]
origpublishers: ["General Council Publication House"]
origdates: ["1914"]
synods: ["General Council"]

---
"Dr. Laird belonged to a group of stalwart preachers and leaders whose influence could not be confined to a congregation or a city or a synod. Among them were numbered such men as Krauth, Krotel, Mann, Spaeth, the Schaeffers, Schmucker, Seiss, Greenwald, Passavant. They rose into prominence at a time when Lutheranism in its English form was passing through a crisis because it had in large measure forgotten the rock whence it was hewn and the pit whence it was digged.

{{% toc %}}

# Those Who Knew Dr. Laird

"To those who knew Dr. Laird as a preacher, a catechist, a pastor, a leader in the Church, and a man among men, the sermons that appear in this book have an added value. It is not only what is being preached, but also what the preacher himself is that enters into the estimate of a sermon; for what is a sermon other than the Gospel seeking utterance through a personality whom it has inspired and transformed?

"Though his modesty forbade him to assume the role of leader in the controversy which preceded and followed the organization of the General Council in 1867, he was a staunch defender of the principles for which it stood. He was wont to speak with much admiration of the men who bore the brunt of the battle, and more than once characterized them with the brief sentence, "There were giants in those days," not once thinking that he was one among them. 

– George W. Sandt

 

# Contents of the Book (199 pages)

- Introduction
- 1. The Love of God
- 2. The Attraction at Bethlehem
- 3. Jesus A Man Among Men
- 4. Men Offended at Christ
- 5. Palm Sunday
- 6. Christ Crucified
- 7. The Resurrection of Christ
- 8. The Ascension of Christ
- 9. The Glorious Church
- 10. The Coming of Christ
- 11. Christ’s Coming and Preparation For It
- 12. Turning From Idols and Waiting For Christ
- 13. Life a Gleaning
- 14. Advent
- 15. The Kingdom Among Men
- 16. Quieting Thoughts About Life
- 17. Coming to Christ
- 18. The Fullness of Salvation
- 19. The Resurrection of the Just
- 20. A Great Commission

# "I have kept back nothing that was profitable for you."

"...It was the comfort of the great apostle to the Gentiles that with a clear conscience he could say, "I have kept back nothing that was profitable for you," and that when he knew that those among whom he had gone preaching the kingdom of God, would see his face no more, he could boldly "take them to record," that he was pure from the blood of all men, and had "not shunned to declare all the counsel of God." However materialists may oppose amid the thickening troubles of these ungodly days; however distasteful it may be to the carnal mind; however scorners may jest and sneer; the ambassador of God must boldly deliver unto the people, all that they have received of the Lord. – From Chapter 20.

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)

![book cover](/img/posts/156wd-laird-selection-of-sermons-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/156wd-laird-selection-of-sermons.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 156wd&body=Please send 156wd-laird-selection-of-sermons.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 156wd&body=Please send 156wd-laird-selection-of-sermons.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}
[^aBY]: [source](https://www.findagrave.com/memorial/108507163/john-henry-harms)