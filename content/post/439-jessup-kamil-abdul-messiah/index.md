---
date: 2019-05-09 19:58:29-04:00
publishDate: 2019-05-09 19:58:29-04:00
title: "The Setting Of The Crescent And The Rising Of The Cross, or Kamil Abdul Messiah, A Syrian Convert From Islam To Christianity by Henry Jessup"
slug: "439-jessup-kamil-abdul-messiah"
categories: ["Lutheran Library Publications"]
tags: ["Jessup","Islam","Biography","Martyrs","Conversion"]
authors: ["Jessup, Henry Harris"]
titles: ["The Setting of the Crescent and the Rising of the Cross"]
origpublishers: ["The Westminster Press"]
origdates: ["1898"]
synods: ["Presbyterian"]
---
"Kamil's history is a rebuke to our unbelief in God's willingness and power to lead Muslims into a hearty acceptance of Christ and his atoning sacrifice.  We are apt to be discouraged by the closely riveted and intense intellectual aversion of these millions of Moslems to the doctrines of the Trinity and of the divinity of Jesus Christ. But Kamil's intellectual difficulties about the Trinity vanished when he felt the need of a divine Saviour. He seemed taught by the Spirit of God from the first. He exclaimed frequently at the wonderful scheme of redemption through the atoning work of Christ.

{{% toc %}}

"I have rarely met a more pure and thoroughly sincere character. From the beginning of our acquaintance in "our flowery bright Beirut," to his last days on the banks of the Tigris, he was a model of a humble, cheerful, courteous, Christian gentleman."


# Book Contents

- Author’s Preface
- Introduction
- 1. The Young Syrian
- 2. Kamil’s Journal
- 3. Kamil’s Love for His Father: Letters and Replies
- 4. The Visit to Obock and Jabuti.
- 5. Proposed Transfer to el Bosra.
- 6. Arrival in el Bosra.
- 7. Personal Danger from Fanatical Muslims
- 8. Kamil’s Last Letter.
- 9. Kamil’s Journals in el Bosra.
- 10. Kamil’s Death.
- Appendix. The Arabian Mission.

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/439-jessup-kamil-abdul-messiah-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/439-jessup-kamil-abdul-messiah.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 439-jessup-kamil-abdul-messiah&body=Please send 439-jessup-kamil-abdul-messiah.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 439-jessup-kamil-abdul-messiah&body=Please send 439-jessup-kamil-abdul-messiah.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2019-05-09 
- _Version 4 update_: 2019-05-09 
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
