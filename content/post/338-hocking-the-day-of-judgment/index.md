---
publishDate: 2019-02-07 01:02:16-05:00
title: "The Day of Judgment: A Novel by Joseph Hocking"
slug: "338-hocking-the-day-of-judgment"
categories: ["Lutheran Library Publications"]
tags: ["Hocking", "Fiction", "Atheism"]
authors: ["Hocking, Joseph"]
titles: ["The Day of Judgment"]
origpublishers: ["Cassell and Company, Ltd."]
origdates: ["1915"]
synods: ["Methodist"]
---
"Mr Hocking's novels have been compared to those of Thomas Hardy, Hall Caine, Baring-Gould, and Stanley Weyman; they are, one and all, stamped with striking and original individuality.  Bold in conception, strenuously high and earnest in purpose, daring in thought, picturesque and lifelike in description, it is not to be wondered at that Mr. Hocking's novels are eagerly awaited by a great and ever-increasing public." – Ward, Lock & Co.



# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/338-hocking-the-day-of-judgment-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/338-hocking-the-day-of-judgment.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 338-hocking-the-day-of-judgment&body=Please send 338-hocking-the-day-of-judgment.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 338-hocking-the-day-of-judgment&body=Please send 338-hocking-the-day-of-judgment.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}

