---
date: 2019-07-28 05:07:36-04:00
publishDate: 2019-07-28 05:07:36-04:00
title: "Jacob Goering: A Biographical Sketch"
slug: "jacob-goering"
categories: ["Biographical Sketches"]
tags: ["Goering", "Lutheran Ministers", "Evangelical Review"]
authors: ["Goering, Jacob", "Krauth, Charles Porterfield"]
synods: ["Pennsylvania Synod"]
---

It was said… at the time of his death, that many generations must pass away, before the world could look upon his equal. From all accounts, he must have been a most extraordinary man, gifted with rare endowments of intellect, and possessed of the noblest qualities of the heart. 

{{% toc %}}

## Early Life

In early life his opportunities for the cultivation of his mind were limited, and yet so active were his native powers, and so faithful was he in the improvement of the advantages he subsequently enjoyed, that he soon rose to an eminent position, and his name has been transmitted with high lustre to posterity. He was a man of great mental activity, of profound thought, earnest and independent inquiry, and of extensive erudition. He was regarded by all as an elegant scholar and an eloquent speaker. His perceptions were strong and clear, his habits of investigation vigorous and accurate, and so quick were his acquisitions, that they seemed almost intuitive. His thoughts, too, were uttered in the clearest, most appropriate and forceful language. Such was his clearness of apprehension, correctness of judgment and precision of expression, that he never found any difficulty in conveying the idea he intended, or making the subject plain to the comprehension of his hearer. He was always intelligible and lucid. He made others understand him, because he understood himself — 

>_Cui leda potenter erit res_ \
>_Nec facundia deseret hunc nec lucidus ordo_. 

As a student he was indefatigable, and his zeal in the pursuit of knowledge was unquenchable. It was absorbing. Nothing could check his ardor, arrest his progress, slacken his efforts, or divert him from his purpose. He was thorough, inquisitive, patient and persevering. His motto was _Nil desperandum_. Him no discouragements disheartened, no fears appalled, no labors wearied, no opposition crushed. All obstacles were disarmed and powerless, when the aim was worthy, and victory his object. 

## Publications Committed To The Flames

Although Mr. Goering was so diligent and wrote much, he published very little.[^bFQ] He seemed to have an utter aversion to the publication of any of his writings. His manuscripts contained discussions, that exhibited his original genius and energetic mind. They were not confined to the examination of theological questions, but they embraced inquiries into the oriental languages, with translations from the most beautiful of the Arabic poets. Unfortunately for literature and the church, his valuable papers, together with all his letters, during his last illness, in compliance with his directions, were committed to the flames. 

[^bFQ]:  He did publish a couple of works on the subject of baptism — _Besiegter Wiedertaufer_, 8 vo. pp. 92, 1753; and _Der Verkappie Priester Aaron_, 1790. Also an answer to a Methodist's remonstrance. Two of these publications were anonymous. 

## Irresistible Power in the Pulpit

Mr. Goering's power in the pulpit was very great. It was irresistible. He would electrify whole assemblies, transferring to them his passion at his will. No one who ever heard him, could fail to admit his uncommon power over the minds of his hearers. He was animated and fervent, and produced the conviction that he was deeply in earnest. He was always in earnest, and with a feeling heart delivered God's truths. On funeral occasions he was particularly happy. There was in his manner a tenderness and a pathos, which made them long remembered. The matter, presented at these times, usually made an abiding and permanent impression. As a pastor, he was active, zealous, and faithful, most devoted to the people of his charge, and indefatigable in his efforts to bring souls to Christ. The smiles of Heaven rested upon his labors. His congregation rapidly increased, and hopeful converts were added to the church. His preaching was of a most evangelical and practical character. The scriptures he exalted. He was not disposed to reject divine truth, because he could not comprehend it, or to elevate human reason above the Bible. The doctrines embraced in the fall of Adam, and the consequent depravity of the human race, the divinity of Christ, and the reconciliation effected by him between God and man, the influence of the Holy Spirit, our own insufficiency and constant need of the promised aid, were the themes upon which he most frequently discoursed. 

It was his practice to present from the pulpit systematic doctrinal instruction, always accompanied with a pointed application and an earnest appeal. Catechetical instruction he valued most highly, as our fathers generally did, and he improved every opportunity afforded him to urge its importance upon the attention of his people. Those who sat under his ministry, considered it as a great privilege. They appreciated his services, and felt that it was a distinguished honor to enjoy the benefits of his teachings. 

It is seldom, that an individual awakens so enthusiastic a regard or secures so strong a hold upon the affections of an entire community. Occasionally we meet with some of his old parishioners, who were introduced by him into the church, and we are struck with their devotion to his memory, their profound veneration for his character, and their grateful appreciation of his services. He possessed the faculty of attaching to him every one, who came within the circle of his influence. There was a charm in his instruction, which none could resist. His kindness of heart and geniality of temper were very striking. He knew so well how to interest the young, and to become their delightful companion. Little children would gather around him and clamber upon his knees, whilst he cordially received their warm caresses, kindly reciprocated their simple greetings, and fervently invoked upon them Heaven's benedictions. 

He was habitually cheerful and uniform in his disposition. His conversation was interesting and often quite facetious, characterized by dry humor, and sometimes abounding with sparkling wit. Yet his manners were dignified. He never let himself down by invading the character of others, or was he unmindful of the position he occupied as a minister of the Most High. He was distinguished for his social qualities and domestic virtues, as a warm hearted and charitable Christian, beautifully illustrating in his life the sincerity of his faith and the power of the gospel. His claims to discipleship none questioned. His qualifications for the joys of the eternal world all admitted. His successor in the pastoral office remarked on a certain occasion, 

>"that Goering would stand among the stars of the first magnitude in the kingdom of heaven." 

## Chronological Facts

But when we seated ourselves for the task assigned us, we proposed to give some chronological facts, that our article might be useful for reference. Rev. Jacob Goering was of German extraction, and born in York County, Pennsylvania, on the 17th of January, 1755. His father was a farmer, and had designed his son for the same occupation, but discovering in him unusual sobriety and reflection, he gave him all the advantage that could be derived from the schools in the neighborhood. The youth was soon distinguished for his assiduity, and displayed great eagerness in the pursuit of knowledge. To study he devoted his days and a great part of his nights. He was accustomed to find his pleasure in books rather than in active sports, and everything in which he engaged, indicated the bent of his mind. So devoted was he to the acquisition of knowledge, and so constant in his application, that he gathered up all the fragments of time, that not a moment might be lost. Such was his passion for study, that when abroad in the field, during the intermissions of labor, the book was immediately taken from his pocket,and the brief, but by him highly prized, interval diligently improved. He made rapid strides in study. His progress was that of a giant. It exceeded everything that had been known in all that region. His memory was so retentive, that it seemed to forget nothing that it read or heard. At school he was quickly far in advance of all his companions, even those who had started long before him, and whose advantages had been superior. With his years this desire for improvement increased, although the means failed. His father perceived that there was little prospect of his son becoming a successful farmer, as his inclinations seemed to lie in so different a direction, and his studious habits did not suit that constant attention and industrious labor, which skilful husbandry demands. Being a youth of promising talents and hopeful piety, he was soon designated for the ministry, and with this view now pursues his studies. He also took charge of an English school near his father's home, which enabled him to carry his studies forward, and to extend his researches. 

## Ministerial Training

When in his eighteenth year, young Goering removed to Lancaster, Pa., for the purpose of prosecuting still further, the course of study already commenced, under the instructions of [Rev. Dr. Helmuth](/henry-helmuth/), at that time pastor of the Lutheran congregation in that place. He became an inmate of his preceptor's family, and with great zest engaged in the work of preparation for the sacred office. A new field was opened for his investigations, which he faithfully and most successfully cultivated. He was distinguished for industry, perseverance and proficiency, and in a short time acquired a familiar acquaintance with the Latin, Greek and Hebrew languages. The excellencies of the young man soon attracted the attention of Dr. Helmuth, who became his devoted friend and faithful counselor. With his pious and learned instructor he remained two years, at the end of which time his theological studies were completed, and his fitness for the work of the ministry acknowledged. 

He was licensed to preach by the Synod of Pennsylvania, and immediately took charge of the Lutheran church in Carlisle, and the congregations in the vicinity. He married in early life, but was soon called to follow to the grave the companion of his bosom. This was to him, at the time, a severe affliction, but it proved a rich blessing. God's providence was sanctified. He came forth from the trial spiritually improved, a more experimental Christian, and prepared to labor with greater zeal for the salvation of souls. From this period he preached the gospel with increased fervency, and appeared more than ever interested in the work to which he had consecrated himself — 

>"Afflictions from above \
> Are angels sent \
> On embassies of love." 

_Those whom the Lord loveth he chasteneth._ All the occurrences of life are intended for the Christian's highest good,and will work out for him _a far more exceeding and eternal weight of glory_. Mr. Goering was subsequently married to a daughter of Rev. J. N. Kurtz, who, with eight children, survived his death. 

## Relocation to Hagerstown, Maryland

In 1786 he received and accepted an invitation tendered him by the Lutheran church in York, Pa. After serving the congregation for five or six years, he was prevailed upon to locate in Hagerstown, Md., for the purpose of building up the waste places in that region, and gathering together our scattered members. During his absence from York, for upwards of a year there was no pastor secured, and such was the love the congregation cherished for him, that they earnestly entreated him to return. They so strongly persisted in their wishes and seemed so unwilling to unite in the choice of another incumbent, that he felt it his duty to resume his connection with the charge. 

He continued to minister to this congregation until his decease. He died at his residence in York, on the 27th of November, 1807, in the fifty-third year of his age, and the thirty-second of his ministry. He had been, for some time, in feeble health. His last illness was slow consumption. But as long as his declining strength allowed, he discharged the duties of his office. When bodily infirmities deprived him of the satisfaction of meeting his people in the sanctuary, he ceased not to exhort those who came to see him, to attend to their highest interests, and to keep eternity constantly in view. After a season of protracted suffering, he was taken home to his heavenly Father, to enter upon the rest promised to the people of God. 

## Heartfelt Grief

Mr. Goering's death was the occasion of heartfelt grief. Not only did the church, in which he was so bright and shining a light, mourn, but the whole community, upon whom he had left a strong impression of his integrity and piety, knew that an irreparable loss had been sustained. No one seemed insensible to what was regarded as a public bereavement. 

>_Quando ullum inveniet parem?_

By all his loss was felt — by all his death was deplored. He was the friend and father of all, and all felt that it was their privilege and their duty to mourn — 

>"Their father, friend, example, guide removed!" 

The funeral services were conducted by Rev. George Geistweit, of the German Reformed church, from the words — _We trust we have a good conscience, in all things willing to live honestly_; and Rev. Emanuel Rondthaler,of the United Brethren church, from the text — _Well done thou good and faithful servant; thou hast been faithful over a few things, I will make thee ruler over many things: enter thou into the joy of thy Lord_. 

We cannot, perhaps, more appropriately close our memoir of Mr. Goering, than by introducing an extract from the sermon preached at his burial by Rev. Mr. Geistweit. The language, employed by a contemporary and colleague in the ministry, will doubtless be read with interest, and valued as an additional testimonial to departed worth. 

After considering the nature and office of conscience, and the comfort which a good conscience affords, the preacher proceeds to examine the life of the deceased, and to show the good conscience he evidenced in the performance of his official duties, and in the general tenor of his life: 

>"As long as he fed the flock entrusted to him by the great Master," 

says the preacher, 

>"I doubt not he could adopt the words of Jeremiah: _That which came out of my lips was right before Thee_. _Be not a terror to me. Thou art my hope in the day of trouble_. Whatever he found enjoined in the word of his Lord, that he preached; not adulterating it, but in simplicity, as in the presence of God, he preached Christ. He had a sincere reverence for God — virtue, faith, love, hope, meekness, humility and patience were eminent traits in his character. He faithfully performed the work of the gospel ministry, exhibiting in the discharge of all his duties, a blameless walk. Constantly and faithfully devoted to his work, he acted in view of his responsibility to the all-searching God, and joyfully declared the whole counsel of God. 

>"To his congregation he showed himself a faithful pastor, watching over it with zealous care. He regarded it as God's own flock, a flock which Jesus had purchased with his own blood. He often looked forward to the account, which he would one day have to give. In all his duties he was animated by love, performing them not by constraint, but willingly, for the promotion of God's glory and the salvation of his fellow men. With him there was no assumption of authority over his people, but in all things he was a correct pattern for them. He preached and persevered, he rebuked, warned and patiently instructed. Your own consciences, brethren, will bear witness to all this, in him the bereaved found consolation and sympathy. The weak and timid approached him and were confident that he would listen to them. The troubled and the tempted resorted to him for counsel, and departed relieved of their heaviness. The hearts of the sick rejoiced in his presence; they were refreshed and revived by his fervent and affecting prayers. When divine judgments threatened he stepped in, and with earnest supplication exhorted to repentance. Like Nathan with David, he fearlessly rebuked the sinner, urged him to forsake sin and to accept happiness. 

>"His preaching was not in words only. It sprang from a heartfelt experience of true religion in his own heart. He had been tried by the assaults of temptation; these conflicts taught him to trust in God, and depend upon his word. With the nature of repentance, faith, the new birth, justification and sanctification, he was experimentally acquainted, and could therefore give unto every one his portion of instruction in due season. We require no more of a steward than that he be faithful, and that he employ the talent entrusted to him for the glory of God and the salvation of his fellow men. We know that your pastor was such; that he labored diligently for your salvation and his own; that in all things he walked uprightly, and could well say in the words of the text: _For I trust I have a good conscience; in all things willing to live honestly._ 

>"After he had exhausted his powers in the vineyard of the Lord, when his strength failed him, and his sickness increased beyond the hope of recovery, he prepared himself for death, as Aaron on Horeb, and as Moses on Nebo. He was composed in his mind, and having given advice in regard to his household, he comforted those, who were weeping around him, bade them farewell, and commended them to the grace of God. He then committed his congregation to the protection of the Most High, and his soul into the care of his heavenly Father. Thus died a father, an instructor, a comforter and a counselor. Thus went out a light in the church of our God. We trust that he has already arrived at the gates of eternal bliss, and his soul has already been borne by the angels into Abraham's bosom. Already the welcome has been given; _Well done thou good and faithful servant; thou hast been faithful over a few things, I will make thee rider over many things; enter thou into the joy of thy Lord._ There he will shine as the brightness of the firmament. He sought to turn many to righteousness; he will shine as the stars forever and ever. Now since his labors are ended, how pleasant will be his rest!" 

# Publication Information

- _Author_: __"Krauth, Charles P."__, Editor.
- _Journal_: __"The Evangelical Review."__
- _Originally Published_: 1854.
- _Lutheran Library Edition_: 2019
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

{{% alert note %}}
{{% biographical-sketch-note %}}
{{% request-pdf %}}
{{% /alert %}}
