---
date: 2018-02-13 12:00:00
title: "Gospel Truths: Presenting Christ and the Christian Life by John Edwin Whitteker"
slug: "143wd-whitteker-gospel-truths"
categories: ["Lutheran Library Publications"]
tags: ["Whitteker", "Sermons", "Start Here"]
authors: ["Whitteker, John Edwin"]
titles: ["Gospel Truths: Presenting Christ and the Christian Life"]
origpublishers: ["United Lutheran Publication House"]
origdates: ["1922"]
synods: ["General Council"]
---

"This little volume of sermons has been prepared in response to repeated appeals from the people who heard them from the pulpit. With some slight discrimination, they have been chosen from a cabinet of upwards of two thousand discourses. 

"In their preparation for pulpit use, two thoughts were constantly kept in mind: to preach Christ as the Way, the Truth, and the Life, with all their redemptive significance, and to implant in the minds and hearts of those who listened to their deliverance, those principles which underlie Christian character and conduct.

"As this new volume goes forth with its message of truth and grace, may it be instrumental in quickening in the hearts of those who read it a deepening sense of spiritual certitudes and an increasing purpose to realize them in their lives. – John Whitteker


# Contents

- About the Author
- Foreword
- 1. The Influence of the Christian Life
- 2. The Question with a Quibble
- 3. The Hem of Christ's Garment
- 4. The Power of Christ
- 5. The Poor: Their Care and Cure
- 6. The Mysteries of Grace
- 7. In the Mountain: On the Lake
- 8. The Greatest in the Kingdom
- 9. Life: And How to Live It
- 10. Promise and Practice
- 11. The Resources of Faith
- 12. The Withered Hand, The Withered Heart
- 13. A Practical Christian Life
- 14. The Spirit that Rejects
- 15. A Bad Man with Good Points
- 16. The Religion of Curiosity
- 17. The Service that Saves
- 18. Ashamed of Jesus
- 19. The Three Men Who Did Not Follow Christ
- 20. Church-life and World-life
- 21. The Ever-Present Future
- 22. The Discernment of the Times
- 23. Repent Rather Than Judge
- 24. Lost and Found
- 25. Natural Sight and Spiritual Insight
- 26. The Truth Makes Free
- 27. Jesus The Home Guest
- 28. "We Would See Jesus"
- 29. The Troubled Heart
- 30. The Vine and the Branches
- 31. Questions That Condemn

# About Rev. John Edwin Whitteker

John Edwin Whitteker was born April 21, 1851, in North Williamsburg, Ontario. At the age of fourteen years he left home to complete his education by his own efforts, and after passing through the grammar school of Morrisburg, Ontario, spent three years in public school work, and then entered Thiel College, at Greenville, Pa., in the autumn of 1871. In 1875 he was graduated from Thiel, taking the second honor in his class, which carried with it his appointment as valedictorian. In 1874, while a student at Thiel, he became the second tutor in that college, and, immediately after his graduation, was made first tutor. Meanwhile he began the study of theology under the direction of the president of the college, following the course laid out by the Lutheran Theological Seminary in Philadelphia. In May, 1877, he was ordained, and continued work in the college until 1888, having meantime been promoted to the position of Adjunct Professor of Latin, and later to the chair of Latin Professor.

In the summer of 1888 Dr. Whitteker became pastor of the Church of the Reformation at Rochester, N. Y., where, in addition to his regular pastoral duties, he did aggressive missionary work. During his pastorate of five years there he established three missions and built two mission churches, both of which became self-sustaining before he left that field of labor. In 1893 Mr. Whitteker was called to Easton, Pa., where he became the pastor of the old historic church of St. John's, serving two years in a very acceptable manner, at the end of which time he was called to the superintendency of the English Home Missions of the General Council, with headquarters in Philadelphia, although still retaining his residence in Easton. For the following three years he remained in that work, and in the fall of 1898 accepted a call to Grace Lutheran Church. at Rochester, Pa., which congregation under his pastoral care became one of the most prosperous in that section. Having accepted a call to the Church of the Holy Trinity, in Lancaster, he entered upon this work Feb. 1, 1901, and was installed as pastor seventeen days later. At the commencement following this event his Alma Mater honored him with the title of Doctor of Divinity.

Dr. Whitteker was united in marriage with Miss Emma Zenette McKee, daughter of the late Prof. David McKee, one of the leading educators of western Pennsylvania, and who, from the founding of Thiel College, in 1870, to the time of his death, in 1898, filled the Chair of Mathematics there. The McKees were descended from an old Scotch-Irish family, the members of which had been professional people for generations. Dr. Whitteker is as prominent in literary work as he is in the educational and the pastoral field. As the author of the "Translation of the Augsburg Confession, With Explanatory Notes," published in 1893, he immediately gained notice, and his "Bible Biographies" are now used as a text-book in all the Lutheran Churches of the General Council. He is also the author of several pamphlets bearing on church doctrine and church life, and has, for several years past, been a contributor to church papers and reviews. The esteem in which his literary work is held is best told in "The Lutheran," the church organ, from which we make the following abstract, from the issue of Nov. 15, 1900: "The writer of the book [Bible Biographies] is one of the most gifted authors in our church. If he were to devote himself entirely to literature, we have no doubt that he would be very widely known as an author. This author is Rev. Prof. J. E. Whitteker, now of Rochester, Pa. For a number of years he was Professor of Latin in Thiel College, and is the author of several works, including a very vigorous twelve-mo book in defense of the Lutheran doctrine of baptism."

(Source: Biographical Annals of Lancaster County, Pa., Beers, 1903, pp. 205-6.) 

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)

![book cover](/img/posts/143wd-whitteker-gospel-truths-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/143wd-whitteker-gospel-truths.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 143wd&body=Please send 143wd-whitteker-gospel-truths.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 143wd&body=Please send 143wd-whitteker-gospel-truths.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2018-02-03
- _Version 4 update_:  
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
