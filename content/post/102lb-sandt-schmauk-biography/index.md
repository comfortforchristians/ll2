---
date: 2019-04-22 13:59:13-04:00
publishDate: 2019-04-22 13:59:13-04:00
title: "Life and Teachings of Theodore E. Schmauk – by George W. Sandt"
slug: "102lb-sandt-schmauk-biography"
categories: ["Lutheran Library Publications"]
tags: ["Biography", "Sandt", "Schmauk", "Missions"]
authors: ["Sandt, George Washington"]
titles: ["Life and Teachings of Theodore E. Schmauk"]
origpublishers: ["United Lutheran Publication House"]
origdates: ["1921"]
synods: ["General Council"]

---

"[Dr Schmauk] ranks as one of the ablest and most consistent defenders of the Lutheran faith. His catholicity of spirit enabled him to put himself in the place of his opponent and see things from the latter's point of view...And yet he never swerved from the strong conservative position he always took by making weak or compromising concessions.

>Lutheranism clings to God's Written Word.  Her motto is the Word of God, the whole Word of God, and nothing but the Word of God, not as a prescriptive letter, but as the power of God unto salvation." – Theodore Schmauk

{{% toc %}}

# Dr. Knubel of _The Lutheran_ writes:

"There is no man in our Church whose Christian consecration has been more evident, whose deep loyalty to the Church has been stronger, whose full participation in her thought and activity has been wider, whose counsel has been more constantly sought and given, whose influence has been more powerful and helpful than that of Dr. Schmauk."

# Recommendation by Henry Eyster Jacobs

"I have just finished a rapid examination of your book. I wanted to form an impression of it as a whole, before entering into the closer examination of details. It has held me fast all day, except when in church, and for two brief breathing spells. I have read enough to lead me without waiting longer to express my intense delight and most sincere gratitude. You have produced an epoch-making book. Not only will it live, but its influence may be more far-reaching than anything that has as yet appeared in the English language within our Church. You have not left the least shred of an argument against the Confessional position unanswered." – Henry Eyster Jacobs

# Dr. Schmauk on the Lutheran Understanding of Salvation

"Our trust is _not salvation by science_, and therefore we are against rationalism which sets man's own thinking above the truth of God. Our salvation is not by religious ceremony, and therefore we are against ritualism, which externalizes the service of God into a sacred and passing show. 

"Our salvation is _not by tumultuous feeling_, and therefore we are against emotionalism which makes light of facts and history and centers all on passing currents in the soul. 

"Our trust is _not in salvation by meditation_, and therefore we are against mysticism which raises the soul to God by an inner and poetic sight. 

These are extremes and one-sided. From them spring Swedenborgianism, spiritualism. Christian Science, theosophy, occultism, and many of the superficial religions of the moment. 

Lutheranism clings to God's Written Word.  _Her motto is the Word of God, the whole Word of God, and nothing but the Word of God_, not as a prescriptive letter, but as the power of God unto salvation.

In the law and the prophets, in the Gospels and Epistles, we find one mighty principle, the man who can stand before God and live, _the man who is counted just in His sight, so to say the good man, is so by faith only_. He is saved by his confidence in that which he finds in the written Word of God, by his trust in the blood of Jesus Christ which cleanseth us from all sin. – Theodore Schmauk

# Contents of the Book

- Theodore Emanuel Schmauk: A Biographical Sketch with Ample Quotations from his Letters and Other Writings
- Preface
- Part 1 – Dr. Schmauk On Live Questions And Issues
  - 1 On The Person Of Christ
  - 2 On The Doctrine Of The Trinity
  - 3 On The Freedom Of Will
  - 4 On Negative Theology
  - 5 The Lutheran Conception Of Salvation
  - 6 On Confessionalism
  - 7 Luther And The New Theology
  - 8 On Progressive Conservatism
  - 9 On Lutheran Union
  - 10 On Lutheran Pulpits For Lutheran Ministers
  - 11 On Lutheran Disunity
  - 12 On The Lodge And Pulpit-Fellowship
  - 13 On Un-Christian Societies
  - 14 On Co-Operation
  - 15 Dangers To The Lutheran Church In Cooperating With Revival Movements
  - 16 The Lutheran Church And External Relationships
  - 17 Two Great Lessons Of Providence
  - 18 The Church And Social Problems
  - 19 On Christmas
  - 20 The Tricky Controversialist
  - 21 On Possibilities Of Union
- Part 2 – A Biographical Sketch
  - 1 - The Schmauk Antecedents
  - 2 - Birth and Boyhood (1860 to 1876)
  - 3 - A Student at College (1876 to 1880)
  - 4 - Student at Seminary (1880 to 1883)
  - 5 - His Early Pastorate as Associate of his Father (1883-1898)
  - 6 - Literary Activities Begin
  - 7 - As Educator - The Pennsylvania Chautauqua
  - 8 - As Historian - No Traitor To His Blood
  - 9 - Editor and Sunday School Leader
  - 10 - Citizen, Patriot and Public Speaker
  - 11 - Death of his Father (1898-1903)
  - 12 - President of the General Council (1903 to 1905)
    - “The One Conservative Lutheran Body”
    - The General Council’s Attitude Toward Modern Evangelism
  - 13 - The Confessional High-Water Mark (1907)
  - 14 - Administrative Problems
  - 15 - A Trying Convention (1909) Dr. Schmauk and the Swedes
  - 16 - “The Confessional Principle” (1907-1911)
  - 17 - The QuadriCentennial Celebration of the Reformation (1917-1920)
  - 18 - The Closing of a Strenuous Life
- Positions Held in the Church
- Author of Following Books


# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)
![book cover](/img/posts/102lb-sandt-schmauk-biography-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/102lb-sandt-schmauk-biography.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 102lb&body=Please send 102lb-sandt-schmauk-biography.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 102lb&body=Please send 102lb-sandt-schmauk-biography.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2018-01-25
- _Version 4 update_: 2019-04-22 
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

