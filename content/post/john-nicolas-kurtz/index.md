---
date: 2019-07-21 05:39:20-04:00
publishDate: 2019-07-21 05:39:20-04:00
title: "John Nicolas Kurtz: A Biographical Sketch"
slug: "john-nicolas-kurtz"
categories: ["Biographical Sketches"]
tags: ["Kurtz", "Lutheran Ministers", "Evangelical Review"]
authors: ["Kurtz, John Nicolas", "Krauth, Charles Porterfield"]
synods: ["First"]
---

>"The first Lutheran minister, ordained in this country to preach the gospel."

John Nicolas Kurtz was born in Lutzelinden, in the Principality of Nassau-Weilburg, and immigrated to this country in 1745. He came to the United States as a _Catechet_, and for two years after his arrival, engaged in the business of teaching as well as preaching, "in consequence of the entire absence," to use his own language, "of competent teachers and the lamentable ignorance of the youth of his parish." 

## Education at Halle and Mission to America

Mr. Kurtz received his preparatory education under the direction of his father, who was principal of a literary institution in his native place. When in his fifteenth year, he was transferred to the high school at Giessen, an institution, in which young men, with the ministry in view, were thoroughly prepared for the work. Having pursued his studies at this place for seven years, with great industry and success, he entered the university of Halle, at a most interesting period in its history, when the immortal Francke was in the meridian of his influence. The instructions, counsels and personal intercourse of this good man, he for several years enjoyed. The varied qualifications of young Kurtz for the missionary work, in connection with a vigorous constitution, soon commended him to his Professors as a suitable candidate for a mission to this Western world. Accordingly, having completed his course of preparation, and given evidence of an increasing desire to engage in this field of labor, he received the appointment, and in company with several other missionaries, bade adieu to his native land, 

>"Forsaking country, kindred, friends and ease," 

for these then inhospitable shores, that he might proclaim the boundless riches of Jesus Christ to his perishing countrymen in their vernacular tongue. The following extract from his diary will furnish the reader with some idea of the state of his mind at this period, and the views he entertained of the object, to which he had devoted himself: 

>"In the year 1744 it pleased my beloved Savior to send me a call by his faithful servant, Dr. Franke, to travel to America. Having obtained the approbation of my dear parents, brothers and sisters, and many Christian friends, I have accepted the call, and in company with my esteemed brother in Christ, Reverend John H Schaum, have prosecuted my journey to this city (Hamburg). Here we have been joined by brother Brunholtz, with whom we are to embark for London, and thence for America. May the Lord Jesus Christ, who was oppressed with affliction, in order that he might be touched with the feeling of our infirmities, powerfully defend and comfort us, and be our guide and safeguard in all our ways! May He, who could command the services of more than twelve legions of angels, commission his holy angels to encamp round about us for protection, that we may safely reach the place of our destination, and become faithful and successful instruments in collecting his wandering sheep, to the honor of his name, and finally exalt us with them to his own everlasting habitations! Amen." 

Such were (he feelings of the young missionary on the occasion of his departure from the land of his birth, the endearments of home and the scenes of his youth! How strong his confidence in God! What humble trust and filial faith! Such was the active, earnest and living piety, which influenced him through life, and marked his character during the fifty years of his ministerial career. 

On his way to this country, Mr. Kurtz was detained for some weeks at Hamburg. Here he formed some valuable acquaintances, among the number Rev. Messrs. Heck, Fiege and the venerable Ziegenhagen, at the time Chaplain to the King of Great Britain; for all of whom he ever afterwards cherished a very high regard, and maintained with them, until death, an uninterrupted correspondence. 

After a long and irksome voyage, he reached Philadelphia on the 15th of January, 1745, where he was most kindly received and cordially welcomed by Dr. H. M. Muhlenberg, who was pastor of the German Lutheran church in that city. Soon after his arrival, he was invited to New Hanover, where he labored for two years, dispensing the word on the Sabbath, and during the week giving instruction to the young. From this point he removed to Tulpehocken, where he remained only a year, requisition having been made for his services by the people of Germantown, Pa., and neighboring congregations, that were famishing for the want of spiritual food. 

## The First Lutheran Synod in America

In the year 1748, the first Lutheran Synod was held in this country, at which meeting Mr. Kurtz was fully set apart to the gospel ministry. At this time there were only eleven regular Lutheran ministers in the United States. There were in attendance at this convention, six clergymen, Messrs. Muhlenberg, Handschuh, Brunholtz, Hartwig, Sandin and Naesman, the last two of whom were Swedish Lutherans. They, however, participated in the deliberations of the Synod, and assisted in the examination and ordination of candidate Kurtz. 

Among the questions proposed to the applicant, we find the following, which will serve to show how carefully this ancient Synod guarded against the introduction of improper individuals into the sacred office: 

- What are the evidences of conversion?
- What is meant by the influence and blessings of the Holy Spirit?
- How do you prove that Christ was not only a teacher, but that he made an atonement for the sins of man?
- Were the apostles infallible in their instructions?
- How do you establish the claims of pedo-baptism?
- How do you prove the eternity of future punishment?

Other questions were also propounded, evidently having a reference to the doctrinal errors which then prevailed, and were beginning to be started in the church. The ordination sermon for the occasion was preached by pastor Hartwig, from the words, _His blood will I require at thy hand_. 

This Synod was established at the suggestion of the Theological Faculty at Halle, for mutual consultation and cooperation among the brethren, and for the purpose of devising means for furnishing the numerous Germans scattered through the land with the preached gospel. The supply of ministers from Europe was altogether insufficient for the demand. In the organization of the Synod, our fathers adopted a very liberal form of church government, similar in many respects to the congregational system of this country. The prominent features embraced in the constitution, were the parity of ministers, the cooperation of the laity in the government of the church, and the voluntary convention of Synod. At the first Synodical meeting laymen were present, and took part in the transaction of business.[^bFN] The elders and deacons of the church, in which Mr. Kurtz had labored as a licentiate, were also called upon to sign his call. [^bFO]  


[^bFN]:  _Hallische Nachrichten_, p. 2S4. 

[^bFO]: _Hallische Nachrichten_, p. 286. 

## Dangers from Indian Massacres

The subject of our sketch, the same year in which he was ordained, returned to Tulpehocken, in obedience to the repeatedly expressed wishes of the congregations, to whom he had previously ministered. He remained among them for twenty-two years, doing the work of his Master, and gathering in many trophies of redeeming grace. His duties were numerous and arduous, but they were discharged with conscientious fidelity and unwearied application, and amidst perils and difficulties, exposures and deprivations scarcely credible. 

At the present day we cannot easily conceive how great was the labor connected with the planting of the church in this Western land. Many were the dangers which beset the early missionaries on all sides, and powerful were the obstacles, which impeded the progress of religion. Ministerial support was inadequate, the places of worship were few, the people were scattered, there was difficulty in traveling, for the want of roads,and frequently the most violent opposition in the discharge of duty had to be encountered. God, in his goodness, raised up for the times the very men that were needed.

During Mr. Kurtz's residence in the charge, the services of the Sanctuary were often conducted even at the imminent risk of life itself, as the ruthless Indian lay in wait for victims, and whole families were sometimes massacred. In traveling to his preaching stations, and visiting his members, this devoted servant of God was repeatedly exposed to danger from the attack of the tomahawk and the scalping knife. During the hours of public worship, the officers of the church stood at the church doors, armed with defensive weapons, to prevent a surprise, and, if necessary, to protect ministers and people from the assaults of the Aborigines. In a letter to Dr. Muhlenberg, in 1757, he says, "that on one day not less than seven members of the congregation were brought to the church for burial, having been murdered by the Indians the evening before. Being anxious to improve the solemn occasion to the spiritual welfare of his hearers, he postponed the interment until the succeeding day, and suffered the mangled bodies to remain in the church, that the congregation might convene. This incident also furnishes an illustration of his deep solicitude for the flock, over which he had been placed, and his ardent desire to labor for their convention to God. 

## Twenty Years At York, Pa.

In the year 1771 Mr. Kurtz, who had, by this time, acquired considerable influence in the church, and had received various marks of confidence and respect, especially in being elected Senior of the Synod, was induced to remove west of the Susquehanna, and to take charge of our Lutheran interests in York, Pa. Here he rendered the church incalculable service. He continued in this region for the space of twenty years, faithfully discharging the duties of the pastoral office, and scattering the good seed of the word, which, watered by the dews of divine grace, took deep root and brought forth much precious fruit. The influence of his labors is yet felt in that whole section of country, and there are those still living, who gratefully bear testimony to the efficiency of his ministry. 

## Retirement to Baltimore

In 1792, being more than three-score years and ten, he regarded it as his duty to retire from active service. His health began to fail, and the infirmities of age to increase. He found no exemption from the common law of our nature — 

>_Labuntur anni: nec pietas moram_ \ 
>_Rugis et instanti senectae_ \
>_Afferet indomitaeque morti._ 

He resigned his charge and removed to Baltimore, Md., taking up his residence with his son, in whose kind family he enjoyed every attention and those grateful marks of tenderness and love, always due to a good father and a faithful servant of Jesus Christ. Here he spent the remainder of his days, waiting for the call of the Lord, and although stricken with years, and worn out with labor, he still occasionally filled the pulpit of his son,[^bFP] until 1794, when he was released from his mortal tabernacle, and translated to his eternal rest. A serene and peaceful death terminated his trials and sufferings in this life, and opened heaven to his emancipated spirit. He departed from this world in the calm sunshine of gospel light. He was buried in Baltimore, and a discourse suitable to the occasion delivered by Rev. J. G. Droldeneir, of the German Reformed church, from the words: _There remaineth, therefore, a rest to the people of God_. 

Perhaps no one of the patriarchs of our church labored more extensively and usefully than he, whose career we have attempted briefly to sketch. His life was long, laborious and successful. His literary attainments, his deep spirituality, his fervent zeal, his pulpit ability and pastoral efforts, have secured to him a high eminence among those distinguished men of God, who at an early period of our history abandoned their native land, and the comforts of home, to proclaim the glad tidings of redemption in this distant country. He was an acceptable, impressive and effective preacher. He presented God's message without fear or favor, declaring the whole truth, regardless of praise or of censure — 

>"By him the violated law spake out \
>Its thunders." 

[^bFP]:  J. Daniel Kurtz, D. D., who now upwards of ninety years is still spared among us as a relic of a former generation. 


The impenitent were brought to realize the depth of their depravity, to experience a conviction of their danger and their guilt, and to feel that out of Christ there was no safety. The contrite in heart were encouraged and directed to the Lamb of God, that taketh away the sins of the world; they were taught to exercise faith in the crucified Redeemer, and to believe, that the bruised reed he would not break, and the smoking flax he would not quench: 

>"By him, in strains as sweet \
>As angels use, the gospel whispered peace." 

## Teaching and Catechizing

During his residence at Germantown and Tulpehocken he, from time to time, visited New York, Philadelphia, Lancaster, Frederick, Hagerstown, and numerous other places, and spent whole months in preaching, catechizing, and instructing the youth of the church. So deeply did he sympathize with our people who were destitute of the means of grace, that he spared no effort for their spiritual improvement. His influence upon the young was very great. He possessed, in an extraordinary degree, the faculty of securing their attention and interesting their affections. He could, without any difficulty, adapt his instructions to the capacity of children. His catechetical lectures, whilst he was pastor of the church at York, were delivered on Sabbath afternoon, and usually attracted larger audiences than the morning services. 

All ages and classes flocked to the church, and listened to his words with an attention and pleasure seldom witnessed. Much of the success, that attended the ministry of this devoted man, we have heard ascribed to the faithful performance of this important department of pastoral labor. He loved the work in which he was engaged, and to which, in his youth, he had consecrated himself. 

Expansive benevolence was a prominent feature in his character. His was a genial spirit, kind and affectionate to all. It is not the privilege of every one to pass through life, enjoying the esteem of so large a circle of friends and to die so generally and deeply lamented. Music furnished to him his principal recreation. He was born a musician, and his natural talent in this direction, he had cultivated in a high degree. His love for it was most decided and enthusiastic. Its influence upon his character was most favorable. It refined his taste, softened his manners, and increased his facilities for doing good. His fondness for good singing contributed very much to the improvement of this part of divine worship in all his charges. 

## Sound, Practical Wisdom

Mr. Kurtz was a man of strong mind. There was nothing brilliant in his mental composition, yet there was soundness and much practical wisdom. His early advantages had been of a high order. The best opportunities for culture had been afforded him. All the powers of his mind had been successfully disciplined and fully developed. His views on subjects generally, were comprehensive, his information was extensive, his reading well selected and thoroughly digested. An estimate of his literary standing, and of the respect entertained for his attainments, may be inferred from the fact that he was specially invited by the Faculty of the College of New Jersey, to be present at their annual commencements, although he was some distance from Princeton. 

## Lutheran Ministers Greatly Respected in Early America

It may here be incidentally stated, that in our early history in this country, the various denominations of Christians manifested for our ministry the greatest regard. Their learning was such as to challenge admiration, and procure the confidence of all, with whom they came in contact. They were everywhere treated with kindness. From all they experienced the most friendly attentions. They lived on the most intimate terms with their contemporaries. Their intercourse with brethren of all creeds was most pleasant. 

Dr. Muhlenberg, in one of his letters, speaks of a visit made him by Dr. Tennant, as a season of spiritual refreshment. He also attended by particular invitation, a convention of the Episcopal church, and met with a most cordial reception. In 1763, Rev. Messrs. Durkee, Peters and Ingliss, of the Episcopal church, Drs. Findly and Tennant, of the Presbyterian church, and Rev. Mr. Whitfield attended a Synodical meeting of the Lutheran church, and by a vote of Synod, Whitfield preached a sermon. He was likewise present at the examination of the children of the Philadelphia congregation on the truths of the Christian religion, at the conclusion of which he delivered an address. 

Our clergymen in that day were men of fervent and practical piety. It was their constant aim to do good. They were intensely interested in the salvation of souls. Their preaching was evangelical and instructive. Their journals show that they constantly prayed for the divine presence, and confidently looked for the promised blessing upon their labors. Daily in the temple, and in every house, they ceased not to teach and preach the gospel of Jesus Christ. The narrative of the establishment and progress of the American Lutheran church, contained in the pages of the _Hallische Nachrichten_, and furnished by Drs. Muhlenberg, Brunholtz, Helmuth, Kunze and others, cannot fail to produce a most favorable impression, respecting these devoted men, their sacrificing labors, and the wonderful results they achieved. Something of their spirit may be gathered from the concluding paragraphs of a document deposited in the cornerstone of Zion's church, Philadelphia, erected in  1776, and addressed to posterity, which, we are sure, will prove interesting to our readers: 

>"And now, dear children and children's children, we commend you to God and the word of his grace, who is mighty to build you up, and to give you an inheritance among all who are sanctified. 

>We confidently trust, that we are not guilty of your blood, if you neglect your salvation in the wilderness of this world. 

>Observe diligently and carefully your church regulations, that in virtue of them, you may always be provided with pastors and teachers, who take heed to themselves and the flock, over which the Holy Ghost shall have set them as overseers, that they may feed the church of God, which he has purchased with his own blood; and act towards these your teachers, so that they may discharge their duties with joy and not with grief, for that is unprofitable to you. 

>Take heed also through the grace of God and the means of his grace, that you may become and abide fruitful branches in Christ, the true vine, children of light, members of his spiritual body, and living stones of the heavenly Zion. 

>Suffer no discord or party spirit to arise among you, but quench its first appearance with Christian love and mildness. 

>Act kindly and neighborly towards the members of our sister churches, and do to them as you wish that they should do to you. 

>Hold fast what you have, that no one may take your crown. 

>Let that mind be in you, which was in Christ Jesus, and walk as he did walk. 

>And if in following him you are tempted by trials and sufferings, think it not strange, but rejoice when you suffer with Christ, so that, in the revelation of his glory,you may have everlasting joy. 

>Now to the God of peace that brought you from the dead, our Lord Jesus, the great Shepherd of the sheep, through the blood of the everlasting covenant, make you perfect in every good work, to do his will, working in you that which is well pleasing in his sight, through Jesus Christ, to whom be glory for ever and ever." 

# Publication Information

- _Author_: __"Krauth, Charles P."__, Editor.
- _Journal_: __"The Evangelical Review."__
- _Originally Published_: 1854.
- _Lutheran Library Edition_: 2019
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

{{% alert note %}}
{{% biographical-sketch-note %}}
{{% request-pdf %}}
{{% /alert %}}
