---
date: 2019-06-18 05:14:54-04:00
publishDate: 2019-06-18 05:14:54-04:00
title: "The Church and Antisemitism by Walter Horton"
slug: "s45-horton-church-and-anti-semitism"
categories: ["Lutheran Library Publications"]
tags: ["Horton", "Short Books", "Anti-Semitism"]
authors: ["Horton, Walter Marshall"]
titles: ["The Church and Antisemitism"]
origpublishers: ["International Missionary Council"]
origdates: ["1947"]
synods: ["N/A"]
---

"The Jews are found in all nations, but are not fully of any nation — a situation which Christians, who are supposed to be in the world but not of the world, holding their citizenship in heaven rather than in any earthly country, should find it easy to understand."

"In the last analysis antisemitism springs from the fact that the Jews are truly God's Messianic people, through whom He offers salvation to the world; while the world (including worldly Jews) does not want to be saved, and is forever ready to crucify its saviors." – Dr. Walter Horton


# Book Contents

The Church And Antisemitism: A paper presented at the enlarged meeting of the International Missionary Council's Committee on the Christian Approach to the Jews held at Basel, Switzerland, June 4-7, 1947. 

- The Church And Antisemitism
  - I. Causes of Antisemitism
      - 1. Economic, Social, Political, Racial Factors In Antisemitism
      - 2. Moral and Religious Factors in Antisemitism
  - II. What the Church Can and Cannot Do About Antisemitism

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/s45-horton-church-and-anti-semitism-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/s45-horton-church-and-anti-semitism.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request s45-horton-church-and-anti-semitism&body=Please send s45-horton-church-and-anti-semitism.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request s45-horton-church-and-anti-semitism&body=Please send s45-horton-church-and-anti-semitism.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 
- _Version 4 update_:  
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
