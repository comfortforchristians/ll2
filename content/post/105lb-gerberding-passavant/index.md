---
date: 2019-04-16 14:57:59-04:00
publishDate: 2019-04-16 14:58:07-04:00 
title: 'The Life and Letters of William Passavant by George Gerberding'
slug: "105lb-gerberding-passavant"
categories: ["Lutheran Library Publications"]
tags: ["Biography", "Gerberding", "Passavant", "Missions", "History","Start Here"]
authors: ["Gerberding, George Henry"]
titles: ["The Life and Letters of William A. Passavant"]
origpublishers: ["The Young Lutheran Company"]
origdates: ["1906"]
synods: ["General Council"]
---

"The Life of Dr. Passavant should have been given to the Church at least a decade ago. 

"In the lives of God’s eminent children we have most useful and delightful information for the mind, inspiration for the spirit, braces for our faith, stimuli for our hope and most effective incentives for our love. Such lives are lived for others. They are not over when those who lived them are gone, but being dead they yet speak. – George Gerberding

 
![Rev. Passavant](/img/posts/william-a-passavant.jpg)

# Passavant on The War In The Gettysburg Seminary Against The Truth

"I feel very sad about the College Board at Gettysburg and its unaccountable action. They mean it ill for the truth, and are ready to make a constitution where none exists, in order to keep down and out the Lutheran faith; but all this will avail them nothing, so long as truth is stronger than error. Oh, how shame will cover them as with a garment a few years hence, when they see the number of students reduced on this account and that from their leading churches. This is a dead certainty. – From Chapter 22

# Sections of the Book

- 1. The Passavant Family
- 2. The Childhood Of William A. Passavant – His Mother’s Influence
- 3. At College
- 4. In The Seminary At Gettysburg
- 5. First Charge And Work In Baltimore
- 6. Beginnings In Pittsburg
- 7. Abroad
- 8. Home Again – Controversy – Beginning Of Charity Work
- 9. Work For Scandinavians And Germans
- 10. Orphan Work
- 11. Life And Work In Pittsburg
- 12. Resigns First Church – Multiplied Labors – Gathers And Builds Churches
- 13. War – Views And Work
- 14. Storm And Stress In The Church
- 15. Work And Influence Among The Scandinavians
- 16. The Founding Of Milwaukee Hospital
- 17. Chicago Hospital – Bassler’s Death – Passavant’s Influence.
- 18. Founding Of The General Council
- 19. Orphan Work – Rochester – Zelienople – Mount Vernon
- 20. Mercy Work In Jacksonville For Epileptics — For Immigrants
- 21. Thiel College – College Life – Mountain Hospitality
- 22. Tribute To Dr. Krauth – Letters – Journeys – Reflections – Reproofs – Deliverances
- 25. The Passavant Institutions

# The Kindness of Dr. Passavant

"A family from Norway consisting of father, mother and four children, through the aid of benevolent persons at home, had obtained the means to emigrate to this country. They fared well across the Atlantic Ocean, and a little farther than Buffalo, N. Y., where the father, by accident, was caught under the wheels of a car which passed over his body and cut off his legs above the knees. The cars passed on at their usual rate, leaving the poor man to his fate on the track. The widowed mother came on West to the Norwegian settlement at Lisbon, Ill., and died of cholera the next day, leaving the four children without relatives or anyone to provide for them. The man with whom these children now live has himself a large family and is in limited circumstances. When I last preached in that neighborhood he spoke to me of the necessity of making some arrangement for their care, and I advised that some of the members divide them amongst their families until I could write whether there was still a place in the Home. The common practice out here has been to bind such children out, regardless of the character of those to whom they are given, or, in other words, to enslave them up to a certain age, a system which I hate from my very soul. We need scarcely add that we immediately wrote 'to send the children on.'" – from Chapter 11. 

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download) 

![book cover](/img/posts/105lb-gerberding-passavant-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/105lb-gerberding-passavant.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 105lb&body=Please send 105lb-gerberding-passavant.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 105lb&body=Please send 105lb-gerberding-passavant.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}

# Publication Information

- _Lutheran Library edition first published_: 2017-10-10
- _Version 4 update_: 2019-04-16
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)





