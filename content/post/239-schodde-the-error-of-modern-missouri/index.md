---
date: 2019-05-24 05:00:00-04:00
publishDate: 2019-05-24 05:00:00-04:00
title: "Walther and the Predestination Controversy or The Error of Modern Missouri by Schodde et al."
slug: "239-schodde-the-error-of-modern-missouri"
categories: ["Lutheran Library Publications"]
tags: ["Schodde", "Allwardt", "Gohdes", "Lenski", "Schmidt", "Stellhorn", "Tressel", "Election", "Predestination", "Predestination Controversy", "Intuitu Fidei", "Walther", "Calvinism", "Book of Concord", "Justification", "Start Here"]
authors: ["Schodde, George Henry","Allwardt, Heinrich August","Gohdes, Conrad B.","Lenski, Richard C. H.","Schmidt, Frederick Augustus","Stellhorn, Frederick William","Tressel, Walter E."]
titles: ["Walther And The Predestination Controversy", "The Error of Modern Missouri"]
origpublishers: ["Lutheran Book Concern"]
origdates: ["1897"]
synods: ["Ohio Synod", "Iowa Synod", "Missouri Synod", "General Council", "General Synod"]
---


The rise of the Synodical Conference under the LCMS has made Universal Objective Justification the Shibboleth of American Lutheranism.  Quite simply, if you do not accept UOJ, you are not considered a "Confessional Lutheran".  

Towards the end of his life, C. F. W. Walther brought forth a teaching of election which many Missouri and other American Lutherans could not reconcile with the Scriptures or the Lutheran Confessions.  The resulting "Predestination Controversy" split the Lutheran Church.

# The Lutheran Church Unanimous

"Since the publication of the Formula of Concord, for some 300 years, the Lutheran Church has unanimously held that the doctrine of our Confession and of the following teachers of our Church harmonized perfectly also in the article of predestination. Modern Missourians are the first "Lutherans" who assert the contrary; it is to be hoped that they will also be the last."

{{% toc %}}


# Summary Book Contents

- Introduction.

- I. The Present Controversy on Predestination

    - I. Historical Introduction.
    - II. The Formula Of Concord And The Old Lutheran Dogmaticians.
    - III. The Doctrine Of Predestination In The Missouri Synod.
        - A. Before The Year 1877.
        - B. The Synodical Report Of The Western District For The Year 1877.
        - C. The Synodical Report Of The Western District For The Year 1879.
        - D. Altes und Neues And Lehre und Wehre Before The General Pastoral Conference At Chicago In The Autumn Of 1890.
        - E. The General Pastoral Conference In The Autumn Of 1880.
        - F. After The Pastoral Conference In The Autumn Of 1880.
        - G. Comparative Summary.
            - 1. What Is Predestination?
            - 2. What Has God Regarded In Election?
            - 3. What Is The Relation Especially Of Faith To Election?
            - 4. In What Sense Does The Formula Of Concord Speak Of Election?
            - 5. Is Man’s Conversion And Salvation In Every Sense Independent Of His Conduct?
            - 6. May We Speak Of Man’s Decision Or “Self-Determination” In Conversion?
            - 7. What Is The Difference Between The Lutheran And The Reformed Doctrine Of Election?
            - 8. How Must The Doctrine Of The Dogmaticians Of The Seventeenth Century Be Regarded?
            - 9. How Is The Doctrine Of Modern Missouri To Be Regarded?
        - H. Appendix: An Ally Of Modern Missouri In Germany.
    - IV. The Doctrine Of Predestination In The Ohio Synod.

- II. “Intuitu Fidei”

    - Part 1. What Do The Lutheran Church Fathers Teach Regarding “God Elected In View Of Faith”?
    - Introductory Remarks.
    - 1. Over against Romanism
    - 2. Sola Fide and Intuitu Fidei
    - 3. Dr.Samuel Huber
    - 4. Correct Understanding of Testimonies
    - 5. Missouri’s New Discovery
    - 6. Missouri Holds Fast the Doctrine of Our Old Teachers and Also Rejects It
    - 7. Meeting of Synodical Conference in Chicago
    - 8. Quotations from Orthodox Publications and Teachers
        - A. Authors Of The Formula Of Concord: David Chytraeus, Jacob Andreae, Christopher Koerner, Martin Chemnitz, Selnecker
        - B. Original Subscribers And Defenders Of The Formula Of Concord: Aegidius Hunnius, The Wittenberg Faculty., The Wuertemberg Theologians., John Wigand., Matthew Vogel., G. Mylius., Stephen Gerlach., Daniel Arcularius., John George Sigwart., Luke Backmeister And Jacob Coler (and Chytraeus)., David Lobech., John Winckelmann., Adam Francisci., Polycarp Leyser., Solomon Gesner., Wolfgang Mamphrasius., John Pappus., Andrew Schaafmann., Philip Nicolai., John Habermann (Avenarius)., Matthias Hafenreffer., Luke Osiander., John Coler., Concluding Remarks.
        - C. The Immediate Pupils Of The Subscribers Of The Formula Of Concord: Introduction., Leonhard Hutter., Frederick Balduin., John Weber., David Runge., George Stampel., Joachim Zehner., Esaias Silberschlag., Wolfgang Franz., Balthasar Mentzer., Peter Piscator., John Schroeder., Luke Osiander, Jr., Albert Grauer., John Foerster., John Gerhard., Justus Feuerborn., Nicolas Hunnius., Conrad Dietrich.
    - Part 2. Do Our Lutheran Fathers Depart From The Confession When Teaching That The Election Took Place In View Of Faith?
        - Does Missouri Claim Our Fathers Departed From the Confession?
        - Did Missouri Hold To Her Present Doctrine in 1872?
        - Colloquy in Columbus 1879 and Following
        - Scriptures, Ordination to Salvation, Acta Huberiana, Wuertemberg Men
    - Part 3. Is The Doctrine That God Has Elected Men To Salvation In View Of Faith Found In Our Lutheran Confession?
        - Intuitu Fidei Found In the Book of Concord
        - The Formula of Concord
        - The Eleventh Article
        - Missouri Guilty of Misusing The Confession.

- III. A Testimony Against The False Doctrine Of Predestination Recently Introduced By The Missouri Synod.

    - General Introduction.
    - Introduction To The Doctrinal Discussion.
    - The Blue Island Theses.
        - Thesis 1.
            - Wider Sense
            - Confession and Universal Counsel
            - Dr. Luther’s Preface to Romans
            - Wider Sense – Gospel
        - Thesis 2.
            - Missouri – Faith in Election – Faith in Justification
            - Missouri Doctrine Of Election And The Biblical Doctrine Of Grace
            - Thoughts of a Troubled Heart
            - Chemnitz, Contingency
            - If You Are To Be Saved
            - Romans 11:33
            - Missouri Repeating Role of Old Israel
            - Election Revealed in Scripture
            - Certainty of Salvation Conditional
            - A Missourian On The Witness Stand
            - Luther To A Heart Troubled About Predestination
            - The Windy Thing on Legs
            - Dr. Walther’s Postil
            - Is It A Mystery Why God Did Not Ordain All To Eternal Life?
        - Thesis 3.
            - Election Revealed In The Gospel
            - Confession on “Election Revealed In The Gospel”
            - Words of Confession as Chief Point of Controversy
            - Missouri and The Thoughts of Reason
            - Is Redemption Only For The Elect?
            - The Eight Points
            - Missouri’s False Construction Of The Eight Points Refuted
        - Thesis 4.
            - We Place No Merit In Man
            - Dr. Walther’s Proof Of Our Synergism
            - Luther, The Confession, The Scriptures Speak As We Do
            - Baier and Huelsemann Speak As We Do
            - Quenstedt Speaks Likewise
            - Faith Impels God
        - Thesis 5.
            - The Point Of The Controversy
            - Matthew 22:1-14
            - Chemnitz on Matthew 22:1-14
            - Romans 8:28-30
            - “Foreknow”
            - Ephesians 1:3-6
            - 2 Thessalonians 2:13
            - 1 Peter 1:1-2
            - Obedience
            - Review of Scripture Passages
            - The Analogy of Faith
            - What Elect Means
            - Election and the Preacher

- The Predestination Controversy: How It Happened.
    - How the Predestination Controversy “Broke Out”

# Essays included

In three sections by Lutheran authors well respected at the time, the history of the Predestination Controversy and the points in conflict are clearly presented and compared with extracts from the writings of the framers of the Lutheran Confessions and other Church Fathers.

This book is also available in separate editions as:

- [Part 1: The Present Controversy on Predestination](https://www.lutheranlibrary.org/240tc-stellhorn-which-predestination-reformed-or-lutheran)
- [Part 2: _Intuitu Fidei_ "In Light of Faith"](https://www.lutheranlibrary.org/241tc-schmidt-intuitu-fidei)
- [Part 3: A Testimony Against the False Doctrine of Predestination Recently Introduced By The Missouri Synod.](https://www.lutheranlibrary.org/242tc-lenski-the-blue-island-theses-election-and-predestination)

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/239-schodde-the-error-of-modern-missouri-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/239-schodde-the-error-of-modern-missouri.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 239-schodde-the-error-of-modern-missouri&body=Please send 239-schodde-the-error-of-modern-missouri.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 239-schodde-the-error-of-modern-missouri&body=Please send 239-schodde-the-error-of-modern-missouri.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}

# Publication Information

- _Lutheran Library edition first published_: 2019-05-24
- _Version 4 update_: 2019-05-24
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
