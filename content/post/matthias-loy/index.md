---
date: 2019-02-11 02:00:00
publishDate: 2019-02-11 02:00:00-05:00
title: "Matthias Loy: A Biographical Sketch"
slug: "matthias-loy"
categories: ["Biographical Sketches"]
tags: ["Loy", "Lutheran Ministers"]
authors: ["Loy, Matthias"]
synods: ["Ohio Synod"]
---

[![Professor Matthias Loy](/img/posts/loy-matthias.jpg)](https://www.lutheranlibrary.org/authors/loy-matthias/)

# Rev. Professor Matthias Loy, D.D. 

The subject of this sketch was born of German parents in Cumberland Co., Pa., in 1828. He was educated at Harrisburg, Pa., and Columbus, Ohio. In 1849 he received and accepted a call as pastor of the German-English Lutheran congregation at Columbus, Ohio. 

In 1860 he was elected President of the Joint Synod of Ohio, etc., which position he occupied consecutively until 1878, when he declined re-election on account of failing health, but in 1880 he was again elected President, and has occupied the position since. In 1864 he was chosen as editor of the _Lutheran Standard_, and has continued as such until the present. In 1865 he was called as Professor of Theology in Columbus Seminary, Columbus Ohio., which position he still holds. Besides the editing of the _Lutheran Standard_ he also edits _Columbus Theological Magazine_, a bi-monthly established by him in 1881. 

In 1881 he was elected to the Presidency of Capital University, Columbus, Ohio. Dr. Loy has contributed largely to Lutheran reviews and periodicals, both German and English. He has edited a number of books and translated several into English. He is the author of [Doctrine of Justification](/171tc-loy-doctrine-of-justification/); Ministerial Office; Sermons on Gospels; [Story of My Life](/180lb-loy-story-of-my-life/).

{{% alert note %}}
This biography is taken from Jens Jensson's _American Lutheran Biographies_, published 1890.
{{% /alert %}}

First published online 2019 at [LutheranLibrary.org](.).