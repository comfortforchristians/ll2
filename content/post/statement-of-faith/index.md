---
date: 2010-09-01 12:00:00
title: Statement of Faith 

---

# The Lutheran Library exists...

- To promote knowledge, understanding and wisdom
- By finding, carefully restoring, and republishing 
- *Accurate*, *well-written*, and *readable* books suitable for self-learning.

All titles are available at no charge in proofread and well-formatted MOBI, EPUB, and PDF versions to work with Kindles<sup>®</sup>, smartphones, tablets, and most other devices.  

{{% toc %}}

# The Sole Rule of Christian Faith

Christian faith is based on the Bible as the sole rule of faith and practice.  Confessions are useful and necessary insofar as they accurately explain the Scriptures.  Traditionally Lutherans have embraced the Lutheran Confessions – also known as the *Book of Concord* or *Concordia* – as a clear description of orthodox, catholic and apostolic Christianity, that saving faith *once delivered to the saints* (Jude 1:3).

# Justification by Faith

The most important thing to grasp is that one is made right with God not by any good things he or she might do.  Justification is by faith only, and that faith rests in the one-time substitutionary death of Jesus Christ for one's sins.  

The Lutheran Library editors *believe, teach and confess* the evangelical Christian faith as described by the *Ecumenical Creeds*, *The Augsburg Confession*, *The Formula of Concord*, and the other sections of the *Book of Concord*.  


# Where  "Lutheran" Comes From

In the early days of the Reformation, *Evangelical* was the preferred name.  *Lutheran* was a derogatory term used against those who believed the Biblical doctrines.  The false implication was that Lutheranism was a departure from historic Christianity.  The truth was that it was a rejection of the medieval errors and abuses which had crept into the Church. 

>What am I, a miserable mass of corruption, that the children of Christ should be called by my name! – Martin Luther[^BC]

The Lutheran Reformation was a return to the historic faith.  Luther:

>I am entering on an arduous task, and it may perhaps be impossible to uproot an abuse which, strengthened by the practice of so many ages, and approved by universal consent, as fixed itself so firmly among us, that the greater part of the books which have influence at the present day must needs be done away with, and almost the entire aspect of the churches be changed, and a totally different kind of ceremonies be brought in, or rather, brought back. 
>
>But my Christ lives, and we must take heed to the word of God with greater care, than of all the intellects of men and angels. I will perform my part, will bring forth the subject into the light, and will impart the truth freely and ungrudgingly as I have received it. For the rest, let every one look to his own salvation; I will strive, as in the presence of Christ my judge, that no man may be able to throw upon me the blame of his own unbelief and ignorance of the truth.[^BD]


[^BC]: Quoted in *Martin Luther – The Hero of the Reformation 1483-1546* by Henry Eyster Jacobs.  Lutheran Library Edition available.  Erlangen, 22: 43 sqq.; Weimar, viii., 670 sqq.

[^BD]:  From *First Principles of the Reformation: The 95 Theses and The Three Primary Works of Martin Luther*. Lutheran Library Edition Available 

# "But Luther was a Hater!"

For those who want to throw out Luther because of statements he made out of his frustration and sadness at those Jewish people and others who would not believe in the Messiah of Israel, we offer these words of the man, from the Church Postil for the Fourth Sunday after Trinity:


>[27]  The same hypocrites are adepts in rejoicing over and taking pleasure in having an opportunity to gossip about the fall and crime of a neighbor, and to stir up his filth. And what other persons do, they always construe in the worst light ... If you were a godly person you should cover up and help to quiet such things, as much as it may be possible for you. 


>[31]  The second part of mercy is that we are to forgive those who offend us. A Christian can never be so greatly offended, that he should not forgive, not only, seven times, but seventy-times seven, as the Lord spake to Peter in Matt. 18:22. Therefore God also forgives a Christian his sins or infirmities, so that he may forgive others their infirmities. This Christ pictured just before in a beautiful parable, which he closed with the words: "So shall also my heavenly Father do unto you, if ye forgive not everyone his brother from your hearts." 


# Prayers requested

For the next generation, that the Lord will plant in them a love of the truth, and that the hard-learned lessons of the past will not be forgotten.
