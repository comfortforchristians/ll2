---
date: 2019-01-21 08:58:13
title: "The Passion for Life by Joseph Hocking"
categories: ["Lutheran Library Publications"]
slug: "330-hocking-the-passion-for-life"
tags: ["Hocking", "Fiction", "Roman Catholicism", "England"]
authors: ["Hocking, Joseph"]
titles: ["The Passion for Life"]
origpublishers: ["Fleming H. Revell Company"]
origdates: ["1920"]
synods: ["Methodist"]
---

Frank Erskine is just at the start of his career when he is given less than a year to live.  He moves from London to the Cornish coast in an effort to find peace before the end. And there his adventures begin.

# About Joseph Hocking

Joseph Hocking was a faithful Welsh minister.  A prolific and popular writer in his lifetime, Rev. Hocking considered the novel an ideal platform for exploring Christian spirituality and the deeper aspects of life.  His books deal with trials, difficulties and issues of faith.

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/330-hocking-the-passion-for-life-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/330-hocking-the-passion-for-life.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 330-hocking-the-passion-for-life&body=Please send 330-hocking-the-passion-for-life.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 330-hocking-the-passion-for-life&body=Please send 330-hocking-the-passion-for-life.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


