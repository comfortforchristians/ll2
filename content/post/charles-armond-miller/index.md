---
date: 2019-02-12 02:00:00
publishDate: 2019-02-12 02:00:00-05:00
title: "Charles Armond Miller: A Biographical Sketch"
slug: "charles-armond-miller"
categories: ["Biographical Sketches"]
tags: ["Miller", "Lutheran Ministers"]
authors: ["Miller, Charles Armond"]
synods: ["Unknown"]
---

Rev. Charles Armond Miller, pastor of College church, Salem, Va., was born March 7, 1864. His primary education was conducted under the personal care of his father. Rev. Dr. J. I. Miller, the founder and for many years Principal of Staunton Female Seminary, now President of Von Bora College, Lura, Va. 

Armond, as he is generally called by older friends, graduated at Roanoke College in 1887, with the first honors of his class, and graduated at the Philadelphia Seminary, 1889. During the vacation of 1888 he was the assistant of Rev. J. E. Bushnell, pastor of St. Mark's, and enjoys the distinction of receiving a most flattering call at this juncture from the College Church before his seminary course was completed. By special arrangements for supply he took the third year of the seminary course while pastor of an influential congregation. 

He married the daughter of Mr. John H. Sherman, of Luray, Va., June 18, 1889. Pastor Miller is a scholar of marked ability and has unusual musical culture. While churchly in his tendency, he is an active Y. M. C. A. worker, and is ever ready for evangelical services upon an inter-denominational platform. 

{{% alert note %}}
This biography is taken from Jens Jensson's _American Lutheran Biographies_, published 1890.
{{% /alert %}}

First published online 2019 at [LutheranLibrary.org](.).