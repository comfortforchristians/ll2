---
date: 2018-08-16 12:00:00
title: "The Burning Of The Old Lutheran Church by Charles Krauth"
slug: "247tc-krauth-burning-of-the-old-lutheran-church"
categories: ["Lutheran Library Publications"]
tags: ["Short Books", "Sermons", "Krauth"]
authors: ["Krauth, Charles Porterfield"]
titles: ["The Burning Of The Old Lutheran Church"]
origpublishers: ["Lutheran Publication Society"]
origdates: ["1905"]
synods: ["General Council"]

--- 
The Burning Of The Old Lutheran Church, On The Night Of September 27th, 1854, a message delivered In The Evangelical Lutheran Church, Winchester, Va., The Nineteenth Sunday After Trinity, 1854.

Writes [Dr. Krauth](/charles-porterfield-krauth/):

"To the blessed Three, the Undivided One, they reared this house...It is consecrated...to our Evangelical Religion only. They did not simply say,  We consecrate it to religion, (though that would have been enough if none were in error as to what religion is,) for even the Pagan calls his dark superstition religion; not simply 'to the Christian religion,' for the Mormon calls his beastly materialism the Christian religion; but they used that definite term which placed their meaning beyond question, just as they found it necessary amid the "gods many and lords many," to say not simply 'to God,' but to 'the one God, the Father, the Son, and the Holy Ghost.'
 
# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)

![book cover](/img/posts/247tc-krauth-burning-of-the-old-lutheran-church-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/247tc-krauth-burning-of-the-old-lutheran-church.pdf)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 247tc&body=Please send 247tc-krauth-burning-of-the-old-lutheran-church.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}
[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 247tc&body=Please send 247tc-krauth-burning-of-the-old-lutheran-church.epub)


