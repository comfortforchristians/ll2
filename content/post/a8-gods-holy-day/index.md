---
date: 2019-08-07 05:33:09-04:00   
publishDate: 2019-08-07 05:33:09-04:00   
title: "[A8] God's Holy Day (The Small Catechism)"
categories: ["This Week With The Small Catechism"]
tags: ["Weekly Catechism", "The Ten Commandments", "Golladay", "Catechism", "First Table", "Lord's Day"]
authors: ["Golladay, Robert Emory"]
titles: ["The Ten Commandments"]
origdates: ["1915"]
---

In all the commandments it is God who is speaking to us. But in these three He tells us how we are to treat Him personally. We are to have no other gods. He, our Creator, the author of all our blessings, claims our adoration. He demands the highest thoughts of our minds, the deepest love of our hearts, our truest service. We are not to profane His holy name. We may use it: indeed, we are to use it, but not abuse it. In the Third Commandment this direct personal relationship is still maintained. We are here reminded of the necessity of a day wherein to contemplate in a direct and undisturbed way, the exalted person and works of God. The Second and Third Commandments are but a continuation, or development, of the First; giving occasion for the elaboration and application of the truths therein contained.

{{% toc %}}

# 8. God's Holy Day

>"Remember the sabbath day, to keep it holy. Six days shalt thou labor, and do all thy work: but the seventh day is the sabbath of the Lord thy God: in it thou shalt not do any work, thou, nor thy son, nor thy daughter, thy man servant, nor thy maidservant, nor thy cattle, nor thy stranger that is within thy gates: for in six days the Lord made heaven and earth, the sea, and all that in them is, and rested the seventh day: wherefore the Lord blessed the sabbath day, and hallowed it." — Exod. 20:8-11. 

__Most__ of you, I trust, remember the lessons of the catechetical class. Indeed, you should have advanced much in your knowledge of God's Word since then. However, if you remember but so much as you then learned you will recall that the commandments are divided into two tables. The first three commandments form the First Table. These treat of man's direct, personal relation to God Himself.

As the subject of our study this morning, let us consider:

>__God's Holy Day__

There are three points we will discuss as briefly as clearness will permit: they are, the Primitive and Mosaic Sabbaths, and the Lord's Day.

## 1. The Primitive Sabbath. 

At the close of the account of creation, Gen. 2:2, 3, the inspired writer says: "And on the seventh day God ended His work which He had made; and He rested on the seventh day from all His work which He had made. And God blessed the seventh day, and sanctified it; because that in it He had rested from all His works which He had made." This was the establishment of the Primitive Sabbath, called also the Paradasaic, and the Pre-Mosaic Sabbath.

There are those who question whether there ever was a Sabbath before Sinai. Their chief arguments are, first, that it is not expressly stated, in the account in Genesis, that it was a day for man's observance; in the second place, that there is no further mention of the day until the giving of the Sinaitic covenant; and, finally, that the institution of the Sabbath at Sinai is sufficient proof that no such day was observed before. We submit that there is some weight in these arguments, but not sufficient to counter-balance the arguments against them.

The words in Genesis about God resting on the seventh day, and blessing it, and sanctifying it, can have no meaning comprehensible to us, aside from this that it was to be an example for man's imitation. God did not need a day of rest for Himself. "The everlasting God fainteth not, neither is weary" (Isa. 40:28). He set apart the day and blessed it for man's use, as a day of rest and worship. This is the view of many of the greatest theologians. It is Luther's view. He says: 

>"These words (namely in Gen. 2) were written alone for us. When God worked six days and rested the seventh, He had it recorded for our sakes, that we should do as He had done." 

Again Luther says: 

>"This day was ordained for Adam before he fell into sin; are we to understand, then, that it was to apply to him after he fell into sin? There is no doubt about it. Adam was to work in the Garden of Eden before the fall, as well as he had to work afterwards. The only difference being that in the one instance the work was a pleasure, a delight; in the other it was attended with so much weariness that it became a thorn in the flesh. In like manner was Adam to observe the Sabbath before and after the fall. The only difference being that in his fallen condition he needed the day for bodily rest more than he did before."

It seems that this is unmistakably substantiated by the words of the Mosaic law contained in our text. The ground here given for setting apart the seventh day as the Jewish Sabbath is the identical one contained in the creation record. If the Jewish people needed a special day of rest and worship, the children of God in the earlier days needed it: and there were children of God in those earlier days. Further, if an appeal to the Hebrews to observe the Sabbath on the ground of God's cessation of active creative work on that day, and His act of sanctifying it, would have a salutary effect two thousand years after the event, would it not have the same, or a greater force soon after the creation? The Sabbath was to be kept by the Hebrews in remembrance of God's creative work and after the example of His rest. This injunction and the reasons assigned would apply to the earlier people as well as to Israel.

It is true, that between the statement in Genesis about God's resting on the seventh day and sanctifying it, and the giving of the Law at Sinai, with its Third Commandment, there is no express mention of the Sabbath. There is, however, much evidence showing that there was a constantly recurring seventh day observed by at least some of the people. And it is best accounted for by the supposition that the people retained some knowledge of the Sabbath ordained by God (Gen. 7:4; 8:10-12; 29:27; 50:10).

The claim that the institution of the Mosaic Sabbath is sufficient evidence that no such day existed previously, is answered by the remainder that all the commandments given at Sinai had been given before; they had been written in man's heart. They were natural laws. There is no valid reason why the Sabbath should be an exception. It was a law of man's nature, reinforced by God's precept and example, that he rest and worship on the seventh day. But sin, as in all other things pertaining to God and Divine things, largely obliterated in the course of time the knowledge of the Sabbath and its observance. At Sinai the Law was repeated and emphasized. This, it appears, is contained in the word of the commandment itself — remember — "remember the Sabbath day."

Another proof confirming the existence of the pre-Mosaic Sabbath we find in the traces of the Sabbath existing among nearly all the ancient nations, such as the Persians, Assyrians and others. A tablet was recently found on the site of ancient Nineveh that is supposed to be as old as the founding of Israel in the time of Abraham. This takes us back more than five hundred years before the giving of the Law at Sinai. This old tablet speaks of the Sabbath, and gives the word in practically its Biblical form. How do we account for this save on the ground that all the ancient peoples knew, by tradition, and at least faintly, of God's original Sabbath established in Paradise.

If we turn to Exodus 16, we find that the children of Israel observed the Sabbath a month, probably many months, before they came to Sinai. It was doubtless the old creation Sabbath they were observing, which was soon to be so strikingly reestablished as a part of the written law of Israel.

Jesus said to the Jews of His day: "The Sabbath was made for man, and not man for the Sabbath" (St. Mark 2:27). The Sabbath was to serve man. In its observance man was to achieve his highest physical and spiritual good. Was God care less of the good of man till the Jewish people came on the scene? Assuredly not. The Sabbath was not alone for the Jews, it was for humanity, so far as its essential nature and purpose is concerned.

It may be said that this question of a primitive Sabbath is one of little importance. I am not ready to grant that it is so. It is associated with a problem of great importance. I am convinced, by Bible teaching and the history of nations, that a seventh day for rest and worship is part of God's plan for the world and has been so from the beginning; that the Third Commandment is but an expression of that which is needed by the very constitution of man, and that he, consequently, can reach his highest physical and spiritual good only when this law of rest and worship is observed. And when we think that in Paradise God's first children, in their state of sinlessness, observed this day to their advantage, it helps to emphasize all the more our need, the world's need, of a day of rest and undisturbed communion with God.

## 2. The Mosaic Sabbath 

The Mosaic Sabbath is the name given to the day of rest and worship reestablished at Sinai and observed by the Jewish people.

For twenty-five hundred years the children of men had experienced the vicissitudes of a sin-cursed life. God's chosen people had spent four hundred years in bondage. At the time of the giving of the Law they had been liberated, and were to be established in a land of their own, where they were to become a great nation. One of the first things God did for them was to reestablish the Sabbath. Let us not forget that one of the ten laws, written by the finger of God Himself, was this Third Commandment reestablishing for the Jewish people, as a national institution, the day of rest and worship.

The Sabbath was to be to the Jewish people, as our text clearly shows, a constant memorial of God's creative power and goodness. At all times, but especially on the day when man is enabled to escape from the absorbing and exhausting round of daily toil, his thoughts should be led to contemplate the greatness and the goodness of God. The diversified wonders and beauties of the earth; the towering mountain peaks, the fertile valleys, the broad spreading plains, the dazzling wonders of the starry heavens — all these things should lead man's thoughts to the great Workman whose handiwork they are.

In addition to being a constant reminder of God's creative work, and of His resting therefrom, the Sabbath was to be to the Jews a reminder of God's faithfulness in delivering them from their bondage. In Deuteronomy five, where the commandments are again recorded, it says: 

>"Remember that thou wast a servant in the land of Egypt, and the Lord thy God brought thee out thence through a mighty hand and an outstretched arm; therefore the Lord thy God commanded thee to keep the Sabbath."

There is no contradiction in the two accounts of the giving of the Law, or in the two things of which the Sabbath was to be a memorial. To all men it was to be a memorial of the creative power and goodness of God. To the Jews, in addition, it was to be a memorial of their deliverance from Egypt; of the wonders God wrought to soften Pharaoh's heart, the passage of the Red Sea, the pillars of fire and cloud, of the food miraculously given, and the like, all of which was to call forth their joyous praise and their most heartfelt thanksgiving.

The Sabbath was to the Jews a constantly recurring reminder of the covenant God had made with them. He had promised them an earthly, national inheritance. Through them also the Redeemer was to be given to the world. Through faith in this Redeemer they were to find grace, pardon and salvation. Of these and other related promises the Sabbath was to be a perpetual reminder:

>"My Sabbaths ye shall keep; for it is a sign between me and you throughout your generations, that ye may know that I am the Lord that doth sanctify you" (Exod. 31:13).

We should not forget that the Jewish Sabbath was also largely a day of rest, a rest in which servants and even beasts of burden participated. The general activities of life ceased. So strictly were the Sabbatic regulations enforced that, at times at least, gross violations were punished with death. But after all, the Sabbath was a day of rest that it might be above all a day of worship. Just what form this worship took in the early days we do not know. But essentially no doubt it was much like the worship of God's children in all other ages; for, fundamentally, man's needs are always the same. The general description of the worship of God's people as given in Deuteronomy thirty-one would apply to our worship in this modern day.

>"When all Israel is come to appear before the Lord thy God in the place which He shall choose, thou shalt read this law before all Israel in their hearing. Gather the people together, men and women, and children, and the stranger that is within thy gates, that they may hear, and that they may learn, and fear the Lord your God, and observe to do all the words of this law."

## 3. The Lord's Day. 

The Jewish Sabbath, so far as a particular day was concerned, and many of the observances therewith connected, was of a ceremonial or temporary character. When they had fulfilled their purpose they came to an end. St. Paul tells us these things were shadows of better things to come. When Christ, who was the One foreshadowed, came the shadow passed away (Gal. 4:5; Col. 2:16:17). But while the ceremonial elements passed away, the moral element, the obligation to worship God, remained. There is to be a day, a seventh day, devoted to rest and the special worship of Almighty God.

In the New Testament there is no trace of a particular ordinance changing the Jewish Sabbath into the Lord's day. Neither did Christ say when He instituted Baptism and the Lord's Supper, that these institutions were to take the place of Circumcision and the Passover. He simply instituted the New Testament sacraments, and the old gradually fell away. So it was with the Lord's day. He simply informally consecrated a new day, and the old gradually fell into disuse.

On the cross Christ said: "It is finished." All types and prophecies were then fulfilled. Redemption was an accomplished fact. A new era was ready to dawn. Jesus rested in the grave after His great work; rested till after the Sabbath was past. Then He came forth on the first day of the week, the author of a new, a spiritual creation. The shadows had passed away, for the Sun of righteousness had arisen. The sorrow which had filled the hearts of the disciples gave way to a boundless joy. They now began to understand the Saviour and His mission. This understanding was completed on Pentecost, and the power to apply it was given at the same time. A holy, burning love filled their hearts; a love which could lead only to worship and to service. This desire to worship naturally brought the disciples together. And in their minds there was but one appropriate time for this united service, a time which Providence had been indicating from the very first — the day of Christ's resurrection, the first day of the week. Was it not on this day that He arose and first appeared to them? Did He not appear a week later to drive away the doubts of which one of the apostles was not able to rid himself? Was it not on the same weekly anniversary of the day of the resurrection that the miraculous gift of the Holy Spirit was be stowed? Were not all their joys and their hopes centered in the facts which this day commemorated?

Some say it was the Church which selected the Lord's day as the special day of worship. In a sense, yes; but in a higher, truer sense it was God Himself. God's providence, which regulates or overrules the minute affairs of our everyday life, was not absent in this important step in the Church's history. God gave us our Lord's day, not by an act of special institution, but by His special providence in guiding his disciples. And we would not be justified in changing the day save by another providential guidance equally clear. This is supported by Scripture evidence lacking, to my mind, very little of the strength of a direct institution. In addition to the reiterated Gospel statements about Christ's rising on the first day of the week, appearing to the apostles once and again on that day, and sending the special enduement of the Holy Spirit on that day, there are many statements in later books of the Bible indicating that the Lord's day was observed as a special day of worship during the lives of the apostles. In Acts twenty, St. Paul tells us that the Christian congregation at Troas assembled on the Lord's day to celebrate the Lord's Supper. In his first Epistle to the Corinthians (16:1, 2) the same apostle, speaking of the collection for the poor at Jerusalem, gives this direction, which he says he had previously given to the church in Galatia: "Upon the first day of the week let everyone of you lay by him in store as the Lord hath prospered him." Again, in Rev. 1:10, St. John says: "I was in the spirit on the Lord's day." These passages, while not discussing the question of the institution or particular observance of the Lord's day, clearly indicate its general observance.

It is interesting also to hear what the earliest church fathers have to say on this subject. Ignatius, the disciple of St. John, tells us that those who had arrived at a newness of hope, no longer observed the Sabbath, "but lived according to the Lord's life." Justin Martyr, who lived about the middle of the second century, says: "On the day called Sunday, there is an assembly of all who live either in the city or in the rural districts, and memoirs of the apostles and the writings of the prophets are read." Dionysius, bishop of Rome, who lived near the close of the second century, says, in a letter addressed to the congregation at Rome: "Today we kept the Lord's holy day, in which we read your letters." The record of Irenaeus, bishop of Lyons, writing about the same time, says: "The mystery of the Lord's resurrection may not be celebrated on any other than the Lord's day, and on this day alone should we observe the breaking of the sacred bread." In the second year of the third century Tertullian wrote: "Sunday we give to holy joy; we have nothing to do with Sabbaths or other Jewish festivals." And in the early part of the fourth century Constantine, by imperial edict, made Sunday the day of rest and worship for the Roman Empire.

Thus we see that the day we observe, our Lord's day, can be traced back, step by step, to the day of Christ's resurrection. It came into general observance in the Church, not by the decrees of synods, or imperial edicts; but by the leading of God's Spirit. Every day, to the child of God, is a Lord's day, a day for worship; still there is a special day of rest and worship, and of it we may truly sing:


>"This is the day the Lord hath made, \
> He calls the hours His own; \
> Let heaven rejoice, let earth be glad, \
> And praise surround the throne.
>
>"Today He rose and left the dead, \
> And Satan's empire fell; \
> Today the saints His triumph spread, \
> And all His wonders tell."
# Publication Information

- _Author_: __"Golladay, Robert Emory"__
- _Title_: __"The Ten Commandments"__
- _Originally Published_: 1915 by Lutheran Book Concern, Columbus, Ohio.
- _Lutheran Library Edition_: 2019
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

{{% alert note %}}
{{% weekly-catechism-note %}}
{{% request-pdf %}}
{{% /alert %}}