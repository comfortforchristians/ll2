---
date: 2019-05-01 10:43:04-04:00
publishDate: 2019-05-01 10:43:04-04:00
title: "The Way of Salvation In The Lutheran Church by George Gerberding"
slug: "430-gerberding-way-salvation-lutheran-church"
categories: ["Lutheran Library Publications"]
tags: ["Gerberding", "Salvation", "Justification", "Conversion", "Catechization"]
authors: ["Gerberding, George Henry"]
titles: ["The Way of Salvation"]
origpublishers: ["Augsburg Publishing House"]
origdates: ["1919"]
synods: ["Augsburg"]
---

"Yes, the Lutheran Church does believe in salvation, in the absolute necessity of its personal application, and in eternal perdition to every one who will not come to God in His own way of salvation – through Jesus Christ.

"And thus the Lutheran system is a _complete_ system. It takes in _everything_ revealed in the Word. It teaches to observe all things that Christ has commanded. It declares the whole counsel of God.

"Whatever may have been the effect of reading these chapters, the writing of them has made the Church of the Reformation, her faith and practices, more precious than ever to the writer. He has become more and more convinced that what Rome stigmatized as "Lutheranism" is nothing else than the pure and simple Gospel of our Lord and Saviour Jesus Christ.

{{% toc %}}

# Chapters

- Introduction. The Story Of The Book.
- Achievements Of The Book
- Preface
- Prefatory Scripture Passages
- 1. All are Sinners.
- 2. All That Is Born Of The Flesh Must Be Born Of The Spirit.
- 3. The Present, a Dispensation of Means.
- 4. Baptism, a Divinely Appointed Means of Grace.
- 5. The Baptismal Covenant Can Be Kept Unbroken. Aim and Responsibility of Parents.
- 6. Home Influence and Training in Their Relation to the Keeping of the Baptismal Covenant
- 7. The Sunday School in Its Relation to the Baptized Children of Christian Parents.
- 8. The Sunday School – Its Relation To Those In Covenant Relationship With Christ, And Also To The Unbaptized And Wandering.
- 9. Catechization.
- 10. Contents, Arrangement and Excellence of Luther’s Small Catechism.
- 11. Manner and Object of Teaching Luther’s Catechism.
- 12. Confirmation.
- 13. The Lord’s Supper – Preliminary Observations.
- 14. The Lord’s Supper – Continued.
- 15. The Lord’s Supper – Concluded.
- 16. The Preparatory Service; Sometimes Called The Confessional Service.
- 17. The Word As a Means of Grace.
- 18. Conversion, Its Nature and Necessity.
- 19. Conversion – Varied Phenomena or Experiences
- 20. Conversion (Concluded)
- 21. Justification.
- 22. Sanctification.
- 23. Revivals
- 24. Conclusion.
- The Reformation Church
- Reader’s Guide Questions


# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/430-gerberding-way-salvation-lutheran-church-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/430-gerberding-way-salvation-lutheran-church.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 430-gerberding-way-salvation-lutheran-church&body=Please send 430-gerberding-way-salvation-lutheran-church.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 430-gerberding-way-salvation-lutheran-church&body=Please send 430-gerberding-way-salvation-lutheran-church.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2019-04-30
- _Version 4 update_: 2019-04-30 
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
