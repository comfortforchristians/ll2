---
date: 2019-06-11 10:40:52-04:00
publishDate: 2019-06-11 10:40:52-04:00
title: An Appeal to the Jewish People by Rabbi Isaac Lichtenstein Of Budapest
slug: "170ms-lichtenstein-appeal-to-the-jewish-people"
categories: ["Lutheran Library Publications"]
tags: ["Short Books", "Missions", "Jewish Christians", "Messianic Jews","Lichtenstein"]
authors: ["Lichtenstein, Isaac"]
titles: ["An Appeal to the Jewish People"]
origpublishers: ["The Hebrew Christian Testimony to Israel"]
origdates: ["1894"]
synods: ["Unknown"]
---
"I entreat you to read this little paper calmly from beginning to end. To read it thoughtfully, without prejudice, and undisturbed by the outcry made by people who are either deceivers themselves or deceived."

"I beg you to read it attentively and honestly, and to judge if its contents are logical, based on sound judgment, and deeply rooted in that Rock on which Moses stood when he beheld the glory of God." – Rabbi Lichtenstein.


This tract was written by an Orthodox Jewish rabbi who - after an honest reading of the New Testament - came to believe that Jesus Christ is in fact the true messiah of Israel. 

## To The Jews Who Reject The Gospel Because of Christian Anti-Semitism.

But what does the Rabbi say to those Jewish people who reject the gospel because of the hatred they have received from non-Jews?

"I tell you on the authority of the plain teaching of the New Testament, that Christians who do not love every one, whose hearts do not beat in love and brotherliness to their neighbor, regardless of class or race, who are revengeful or do evil of any kind, that such are Christians in name only... whom every true Christian, every real follower of Christ, must denounce as unclean, and avoid, – Shadows of the night which disappear before the light of day. 

## Rabbi Lichtenstein's Prayer

Here is a prayer Rabbi Lichtenstein gave at a Jewish missions conference in 1895:

"Almighty Heavenly Father, Eheyeh asher Eheyeh, Sovereign Ruler of past, present and future; we bless You for our past, and thank You, that in Your inscrutable wisdom, You have chosen us out of all peoples of the earth, to give us knowledge of the truth, and to make us witnesses of Your Covenant of everlasting life. Our present is dark, gloomy and desolate; but we trust Your word, O Father, that to all eternity You will not forsake Your people Israel, and we hasten forward full of hope to a glorious future, for You have sent Your heralds in the Name of Your beloved Son, Yeshua the Messiah, to comfort the mourning Daughter of Zion.
"
"Turn us again to Yourself, O Eternal, renew our days as in the former years.  – Amen.

[Source: Quiñónez, Jorge. *Introduction to the Collected Writings of Rabbi Isaac Lichtenstein (1824-1909)*. Retrieved 2017-11-24 from http://www.lcje.net/bulletins/2003/71/71_03.html]

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download) 

![book cover](/img/posts/170ms-lichtenstein-appeal-to-the-jewish-people-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/170ms-lichtenstein-appeal-to-the-jewish-people.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 170ms&body=Please send 170ms-lichtenstein-appeal-to-the-jewish-people.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 170ms&body=Please send 170ms-lichtenstein-appeal-to-the-jewish-people.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2017-11-25
- _Version 4 update_: 2019-06-11 
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
