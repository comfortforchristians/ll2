---
date: 2018-10-22 10:09:00
title: "Lutheran Library Catalog Fall-Winter 2018"
slug: "title-catalog-fall-2018"
categories: ["Lutheran Library Newsletters"]
---
With best wishes to you for Reformation Day from the Lutheran Library.

# Catalog of Titles

All titles are available to you in a variety of formats at no charge. Read. Study. Reflect. _Grow in the knowledge of the grace of our Lord_.

With best wishes to you for Reformation Day 2018 from the Lutheran Library.


- 101 [Lehmanowsky, John Jacob. _A Tale of Two Captains_](/101lb-lehmanowsky-two-captains/)
- 102 [Sandt, George Washington. _Biography of Theodore Schmauk_](/102lb-sandt-schmauk-biography/)
- 103 [Gerberding, George Henry. _New Testament Conversions_](/103tc-gerberding-new-testament-conversions/)
- 104 [Schmauk, Theodore Emanuel. _The Confessional Principle_](/104tc-schmauk-confessional-principle/)
- 105 [Gerberding, George Henry. _Biography of William Passavant_](/105lb-gerberding-passavant/)
- 106 [Scriver, Christian. _Gotthold's Emblems_](/106wd-scriver-gottholds-emblems/)
- 108 [Gerberding, George Henry. _The Lutheran Country Church_](/108tc-gerberding-lutheran-church-in-the-country/)
- 109 [Jacobs, Henry Eyster. _A Summary of the Christian Faith_](/109tc-jacobs-summary-christian-faith/)
- 110 [Krauth, Charles Porterfield. _Infant Baptism in Calvinism_](/110tc-krauth-calvinist-infant-baptism/)
- 111 [Gerberding, George Henry. _What's Wrong With The World_](/111ln-gerberding-whats-wrong-with-the-world/)
- 112 [Greenwald, Emanuel. _Why The Reformation?_](/112tc-greenwald-why-the-reformation/)
- 114 [Jacobs, Henry Eyster. _Martin Luther: The Hero of the Reformation_](/114lb-jacobs-martin-luther-hero-reformation/)
- 116 [Gerberding, George Henry. _The Priesthood of Believers_](/116tc-gerberding-priesthood-of-believers/)
- 117 [Jacobs, Henry Eyster. _The Book of Concord: Epitome_](/117tc-jacobs-book-of-concord-epitome/)
- 120 [Luther, Martin. _First Principles of the Reformation_](/120tc-luther-first-principles-reformation/)
- 121 [White, Ralph Jerome. _Six Years in Hammock Land_](/121ms-white-six-years-in-hammock-land/)
- 125 [Hosmer, James Kendell. _The Story of the Jews_](/125ln-hosmer-story-of-the-jews/)
- 133 [Greenwald, Emanuel. _Justification By Faith_](/133tc-greenwald-justification-by-faith/)
- 138 [Dolbeer, William Henry. _The Benediction_](/138tc-dolbeer-the-benediction/)
- 141 [Fesperman, Joseph Hamilton. _The Life of a Sufferer_](/141lb-fesperman-life-of-a-sufferer/)
- 143 [Whitteker, John Edwin. _Gospel Truths_](/143wd-whitteker-gospel-truths/)
- 144 [Whitteker, John Edwin. _Church and State_](/144tc-whitteker-church-and-state/)
- 145 [Stump, Joseph. _Melanchthon_](/145lb-stump-melanchthon/)
- 149 [Greenwald, Emanuel. _Pastor Louis Harms and the Church at Hermansburg_](/149ms-greenwald-louis-harms-and-hermansburg/)
- 155 [Miller, Charles Armand. _The Perfect Prayer_](/155wd-miller-the-perfect-prayer/)
- 156 [Laird, Samuel. _Selection of Sermons_](/156wd-laird-selection-of-sermons/)
- 158 [Morris, John Gottlieb. _Life Reminiscences of an Old Lutheran Minister_](/158lb-morris-old-lutheran-minister/)
- 163 [_Lutheran Treasury of Prayers_](/163wd-lutheran-treasury-of-prayers/)
- 168 [Brown, James Allen. _The Anti-Lutheran Theology of Dr. S. S. Schmucker_](/168tc-brown-theology-s-s-schmucker/)
- 169 [Einspruch, Henry. _The Most Noted Jewish Book in the World_](/169ms-einspruch-most-noted-jewish-book-in-the-world/)
- 170 [Lichtenstein, Isaac. _An Appeal to the Jewish People_](/170ms-lichtenstein-appeal-to-the-jewish-people/)
- 171 [Loy, Matthias. _The Doctrine of Justification_](/171tc-loy-doctrine-of-justification/)
- 176 [Neve, Juergen Ludwig. _Churches and Sects of Christendom_](/176tc-neve-churches-sects-christendom/)
- 180 [Loy, Matthias. _The Story of My Life_](/180lb-loy-story-of-my-life/)
- 185 [Dau, William Herman Theodore. _Luther Examined and Reexamined_](/185tc-dau-luther-examined-and-reexamined/)
- 188 [Schuh, Lewis Herman. _Funeral Sermons of Lutheran Divines_](/188wd-schuh-funeral-sermons/)
- 189 [Greenwald, Emanuel. _The Baptism of Children_](/189tc-greenwald-baptism-of-children/)
- 190 [Long, Simon Peter. _The Way Made Plain_](/190wd-long-the-way-made-plain/)
- 196 [Kiess, Frank Albert. _My experiences in the mission field of South Dakota_](/196ms-kiess-mission-field-of-south-dakota/)
- 201 [Long, Simon Peter. _Prepare to Meet Thy God_](/201wd-long-prepare-to-meet-thy-god/)
- 207 [Harms, John Henry. _The Victory of Faith: Lutheran Meditations_](/207wd-harms-victory-faith-lenten-season/)
- 209 [Horine, Mahlon. _The Book of Ruth_](/209wd-horine-book-of-ruth/)
- 212 [Huber, Eli. _Food For the Heavenly Way_](/212wd-huber-food-for-the-heavenly-way/)
- 213 [Ochsenford, Solomon Erb. _The Passion Story as Recorded by the Four Evangelists_](/213wd-ochsenford-the-passion-story/)
- 221 [Keyser, Leander Sylvester. _The Conflict Between Fundamentalism and Modernism_](/221tc-keyser-conflict-fundamentalism-modernism/)
- 222 [Keyser, Leander Sylvester. _Election and Conversion. A Frank Discussion of Dr. Franz Pieper's Book_](/222tc-keyser-election-conversion-frank-discussion-pieper/)
- 225 [Young, Egerton Ryerson. _My Dogs in the Northland_](/225ms-young-my-dogs-in-the-northland/)
- 236 [Sheldon, Henry Clay. _Theosophy and New Thought_](/236tc-sheldon-theosophy-and-new-thought/)
- 240 [Lenski & Stellhorn. _Which Predestination: Reformed or Lutheran?_](/240tc-stellhorn-which-predestination-reformed-or-lutheran/)
- 241 [Lenski, Schmidt & Gohdes. _Intuitu Fidei Election In View of Faith_](/241tc-schmidt-intuitu-fidei/)
- 242 [Lenski, Allwardt & Tressel. _Election and Predestination: The Blue Island Theses_](/242tc-lenski-the-blue-island-theses-election-and-predestination/)
- 247 [Krauth, Charles Porterfield. _A Sermon on the Burning of the Old Lutheran Church_](/247tc-krauth-burning-of-the-old-lutheran-church/)
- 249 [Sander, John. _Devotional Readings from Luther's Works_](/249lu-sander-devotional-readings-luthers-works/)
- 253 [Mahan, Milo. _Palmoni: The Numerals of Scripture_](/253tc-mahan-palmoni-numerals-of-scripture/)
- 259 [Morris, John. _To Rome and Back Again_](/259wd-morris-to-rome-and-back-again/)
- 272 [Wells, Amos Russel. _A Bible Year_](/wells-a-bible-year/)
- s01 [Krauth, Charles Porterfield. _Sayings of Charles Porterfield Krauth_](/s01-krauth-sayings/)
- s02 [Greenwald, Emanuel. _The True Church_](/s02-greenwald-the-true-church/)
- s03 [Wolf, Edmund Jacob. _John Burns: Hero of Gettysburg_](/s03-wolf-john-burns-hero-gettysburg/)

# "Extras"

- e10 [Diaz, Abby. _The Cats' Arabian Nights_](/e10-diaz-cats-arabian-nights/)
- e15 [Grimalkin, Tabitha. _Tales from Catland_](/e15-grimalkin-tales-from-catland/)
- e16 [Elwes, Alfred. _The Adventures of a Cat_](/e16-elwes-adventures-of-a-cat/)
- e17 [Elwes, Alfred. _The Adventures of a Dog_](/e17-elwes-adventures-of-a-dog/)
- e18 [Elwes, Alfred. _The Adventures of a Bear_](/e18-elwes-adventures-of-a-bear/)
- e19 [Jackson, Helen Hunt. _Letters from a Cat_](/e19-jackson-letters-from-a-cat/)
- e22 [Jackson, Gabrielle. _The Adventures of Tommy Post Office_](/e22-jackson-adv-tommy-postoffice/)
- e25 [Cowles, Julia. _Crow's Language Lessons_](/e25-cowles-crows-language-lessons/)
- e26 [Smith, Jeanie. _Zephyr: A Christmas Story_](/e26-smith-story-of-zephyr-a-christmas-story/)
- e28 [Tappan, Eva March. _Dixie Kitten_](/e28-tappan-dixie-kitten/)
- e32 [Bacon, Peggy. _The True Philosopher and Other Cat Tales_](/e32-bacon-true-philosopher-and-other-cat-tales/)
- e41 [Carr, Walter E. _The Story of Five Dogs_](/e41-carr-story-of-five-dogs/)

# The Lutheran Library

The goal of the Lutheran Library is to re-release well-written and readable books from sound, faithful American Lutherans of the past for the enjoyment and edification of a new generation.  All books are available at [lutheranlibrary.org](https://www.lutheranlibrary.org/) for free download in a variety of formats for Kindle, Apple, and other devices.

Your help is appreciated in spreading the word as often and in as many ways as you feel is appropriate.

May God bless you and keep you, help you, defend you, and lead you to know the depth of His love.  *Amen*