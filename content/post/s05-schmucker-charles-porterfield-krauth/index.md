---
date: 2019-05-28 10:37:49-04:00 
publishDate: 2019-05-28 10:37:49-04:00 
title: "A Compact Biography of Charles Porterfield Krauth by Beale Melanchthon Schmucker"
slug: "s05-schmucker-charles-porterfield-krauth"
categories: ["Lutheran Library Publications"]
tags: ["Short Books", "Schmucker", "Krauth", "Biography","Evangelical Review"]
authors: ["Schmucker, Beale Melanchthon"]
titles: ["Charles Porterfield Krauth: A Compact Biography"]
origpublishers: ["Lutheran Church Review"]
origdates: ["1888"]
synods: ["General Council"]
---
"Dr. Krauth was beyond all question the most learned and distinguished among all Lutheran theologians that use the English Language, and the great scholars of our church in other parts of the world have long ago ranked him among the chief scholars of the great church of theologians." –  Dr. G. F. Krotel

"He understood the faith, and he gave his best energies to its exposition, inculcation and defense against all assailants." –  Dr. Joseph Seiss

# About The Author

"Beale M. Schmucker, by nature and education, was a great lover of books, and his friendship with Dr. Charles P. Krauth greatly developed and nourished this love. Their letters of those early years, 1849, when a lively and regular correspondence was carried on, deal sometimes exclusively with lists of new books, catalogs, prices, and the prospects of securing some rare and valuable volumes. 'How glorious a thing the gathering of books is!' he says in [Read more...](/beale-m-schmucker/)

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/s05-schmucker-charles-porterfield-krauth-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/s05-schmucker-charles-porterfield-krauth.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request s05-schmucker-charles-porterfield-krauth&body=Please send s05-schmucker-charles-porterfield-krauth.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request s05-schmucker-charles-porterfield-krauth&body=Please send s05-schmucker-charles-porterfield-krauth.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2019-02-02
- _Version 4 update_:  2019-05-28
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
