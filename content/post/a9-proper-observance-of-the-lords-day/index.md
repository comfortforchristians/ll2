---
date: 2019-08-14 05:43:04-04:00
publishDate: 2019-08-14 05:43:04-04:00
title: "[A9] The Proper Observance Of The Lord's Day (The Small Catechism)"
categories: ["This Week With The Small Catechism"]
tags: ["Weekly Catechism", "The Ten Commandments", "Golladay", "Catechism", "First Table"]
authors: ["Golladay, Robert Emory"]
titles: ["The Ten Commandments"]
origdates: ["1915"]
---

The day of rest and worship, as it has been observed from the creation to the present time, we have already considered. This subject, considered from the viewpoint chiefly of the day itself, is one of no slight importance. The very fact that for all these thousands of years there has been a day so observed gives great weight to every plea for its proper continuance and observance. But after all it is not the day in itself which is the matter of chief importance. It is to serve a purpose, and that purpose is the chief concern. Christ says: "The Sabbath is for man, and not man for the Sabbath." This means that the Sabbath was not a day set apart to be observed for the sake of the day. If there were no people to be served there would be no Sabbath. This day was given to promote man's good. The same applies to the Lord's day. It is a measure of time just as any other day, a day consecrated by many sacred  memories; but its object is to bring these things to man's attention, that he may be blessed through their acceptance. The Proper Observance of the Lord's Day is the important subject to which we shall give our attention at this time.

{{% toc %}}

# 9. The Proper Observance Of The Lord's Day

>"Remember the Sabbath day to keep it holy." — Exod. 20:8.

> The Sabbath was made for man, and not man for the Sabbath." — St. Mark 2:27.

> And let us consider one another to provoke unto love and to good works: not forsaking the assembling of ourselves together, as the manner of some is; but exhorting one another: and so much the more, as ye see the day approaching." — Heb. 10:24-25. 

## The Lord's Day as a Day of Rest.

That the Sabbath, in both its pre-Mosaic and Mosaic forms, was in part a day of rest, we remember. This means that it was to be preceded by days of toil and burden-bearing. In the true sense of the word there can be no rest till men have borne the burden and heat of the day. That this is contained in the Third Commandment many do not seem to remember. The commandment as given to the Jews says: "Six days shalt thou labor, and do all thy work."

Many people look upon work as a curse, and one of their ideals of life is a state of freedom from toil. This is a perverted conception of life. God's plan for man is that he shall be a worker, even as He the everlasting God is Himself a worker. Sin has put the sting into toil and made it burdensome, but man's original state of innocency was one of blessed activity.

In the New Testament God's law respecting work is reenacted. I speak of it as God's law because the expression of His will is law for his children. To certain people who, because of erroneous opinion, had become idle, the Lord through his apostle says: "We beseech you… work with your own hands, as we commanded you; that ye may walk honestly toward them that are with out, and that ye may have lack of nothing" (1 Thess. 4:10-12). Again the Lord says to these same people: "When we were with you, this we commanded you, that if any would not work, neither should he eat" (2Thess. 3:10).

These and similar truths need to be brought forcibly home to a considerable portion of humanity. The dream of many is of a modern Arcadia where the cornucopia shall pour into the lap of idleness all the blessing of bountiful nature. Such people would have life to be one grand frolic. No condition would sooner bring ruin to the children of men. Work is necessary to man's well-being. This is so irrespective of the fact that this is his usual means of supporting his physical life. Work helps to impress the truth that life is real, life is earnest. Work helps to develop character; it is a blessing, not a curse. Work reminds the thinking workman of the fact that he is privileged to cooperate with the Master Workman in carrying out His plans for the general good of humanity. And if things were among men as they should be, no one, though he could command thousands of dollars, would be permitted to partake of God's bounty till he had first rendered some adequate service for that which he receives.

There is, however, a danger on the other side. Those who would have without working are many. But those in whom the spirit of greed is abnormally developed are also many. Urged on by the hope of gain, these people work every day if possible, and just as many hours each day as possible. And where possible, if they employ help, they require the same of them. To prevent us from bringing about our own undoing, God ordained a day of rest. The particular day made obligatory upon the Jews is not binding upon us; but God has led his people in the choice of a day, and it is still a seventh day.

Our physical welfare demands this regularly recurring day of the cessation of toil. The most practical and painstaking investigation has proven beyond a shadow of doubt that a man can accomplish more work in a given time, say a month or three months, and do it better, if he rests one day out of seven than if he works continuously. God wants us to work, but at the proper time He wants us to rest. He does not want us to become drudges, mere working machines. The person who works seven days a week instead of six will sooner or later have to pay the penalty in the form of physical ills. God knew what He was doing when He provided a day of rest for man. It was to meet a law of man's nature. And His motive was love for man, a desire to promote his well-being.

The labor problem is a burning one in our day. It is often asserted that the laboring class has turned against the Church. Perhaps no larger proportion of this class has turned against the Church than of any other. If the working man, as well as all other men, knows what is best for him, if he wants to keep his heritage of robust manhood, and manhood's vision of better things, let him not forget or despise God's ordinances, especially this pertaining to the Lord's day.

Those who make of the Lord's day a day of pleasure, in the pursuit of which they probably put more energy than they do in a day of toil, are not keeping it to their physical welfare.

On the other hand, when we speak of the Lord's day as a day of rest we do not mean a day of sleepy inactivity. People often make of the Lord's day an occasion for eating and drinking to excess. The result is that they do themselves much harm, far overbalancing the benefit derived from their rest. This helps to account for the well known fact that there are twice as many accidents happen in places of business on Monday as on any other day. For the Lord's day to be a day of rest it must not be a day of indulgence; and it need not be, indeed, it ought not be, a day of torpid inactivity.

Considered merely from the point of view of a day of rest, the Lord's day should be a time for looking at ourselves and the world from another angle than that of the daily round of toil. It is a time for special contemplation of what is back of all this phenomena of life, for the study of our deeper selves, of our whence and our whither, of our place in the sun. To do this in a reasonable way will not interfere with the proper observance of the day as a day of rest.

## The Lord's Day as a Day of Worship 

As a day of rest the Lord's day is worthy of our careful study. As such a gift of God it should in spire men to gratitude and thanksgiving. But where the chief emphasis is to be placed in the consideration of this commandment Luther well shows in his explanation of it. He says that it means that 

>"we should fear and love God, that we may not despise preaching and His Word; but hold it sacred and gladly hear and learn it."

In this explanation, as in so many others, Luther showed his masterly, I unhesitatingly say, his God-given wisdom. He knew that unless men truly loved God, honored His Word, and from such motives kept His holy day, rules and regulations imposed from without would be in vain.

Israel in her days of worst hypocrisy and formalism had more stringent regulations for Sabbath observance than ever before. But there was always a way of getting around them when they wished so to do. And even when they outwardly kept the day God declared that He was not thereby honored. The day was not rightly kept because the right motive was lacking.

The Lord's day may not be rightly kept though a man refrain from every form of activity. Many a person remains at home all day Sunday and imagines that he has properly kept the day. In activity and silence do not meet the requirements of this commandment. The Lord's day is not rightly kept till God is rightly honored. This does not mean that the other six days may be so completely secularized that God need never be thought of during them. God is to be honored every day. He is to be honored in all things. In the daily toil of life, performed faithfully by a child of God, to the end that thereby His name may be glorified, and His plans furthered, God is truly honored. The true child of God can not live through six busy, trying days without being impelled to find times, though they be but brief moments, in which to speak to his all-wise and all-loving Father. His own needs will drive him to this, also his sense of gratitude. But this is not all that our good requires, that God asks, and His honor demands. God is a universal sovereign. The honor of His name, the good of His cause among men, requires that He be publicly acknowledged, and publicly worshipped by his loyal, loving subjects. This is the side of obligation. But true worship is not wholly a matter of outer necessity. It is on the part of true worshippers equally a matter of inner compulsion. Our sense of need nowhere else to be supplied, our consciousness of dependence on the arm of the Almighty, the soul-craving which can be met only by fellowship with the Author of our being, these things impel to worship from within. And those of kindred spirit are drawn by the invisible bond of fellowship with God to unite in their supplication and adoration.

All this of which we have been speaking centers in the public worship on the Lord's day, though it is not confined to it. God has given us His holy Word, through which He speaks to us, through which we learn rightly to know Him. God has ordained that this Word, which is not only a Word giving instruction, but a Word bearing power to quicken dead souls, be publicly proclaimed. And that along with the reception of this truth, with its light and life giving power, there should be joined our sacrificial offerings of prayer, praise and adoration. And no one who habitually absents himself, without necessity, from the public services of God's house is keeping this Third Commandment which says: "Remember the Sabbath day to keep it holy." Such an one does not honor God because he does not honor God's Word.

There are instances in rural districts, in cases of sickness, and occasionally from other cause, where men are prevented from regularly participating in these public services. This need not keep one from meeting God's requirement as set forth in the Third Commandment. If one cannot meet with a congregation of the same faith, let him meet with the church in his own house. This is the way the Christians often did in the early days. Where but two or three are assembled in the name of God He is with them as truly as if they were assembled with a great congregation. And though a man be a stranger in a strange land, he need not be deprived of his spiritual privileges and blessings. God and one devout soul, in a closet or on a desert waste, meet all the requirements of the highest and most blessed service.

We need the Lord's day and its holy ministries aside from the direct spiritual blessings we receive at the time. We need it in order to be kept mindful of our relationships, in heaven and on earth. We need it to keep from becoming too much absorbed in the things of this world. It furnishes us equipment for the struggles of the week. We need patience, courage, vision, strength; these hours of communion with God, and contemplation of heavenly things, supply these much needed qualities. The light which the Lord's day throws upon the great problems of life serves as a perpetual reminder of the true meaning of the other six days of the week, 8 and keeps us in preparation for the eternal Sabbath that awaits all the redeemed and purified children of God.

Pastors are often asked whether there are any rules by which the Christian may direct his general conduct on the Lord's day. There are no specific regulations beyond those we have already given. For the body it is to be a day of rest and recuperation. For the soul it is to be a day of instruction in the truths of God's Word, a day of worship, of holy meditation. Beyond this we have no specific directions. However, this is a safe general rule; the conduct of life on this day should not belie the profession we make with our lips. If one is truly spiritually minded, well instructed in the Word of God, living in fellowship with Christ, truly desirous of honoring God and of enjoying His blessings, it can be safely left to such a person's conscience as to the specific manner in which that portion of the Lord's day shall be spent which is not devoted to worship.

Those who are closely confined during the week, after having devoutly taken part in the services of God's house, need have no scruples as to the propriety of taking a quiet walk in the country, thus to enjoy the beauties of God's handiwork. With equal propriety a Christian may call on his friends or spend the time in reading useful books. If we have in our hearts the law of the indwelling Christ, the law of faith and the love begotten by faith; if we seek the rule of right living in God's Word, we will not go far amiss in our observance of the Lord's day, or any other day. One thing is certain, people so influenced will not make of the Lord's day a day for idly gadding about, nor spend it in the mad chase after pleasure, nor make of it, if it can possibly be avoided, a day of toil. The tendency to make of the Lord's day a mere holiday is not in keeping with the teaching or spirit of God's holy Word. 

It has always been observed that there is a close relationship between the manner of observing the Lord's day and the state of the Christian Church. Where God's holy day is sanctified; where it is ob served as a day of prayer and praise, where the object of men is to get a better knowledge of God through the study of His holy Word, and God is honored as the sovereign of men's hearts, there the Church will be found to be in a flourishing condition. But where the Lord's day becomes a Continental or Mexican holiday, a day of riotous pleasure, of fun and frivolity, there the Church loses its power. Indeed, it was observed by St. Augustine, almost fifteen hundred years ago, that when the Lord's day is not kept as a Lord's day it soon degenerates into a devil's day. There is usually more wickedness perpetrated on this day than on any other one of the seven. This is accounted for in part by the fact that more people are at leisure then than at any other time, and when not controlled by the Spirit of God soon fall into evil ways.

Parents, where and how do you spend the Lord's day? Are you, by word and example, showing how the Lord's day should be kept? Are you making it a day of brightness, one of the brightest of all the week? Are you leading the way to the house of God and to the family altar? Are you making it a day which sheds its benediction over all the other days of the week?

Parents, tell me where your boys and girls are on Sunday, and what they are doing, and we will be able to tell you with much accuracy the kind of characters they are cultivating, and the kind of men and women they will likely come to be. Those who have no reverence for the Lord's day, no love for the services of God's house, have no reverence for God Himself, no respect for His laws. And it is not difficult to tell to what end they will come, unless a change is made; for such people will have little regard for any human institutions or laws further than they can be made to serve their own selfish interests.

Human institutions have come and gone. The forms of even not a few Divine ordinances have changed. This is true, as to some of its non-essentials, of the day of rest and worship. But as to its essence and fundamental purpose the Sabbath has survived all changes. Men and nations with set purpose have sought to destroy it, but it has survived all onslaughts. It is not everywhere ob served as it should be, but everywhere this day of days, this day of rest and worship, this day which God gave us for the rehabilitation of tired bodies and tried souls, speaks forth its benediction to the children of men; pointing them to the skies, and keeping them mindful of the fact that there remaineth a rest for the people of God — an eternal Lord's day of uninterrupted and unmediated fellow ship with the Lord of life and glory.

>"Thine earthly Sabbaths, Lord, we love, \
>But there's a nobler rest above; \
>To that our longing souls aspire, \
>With ardent love and strong desire."

# Publication Information

- _Author_: __"Golladay, Robert Emory"__
- _Title_: __"The Ten Commandments"__
- _Originally Published_: 1915 by Lutheran Book Concern, Columbus, Ohio.
- _Lutheran Library Edition_: 2019
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

{{% alert note %}}
{{% weekly-catechism-note %}}
{{% request-pdf %}}
{{% /alert %}}