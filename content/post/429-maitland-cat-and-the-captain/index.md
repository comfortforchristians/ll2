---
date: 2019-05-16 15:28:57-04:00
publishDate: 2019-05-16 15:28:57-04:00
title: "Cat and the Captain: The True Story of a Cat and Dog"
slug: "429-maitland-cat-and-the-captain"
categories: ["Lutheran Library Publications"]
tags: ["Maitland","Cats","Dogs","Animals","Children"]
authors: ["Maitland, Julia Charlotte"]
titles: ["Cat and the Captain"]
origpublishers: ["Grant and Griffith"]
origdates: ["1854"]
synods: ["N/A"]
---

"The Author begs to assure her young readers that the principal circumstances on which this little story is founded are true. The friendship between the two animals, the dog's journey home, and return in company with his friend, are facts which occurred within her own knowledge."




# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/429-maitland-cat-and-the-captain-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/429-maitland-cat-and-the-captain.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 429-maitland-cat-and-the-captain&body=Please send 429-maitland-cat-and-the-captain.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 429-maitland-cat-and-the-captain&body=Please send 429-maitland-cat-and-the-captain.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_:  2019-05-16
- _Version 4 update_: 2019-05-16 
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
