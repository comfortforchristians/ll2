---
date: 2019-04-24 05:16:47-04:00
publishDate: 2019-04-24 05:16:47-04:00
title:  First Principles Of The Reformation – The Three Primary Works Of Luther And The 95 Theses by Henry Wace and C. A. Buchheim
slug: "120tc-luther-first-principles-reformation"
categories: ["Lutheran Library Publications"]
tags: ["Luther", "Wace", "Buchheim", "Confessions", "Christian Liberty"]
authors: ["Luther, Martin", "Wace, Henry", "Buchheim, Charles Adolphus"]
titles: ["First Principles Of The Reformation – The Three Primary Works Of Luther And The 95 Theses"]
origpublishers: ["Lutheran Publication Society"]
origdates: ["1885"]
synods: ["Anglican"]

---

Many people have opinions about Martin Luther, but few have actually read his words.  This small volume includes what church scholars Henry Wace and C. A. Buchheim consider Luther's three primary works.  These are the *Address to the Nobility of the German Nation*, *Concerning Christian Liberty* and *On the Babylonian Captivity of the Church*.  The famous *95 Theses* are here too, as well as two helpful introductions, one theological, and the other historical.  
 
{{% toc %}}

# I. *Address to the Nobility of the German Nation*

This first essay was published in 1520, just when the Bull of excommunication against Luther was being prepared. Luther's central assertion, says Wace, is that, "the functions of the clergy are simply ministerial (exercising) on behalf of all, powers which all virtually possess." 

Luther writes with "...astonishing vigor, frankness, humor, good sense, and...intense moral indignation... So tremendous an indictment...could hardly be paralleled in literature."[^BV]

In the section, *The Three Walls of the Romanists* Luther uses Scripture and logic to destroy the three barriers the Roman Catholic Church erects against any opposition.  These are:

1. Temporal power has no jurisdiction over spirituality
2. No one may interpret the Scriptures but the Pope
3. No one can call a Council but the Pope

The essay essay ends with the *Twenty-seven Articles respecting the Reformation of the Christian Estate*.

# II. *Concerning Christian Liberty* 

This second treatise was addressed to Pope Leo X.  Luther intended it as a peace offering.[^BW]  It's a working out of the character of a Christian Life of a person truly justified by faith.  

1.  A Christian is the most free Lord of all, and subject to none, and yet,
2.  A Christian is the most dutiful Servant of all, and subject to every one.

# III. *On the Babylonian Captivity of the Church*

To Luther, the freedom granted through faith is bounded by the Word.  In *On the Babylonian Captivity of the Church*, Luther applies this principle to the sacraments recognized by the Roman Catholic Church.  He accepts only the first two –  Baptism and the Lord's Supper –  as holding to the Biblical conditions of being (a) a visible sign, (b) instituted by Jesus Christ, and (c) marking a Divine promise.

1. The Lord's Supper
2. Baptism
3. Penance
4. Confirmation
5. Marriage
6. Orders
7. Extreme Unction

# Also included

The *95 Theses*, *Theological Introduction by Dr. Wace*, and *Historical Introduction by Professor Buchheim*

The Editor of the Lutheran Library presents this book to you with the prayer that it may feed your faith.  Here is the good nourishment of the Word presented by our brother the great teacher, Martin Luther.  If you've read no other Luther (and even if you have), here is a place to begin.

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download) 

![book cover](/img/posts/120tc-luther-first-principles-reformation-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/120tc-luther-first-principles-reformation.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 120tc&body=Please send 120tc-luther-first-principles-reformation.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 120tc&body=Please send 120tc-luther-first-principles-reformation.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}
[^BV]: Wace. *Theological Introduction*. 

[^BW]: Ibid. 

# Publication Information

- _Lutheran Library edition first published_: 2017-10-02 
- _Version 4 update_: 2019-04-23 
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
