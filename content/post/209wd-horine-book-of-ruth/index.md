---
date: 2018-02-10 12:00:00
title: "Practical Reflections on the Book of Ruth by Rev. M. C. Horine"
slug: "209wd-horine-book-of-ruth"
categories: ["Lutheran Library Publications"]
tags: ["Horine"]
authors: ["Horine, M. C."]
titles: ["Practical Reflections on the Book of Ruth"]
origpublishers: ["Lutheran Publication Society"]
origdates: ["1892"]
synods: ["General Council"]

---
Ruth forsook father and mother, country and friends, to cast in her lot with the people of God, and her name is linked with that of the greatest king of Israel, and with that of the great Redeemer of mankind. In her noble act of self-sacrifice she honored God; and God, in accordance with His promise, honored her. No one ever lost anything by being true to God. He is able to reward His faithful servants, and He does reward them liberally.

 
{{% toc %}}

# The Bible – A Perennial Fountain

"The Bible is a perennial fountain which sends out a continuous stream of living water to refresh the thirsting souls of weary men... The book of Ruth is one of the brightest gems of sacred biography. it abounds in beautiful, touching and interesting incidents, and while it charms and pleases the reader, it at the same time instructs and improves him." 

# From the Introduction

"The discourses that make up the little volume before us were prepared by a well-known Lutheran pastor for the members of his own congregation.
They are based on continuous sections of the Book of Ruth; and they gain not a little advantage from the fact that they have for their foundation a portion of the Divine word so full of interest and so rich in material for useful instruction.

"That the Book of Ruth is interesting is a universally acknowledged fact. The story it tells holds the attention from first to last. The characters it delineates awaken sympathy in all hearts. The pictures it paints are specially attractive and afford enjoyment to young and old, to cultured and rude. The scene in which Ruth makes known her unalterable purpose to cleave to Naomi, let come what will, is one of surpassing tenderness and loveliness. 

# About Rev Horine

Mahlon C. Horine was born July 14, 1838 in Frederick County, Maryland. In 1861 he married Emma Frances Winebrenner in Gettysburg. They had six children.

Dr. Horine graduated from Gettysburg College in 1861, Gettysburg Lutheran Seminary in 1863, and earned D.D. from Muhlenburg College in 1892. He served pastorates at Smithsburg, Maryland 1865-1869, Dayton, Ohio 1869-1870, Zanesville, Ohio 1870-1873, Danville, Pa. 1873-1881, St. James. Reading, Pa., 1881-1909, and Trinity Lutheran, Monoa, DE 1913-1917. 

His ordination in 1864 was at the Maryland Synod, Baltimore, Maryland. Rev. Horine went to be with the Lord on May 16, 1917.[^aC5]

"The Bible is a perennial fountain which sends out a continuous stream of living water to refresh the thirsting souls of weary men... The book of Ruth is one of the brightest gems of sacred biography. it abounds in beautiful, touching and interesting incidents, and while it charms and pleases the reader, it at the same time instructs and improves him." 

# Contents

- Practical Reflections On The Book Of Ruth By Rev. M. C. Horine, A. M.,
- Copyright Page - Originally published 1892 by The Lutheran Publication Society,
- Philadelphia.
- Introduction.
- Preface.
- Chapter 1. Elimelech.
- Chapter 2. Ruth’s Resolve.
- Chapter 3. Naomi’s Return To Bethlehem.
- Chapter 4. Gleaning In The Field Of Boaz.
- Chapter 5. Boaz Meeting Ruth.
- Chapter 6. Ruth’s Return To Naomi.
- Chapter 7. The Engagement.
- Chapter 8. Removing Legal Hindrances.
- Chapter 9. The Marriage.
- About the Author

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)

![book cover](/img/posts/209wd-horine-book-of-ruth-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/209wd-horine-book-of-ruth.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 209wd&body=Please send 209wd-horine-book-of-ruth.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 209wd&body=Please send 209wd-horine-book-of-ruth.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}

[^aC5]: John D. Barrett. Retrieved 2018-02-07 from http://www.frontierfamilies.net/family/horine/JohnHenryHorineG8.htm