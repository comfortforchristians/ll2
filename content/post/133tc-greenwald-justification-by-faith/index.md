---
date: 2017-10-19 12:00:00
title: 'The True Church: Its Way of Justification and Its Holy Communion by Emanuel Greenwald'
slug: "133tc-greenwald-justification-by-faith"
categories: ["Lutheran Library Publications"]
tags: ["Greenwald", "Justification", "Lord's Supper"]
authors: ["Greenwald, Emanuel"]
titles: ["The True Church: Its Way of Justification and Its Holy Communion"]
origpublishers: ["Lutheran Book Store"]
origdates: ["1876"]
synods: ["Ohio Synod"]

---
"The force of a plain passage of God's Word is sought to be evaded by the flippant reply, 'Yes, so it reads in your Bible,' as if we had made the Bible. No, it is God's Word, and not man's word, and we have it in our hands, just as Christ spoke it, and the apostles wrote it, and the old primitive Church read it, and all Christendom, from that time to this, believed and practiced it. 

{{% toc %}}

# Justification by Faith Alone

"The doctrine of Justification by Faith in the atonement for sin effected for us by the obedience unto death of our Lord Jesus Christ, distinguishes Christianity from all other religions in the world.

"And the doctrine of Justification by Faith alone, was the turning point of the Reformation; it was the experience of its necessity and efficacy in the heart of Martin Luther that constituted his best qualification for the work of the Reformation; and as it distinguished the Lutheran Church from the Church of Rome, so it has come to be regarded as the distinguishing mark of separation between Protestantism and Romanism.

# God's Word

"God's Word is the only infallible guide and teacher. We have it in our hands, just as Christ spoke it, and the apostles wrote it, and the old primitive Church read it, and all Christendom, from that time to this, believed and practiced it. We have it pure and true as it came from the mouth of God himself, in the very words in which He inspired it, and clothed with infallible divine authority." 
 
# About Dr. Greenwald

Emanuel Greenwald was born near Frederick, Maryland, Jan. 13, 1811, and was, like the prophet Samuel of old, dedicated by his pious parents to the holy office from his earliest infancy. His theological studies were pursued under the private supervision of Rev. David F. Schaeffer, who similarly prepared no less than fourteen other young men, in his own parsonage, for the work of the ministry. 

Dr. Greenwald's first parish was New Philadelphia, Ohio, and all the adjoining country within a radius of fifteen miles in every direction. At one time he supplied fourteen preaching points on Sundays and week-days. In 1842 he was elected as the first editor of the Lutheran Standard, and from 1848 to 1850 he was the president of the English District Synod of Ohio. 

The years 1851 to 1854 were spent in the city of Columbus, during which time he held many responsible positions on important boards, committees, etc. In September, 1854, he accepted a call to the pastorate of Christ Church, Easton, Penn., which he faithfully served for twelve years. His fourth and last parish was Holy Trinity Church of Lancaster, Penn., in which he labored from May, 1867, up to his death in December, 1885. He began preaching every Thursday evening at a mission point in the northern part of the city, which soon developed into Grace Church, and afterwards started another mission in the western section which was the nucleus of Christ Church. An assistant being necessary for the increasing field, Rev. Charles S. Albert served in this capacity, then Rev. David H. Geissinger, then Rev. John \V. Rumple, then Rev. C. Elvin Haupt, then Rev. Ezra K. Reed, then Rev. Charles L. Fry. Long after his own generation will his memory continue to be revered as an ideal pastor and a man of pre-eminent saintliness. C. L. F.[^acK]

Also available from The Lutheran Library:  [Why the Reformation?](/112tc-greenwald-why-the-reformation/)



# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download) 

![book cover](/img/posts/133tc-greenwald-justification-by-faith-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/133tc-greenwald-justification-by-faith.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 133tc&body=Please send 133tc-greenwald-justification-by-faith.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 133tc&body=Please send 133tc-greenwald-justification-by-faith.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}
[^acK]: Jacobs, Henry Eyster, ed. (1899) *The Lutheran Cyclopedia*. New York: Charles Scribner's Sons 