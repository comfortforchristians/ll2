---
date: 2019-05-29 05:46:07-04:00
publishDate: 2019-05-29 05:46:07-04:00
title: "Miracles by Theophilus Stork"
slug: "s08-stork-miracles"
categories: ["Lutheran Library Publications"]
tags: ["Stork", "Miracles","Short Books","Evangelical Review"]
authors: ["Stork, Theophilus"]
titles: ["Miracles"]
origpublishers: ["Evangelical Review"]
origdates: ["1850"]
synods: ["General Synod"]
---

"Miracles in the Christian system are like the massive subterranean arches and columns of a huge building. Miracles support the edifice, and upon a divine foundation. 'They show us, that if the superstructure is fair and beautiful to dwell in, and if its towers and endless flight of steps appear to reach even up to heaven, it is all just what it seems to be; for it rests upon the broad foundation of the Rock of Ages.' — Ware. 

# Secret Prejudice Against Miracles
"There has always been a secret prejudice against miracles.

"Even where it does not reach this formidable aspect of repugnance to miracles, asserting the essential incredibility of such facts, there is still a reluctance in many minds to admit these departures from the order of nature predicated in miracles. And if we mistake not, there is a tendency in this age, and in this country, to depreciate, if not altogether to overlook, these primitive and distinctive evidences of revelation. 

"A modified form of this feeling may be seen in many honest believers in their disposition to overlook the miracles as the wonders of a distant age, answering an important purpose in the first introduction of Christianity, but of little use now as evidences of their religion; and the consequent inclination to resort exclusively to the internal evidence. They are satisfied with the intrinsic excellence of their religion — its adaptation to their spiritual wants, and the secret responses of their own hearts to its teachings — this is all the evidence they desire. They are ready to exclaim with Coleridge, "Evidences of Christianity! I am weary of the word. Make a man feel the want of it; rouse him, if you can, to the self-knowledge of his need of it; and you may safely trust it to its own evidence."

"But those who unite in this fervid exclamation forget that miracles are fundamental to the very existence of objective Christianity. And although in their spiritual apprehension and experience of its blessed truths, they may not feel the necessity of miracles to confirm their faith in religion, still they are, in fact, the ultimate basis upon which the whole system rests.


# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/s08-stork-miracles-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/s08-stork-miracles.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request s08-stork-miracles&body=Please send s08-stork-miracles.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request s08-stork-miracles&body=Please send s08-stork-miracles.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2019-05-29
- _Version 4 update_: 2019-05-29 
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
