---
date: 2019-07-11 05:30:57-04:00 
publishDate: 2019-07-11 05:30:57-04:00 
title: "The Trial of Professor Luther A. Gottwald by Wittenberg Seminary"
slug: "495-gotwald-trial-wittenberg-seminary"
categories: ["Lutheran Library Publications"]
tags: ["Gottwald"]
authors: ["Gottwald, Luther Alexander"]
titles: ["The Trial of Luther A. Gotwald"]
origpublishers: ["Lutheran Publication Society"]
origdates: ["1893"]
synods: ["General Synod"]
---

In 1893 an attempt was made by liberal elements in the General Synod to remove Dr. Luther Gotwald from Wittenberg Seminary. He was said to be guilty of teaching the Augsburg Confession as, "a correct expression or exhibition of fundamental divine truth". 

The record of the attacks against this conservative Lutheran makes riveting reading and speaks directly to the battles faithful Christians face from within the church.

# Book Contents

- Preface.
- The Charges
    - Charges As Preferred Against Dr. Gotwald By A. Gebhart, Joseph R. Gebhart, And E. E. Baker.
    - Motion By Dr. Gotwald’s Counsel With Regard To The Charges Preferred.
    - Argument Of Counsel For Defendant In Support Of Motion To Amend Specifications.
    - The Action Of The Board Upon The Defendant’s Motion.
    - The Charges As “Made More Specific,” Upon Which Dr. Gotwald Was Tried.
    - Response To The Charges By Dr. Gotwald’s Counsel.
    - "Official Statement"” Concerning The Trial By Officers Of The Board.
- The Reply: Lutheran Confessionalism In The General Synod: A Reply To The Charges Of My Assailants.
    - I. The Augsburg Confession.
    - II. The Lutheran Symbolical Books Other Than The Augsburg Confession.
    - III. The Distinctive Doctrines Of The Lutheran Church.
        - a. Baptismal Regeneration.
        - b. The Real Presence in the Lord’s Supper.
        - c. Private Confession and Absolution.
    - IV. Lutheran Catholicity And Union.
    - V. Experimental And Practical Piety.

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/495-gotwald-trial-wittenberg-seminary-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/495-gotwald-trial-wittenberg-seminary.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 495-gotwald-trial-wittenberg-seminary&body=Please send 495-gotwald-trial-wittenberg-seminary.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 495-gotwald-trial-wittenberg-seminary&body=Please send 495-gotwald-trial-wittenberg-seminary.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 
- _Version 4 update_:  
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
