---
date: 2018-08-16 12:00:00
title: "Election and Conversion. A Frank Discussion of Dr. Franz Pieper's Book by Leander Sylvester Keyser"
slug: "222tc-keyser-election-conversion-frank-discussion-pieper"
categories: ["Lutheran Library Publications"]
tags: ["Pieper", "Keyser"]
authors: ["Keyser, Leander Sylvester"]
titles: ["Election and Conversion. A Frank Discussion of Dr. Franz Pieper's Book"]
origpublishers: ["The German Lutheran Literary Board"]
origdates: ["1914"]
synods: ["Iowa Synod"]

--- 
"How much the Bible makes of faith! How little, comparatively, of election! Everywhere Christ insisted on faith and belief, while scarcely more than half a dozen times does He refer to "the elect," and almost always in passages whose interpretation is more or less difficult. 

{{% toc %}}

# Faith in the Epistles

"Note how often faith is mentioned in the epistles. Two of Paul's epistles – Romans and Galatians – were expressly written to prove that men are justified by faith, and not by the deeds of the law or their own righteousness. The letter to the Hebrews devotes a whole chapter – the 11th – to a panegyric on the heroes of faith. It declares that "without faith it is impossible to please Him; for he that cometh to God must believe that He is, and that He is a rewarder of all them that diligently seek Him." Our point is that faith is the outstanding doctrine of the New Testament, and therefore should take precedence of a doctrine like election, which is treated more incidentally."

# Faith is the Regulative Principle

Whether we have gathered up all the links in our argument or not, this is sure: we have made faith in Christ the central and regulative principle, just as Paul did, just as Luther did, just as the Augustana and all other Lutheran Symbols do. If anything in our Lutheran system of doctrine must bend, or step aside, it cannot be faith in Christ; for He is the express image of God's person, His perfect revelation, and faith in Him is our only hope. – From Chapter 4 


# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)

![book cover](/img/posts/222tc-keyser-election-conversion-frank-discussion-pieper-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/222tc-keyser-election-conversion-frank-discussion-pieper.pdf)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 222tc&body=Please send 222tc-keyser-election-conversion-frank-discussion-pieper.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}
[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 222tc&body=Please send 222tc-keyser-election-conversion-frank-discussion-pieper.epub)


