---
date: 2019-07-15 09:17:19-04:00
publishDate: 2019-07-15 09:17:19-04:00
title: "Life Reminiscences of an Old Lutheran Minister by John Gottlieb Morris"
slug: "158lb-morris-old-lutheran-minister"
categories: ["Lutheran Library Publications"]
tags: ["Biography", "Morris", "Start Here"]
authors: ["Morris, John Gottlieb"]
titles: ["Life Reminiscences of an Old Lutheran Minister"]
origpublishers: ["Lutheran Publication Society"]
origdates: ["1896"]
synods: ["General Synod", "Maryland Synod"]

---

"…Even after the Seminary was established at Gettysburg, systematic and sustained, but covert, attack upon the Symbolical Books was made. The result was that the books were not regarded with favor by many of the ministers and students, and very many did not accept the doctrine of the sacraments as taught in the Lutheran Church. 

"This continued to be the state of affairs for many years. There were some that were true Lutherans despite these adverse circumstances. Strange to say, not a few underwent a sort of reacting process, and absolutely were converted to the true church doctrine by the very agency diligently employed to deter them from it. These men did not venture to be demonstrative, but so soon as they became free from the painful shackles by which they were fettered, they professed the true doctrine. 

"A large number, however, were Zwinglians (not even Calvinists) on the sacraments. That is, they were not Lutherans, and were satisfied with opposing the doctrines of the Church without bothering themselves about any school of theology. 

# About Dr. Morris

"John Gottlieb Morris attended Princeton College and Dickinson College. He… attended Princeton Theological Seminary and was a member of the first class in Gettysburg Theological Seminary. He founded the _Lutheran Observer_ and was president of the Maryland Synod and the General Synod. He and his nephew founded the Lutheran Historical Society. Morris was a frequent lecturer before the Smithsonian Institution and author of the Catalogue of the Described Lepidoptera of North America (1860), among other scientific and religious publications. – William and Mary Special Collections Database.[^aAR]

# Contents

(281 pages)

- Preface
- Chapter 1. Youth
- Chapter 2. Student Life At Princeton And Dickinson Colleges. 
- Chapter 3. Student Life.
- Chapter 4. Licensed To Preach – Gettysburg Seminary.
- Chapter 5. Call To Baltimore And Pastoral Life; 1827 To 1860.
  - Brief History Of The First English Lutheran Church [In Baltimore]
  - Anti-popery
  - Difficulties
- Chapter 6. Early History Of The Lutheran Observer.
- Chapter 7. Scientific Studies And Offices.
- Chapter 8. Resignation As Pastor; Librarian Of The Peabody Institute.
- Chapter 9. Summer Residence At Lutherville. - Lectures And Readings.
- Chapter 10. Church Correspondence.
- Chapter 11. The Diets.
  - Academy Of Lutheran Church History In The United States.
  - What Were My Reasons?
  - Lutheran Ministers’ Mutual Insurance League
  - Preaching In Other Pulpits
  - Good Advice From Members
  - Evangelical Alliance
  - Fliedner, Of Kaiserswerth
  - Consubstantiation
- Chapter 12. Church Miscellany.
  - Style Of Preaching
  - Argument For Study
  - State Of Theology
  - Progress
  - D. D.’s In Our Church
  - Catechization, Pastoral Visiting, And Other Functions
  - Luther Memorial Meetings In 1883
  - Luther Statue
  - The Luther Statuette
  - Election Of Professors And Presidents
- Chapter 13. General Miscellany. 
  - Excursions
  - Private Libraries
  - The Rebellion [American Civil War]
  - Giving Offence Unintentionally
  - Köstlin’s Life Of Luther
  - Bad Treatment
  - House Robbed
  - Curious Wedding Event
  - Kossuth In Baltimore
  - Lists Of Lutheran Publications
  - Visits From Foreigners 
- Chapter 14. Offices Held – Published Writings And Manuscripts – Papers Read Before Historical Societies In Maryland – Learned Societies. 
- Chapter 15. Last Days.
  - In Memoriam. Rev. John G. Morris, D. D. , LL.D.


# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download) 

![book cover](/img/posts/158lb-morris-old-lutheran-minister-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/158lb-morris-old-lutheran-minister.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 158lb&body=Please send 158lb-morris-old-lutheran-minister.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 158lb&body=Please send 158lb-morris-old-lutheran-minister.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}

[^aAR]: "Morris, John G." Retrieved 2017-12-01 from scdb.swem.wm.edu/?p=creators/creator&id=1286


# Publication Information

- _Lutheran Library edition first published_: 2018-06-28
- _Version 4 update_: 2019-07-15 
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
