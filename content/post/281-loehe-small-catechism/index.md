---
date: 2018-12-13 12:00:00
title: "Questions and Answers to the Six Parts of the Small Catechism of Dr. Martin Luther by William Loehe"
slug: "281-loehe-small-catechism"
categories: ["Lutheran Library Publications"]
tags: ["Catechism", "Loehe", "Luther"]
authors: ["Loehe, William"]
titles: ["Questions and Answers to the Six Parts of the Small Catechism of Dr. Martin Luther"]
origpublishers: ["W. J. Duffie"]
origdates: ["1893"]
synods: ["Synod of the South"]

--- 

"The excellence of this explanation is, that it attempts no more than to analyze and explain Luther's Catechism itself. It does not try to find in it the whole scheme of doctrine. It is intended, like Luther's Catechism, to show a house-father how to teach his household." 

"The father, the children, the household, should use, pray, learn, prize it; and so it will become the cruse of the woman of Sarepta, in which the oil never fails." 


This is part of the [Lutheran Library Catechism Series](/tags/catechism/).  
Be sure also to download [Luther's Small Catechism](/302-jacobs-luther-small-catechism/)

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)

![book cover](/img/posts/281-loehe-small-catechism-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/281-loehe-small-catechism.pdf)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 281&body=Please send 281-loehe-small-catechism.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}
[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 281&body=Please send 281-loehe-small-catechism.epub)

"Luther's _Smaller Catechism_ is the best text book for religious instruction that has yet been offered to the church. For nearly 400 years it has held this place in our church and millions have drank this "milk of the Gospel" as the very best food for spiritual babes. 

