---
date: 2018-01-18 12:00:00
title: "My experiences in the mission field of South Dakota during the years 1892-1897 by Frank Albert Kiess, (1867-1942)"
slug: "196ms-kiess-mission-field-of-south-dakota"
categories: ["Lutheran Library Publications"]
tags: ["Missions", "Kiess"]
authors: ["Kiess, Frank Albert"]
titles: ["My Experiences in the Mission Field of South Dakota During The Years 1892-1897"]
origpublishers: ["The Lutheran Press"]
origdates: ["1932"]
synods: ["Missouri Synod"]

excerpt: "A young couple, with their first baby, were visiting the young mother's parents who lived a few miles from their home. On their way home they were over taken by a blizzard. After the young man had turned his team loose he began to pile heaps of snow around the box sled, covered his wife up well, she holding the little baby girl close to her bosom. The young man even took off his fur coat and spread it over wife and child and then to keep the wind from blowing the covers off laid on top of it..."
---

 
{{% toc %}}

# The Cyclone Church

"Cyclones are very common in Dakota. I witnessed a cyclone when I was about 15 miles from shelter of any kind – out in the open prairie. When the first hail began to come down I hurriedly unhitched the ponies and let them go wherever their instinct directed them. I took down the top of the buggy and piled stones in the buggy to weight it down, then I wrapped myself from head to foot in a blanket. When the hail struck me it felt as though someone were throwing stones at me. I dared not uncover. Soon a soaking rain drenched me and the buggy under which I crawled was lifted up and taken from my sight. I finally found it with all the stones in it over a mile from where it had been. The top was gone, the axles bent almost beyond repair. While I was trying to repair the buggy another hailstorm came up but no cyclone accompanied it, but the stones were large enough to kill a man. I saved myself by running to a nearby straw-stack and crawling into it. After that storm had passed and a little rain, all was over. But where were my ponies? Were they dead or alive; did they make for home or dig themselves into a straw-stack? The only thing for me to do was to walk in the direction the cyclone had gone, so walked miles – how many I never knew – but no trace of the ponies and no one of whom I could inquire. By this time it began to get dark.

"Now why did I have to go through this storm and all the excitement? Through that storm in which I almost lost my life, I became acquainted with those people. I asked them if they would invite all the farmers and their families in the surrounding section of from 10 to 15 miles to come together on a certain day and I would conduct a service. They consented gladly. For a year I held services in a schoolhouse which was centrally located and believe it or not, the people came every two weeks for services and some came a distance of 20 miles. In the course of time these people built a neat little church which was dedicated. The congregation multiplied and now a parsonage is built beside the church and a resident Lutheran pastor is living in their midst. I met the pastor of this congregation, and he told me the people still say, "God through a cyclone started our congregation".

# The Blizzard Baby

"A young couple, with their first baby, were visiting the young mother's parents who lived a few miles from their home. On their way home they were over taken by a blizzard. On the ground was about a foot of snow. They were riding in a sled. The Dakota sleds are mostly hand made. A dry-goods box of large size put on runners made of 12 x 2 in. planks, a wagon tongue and that completes the sled. A few armfuls of straw is thrown into the box, plenty of blankets of genuine wool (mine cost $15.00), fur coats for women as well as men and zero weather can do no harm but a blizzard will, if you are caught in it. When this young couple could not find their home, the young man saved the horses by unhitching them and turning them loose. They will travel for miles until they find a straw-stack to dig themselves into. They will not do so while hitched to a sled or any other vehicle, only when left to themselves. After the young man had turned his team loose he began to pile heaps of snow around the box sled, covered his wife up well, she holding the little baby girl close to her bosom. The young man even took off his fur coat and spread it over wife and child and then to keep the wind from blowing the covers off laid on top of it. How did we find them after a day and a night? The father and mother dead, the little infant still alive, but getting cold. The grandparents took the little one and raised it. When I attended a Walther League convention held in Detroit, a young lady came up to me and asked me if I had done mission work in South Dakota. I said "Yes, I did, many years ago". She said "Do you remember of baptizing a baby called the blizzard baby whose parents died in a blizzard and the baby was found alive?" "Yes, I do," I replied. She said "I am the blizzard baby" and tears came to her eyes. What a pleasant meeting we had there in Detroit. 

# Contents

(108 pages)

- Preface
  - Mission Trips Made.
  - Congregations and Preaching Places.
- Chapter 1. Childhood Days, College and Seminary.
- Chapter 2. My First Call And Congregation.
  - Ordination and Installation.
  - The Western Trip.
  - My First Year Of Mission Service.
  - My Own First Experience of a Dakota Blizzard.
  - How God Prevented Me From Meeting With a Sad Accident.
  - Two Trips Never To Be Forgotten.
  - My Second Trip, Never To Be Forgotten, To North Dakota.
  - First Christmas Celebration In My Mission Field.
  - The Most Pleasant Of All My Trips and The Most Fatal.
  - Serenaded By Choir and Did Not Notice It. 
- Chapter 3. Second Year Of My Mission Work In South Dakota. 
  - Some Happenings Of Sad and Joyous Nature During The Summer, 1893.
  - In Danger of My Life Among The Indians
  - An Expensive Company, November 1893.
- Chapter 4. Third Year In My Mission Work In South Dakota.
  - Real Welcome And Lasting Company Arrived In The Lonesome Prairie.
- Chapter 5. Fourth Year Of My Mission Work In South Dakota.
  - Churches Built and Dedicated.
  - Snowbound – A Snowbound Train.
  - The First Mission Festival in South Dakota.
  - Going Home to Mother After Being Away so Far From Her for Three Years. 
  - Marks the Fifth Year.
- Chapter 6. My Experiences in the Mission work in South Dakota.
  - Pleasant Experiences and Unpleasant.
  - A Majestic Winter.
  - Snow Melts, Water, Floods, Everywhere Water, Creeks Overflowing, Prairie Covered With Water.
  - Trips Made In Winter and Through Deep Water.
- A Few Short Remarks According To My Memorandum, 1896-1897.
- Distances Traveled by Ponies and Railroads.

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download) 

![book cover](/img/posts/196ms-kiess-mission-field-of-south-dakota-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/196ms-kiess-mission-field-of-south-dakota.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 196ms&body=Please send 196ms-kiess-mission-field-of-south-dakota.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 196ms&body=Please send 196ms-kiess-mission-field-of-south-dakota.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}

