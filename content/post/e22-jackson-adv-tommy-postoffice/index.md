---
date: 2019-04-24 09:28:01-04:00
publishDate: 2019-04-24 09:28:01-04:00
title: "The Adventures Of Tommy Post Office: The True Story Of A Cat by Gabrielle Emilie Jackson"
slug: "e22-jackson-adv-tommy-postoffice"
categories: ["Lutheran Library Publications"]
tags: [ "Cats", "Animals", "Children", "Fiction"]
authors: ["Jackson, Gabrielle Emilie"]
titles: ["The Adventures of Tommy Post Office"]
origpublishers: ["E. P. Dutton and Company"]
origdates: ["1910"]
synods: ["N/A"]

---

"By this time Tommy was six months old, and as full of pranks as a six-months-old kitten well could be. His education had begun the very moment he entered the Hartford postoffice, so it is not surprising that by the time he had spent half a year there he was really remarkable.


# Chapters

- The Adventures Of Tommy Postoffice – The True Story Of A Cat
- 1. Tommy’s First Appearance
- 2. Tommy Becomes A Pensioner
- 3. Tommy As Commissariat
- 4. Tommy In The Guise Of Cupid
- 5. Tommy Pays Off Old Scores
- 6. Tommy Enters The Cat Show
- 7. Tommy’s Escape
- 8. Tommy’s Ordeal By Fire
- 9. The Postoffice Becomes A Hospital
- 10. Tommy’s And Barbara’s Farewell Performance
- 11. Tommy Postoffice And “Owney” Say Farewell

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download) 

![book cover](/img/posts/e22-jackson-adv-tommy-postoffice-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/e22-jackson-adv-tommy-postoffice.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request e22&body=Please send e22-jackson-adv-tommy-postoffice.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request e22&body=Please send e22-jackson-adv-tommy-postoffice.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2017-08-06 
- _Version 4 update_: 2019-04-24
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
