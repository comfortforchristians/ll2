---
date: 2018-11-20 12:00:00
title: "Martin Luther's Small Catechism translated by Henry Eyster Jacobs"
slug: "302-jacobs-luther-small-catechism"
categories: ["Lutheran Library Publications"]
tags: ["Catechism", "Confessions", "Book of Concord", "Jacobs", "Luther"]
authors: ["Jacobs, Henry Eyster"]
titles: ["Martin Luther's Small Catechism"]
origpublishers: ["United Lutheran Publication House"]
origdates: ["1911"]
synods: ["General Council"]

--- 
"The Law, therefore, can make no Christian, for the wrath and displeasure of God abide upon us forever, as long as we cannot keep it and do what God demands of us; but the faith of the Creed brings pure grace, and makes us godly and acceptable to God. 

"For by the knowledge of this we love and delight in all the commandments of God; because we see that God, with all that he has, gives himself to us – the Father, with all creatures; the Son, with his entire work; and the Holy Ghost, with all his gifts – to assist and enable us to keep the Ten Commandments. – Martin Luther.
---

Be sure also to download [Questions and Answers on the Small Catechism](/282-schuh-small-catechism/)

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)

![book cover](/img/posts/302-jacobs-luther-small-catechism-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/302-jacobs-luther-small-catechism.pdf)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 302&body=Please send 302-jacobs-luther-small-catechism.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}
[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 302&body=Please send 302-jacobs-luther-small-catechism.epub)

