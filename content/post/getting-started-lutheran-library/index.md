---
date: 2017-09-01 12:00:00
layout: page
title: Frequently Asked Questions (FAQ)
aliases:
  - "/getting-started-lutheran-library/"
slug: "faq"

---
{{% alert note %}}
For any questions not answered here, send an email to __editor__ at __lutheranlibrary.org__ (replace the 'at' with '@').
{{% /alert %}}

{{% toc %}}

# Is there a listing by Author? 

[Lutheran Library Catalog by Author](/authors/)

# By Synod or Denomination?

[Lutheran Library Catalog by Synod](/synods/)


# How do I know which type of eBook to download?

All of the books published by the Lutheran Library are available in three formats:  

__PDF__ is the most widely known.  It is best for printing, reading on a computer, or sharing with other people.

If you have a Kindle, __MOBI__ is the best format for you.

If you have an iPad or other Apple device, __EPUB__ is your format.


# You have so many books. Where should I start?

- [Must Reads](/tags/must-reads)

Or start with one of these American Lutheran orthodox heroes:

- [Matthias Loy](/authors/loy-matthias/)
- [Charles Krauth](/authors/krauth-charles-porterfield/)
- [Theodore Schmauk](/authors/schmauk-theodore-emanuel/)
- [Henry Eyster Jacobs](/authors/jacobs-henry-eyster/)

A wonderful Twentieth Century preacher:

- [Simon Peter Long](/authors/long-simon-peter/)

And for a most readable reference about any denomination:

- [Churches And Sects Of Christendom by J. L. Neve](/176tc-neve-churches-sects-christendom/)

A complete author list:  [Lutheran Library Authors](/authors/).

# How much do the books cost?

The Lutheran Library is a Christian ministry.  All books are available to you at no charge.  

# Copyright questions

Lutheran Library titles are published under a Creative Commons Attribution 4.0 International [(CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/) Copyright. Please respect the labor that has gone into the restoration of these works.

You are free to:

- _Share_ — copy and redistribute the material in any medium or format
- _Adapt_ — remix, transform, and build upon the material for any purpose.

Under the following terms:

- _Attribution_ — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.

- _No additional restrictions_ — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.

# Can we translate your books?

Seminaries, Christian Schools, anyone is permitted to translate these items without charges, fees, or royalties.

# Where do you get the artwork for the covers?

The book covers and website images are two dimensional reproductions of artworks made before 1922 and are considered to be in the public domain.[^Be]


[^Be]: These photographs of artworks are in the public domain in the United States because they were published (or registered with the U.S. Copyright Office) before January 1, 1923.  See also https://www.avvo.com/legal-answers/copyright-laws-on-very-old-art-works-39011.html

# Something's not working.

Some browsers and devices don't like links that launch email messages.  If you cannot download an ebook, send a request to:

__editor__ at __lutheranlibrary.org__ (replace the 'at' with '@').

# How can I support the work of the Lutheran Library?

Telling others of the ministry by mentioning the books at church, through reviews, or via email or social media is needed and always appreciated.

# Are you accepting Manuscript Submissions?

Not at the present time.  If you are looking to publish a book, we recommend you consider Kindle Direct Publishing.  They can also recommend people who will prepare the manuscript for submission for a fee.  