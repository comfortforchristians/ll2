---
date: 2018-10-29 12:00:00
title: "The New Theology: S. S. Schmucker And Its Other Defenders by James Allen Brown"
slug: "168tc-brown-theology-s-s-schmucker"
categories: ["Lutheran Library Publications"]
tags: ["Apostasy", "Schmucker", "Brown"]
authors: ["Brown, James Allen"]
titles: ["The New Theology: S. S. Schmucker And Its Other Defenders"]
origpublishers: ["Henry B. Ashmead"]
origdates: ["1857"]
synods: ["Maryland Synod", "General Synod"]

---
"Suffice it to say that faith is faith, and not obedience, or love or delight in God, or any other distinct grace or virtue. Nor does the Bible say being justified by obedience, or love, or delight, or good works, but 'being justified by faith, we have peace with God.'"

{{% toc %}}

# Justification by Faith versus Schmucker's "New Theology"

"The substance of this fundamental doctrine may be considered as embraced in these two points – First, that the _ground_ of the sinner's justification before God is not any righteousness or merit of his own, but the merit and righteousness of Jesus Christ; and, secondly, that the _condition_ of receiving this is not any virtue or morality on the part of the sinner, but faith alone, to the exclusion of everything else as a necessary part in the work of justification. This is the view that we understand to be taught in the word of God, and in the confessions of evangelical churches. It is hardly to be supposed that any one would be found in the Lutheran church, directly, and in so many words, opposing the doctrine. The question with us is, not whether this be so, but whether views have not been taught and extensively promulgated, that are in direct conflict with any correct understanding of the doctrine, and which, if suffered to prevail, will undermine the very foundations of our faith? The very thought of such a thing should excite our vigilance, and lead us to look to the priceless legacy handed down by Apostles and Reformers." – Chapter 1 

# About the Author, Rev. James Allen Brown

"The theological attainments of Dr. Brown were extensive, and his general scholarship universally acknowledged. His knowledge was accurate; he knew things thoroughly; his thoughts were clear as the atmosphere, and his temperament cool and calm as a morning breeze. No opponent could throw him off his guard, and he was a dangerous man to encounter in debate, unless your cause was manifestly right. He was not born within our [Lutheran] fold, but from conviction entered it after he had attained to manhood, and heartily espousing our cause, he maintained it vigorously to the end. 

Dr. Brown possessed a moral courage that nothing could daunt. If the whole history of his experience in South Carolina, at the breaking out of the Rebel [American Civil] war, and of his firmness in maintaining his principles, were told, it would excite the admiration of friend and foe. His courage in opposing the theological teaching of the man who had been his own professor in the Seminary eighteen years before, in a strong pamphlet, and showing his un-Lutheranism, deserves the highest praise. Many more characteristic incidents might be given. 

– [John G. Morris](/john-gottlieb-morris/). [Life Reminiscences of an Old Lutheran Minister](http://www.lutheranlibrary.org/158lb-morris-old-lutheran-minister/) 

# Contents

(80 pages)

- About the Author, Rev James Allen Brown (1821-1882)
- Introduction
- The New Theology - Article extracted from the July 1857 Evangelical Review
- Regeneration
- Justification
  - 1 The Word of God.
  - 2 The Confessional Writings of the Church.
- Reply To Dr. Schmucker’s Article.
  - I. Dr. Schmucker’s Reasons for Not Discussing Our Article
    - 1 “The spirit of Rev. B’s article is generally thought not to be such as became  him, under the circumstances of the case.”
    - 2 “Because the entire article of Rev. B. is confused and unsystematic, showing   that he has studied Belles Lettres more successfully than Logic or Hermeneutics”
    - 3 “his manifest want of acquaintance with Lutheran Theology”
    - 4 “glaring misapprehensions and consequent misrepresentations”
  - II. Dr. Schmucker’s Defense of His Own Views
    - 1 Natural Depravity, or Original Sin.
    - 2 Regeneration.
  - Summary
    - 1 Dr. Schmucker alleges that “regeneration merely restrains the natural  depravity, or innate sinful dispositions.
    - 2 Dr. Schmucker alleges that regeneration “must consist mainly in a change of  that increased predisposition to sin arising from action, of that preponderance of   sinful habits formed by voluntary indulgence of our natural depravity, after we   have reached years of moral agency.”
    - 3 – Justification.

# Faith is not obedience

"Suffice it to say that faith is faith, and not obedience, or love or delight in God, or any other distinct grace or virtue. Nor does the Bible say being justified by obedience, or love, or delight, or good works, but 'being justified by faith, we have peace with God.'

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download) 

![book cover](/img/posts/168tc-brown-theology-s-s-schmucker-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/168tc-brown-theology-s-s-schmucker.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 168tc&body=Please send 168tc-brown-theology-s-s-schmucker.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 168tc&body=Please send 168tc-brown-theology-s-s-schmucker.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}
