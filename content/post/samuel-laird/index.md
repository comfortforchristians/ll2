---
date: 2019-02-09 02:00:00
publishDate: 2019-02-09 02:00:00-05:00
title: "Samuel Laird: A Biographical Sketch"
slug: "samuel-laird"
categories: ["Biographical Sketches"]
tags: ["Laird", "Lutheran Ministers"]
authors: ["Laird, Samuel"]
synods: ["General Council", "Pittsburgh Synod"]
---

[![Samuel Laird](/img/posts/laird-samuel.jpg)](https://www.lutheranlibrary.org/authors/laird-samuel/)

# Rev. Samuel Laird, D.D. 

The Rev. Samuel Laird, D. D., was born on the 7th of February, 1835, in New Castle Co., Del. When about six years of age his parents removed to Philadelphia, where he continued to live until after entering the ministry. He was baptized and subsequently confirmed by the Rev. Philip F. Mayer, D. D., pastor of St. John's Evangelical Lutheran church, of Philadelphia. He was educated in the public schools of the city, graduating from the high school in 1852, having completed the full classical course of study. In the fall of the same year he entered the Sophomore class of the University of Pennsylvania, and graduated from the department of Arts in 1855. 

He engaged in teaching and was employed in an academy in Philadelphia, giving instruction in Latin, mathematics and English literature. It was his intention to prepare himself for the bar, and for this purpose after a year's time commenced the study of law in the office of Benjamin Gerhard, Esq., but abandoned it for the ministry. His theological training was under the direction of Rev. Dr. J. A. Seiss, at that time his pastor, and the Rev. Dr. W. J. Mann, with the advice of the Rev. Dr. Charles Porterfield Krauth. He was received into the ministry Oct. 14, 1861, and accepted a call from St. Luke's English Evangelical Lutheran church of Philadelphia, and entered on the duties of his office there Dec. 1, 1861. 

On Sept. 1, 1864, he removed to Lancaster, Pa., where he took charge of Holy Trinity church. In 1867 he became pastor of the First English Evangelical Lutheran church of Pittsburg, Pa., where he remained for over twelve years, when he accepted a call from St. Mark's church, Philadelphia, in 1879, of which church he still has charge (1890). 

In addition to the duties of the pastorate he has filled various positions in the church, having been President of the Pittsburg Synod three years, Secretary of the General Council nine years, Treasurer of the Ministerium of Pennsylvania four years, and has also served on a number of important committees. His ministerial life has been in the line of practical rather than literary activity; he has been engaged in editorial labor and furnished a number of sermons for the public press.

He entered the ministry when the English portion of the Church especially, was engaged in a series of doctrinal discussions which led to a reaction from indifference and latitudinarianism to a better appreciation of the truth as held and taught by the Evangelical Lutheran church. From the very first he occupied a conservative position, and his influence has been exerted in favor of a strict adherence to the Confessions, and of a church life in conformity therewith. He took part in the formation of the General Council and has always upheld and promoted its interests. 

{{% alert note %}}
This biography is taken from Jens Jensson's _American Lutheran Biographies_, published 1890.
{{% /alert %}}

First published online 2019 at [LutheranLibrary.org](.).