---

date: 2019-02-07 02:00:00
publishDate: 2019-02-07 02:00:00-05:00
title: "Emanuel Greenwald: A Biographical Sketch"
slug: "emanuel-greenwald"
categories: ["Biographical Sketches"]
tags: ["Greenwald", "Lutheran Ministers"]
authors: ["Greenwald, Emanuel"]
synods: ["Ohio Synod", "Pennsylvania Synod"]
---

[![Emanuel Greenwald](/img/posts/greenwald-emanuel.jpg)](https://www.lutheranlibrary.org/authors/greenwald-emanuel/)


# Rev. Emanuel Greenwald, D. D. 

Emanuel Greenwald, D. D. was "a good man and full of the Holy Ghost and of faith." He was a good boy, this carpenter's son, born January 13, 1811, who grew to man's estate in quiet Frederick, Md. His father's habit of reading Arndt's _True Christianity_ and Jay's _Morning and Evening Exercises_, and his mother's serious conversations with him about God and Christ and his soul's salvation made a lasting impression on Emanuel. At two years of age he was consecrated to the ministry. At eighteen he was catechized and confirmed in German. Previous to this he helped his father at farming and carpentering, and attended school a few winters, but now he became a private theological student of his pastor, the renowned Dr. David F. Schaeffer. During his five years' tuition this country boy walked 14,000 miles in getting his education. It was of the most solid, orthodox stamp; the body of divinity which young Greenwald became possessed of was no mere coat of mail with vitals of iron, but a living, breathing body with a substantial backbone and glowing heart. From his preceptor's study the candidate went into the country churches round about to preach repentance and faith. 

In 1831, equipped with the license of the Maryland Synod, and mounted upon "Old Pete," his whole wardrobe and library in the saddlebags, brave young Emanuel rode westward to distant Ohio. He settled at New Philadelphia, Tuscarawas Co., fifty-three miles northwest of Steubenville. Here he spent twenty years, doing the work of an evangelist in several counties. He built up ten congregations. Though in the saddle most of the time, he prosecuted his studies. He wrote an unpublished volume of "_Evidences of Christianity_." Though ordained by the German Joint Synod of Ohio, in 1836, his ministry was almost exclusively in English. This is shown by his participation in 1836, in the formation of the English Synod of Ohio. He was for some time the Synodical secretary, and Oct. 24, 1842, issued the first number of the Synodical weekly, the _Lutheran Standard_. 

From being president of the Board of Trustees of the new Capitol University, Columbus, Rev. Greenwald became, in 1851, pastor of what might be called the college church, the first English Lutheran church of Columbus. He was in the prime of life, of robust build, and in excellent health. He was as successful in his town parish as in his country work. Through the _Standard_ he fought the "New Measures" of the _Observer_, and for so mild a man, his ardor as a controversialist was astonishing. Yet, warrior as he was to the end, battling during his last days against vice, Atheism and Romanism, he never forfeited the respect of good men by coarseness of language or unseemly ebullitions of temper. Like John, he was a "son of thunder," and at the same time a "beloved disciple." Yet he shrank from controversy within the church, especially in the second part of his life, from 1854, when he went east to become pastor of Christ Church, Easton, to 1885, when, Dec. 21, he died as pastor of Trinity, Lancaster. 

His Easton work brought him before the church as the model pastor, conscientious, methodical, untiring, affable to all, ever ready to bind up the broken hearted, to give light to them that sit in darkness and in the shadow of death, and to guide the trembling inquirer into the way of peace. His sermons, which he now always wrote and delivered with a rapid but distinct enunciation, were solid, edifying discourses; his catechization thorough, well supported by scriptural citations; his missionary work extensive. He nurtured the young life of the parish; he took an active part in the Sunday school; he organized a congregation of colored people; he preached in a country village nearby; he collected large sums for defraying the parish debts. As president of the East Pennsylvania synod, he stood at the head of his brethren and wore gracefully the D. D. conferred by Pennsylvania College in 1859. 

Rendered uncomfortable by complications growing out of the celebrated Ft. Wayne rupture, which gave rise to the establishment of a more strictly confessional body, the General Council, Dr. Greenwald in 1867 accepted a call which made him pastor of perhaps the largest English Lutheran parish in Pennsylvania. On the solid foundations laid by his predecessors since 1729, Dr. Greenwald built up a congregation zealous in city mission work. Two swarms were sent out of the old hive — Grace Church, Rev. C. E. Haupt, pastor, and Christ Church, Rev. E. L. Reed, pastor. Besides this Trinity people renovated their large church, and erected the most complete parish and Sunday-school building in the Synod. The Doctor's work so grew on him, and his infirmities increased, that he called to his help in the parent church Rev. C. L. Fry, who succeeded him. 

Visiting from house to house, seeking the lost sheep and comforting the faithful, preaching first in his own missions and then to a neglected German Church out in the country, riding about on the Monday following communion days to administer the sacrament to the many aged and sick, the Doctor won so firm a hold on all classes in Lancaster that the city made great lamentation when his ten years' struggle with agonizing disease ended in death. 

While at Lancaster he guided the deliberations of the Pennsylvania Synod, seeming less a president than a bishop, as, attired in his flowing black robes, he preached the Word and laid holy hands on generations of young candidates. 

Here too he blossomed out into authorship, and enriched the literature of the English Lutheran Church of the world with a series of practical, doctrinal, devotional, and controversial works which constitute his abiding monument. The Doctor was an easy and copious writer. His style is lucid, terse Anglo Saxon. His writings are bathed in love. Rising from a perusal of them the reader unhesitatingly pronounces the author a "good" man. Whatsoever things are spiritually lovely are illustrated and commended in Dr. Greenwald's works. His list forms part of the heart history of the Church: The Lutheran Reformation, 1868; An Order of Family Prayer, subsequently reissued as Jesus our Table Guest. Questions on the Gospel for the Church Year, two volumes, 1868, many editions. Meditations for Passion Week, 1873. The Young Christian's Manual of Devotion, 1874. Questions on the Epistles for the Church Year. The True Church, its Way of Justification, and its Holy Communion, 1875. Sprinkling the True mode of Baptism. True and False Spirituality in the Lutheran Church, a paper at the Diet of 1877. Dangers of Atheism, a sermon, 1883. The Child's Book, for infant school instruction, 1884. Sacred Places. The Devout Christian Series, sermons, 1884. Meditations for the Closet, 1884. Articles for the _Lutheran Church Review_. 

With his beloved spouse, Lavinia (Williams, of New Philadelphia), he was privileged to celebrate in 1881 the semi-centennial of his ministry, and in 1884 his golden wedding. 

The well spent life is graphically told by pen and pencil by Rev. C. E. Haupt in "The Life of Emanuel Greenwald," 1889. – W. K. Frick.

(The above section from Jensson, Jens Christian. _American Lutheran Biographies_. Milwaukee, WI: 1890.)

[Books by Emanuel Greenwald Available from Lutheran Library](http://www.lutheranlibrary.org/tag/greenwald-emanuel-1811-1885/)


{{% alert note %}}
This biography is taken from Jens Jensson's _American Lutheran Biographies_, published 1890.
{{% /alert %}}

First published online 2019 at [LutheranLibrary.org](.).