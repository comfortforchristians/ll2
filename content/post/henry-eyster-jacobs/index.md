---
date: 2019-01-29 09:28:57
publishDate: 2019-01-29 09:28:57-05:00
title: "Henry Eyster Jacobs: A Biographical Sketch"
slug: "henry-eyster-jacobs"
categories: ["Biographical Sketches"]
tags: ["Jacobs", "Lutheran Ministers"]
authors: ["Jacobs, Henry Eyster"]
synods: ["General Council"]
---

[![Henry Eyster Jacobs](/img/posts/jacobs-henry-eyster.jpg)](https://www.lutheranlibrary.org/authors/jacobs-henry-eyster/)

# Rev. Henry E. Jacobs, D.D. 

Rev. Henry Eyster Jacobs, D. D., was born at Gettysburg, Pa., on November 10, 1844, of Lutheran parentage, his father being at the time Professor of Natural Sciences and Mathematics at Pennsylvania College. 

At a very early age he evinced that systematic and zealous ability which has always since been one of his leading characteristics, and which has placed him, though not yet past middle-age, among the foremost ranks of the Lutheran theologians of America. 

He graduated in 1862 from Pennsylvania College and in the ensuing scholastic year began his studies in the theological seminary there. In the following two years of his seminary course the country was undergoing the frightful throes of civil war, and much of his time during vacation was spent in administering to the wants of wounded soldiers of the Union. He finished the theological course in 1864 and was at once engaged as tutor in the college. This position he retained until 1867, when the pressing needs in the Home Mission field called him to Pittsburg, Pa. 

After several years of labor in this important work he was made principal of Thiel Hall, then at Phillipsburg, Pa., at the same time undertaking the office of pastor to the church there. In 1870 he was elected to the chair of Latin and History in Pennsylvania College, in connection with which institution he remained until 1883, when he was called to fill the chair of Systematic Theology in the Evangelical Lutheran Theological Seminary at Philadelphia, made vacant by the death of that eminent scholar and theologian, Rev. Charles Porterfield Krauth, D.D., LL.D. 

During all these many changes of work and location it is to be remembered that Dr. Jacobs diligently kept up his studies of the Lutheran Confessions under circumstances and hindrances which would have discouraged any but the most thorough and determined student. 

The death of Dr. Krauth caused an opening in the ranks of the Lutheran Church in America, which has never since been closed. His loss will always be felt by the Church at large, but the most pressing want was that which was created by his removal from the faculty of the Seminary. In the Providence of God, however, a man was being raised up to fill such a want. More than twenty years of unremitting research had ripened a scholar, and when the time came, the Church did not have to suffer for lack of the necessary instrument to carry on her work, but at once Dr. Jacobs was called forth to perform the service for which his rare attainments so well fitted him. 

In another sphere of work also. Dr. Jacobs promises to make his scholarship felt to the advantage of the Lutheran Church. In the death of Dr. Schmucker, theological science lost one of its most distinguished specialists, and the Lutheran Church a devoted son, whose noblest monument will be her forms of worship and of pastoral ministration. The appointment of Dr. Jacobs to the General Council's Liturgical Committee, with Drs. Seiss and Spaeth, enlisted his energy and scholarship in this work. 

In the literary field Dr. Jacobs' labors have been eminently those of the dogmatician, and he deserves the unbounded gratitude of the English-speaking Church for his successful work in the translations of the Lutheran Confessions into English from their German and Latin originals. His historical introductions and explanatory notes to these translations have secured for him a wide celebrity. 

He has, since 1883, been editor-in-chief of the _Lutheran Church Review_, through the pages of which periodical there have emanated articles from his pen such as his reviews of works of Drs. Schaff and Shedd, which have earned for the theological views represented by him broader and more intelligent recognition among the leading minds of other denominations in this country. 

He is also a regular and frequent contributor to the Church papers, and in this field his productions have treated of questions which are constantly arising in connection with the growth and development of the Church, and in which it is most necessary to direct and educate the popular mind. 

Dr. Jacobs has always belonged to the conservative wing of the Lutheran Church, and has steadily and  ably opposed all innovations and changes which are the result of the Church's coming in contact with surrounding denominations and sects whose foundations are not built of the same rugged and enduring material of truth as those of our own beloved Lutheran Church. 

His experience as a teacher has been an almost uninterrupted one, extending over a period of a quarter of a century, and he possesses, preeminently, the invaluable requisite of being able to impart the same clear, decided knowledge which he himself possesses, and a magnetic personality which secures interest in, and attention to, his instructions. 

His deciding to move to Mt. Airy, followed naturally after his election as House Father or rector, and the classes of future years will inevitably derive much benefit from coming into close personal contact with one who so well understands a student's strength, a student's weaknesses, and a student's wants. — _Indicator_. 

There are in Dr. Jacobs special qualifications for the important post he holds at the seminary. Like Dr. Krauth, a child of the covenant, he grew up like him in the sanctity of a Christian home and the atmosphere of thorough scholarship. The growth of faith and learning went hand in hand, and before men were aware, the modest student had developed into a Christian manhood and a scholarship of unusual prominence. First a tutor in the college at Gettysburg, then principal of Thiel Hall, then Latin professor in Pennsylvania College, and afterwards Greek professor in the same institution, he passed up, step by step, through the varied branches and studies of these positions, mastering every one thoroughly and making full proof of his ability in all. So, too, his studies during these years made him at home in the German language, out of whose treasures of theology and literature he has already done so much, by translation and otherwise, to increase the sphere of the Church's knowledge. 

The long familiarity with young men, the intimate acquaintance with their weaknesses and their virtues, the helpfulness of his spirit, and the entire absence of every element of cheat and sham, and the felt presence of Christian nobility in his character, all give him special qualifications for the training of our future ministry. But most of all, and best of all, there is in Dr. Jacobs not only the assurance of a personal faith in Christ, but the assurance of the absolute truth of Christ's teachings, as confessed by our Evangelical Church. How he was led to both, need not here be told. It is enough to say that, as in the case of some others, it was not by earthly teachers, but by the Holy One, "who hath the key of David, who openeth and no man shutteth, and shutteth and no man openeth." In bowing before the authority of Christ he literally gave up all, resigning position, and going forth he knew not where, that he might be free to confess the whole truth as it is in Jesus, The strange result is known. He returned to honorable positions, to confidential relationships, to helpful associations, and to important services in confessing, defending and propagating the faith which was dearer to him than life. — _Workman_. 

Some of Dr. Jacobs' publications: 

- "Question of Latinity," 
- "Adoption," 
- "Conservative Reformation," 
- "Luther as a Translator," 
- "Confessional Principle of the Lutheran Church," 
- "Book of Concord," 
- "Address at Greenville," 
- "Augsburg Confession, translated by Taverner," 
- "Schmid's Dogmatik (with Dr. Hay)," 
- "St. Stephan's Tracts," 
- "Modern Calvinism," 
- "Hutter's Compendof Theology (Tr.)," 
- "Doctrine of the Ministry." 

{{% alert note %}}
This biography is taken from Jens Jensson's _American Lutheran Biographies_, published 1890.
{{% /alert %}}

First published online 2019 at [LutheranLibrary.org](.).