---
date: 2019-08-08 05:39:11-04:00
publishDate: 2019-08-08 05:39:11-04:00
title: "Martin Luther, Friend or Foe of the Jews by Armas Holmio on 'Concerning the Jews and the Their Lies'"
slug: "s91-holmio-martin-luther-friend-or-foe-of-the-jews"
categories: ["Lutheran Library Publications"]
tags: ["Holmio", "Short Books", "Anti-Semitism"]
authors: ["Holmio Armas"]
titles: ["Martin Luther Friend or Foe of the Jews"]
origpublishers: ["National Lutheran Council"]
origdates: ["1949"]
synods: ["National Lutheran Council","General Synod", General Council", Ohio Joint Synod", Iowa Synod", Augustana Synod", Norwegian Synod", "The Lutheran Free Church", "Danish Church", "United Synod South" ]
---

"One of the areas in which the Reformer has been repeatedly misrepresented has to do with the relationship of the Church with the Jewish people, frequent attempts being made to link him with modern anti-Semitism. Such attempts call for clarification by the Church of the Reformer's actual position.

"[Luther] reflects his true heart attitude toward the Jews with this prayer: 

>'O God, heavenly Father, turn and let thy wrath over them be brought to an end, for the sake of thy dear Son. Amen.'  

Once Luther refers to the words of St. Paul in Romans 10:1: 

>'Brethren, my heart's desire and prayer to God for Israel is, that they might be saved.'


{{% toc %}}

# "Concerning the Jews and their Lies" 

"The cause of the controversy between Luther and the Jews is Christ. Everything else is more or less incidental matter. Therefore, <em>Concerning the Jews and Their Lies</em> becomes a peculiar apology of Christianity against Judaism. Luther proves with numerous quotations from the Old Testament, that Jesus, born of the Jews, is the promised Messiah of Israel and the Saviour of the world…

"But Luther's Jewish contemporaries did not understand that 'the prophecies of the old prophets were fulfilled in the days of Herod the King, and that the Light, which came from the house of David, will shine not only over Judah and Israel but over the nations of the world as well.' 'As St. Paul had great heaviness and continual sorrow in his heart for his brethren who rejected Christ (Romans 9:2), so have we Christians waited for 1500 years for the conversion of Israel.' – Dr Homio, quoting Luther.

Luther:

>"No matter how greatly we boast of ourselves, we are, nevertheless, pagans and the Jews blood kin of Christ; we are distant and strangers, they are near relatives of our Lord, his cousins and brothers." "They belong to Christ before us."


# Book Contents

- Foreword
- “The Jews and Their Lies by Dr. Martin Luther”
- Background of the Jewish Policy of the Reformation
- The Jewish Policy of Luther Before the Reaction
- The Reaction, and “Concerning the Jews and Their Lies”
- Reasons For Luther’s Changed Attitude
- The “Crusaders” Translation Omits the Positive Parts of Luther’s Book

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/s91-holmio-martin-luther-friend-or-foe-of-the-jews-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/s91-holmio-martin-luther-friend-or-foe-of-the-jews.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request s91-holmio-martin-luther-friend-or-foe-of-the-jews&body=Please send s91-holmio-martin-luther-friend-or-foe-of-the-jews.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request s91-holmio-martin-luther-friend-or-foe-of-the-jews&body=Please send s91-holmio-martin-luther-friend-or-foe-of-the-jews.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2019-08-08
- _Version 4 update_:  2019-08-08
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
