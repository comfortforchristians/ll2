---
date: 2019-04-23 09:36:48-04:00
publishDate: 2019-04-23 09:36:48-04:00
title: "Dixie Kitten by Eva March Tappan" 
slug: "e28-tappan-dixie-kitten"
categories: ["Lutheran Library Publications"]
tags: [ "Cats", "Animals", "Children"]
authors: ["Tappan, Eva March"]
titles: ["Dixie Kitten"]
origpublishers: ["Houghton Mifflin Company"]
origdates: ["1910"]
synods: ["N/A"]

---

"There were piles of hay and straw, there were bags of grain, there were rakes and spades and wheelbarrows, there was a carriage, and there was a sleigh. Dixie climbed up one of the shafts of the sleigh and stretched out her paw to touch a bell. She only wanted to see what it was...

# Chapters

1. The Home Nest
2. Leaving Home
3. Dixie Finds A Friend
4. Dixie and the Cottage
5. Dixie’s Troubles
6. The Little Mothercat
7. Dixie is Deserted
8. A Happy Little Cat
9. The New House
10. Dixie in her Home
11. Dixie in her Home (continued)

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download) 

![book cover](/img/posts/e28-tappan-dixie-kitten-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/e28-tappan-dixie-kitten.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request e28&body=Please send e28-tappan-dixie-kitten.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request e28&body=Please send e28-tappan-dixie-kitten.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2017-12-26 
- _Version 4 update_:  2019-04-23
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
