---
date: 2018-11-13 12:00:00
title: "The First Free Lutheran Diet Edited by Henry Eyster Jacobs"
slug: "300-first-free-lutheran-diet"
categories: ["Lutheran Library Publications"]
tags: ["Jacobs", "Morris", "Krauth", "Brown", "Greenwald", "Valentine", "Conrad", "Stork", "Krotel", "Seiss", "Wedekind", "Mann"]
authors: ["Jacobs, Henry Eyster", "Morris, John Gottlieb", "Krauth, Charles Porterfield", "Brown, James Allen", "Greenwald, Emanuel", "Valentine, Milton", "Conrad, Frederick", "Stork, Charles Augustus", "Krotel, Gottlob Frederick", "Seiss, Joseph Augustus", "Wedekind, Augustus, Charles", "Mann, William Julius" ]
titles: ["The First Free Lutheran Diet"]
origpublishers: ["J. Frederick Smith"]
origdates: ["1878"]
synods: ["General Council", "General Synod"]

--- 

The _First Free Diet_ [formal discussion] _of the Lutheran Church_ was held at St. Matthew's Church in Philadelphia on December 27-28, 1877.  It had representatives from four of the largest Synods at the time.  The speakers included some of the great defenders of orthodox Lutheranism in America: Emanuel Greenwald, Charles Krauth, Henry Eyster Jacobs and others.  
The remarks made by participants are of particular interest.

{{% toc %}}

# Included:

-  The Relations Of The Lutheran Church To The Denominations Around Us by [Charles Porterfield Krauth](/authors/krauth-charles-porterfield/).

-  Misunderstandings And Misrepresentations Of The Lutheran Church by [Joseph Augustus Seiss](/authors/seiss-joseph-augustus/).

-  True And False Spirituality In The Lutheran Church by [Emanuel Greenwald](/authors/greenwald-emanuel/).

-  The Augsburg Confession And The Thirty-nine Articles Of The Anglican Church. by [John Gottlieb Morris](/authors/morris-john-gottlieb/).

-  Theses On The Lutheranism Of The Fathers Of The Church In This Country by [William Julius Mann](/authors/mann-william-julius/).

-  The Four General Bodies Of The Lutheran Church In The United States by [James Allen Brown](/authors/brown-james-allen/).

-  The History And Progress Of The Lutheran Church In The United States by  [Henry Eyster Jacobs](/authors/jacobs-henry-eyster/).

-  Education In The Lutheran Church In The United States by  [Milton Valentine](/authors/valentine-milton/).

-  The Interests Of The Lutheran Church In America As Affected By Diversities Of Language by Diller Luther, M. D., Reading, Pa. 

-  The Characteristics Of The Augsburg Confession by [Frederick W. Conrad](/authors/conrad-frederick/).

-  Liturgical Forms In Worship by  [Charles Augustus Stork](/authors/stork-charles-augustus/).

-  The Divine And Human Factors In The Call To The Ministerial Office, According To The Older Lutheran Authorities by G. Diehl.

-  The Educational And Sacramental Ideas Of The Lutheran Church In Relation To Practical Piety by [Augustus Charles Wedekind](/authors/wedekind-augustus-charles/).

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)

![book cover](/img/posts/300-first-free-lutheran-diet-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/300-first-free-lutheran-diet.pdf)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 300&body=Please send 300-first-free-lutheran-diet.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}
[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 300&body=Please send 300-first-free-lutheran-diet.epub)

