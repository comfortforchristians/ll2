---
date: 2019-01-24 06:50:09
publishDate: 2019-01-26 06:51:10-05:00
title: "'It Is The Will Of God!' Mission Work in Islamic Lands by William Tisdall"
slug: "s06-tisdall-it-is-the-will-of-god"
categories: ["Lutheran Library Publications"]
tags: ["Islam", "Short Books", "Crusades", "Church History", "Missions"]
authors: ["Tisdall, William St. Clair"]
titles: ["'It Is The Will Of God!': A Plea For A Great Extension Of Mission Work In Islamic Lands"]
origpublishers: ["Church Missionary Society"]
origdates: ["1891"]
synods: ["Anglican"]
---
William Tisdall was an expert in Islam and the Koran, and fluent in Arabic, Persian, and other languages.  One of his most valuable books is _Islamic Objections to Christianity_, which will be re-released later this year by The Lutheran Library.  

{{% toc %}}

# Some Men Know God

"'Some men know God,' said a Muslim friend of mine, who had knelt weeping with me to pray to God for light and peace — 'some men know God in a way that I do not know Him, though I have long sought for Him. Pray to God for me that, if it be His will, I too some day may know Him.' God has not left Himself without a witness among such men. All we need is to go forward boldly, prayerfully, and trustfully in this grand and glorious work. 

# The Influence of Islam on Hearts and Lives

"Islam has long exercised, and even now exercises, over the hearts and lives of many millions of Muhammadans an influence which, for strength and earnestness of conviction and the zeal which accompanies it, can hardly find its parallel in the whole history of the world. 

"__God__ is regarded by the Muslims as __an Almighty Ruler__, entirely different in every possible respect from every one of His creatures, and __separated from them by an impassable gulf__. He is ...__arbitrary__ and __despotic__...and it is __gross blasphemy to call Him our Father__. He is our Master and Owner, and __we are His slaves__...

"Muhammad is the last and greatest of His Prophets; for his sake the Universe came into existence, and the Qur'an revealed through him has superseded all previous revelations of God. The Atoning Death of our Lord is denied, His Deity scouted as a blasphemous fable, and the Qur'an assures us that "God can destroy Jesus, Son of Mary," as readily as any other part of creation. 

[In the above quote __emphasis__ added.]
# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/s06-tisdall-it-is-the-will-of-god-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/s06-tisdall-it-is-the-will-of-god.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request s06-tisdall-it-is-the-will-of-god&body=Please send s06-tisdall-it-is-the-will-of-god.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request s06-tisdall-it-is-the-will-of-god&body=Please send s06-tisdall-it-is-the-will-of-god.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}

