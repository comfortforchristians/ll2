---
date: 2019-04-27 05:14:42-04:00
publishDate: 2019-04-27 05:14:42-04:00
title: Gotthold's Emblems by Christian Scriver
slug: "106wd-scriver-gottholds-emblems"
categories: ["Lutheran Library Publications"]
tags: ["Scriver", "Wisdom", "Devotional"]
authors: ["Scriver, Christian"]
titles: ["Gotthold's Emblems"]
origpublishers: ["Gould and Lincoln"]
origdates: ["1859"]
synods: ["Lutheran Germany"]

---

'Gotthold's Emblems' is a unique book of wisdom devotions written by Rev. Christian Scriver.  From the time of its first publication this beloved volume has gone through more than 28 editions.

>You may shake or push the magnetic needle from its position, continued Gotthold, but it returns to it the moment you leave it to itself.
>
>In like manner, believers may fall into sin,  and deviate from the line of duty; but no sooner have they leisure for reflection, than they endeavor to amend, and resume a life of godliness. On the contrary, the wicked watch for opportunities to do evil, and yield to all the temptations of the devil and the world.

{{% toc %}}

# More than 28 editions

*Gotthold's Emblems* is a unique book of wisdom devotions written by Rev. Christian Scriver.  

From the time of its publication to the mid 1800's *Gotthold's Emblems* went through at least 28 German editions.[^Bb]  The publisher of the 1846 English language version writes:

>It is, indeed, a matter of surprise, that a work of such preeminent merit should have circulated in German homes for nearly two centuries, without finding an English translator...the extraordinary value attached to stray copies by the families in which they were heirlooms, made it difficult for an editor to obtain a single copy, even for use in preparing a new edition. The publishers are not aware that any work of its precise character is to be found in the English language.[^Bc]

# About Christian Scriver

Christian Scriver was born Jan. 2, 1629, at Rendsburg, Prussia. His early years were spent during the trying times of the Thirty Years' War. He was educated at Rostock (1647). In 1653, archdeacon at Stendal; 1667, preacher at Magdeburg, where he served for 23 years, refusing repeated honorable calls to Berlin, Stockholm, etc. In 1690, he was persuaded to accept a call as chief court-preacher, at Quedlinburg, Saxony, where he died of apoplexy, April 5, 1693. 

Scriver was unquestionably sound in his Lutheranism, though he earnestly protested against the mistakes which were becoming more and more pronounced in the church of his time. Together with Heinrich Müller, he was called to prepare the way for the Pietism of the succeeding period, which was a reaction against the dead orthodoxy which had become characteristic of the Lutheran Church. 

Scriver is particularly distinguished for his writings, of which there are many. The most noted of his works is the 5 volume Seelen-Schatz ("The Soul's Treasure"), which is ascetic in character.[^acK]





# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download) 


![book cover](/img/posts/106wd-scriver-gottholds-emblems-1024.jpg)

<a name="download"></a><br />


[PDF](/pdf/106wd-scriver-gottholds-emblems.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 106wd&body=Please send 106wd-scriver-gottholds-emblems.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 106wd&body=Please send 106wd-scriver-gottholds-emblems.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2018-05-22
- _Version 4 update_: 2019-04-26 
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)







[^acK]: Jacobs, Henry Eyster, ed. (1899) *The Lutheran Cyclopedia*. New York: Charles Scribner's Sons 

[^Bb]: Ibid. "American Publisher's Note" 

[^Bc]: Ibid.