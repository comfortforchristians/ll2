---
date: 2019-09-01 05:10:46-04:00
publishDate: 2019-09-01 05:10:46-04:00
title: "Christopher F. Bergman: A Biographical Sketch"
slug: "christopher-f-bergman"
categories: ["Biographical Sketches"]
tags: ["Bergman", "Lutheran Ministers", "Evangelical Review"]
authors: ["Bergman, Christopher", "Krauth, Charles Porterfield"]
synods: ["Synod of South Carolina"]
---

Our church in the South… sustained a severe loss in the death of another faithful watchman on the walls of Zion, who fell in the harness, in the midst of his days and of his usefulness, while pastor of the Evangelical Lutheran Church at Ebenezer, Ga. The early departure of Bergman filled many a heart with the most intense sorrow, and spread a deep gloom, not only upon his bereaved congregation, but upon our whole Southern Zion. His name is enshrined in the affections of the church; his memory is worthy of a place among those, who have fought the good fight of faith, and laid hold of eternal life, who have labored for the furtherance of religion, and the dissemination of the gospel. 

{{% toc %}}

## Christopher F. Bergman. 

>_Omne capax movet urna nomen._

Christopher F. Bergman was born at Ebenezer, Georgia, January 7th, 1793. He was the only son of Rev. John E. Bergman, who had been born and educated in Germany, and sent to Georgia, as successor to Rev. Christian Rabenhorst. The subject of our present reminiscences received his training under the exclusive care and direction of his learned and venerable father, by whom no efforts were spared to fit his son for an active and useful life. He bestowed upon his culture the most careful attention, and diligently sought to instill upon his youthful mind virtuous principles. Whilst endeavoring to unfold his mental faculties, and to aid him in the acquisition of knowledge, he did not forget to instruct him in the way of life, and to lead him to (he contemplation of those subjects, which pertained to his everlasting peace. These efforts were not without their influence. The pious lessons were treasured up in the heart. In the morning of life the son was brought under the power of divine truth, and united with the church. 

## Early Calvinism.

It was not, however, until some years after, that he dedicated himself to the ministry of reconciliation; and when he came to the decision that he was called to the work, he was on the point of making application for licensure to the Presbyterian church, and of seeking a field of labor in that connection, in consequence of his strong sympathy with some of the doctrinal views of the Calvinistic system. This was a source of sincere regret, not only to his aged father, who thought that a door of usefulness was opened in the Lutheran church, but also to the congregation at Ebenezer, who had for a long time entertained the hope, that when the tomb had closed upon their revered pastor, his son, who had been raised among them, and had already won upon their affections by his exemplary conduct, would be able to minister among them in holy things. Their expectations seemed almost frustrated, and their cherished plans defeated. 

## The influence of Dr. Bachman

A single circumstance, however, changed the determination of the young man, and gave a different turn to the aspect of affairs. One of our ministers, Rev. Dr. Bachman, whose faithful and efficient services are so well known and appreciated in the church, about this time made a visit to Savannah, for the purpose of resuscitating a Lutheran congregation in that city. Whilst there, having heard of the condition of our church at Ebenezer, whose pastor was declining in health, and rapidly sinking into the grave, he determined to extend his visit thither. On this occasion he met with Mr. Bergman for the first time, and in a conversation with him, succeeded in removing the difficulties which had long perplexed his mind, and in giving a new direction to his theological opinions, in fully satisfying him as to the scriptural character of the doctrinal basis of the Lutheran church, and of her claim upon his services. The last hours of the dying parent were cheered by the result, and the congregation rejoiced in the prospect of the accomplishment of their fondly cherished wishes. 

## The Church in Ebenezer, Georgia

At this period in our narrative we must pause, for a moment, and introduce a brief sketch of our church in Ebenezer, with the origin of which so many interesting associations are connected. As early as the year 1734, a colony of Lutherans established themselves in Georgia. They came from Salzburg, formerly a district of Bavaria, and restored to the Austrian dominions at the peace of 1814. The victims of persecution at home, they sought an asylum in this country, which offered protection to the oppressed. They were accompanied by two able and faithful ministers of the gospel, Messrs. Bolzius and Gronau, who, for many years, preached the word in its purity, and ministered to the spiritual wants of this interesting people. The colony originally consisted of ninety-one individuals, who settled about twenty-four miles north of Savannah, and, in gratitude to God for the gracious leadings of his Providence, gave to their settlement the name of Ebenezer. To this colony others were soon added. God was in their midst and blessed them. The church prospered. A deep toned piety prevailed, and Christ's kingdom was extended. The various trials through which they had passed in their native land, the cruel persecution and painful adversity they suffered, had greatly improved their character, fitted them more fully to appreciate the change in their condition, and to value the privilege of worshiping God under their own vine and fig tree, without fear or molestation. 

Through the instrumentality of Senior Urlsperger, of Augsburg, aid was furnished them by the British Society for the Promotion of Christianity, and the oppressed Salzburgers were enabled to reach the place of their destination. Large sums of money were raised for them in Germany and England, and the British Parliament voted £26,000 for their relief. Friends were raised up in all directions. They met with the kindest sympathy, and awakened the sincere regard of Christians of every name. Whitfield especially took a lively interest in the welfare of the church; he lived on the most intimate terms with their ministers, and furnished a tangible proof of his friendship, by affording pecuniary assistance and securing contributions for the congregation from different sources. A Presbyterian clergyman, on one occasion, accompanied Whitfield to Ebenezer, and assured our ministers of the deep respect entertained by his brethren for the Lutheran church, also informing them that the perusal of Luther's Preface and Commentary on Galatians, had produced among the English inhabitants in the colony of Virginia, a great awakening, "so that they were holding meetings on the Lord's Day; that they were seeking edification and growth in religion, through the writings of Luther, and were desirous of connecting themselves with the Lutheran church."[^bHj] 

[^bHj]:  Several interesting incidents are on record, in the early history of the colony of Virginia, of the influence of Luther's writings in producing seriousness, and in leading individuals to a knowledge of the truth as it is in Jesus. At this period the Church of England was the established religion, and much formality prevailed. Rev. Samuel Davies writes to the Bishop of London,

    "that a little before the year 1743, about four or five persons, heads of families in Hanover, had deserted from the established church, not from any scruples in reference to her ceremonial peculiarities, the usual cause of non-conformity, much less about her excellent articles of faith, but from a dislike to the doctrines generally delivered from the pulpit, as not savoring of experimental piety, nor suitably intermingled with the glorious peculiarities of the religion of Christ. These families were wont to meet in a private house on Sundays, to hear some good books read, particularly Luther's; whose writings were the principal cause of their leaving the church." 

    It is also stated, "that a gentleman got possession of Luther on the Galatians. Deeply affected with what he read, so different from what he had heard from the pulpit of the parish church, he never ceased to read and pray till he found consolation in believing in Christ Jesus the Lord his righteousness." 

    Several refused to attend the ministrations of the church, and determined to subject themselves to the payment of the fines imposed by law. They agreed to meet every Sabbath alternately, at each other's houses, and spend the time with their families in prayer and reading the scriptures, together with Luther's Commentary on the Galatians — an old volume which had, by some means, fallen into their hands." 

    Mr. Morris moreover remarks, among other things respecting the interesting awakening that existed at the time, "that as we knew but little of any denomination of dissenters, except Quakers, we were at a loss what name to assume. At length recollecting that Luther was a noted Reformer, and that his book had been of special service to us, we declared ourselves Lutherans ."— _Vide Sketches of Virginia_, by Rev. W. H. Foote, D. D. 

From time to time the Salzburgers received fresh accessions from Europe, and in 1746, pastor Lembke, and in 1752 pastor Rabenhorst arrived. In 1735 these colonists had erected an Orphan House at Ebenezer, to which benevolent enterprise liberal contributions were given by Whitfield, who likewise presented them with a bell for one of the churches they had erected. The colony was beginning to assume a most promising appearance, but during our revolutionary struggle, the church here, as in other places, deteriorated; its interests greatly suffered. Our members were warmly attached to the principles of the revolution, and naturally sided with the American part}'. At the very commencement of the difficulties, they took an active part in favor of liberty. When efforts were made by the opposition to identify them with the cause of Great Britain, they resisted the attempt, and thus reasoned: 

>"We have witnessed the evils of tyranny in our native country; for the sake of liberty we have left home, houses and estates, and have taken refuge in the wilds of Georgia; shall we again submit to bondage? No! we will not." 

Upon this principle they acted, and for their love of freedom they were driven from their adopted home during the ascendency of the British arms. Their cherished prospects were almost ruined during the war. Their house of worship was converted by the enemy into a stable for their horses, and a hospital for the sick. At the termination of the war, when the success of the Americans allowed these exiles to return to Ebenezer, they were obliged to commence operations anew, to rebuild their village, to revive spirituality, and to recover the ground they had lost during the contest. Long and arduous were their labors, but they did not despair. Their troubles, too, were increased in consequence of pastor Rabenhorst's death occurring soon after. 

Although destitute of regular ministerial supplies, they kept together as a congregation, and trusted in God, the God of their fathers, who had so often brought relief, and in times past, been their solace and strong refuge. About the year 1784, soon after the declaration of peace, and the acknowledgment of our independence, the elder Bergman reached this country, and at once proceeded to Ebenezer, having been sent thither by the friends of the Salzburgers in Europe, as successor to the lamented Rabenhorst. 

## German language only

Pastor Bergman was a fine scholar and a most excellent Christian, and served the congregation at Ebenezer for thirty-six years. Unfortunately, however, for the interests of Lutheranism, he conducted the exercises of the sanctuary exclusively in the German language. This course was most impolitic, and proved here, as elsewhere, detrimental to the progress of our church. In former years no evil was experienced from this source. The members of the congregation were either Germans, or the immediate descendants of Germans, and Bolzius, Gronau, Lembke and Rabenhorst, labored effectually in their vernacular tongue. But the times had changed. The tide of immigration from Europe had been diverted from the Southern States to other sections of the land; the rising generation, surrounded on all sides by those, whose knowledge was limited to the English, in the course of time, lost the language of their fathers, and derived comparatively little benefit from German preaching. They consequently took no interest in the services of their own church, conducted in a tongue, which they with difficulty comprehended: they therefore naturally withdrew, and united with other denominations. Hence the congregation which half a century before required the services of three ministers, towards the close of the eighteenth century was reduced to a very small band; a remnant only of the former large and flourishing congregation remained. 

In the retrospect, how often have we to regret the rigid adherence of many of the patriarchs of our church to the German language. How many thousands, for this reason, lost their ecclesiastical attachment, abandoned their paternal communion, and sought instruction and edification among our brethren of a different name, whilst others became irregular and careless attendants upon the services of their own denomination, and altogether negligent of the claims of religion! What a different position would the Lutheran church at this day occupy, if a different course had been pursued, a wiser policy adopted. With the Divine blessing, the place of our tent would have been enlarged, our cords lengthened, our stakes strengthened, and our church would have become one of the strongest and most influential in the land! 

## Rev Bergman licensed to preach

But to proceed with our sketch; in accordance with his convictions of duty, and the earnest solicitations of the congregation at Ebenezer, the subject of our memoir consented to assume the pastoral office, and become the successor of his venerable father. At the meeting of the Synod of South Carolina and adjacent States, held in the autumn of 1824, he applied for license, and the committee appointed to examine him, reported "that he had received a classical education, and carefully attended to his theological studies under the care of his father, and they considered him well qualified for the ministry." He was accordingly set apart to the solemn work of preaching the gospel, and at once entered upon his responsible duties. He consecrated his whole being to the work, and with this consecration he allowed nothing else to interfere. He labored faithfully, acceptably and usefully. His people learned to love him and to profit by his ministry. 

## Death by Consumption

His labors were not in vain. But his career was brief. Before many years disease developed itself in his system; consumption, that foe of thousands, was found rapidly undermining the citadel of life, and insidiously and gradually advancing, until it laid him prostrate. All that medical skill, or the most devoted affection could do to avert the stroke, proved unavailing. On the morning of the 26th of March, 1832, at so early a period of his life and labors, he was summoned to share the joys of a departed parent in the realms of bliss, having been the honored pastor of his father's charge, during a period of not quite eight years. On the occasion of his death, a most touching and appropriate discourse was pronounced by Rev. S. A. Mealy, of Savannah Ga., from the words of the Apostle: "_I would not have you to be ignorant, brethren, concerning them which are asleep; that ye sorrow not even as others, which have no hope. For if ye believe that Jesus died and rose again, even so them also which sleep in Jesus will God bring with him._" 

## Noble character and eminent Piety

From all that we have been able to gather respecting this man of God, we infer that he possessed superior natural abilities, improved by diligent culture; that he was a man of noble character and eminent piety. Although young in the ministry, his sphere of usefulness was daily increasing; had he lived, he would have exercised a very great influence upon the church. He was very much devoted to the work, to which he had dedicated himself. Deeply impressed with its responsibilities, he faithfully fed the flock entrusted to his care. Sensible that he must watch as one, that would have to give an account of his stewardship, he was indefatigable, and most earnestly dispensed the message of salvation. Asa pastor, he was very efficient. He was emphatically a good minister of Jesus Christ. No part of his duty was ever neglected. He never failed fully to meet the demands made upon him. He knew so well how to sympathize with the afflicted, to weep with those that wept, to bind up the broken-hearted, to dry up the tears of the afflicted, to soothe the sorrowful, to console the dying with words of heavenly peace, to minister to all (he spiritual wants of his people. By his brethren in the ministry he was highly esteemed. He was annually and unanimously elected Secretary of the Synod, from the time of his ordination, until the last convention which he was permitted to attend. 

## Bergman's Preaching

As a preacher, Mr. Bergman is said to have been pleasing and impressive, easy and natural, plain and practical, fervent and instructive. In the preparation of his discourses, he never seemed to be influenced by a desire to secure popular applause. There was no attempt to display his learning. He was even disposed to reject all ornament. He generally wrote down his thoughts just as they occurred to his mind. If he sought for expressions at all, it was for those, which would move the souls of his hearers, arouse the conscience and reform the life, rather than for those which would gratify the ear or please the fancy. He was unwilling to cultivate any art, for the mere purpose of giving effect to his delivery. He relied upon the influence of the truth. He was confident of the verification of the promise, that the word would not return void, but would accomplish that whereunto it was sent. His sermons were characterized by native good sense and correctness of sentiment. Christ was the sum and substance of them all. The doctrines of the Reformation, as taught by our own Luther, he cordially believed, and earnestly defended. 

## Social relations

In social intercourse, Mr. Bergman was dignified and grave. His appearance and his manner forbade any rude approach or undue familiarity. To those who were not intimately acquainted with him, perhaps he seemed inaccessible and austere. He was cautious and reserved, yet beneath all, there was a warm heart and deep affection. He was ardently attached to those, whom he loved. He was neither obtrusive nor officious, yet he was affable and kind, free from offense towards all men. He never spoke or acted unadvisedly. It was not his practice to speak unkindly of others. He was cheerful, but never trifling. He never encouraged hilarity, which was inconsistent with the ministerial character. In all the relations of life he reflected honor upon the position which he occupied. His piety was seen in every transaction, and was the basis of all his excellencies. It was not merely theoretical, but practical, humble, serious and uniform, burning as a steady flame, and acquiring increasing brightness from its continuance, radiating a constant light, and exerting a conservative influence upon all, with whom he came in contact. He lived in the continued exercise of a virtuous and Christian self-control, and labored to bring every thought into captivity to the obedience of Christ. He exercised a simple faith in all the teachings of the Bible, and his faith worked by love, purified the heart, and overcame the world. His was "the wisdom that is from above, first pure, then peaceable, gentle and easy to be entreated; full of mercy and good fruits, without partiality and without hypocrisy;" and ever brought forth "the fruit of the spirit, love, joy, peace, long suffering, gentleness, goodness, fidelity, meekness, temperance." As he advanced in years he grew in piety, and advanced in Christian knowledge. His path was "as the shining light, that shineth more and more unto the perfect day." It is a mysterious, though wise dispensation, which removed one so eminently fitted for usefulness, in the strength of his influence; but his work was done, and he went to receive his reward. " My thoughts are not your thoughts, neither are your ways my ways, saith the Lord." "For as the heavens are higher than the earth, so are my ways higher than your ways, and my thoughts than your thoughts." 

## His passing

But it was not only during his life, but in the hour of death, we see the sincerity and depth of the principles he professed. His triumphant departure furnishes another to the many monuments which are erected along the highway of life, to show the power of religion, to the praise of Him who giveth his people the victory. 

>&emsp;&emsp;"Even in death, \
>In that dread hour, when with a giant pang, \
>Tearing the tender fibres of the heart, \
>The immortal spirit struggles to be free. \
>Then, even then, that hope forsakes him not. \
>For it exists beyond the narrow verge \
>Of the cold sepulchre." 

Mr. Bergman died, through the triumphs of grace, as he lived, meek, humble, patient, full of hope and confidence. He was perfectly composed, in view of the prospect before him. He found the promise made good. He was sustained by the all-powerful consolations of religion, and met with a peaceful and triumphant end. As he drew near the cold river of death, and prepared to cross its banks, no fear disturbed his mind. Not a cloud cast a shadow over his mind. He enjoyed a calm, full assurance of a blessed immortality. Said he, 

>"I can look at the grave without any dread." 

Being asked if he had any doubts of his acceptance with God, he replied,  

>"None! Blessed be the God and Father of my Lord Jesus Christ, I have no doubts." 

To one who inquired, whether if it were the Divine will, he would not wish to be spared a little longer to his family and congregation, he said: 

>"If it is the Divine will, I would rather go now. I feel that for me to depart and be with Christ, is far better. I think I can truly say, for me to live is Christ, to die is gain." 

On the day preceding his death, he was visited by several members of his congregation. He recognized them all, and clasped them by the hand, bidding them a final adieu, and addressing to each a parting exhortation. A friend who was sitting by his side, apprehending that his end was approaching, inquired whether he did not wish to see his wife and babe. He replied, "Not now — I have for some time been anticipating this event, and my God enabled me to give them up three weeks ago, to surrender them with entire confidence to his care and protection." The ties of kindred and the attraction of an interesting and increasing sphere of usefulness, were insufficient to reconcile him to the thought of living, after he had obtained a near vision of the more blessed ties, and the higher and purer service of the heavenly world. To a ministerial brother, who remarked, now is the time to test the full value of the religion which you have so long professed, and which you have faithfully preached, he at once rejoined, 

>"O death, where is thy sting? O grave, where is thy victory? Thanks be to God who has given me the victory through my Lord Jesus Christ."

He then dwelt for some time on the expression faithfully preached. At length he broke forth,

>"Not unto me, O Lord, not unto me, but unto thy name be all the praise. We have this treasure in earthen vessels, that the excellency of the power might be of God and not of us." 

Just before he died, he desired this brother to pray with him, and he distinctly, though feebly, repeated every word, and concluded the prayer with Amen. He lay composed for some time, and then bade his friend an affectionate farewell, after which he repeated those beautiful lines: 

>"Cease fond nature, cease thy strife, \
>And let me languish into life," 

These were his last words. He soon ceased to breathe. The silver cord was gently loosed, and the spirit returned to God who gave it. 

>"He died as sets the morning star, which goes \
>Not down behind the darkened West, nor hides \
>Obscured amidst the tempests of the sky, \
>But melts away into the light of heaven." 

His redeemed spirit, released from its frail tenement, ascended to those celestial mansions, to receive the gracious salutation, and to hear the welcome plaudit, "Well done good and faithful servant, enter thou into the joy of thy Lord." 

## Lessons from Rev. Bergman's life

If, as the poet tells us, there is a sacredness which surrounds  "the chamber, where the good man meets his fate," let the odor of its sanctity be embalmed for the benefit of future generations. Let it be preserved for the encouragement of the church in distant times. Let us derive from this hallowed spot, consolation in our trials, and incentives to renewed exertion in the cause of Christ. As we gather around the tomb, may we gain lessons of practical wisdom, and improve the solemn truths designed for our spiritual good. May we be impressed with the comparative worthlessness of all earthly possessions, for in the words of sacred authority, "What will it profit a man, if he shall gain the whole world, and lose his own soul." Let us listen to the admonition, how short is time, and how frail our hold upon it, what responsibilities we sustain, and what important issues are before us. "Man is of few days and full of trouble, he cometh forth like a flower and is cut flown: he fleeth also as a shadow and continueth not." May we so live and "walk with God," that death will be to us only an admission into higher life, that when summoned from time to eternity, we may commit our departing spirits to Him who gave them, with humble trust, with filial prayer, with undying hope, that "when Christ, who is our life shall appear, we may appear with Him in glory;" that survivors may discover in their remembrance of us springs of comfort, testimonies to the power of religion, encouragements to virtue and piety, and pledges of immortality. 

# Publication Information

- _Author_: __"Krauth, Charles P."__, Editor.
- _Journal_: __"The Evangelical Review."__
- _Originally Published_: 1854.
- _Lutheran Library Edition_: 2019
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

{{% alert note %}}
{{% biographical-sketch-note %}}
{{% request-pdf %}}
{{% /alert %}}
