---
date: 2019-05-23 11:56:03-04:00
publishDate: 2019-05-23 11:56:03-04:00
title: "The Present Controversy on Predestination. Part 1 of Walther and the Predestination Controversy or The Error of Modern Missouri by Schodde et al."
slug: "240tc-stellhorn-which-predestination-reformed-or-lutheran"
categories: ["Lutheran Library Publications"]
tags: ["Schodde", "Stellhorn", "Election", "Predestination", "Predestination Controversy", "Intuitu Fidei", "Walther", "Calvinism", "Book of Concord", "Justification"]
authors: ["Schodde, George Henry", "Stellhorn, Frederick William"]
titles: ["The Present Controversy on Predestination", "The Error of Modern Missouri"]
origpublishers: ["The Lutheran Book Concern"]
origdates: ["1892"]
synods: ["Ohio Synod", "Iowa Synod", "Missouri Synod", "General Council", "General Synod"]

--- 

The rise of the Synodical Conference under the LCMS has made Universal Objective Justification the Shibboleth of American Lutheranism.  Quite simply, if you do not accept UOJ, you are not considered a "Confessional Lutheran".  

Towards the end of his life, C. F. W. Walther brought forth a teaching of election which many Missouri and other American Lutherans could not reconcile with the Scriptures or the Lutheran Confessions.  The resulting "Predestination Controversy" split the Lutheran Church.

# Faith Can Never Be Considered A Work

"When the Reformed theologian Beza raised the objection: 'It is false that foreseen faith is the cause of predestination or of the elect, for this is the doctrine of Pelagius,' he [Chemnitz] answered: 'Faith in Christ is not a work of nature or of our human powers, but the work of the Holy Spirit. Therefore, when we teach that faith in Christ is the cause of the eternal election of God unto adoption, it is by no means related to the Pelagian heresy; for the Pelagians attributed to human powers what the Holy Spirit alone can produce and work.'" 

# The Lutheran Church Unanimous

"Since the publication of the Formula of Concord, for some 300 years, the Lutheran Church has unanimously held that the doctrine of our Confession and of the following teachers of our Church harmonized perfectly also in the article of predestination. Modern Missourians are the first "Lutherans" who assert the contrary; it is to be hoped that they will also be the last."

{{% toc %}}

This book is part 1 of [Walther and the Predestination Controversy or The Error of Modern Missouri](/239-schodde-the-error-of-modern-missouri/)

- [Part 2: _Intuitu Fidei_ "In Light of Faith"](/241tc-schmidt-intuitu-fidei)
- [Part 3: A Testimony Against the False Doctrine of Predestination Recently Introduced By The Missouri Synod.](/242tc-lenski-the-blue-island-theses-election-and-predestination)


# Summary Book Contents

- Introduction.

- I. The Present Controversy on Predestination

    - I. Historical Introduction.
    - II. The Formula Of Concord And The Old Lutheran Dogmaticians.
    - III. The Doctrine Of Predestination In The Missouri Synod.
        - A. Before The Year 1877.
        - B. The Synodical Report Of The Western District For The Year 1877.
        - C. The Synodical Report Of The Western District For The Year 1879.
        - D. Altes und Neues And Lehre und Wehre Before The General Pastoral Conference At Chicago In The Autumn Of 1890.
        - E. The General Pastoral Conference In The Autumn Of 1880.
        - F. After The Pastoral Conference In The Autumn Of 1880.
        - G. Comparative Summary.
            - 1. What Is Predestination?
            - 2. What Has God Regarded In Election?
            - 3. What Is The Relation Especially Of Faith To Election?
            - 4. In What Sense Does The Formula Of Concord Speak Of Election?
            - 5. Is Man’s Conversion And Salvation In Every Sense Independent Of His Conduct?
            - 6. May We Speak Of Man’s Decision Or “Self-Determination” In Conversion?
            - 7. What Is The Difference Between The Lutheran And The Reformed Doctrine Of Election?
            - 8. How Must The Doctrine Of The Dogmaticians Of The Seventeenth Century Be Regarded?
            - 9. How Is The Doctrine Of Modern Missouri To Be Regarded?
        - H. Appendix: An Ally Of Modern Missouri In Germany.
    - IV. The Doctrine Of Predestination In The Ohio Synod.


- The Predestination Controversy: How It Happened.
    - How the Predestination Controversy “Broke Out”


# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)

![book cover](/img/posts/240tc-stellhorn-which-predestination-reformed-or-lutheran-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/240tc-stellhorn-which-predestination-reformed-or-lutheran.pdf)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 240tc&body=Please send 240tc-stellhorn-which-predestination-reformed-or-lutheran.mobi)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 240tc&body=Please send 240tc-stellhorn-which-predestination-reformed-or-lutheran.epub)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}



# Publication Information

- _Lutheran Library edition first published_: 2018-06-05
- _Version 4 update_: 2019-05-23 
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
