---
date: 2019-01-18 14:55:11 
title: "New Lutheran Library Website is Live!"
new-lutheran-library-website: "new-lutheran-library-website"
categories: ["Lutheran Library Newsletters"]
tags: ["Website"]
---
We're happy to announce a completely redesigned [LutheranLibrary.org](/) .

The new site should make it easier for you to find and access excellent Christian reading material from the Lutheran Library.  Even if you are not ready to start any new books now, please take a look and let us know what you think.  

Some highlights:

{{% toc %}}

# Much Faster on Tablets, Phones, and Computers with Slow Connections

![comment example](/img/posts/new-website-iphone.jpg)


# Improved Navigation

A clear menu is available on every page.


![comment example](/img/posts/new-website-menu.jpg)

From left to right:

- __Home__: returns to the front page of the site.
- __Publications__: provides a detailed catalog of all books available to you through the Lutheran Library.
- __Authors__: provides an active "clickable" list of all authors.  In future this may include biographical information.
- __News__: provides a simple list of "newsletter" posts like this one.
- __<i class='fas fa-question'></i>__: Frequently Asked Questions.
- __<i class='fas fa-church'></i>__: About the Lutheran Library.
- __<i class='fas fa-envelope'></i>__: Contact Form.
- __<i class="fas fa-moon"></i>__: Change theme light to dark.

# Extensive "metadata"

"Metadata" is simply indexing information, like the label on a file folder.  These "tags" allow you to easily find related titles.

![metadata example](/img/posts/new-website-metadata.jpg)

In this example:

- <i class="fas fa-user"></i>: The Author is [Simon Peter Long](/authors/long-simon-peter/).
- <i class="fas fa-book"></i>: The Title is _The Great Gospel_.
- <i class="fas fa-building"></i>: It was originally published by [The Lutheran Book Concern](/origpublishers/lutheran-book-concern/),
- <i class="fas fa-calendar"></i>: in [1904](/origdates/1904/),
- <i class="fas fa-church"></i>: and is associated with [the Ohio Synod](/synods/ohio-synod/).
- <i class="fas fa-layer-group"></i>: It is a Lutheran Library publication.
- <i class="fas fa-tags"></i>: Some subject tags: [Long](/tags/long/), [Sermons](/tags/sermons/), [Must Reads](/tags/must-reads/).

# Comments

Commenting has been enabled throughout the site.  

![comment example](/img/posts/new-website-comments.jpg)

You'll find the comments entry form at the very bottom of each page.

# Summary

These books are for you, and for the next generation.  Let us know how we can best serve you, our brothers and sisters in Christ.  

You can help by spreading the word.  We are not on social media, so any help you can provide by letting others know about the Lutheran Library is of great help.

# About the Lutheran Library

The goal of the Lutheran Library is to re-release well-written and readable books from sound, faithful American Lutherans of the past for the enjoyment and edification of a new generation.  All books are available at [lutheranlibrary.org](/) for free download in a variety of formats for Kindle, Apple, and other devices.

Your help is appreciated in spreading the word as often and in as many ways as you feel is appropriate.

May God bless you and keep you, help you, defend you, and lead you to know the depth of His love.  *Amen*