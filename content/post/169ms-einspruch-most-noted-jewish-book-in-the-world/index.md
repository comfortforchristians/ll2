---
date: 2019-06-06 07:48:54-04:00
publishDate: 2019-06-06 07:48:54-04:00
title: "The Most Noted Jewish Book In The World by Henry Einspruch"
slug: "169ms-einspruch-most-noted-jewish-book-in-the-world"
categories: ["Lutheran Library Publications"]
tags: ["Jewish Christians", "Missions", "Messianic Jews", "Einspruch"]
authors: ["Henry Einspruch"]
titles: ["The Most Noted Jewish Book In The World"]
origpublishers: ["The Lutheran Hebrew Center"]
origdates: ["1926"]
synods: ["Lutheran Hebrew Center Baltimore"]
aliases: ["/125ln-hosmer-story-of-the-jews/"]

---

"What is this book? Its Hebrew name is הברית ההדשה (Heb’rith Hechedasha) or the New Covenant. It is a collection of twenty-seven writings, biographic, historic, prophetic, and a number of personal letters. Its authors, with but one exception, were Jews; the Hero around whom the whole book centers, was a 'Jew of Jews.'  Yet, notwithstanding this racial background, it has been regarded of such value as to warrant its translation into some 900 different languages and dialects, a thing that cannot be said of any other book. 

"No living being has the remotest idea how many copies or portions of this book have been printed, but today its circulation easily reaches the billion mark. Last year alone some twenty million copies were printed in England and America. As over against the millions of books issued today it ranks supreme as the world’s 'best seller.' And, to quote again Claude G. Montefiore: 'If it is an improper ignorance not to have read some portions of Shakespeare or Milton, it is, I am inclined to think, a much more improper ignorance not to have read the Gospels' (the first four portions of the book). – Henry Einspruch

"What a book! Vast and wide as the world, rooted in the abysses of creation, and towering up beyond the blue secrets of heaven – the whole drama of humanity is in this book!  – Heine

# Contents 

- The Most Noted Jewish Book
- The Hero of the Book
- The Gospels – “Jewish Literature”
- The Teaching of the Book
- The Gospels and the Rabbinic Literature
- Parallels between the Gospels and the Talmud
- The Ideal of Jesus
- About the Author - Henry Einspruch

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/169ms-einspruch-most-noted-jewish-book-in-the-world-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/169ms-einspruch-most-noted-jewish-book-in-the-world.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 169ms&body=Please send 169ms-einspruch-most-noted-jewish-book-in-the-world.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 169ms&body=Please send 169ms-einspruch-most-noted-jewish-book-in-the-world.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2017-11-24
- _Version 4 update_:  2019-06-06
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
