---
date: 2019-05-06 19:20:42-04:00
publishDate: 2019-05-06 19:20:42-04:00
title: A Lutheran Treasury of Prayers
slug: "163wd-lutheran-treasury-of-prayers"
categories: ["Lutheran Library Publications"]
aliases: 
  - /163wd-alpb-treasury-of-prayers/ 
  - /163wd-a-lutheran-treasury-of-prayers/
tags: ["Prayer"]
titles: ["A Lutheran Treasury of Prayer"]
origpublishers: ["American Lutheran Publication Board"]
origdates: ["1906"]
synods: ["Missouri Synod"]

---
Prayers for all different types of situations easily accessible for use as needed. Keep it on your smartphone or laptop. 

{{% toc %}}

# God Hears Us

"DEAR FATHER In heaven, Thou lovest me, I know, because I love Thy Son, Jesus Christ, my Savior. Trusting in this I do now confidently pray Thee to hear me and to grant what I ask – not because I am so holy and pious, but because I know that Thou, for Christ’s sake, wilt readily give and grant all things to us. In His name I now come before Thee, offer my prayer – no matter who I am as to my person – confident that it is “yea” and surely heard. Amen.


# Preparatory Prayers
- P1. For True Devotion
- P2. For True Worthiness in Prayer
- P3. A Prayer of Comfort with Respect to Sin and Unworthiness
- P4. A Prayer Based on God’s Command and Promise
- P5. Assurance of Being Heard for Christ’s Sake
- P6. God Hears Us
- P7. Preparation for the Lord’s Supper

# Daily Prayers

This section includes morning and evening prayers for each day of the week.

# Miscellaneous Prayers
- M1. Prayer on a Birthday
- M2. A Prayer for Children
- M3. A Prayer for Young People
- M4. A Prayer for Servants
- M5. Prayer of a Husband
- M6. Prayer of a Wife
- M7. Prayer for the Aged
- M8. A Prayer for the Proper Performance of One’s Duties
- M9. A Prayer Before Work
- M10. A Prayer for Blessing in One’s Calling
- M11. A Prayer of Luther on the Passage: “Casting all your Care Upon Him, for He Careth for You”
- M12. Prayer in View of an Undertaking of Moment
- M13. Thanksgiving upon Completion of Work
- M14. Prayer While on a Journey
- M15. Prayer when Putting Out to Sea
- M16. Prayer of a Voyager in a Storm
- M17. Brief Instruction on Confession and Absolution
- M18. A Poor Sinner Laments the Impenitence of his Heart
- M19. Prayer for True Conversion
- M20. Prayer for Forgiveness of Sin
- M21. A Prayer for Reconciliation with our Neighbor
- Brief Instruction on The Lord’s Table
- Why do you intend to go to Holy Communion?
- The benefit derived from Holy Communion is of a threefold nature.
- M22. Morning Prayer for a Communicant
- M23. Prayer for a Worthy Communion
- M24. A Prayer of Thanks upon Partaking of Holy Communion
- M25. Prayer for Preservation from Sin, and for their Remission when Committed
- M26. A Prayer of Meditation on the Sufferings of Christ
- M27. Prayer at Time of Drought
- M28. Prayer for Sunshine in a Season of Continued Rains
- M29. Prayer for the Fruits of the Field
- M30. Prayer During a Thunderstorm
- M31. A Prayer of Thanks after the Thunderstorm
- M32. Prayer in Great Weakness of Faith
- M33. A Prayer in Great Distress and Danger
- M34. A Prayer of Thanks after a Safe Journey
- M35. Thanks for Deliverance from Afflictions
- M36. Morning Prayer for a Sick Person
- M37. Another Morning Prayer for a Sick Person
- M38. Evening Prayer for a Sick Person
- M39. Another Evening Prayer for a Sick Person
- M40. A Prayer in Sickness
- M41. Prayer of Contrition in Sickness
- M42. Brief Prayers for the Sick – For Each Hour of the Day
- M43. Thank Offering after Recovery
- M44. Three Prayers in the Hour of Death
- M45. A Prayer of Those Present for a Person in the Agonies of Death
- M46. A Short Litany together with Prayers for a Dying Person
- M47. A Prayer after the Patient has Departed in the Lord

# Prayers For Festival Days

- Prayer at Beginning of a New Church Year
- Prayer for the Advent Season
- A Christmas Prayer
- A Prayer on New Year’s Day
- Prayer on Epiphany
- Prayer on the Day of Purification
- Prayer on Day of Annunciation
- Prayer on Maundy-Thursday
- Prayer on Good Friday
- Prayer on Eastertide
- Prayer on Ascension Day
- Prayer on Day of Pentecost
- Prayer on the Festival of Holy Trinity
- Prayer on Day of John the Baptist
- Prayer on the Day of the Visitation of Mary
- Prayer on St. Michael’s Day
- Prayer on Reformation Festival
- A Prayer on the Day of Church Dedication
- Prayer on Harvest Festival
- Prayer on Mission Festival
- Prayer of Thanks

# Hymns 

- Morning Hymn
- Evening Hymn
- Cross and Comfort
- Beloved, “It is well!”
- The Christian Life
- Death and Eternity
- Abide With Me
- I Fall Asleep In Jesus’ Wounds
- “Forever with the Lord!” 

# A Mother’s Prayer

- Prayer of a Mother for Wayward Children
- Prayer of a Mother before being Delivered of a Child
- Thanksgiving after a Happy Birth
- Prayer of a Husband at the Birth of a still-born Child
- Prayer of a Mother who has given Birth to a Deformed Child.
- Prayer before being Subjected to an Operation
- Prayer for a Happy Death
- Prayer of a Dying Person for his Family and Friends

# Prayers For Children

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download) 

![book cover](/img/posts/163wd-lutheran-treasury-of-prayers-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/163wd-lutheran-treasury-of-prayers.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 163wd&body=Please send 163wd-lutheran-treasury-of-prayers.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 163wd&body=Please send 163wd-lutheran-treasury-of-prayers.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2017-12-02
- _Version 4 update_: 2019-05-06 
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
