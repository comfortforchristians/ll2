---
date: 2018-01-23 12:00:00
title: "Prepare to Meet Thy God by Simon Peter Long"
slug: "201wd-long-prepare-to-meet-thy-god"
categories: ["Lutheran Library Publications"]
tags: ["Long", "Short Books"]
authors: ["Long, Simon Peter"]
titles: ["Prepare to Meet Thy God"]
origpublishers: ["F. J. Heer Printing Co."]
origdates: ["1908"]
synods: ["Ohio Synod"]


---
"He that believeth not shall be damned," says your Lord and Savior Jesus Christ. You believe this. Pray tell me how can you let your relative, friend, neighbor, or enemy, ignorantly live in his sins and be damned? 

"If he wanted the truth, he could find it in God's Word and many good books taken from that Word; but he does not go to the house of God and does not read God's Word and does not care about the future. Shall you let him perish? Not so long as there is life and hope. 

{{% toc %}}

# Don't Get Angry At The Dead

"We must not get angry at the dead because they do not move, nor must we let those who are ignorant of God's Word perish in their sins. To aid you in helping your pastor to gather in souls from a perishing world to take instructions in the saving doctrines of God's Word, this little message has made its appearance.

# From the Introduction

"It was said of Baxter that he could set the world on fire while Orton was lighting a match. If Dr. Luther were living today, he would set the world on fire more than many of his followers. It would pain him to know how few souls some long-established Lutheran congregations had won for Christ from the highways and hedges. 

"I love _My church, my dear old church_ and God loves her; and, with her truth, she can win more souls than any other; but she cannot do it by expecting the spiritually dead to run after her; by having ministers who will please the devil by not entering, or not leaving their study; by baptizing and confirming only her own children; by thinking that the whole mission problem would be solved, if only we had more money; by having few family altars; by having congregations which expect their pastors to do all the work in God’s vineyard! 

"The Lutheran Church, with her pure doctrine, could set the world on fire if every Lutheran would obey Christ’s command: _Go – Work – Today – In My Vineyard!_ – S. P. Long

# Contents (72 pages)

- About the Author, Simon Peter Long (1860-1929)
- Preface
- 1. There Is A God
- 2. God Is Your God
- 3. God Finds You Lost
- 4. God Has Salvation For You
- 5. God Reasons With You
- 6. God Warns You
- Concluding Remarks
  - 1. Remember God can speak (through His Word).
  - 2. God is your Father and a father would speak to his children.
  - 3. God has spoken.
  - 4. Your God tells you that your soul is worth more than all the world.
  - 5. If you are not prepared to meet your God now, even though you could gain the whole world in one hour, you would not have the time.
  - 6. Seek the better acquaintance with your God and yourself. God knows Himself and you.
  - 7. A good citizen must know the laws of his country. He who wants to prepare to meet his God must learn God’s law, the ten commandments.
  - 8. But you cannot please God without faith.
  - 9. Through His Word the Holy Spirit comes to you.
  - 10. After God can say of you as He said of Saul, “Behold, he prayeth!” then remember that He accepts you as His child by Holy Baptism.
  - 11. The Lord wants you to come to His Supper.
  - 12. This leads me to the last rule, which should be first and last in your mind: Believe God.
- Lutheran Library Publishing Ministry Catalog

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download) 

![book cover](/img/posts/201wd-long-prepare-to-meet-thy-god-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/201wd-long-prepare-to-meet-thy-god.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 201wd&body=Please send 201wd-long-prepare-to-meet-thy-god.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 201wd&body=Please send 201wd-long-prepare-to-meet-thy-god.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}

