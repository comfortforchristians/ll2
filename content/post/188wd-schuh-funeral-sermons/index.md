---
date: 2018-05-29 12:00:00
title: "Funeral Sermons by Lutheran Divines edited by Rev. Lewis Herman Schuh"
slug: "188wd-schuh-funeral-sermons"
categories: ["Lutheran Library Publications"]
tags: ["Pastoral", "Sermons", "Schuh"]
authors: ["Schuh, Lewis Herman"]
titles: ["Funeral Sermons by Lutheran Divines"]
origpublishers: ["Lutheran Book Concern"]
origdates: ["1918"]
synods: ["Ohio Synod"]

---
An excellent collection of 57 sermons were delivered by Revs. Lenski, Tressel, Schuh, Gohdes, Bauslin, Troutman, Long and others.  Particularly good are numbers 12, 19, 22, 30, 38, and 45.

{{% toc %}}

# Where God's Fountains Of Comfort Flow

"You are today directed to God's Word by God's servant. You are told where God's fountains of comfort flow. You are being pointed to the green pastures and the living waters of God's Word. 

"If you go to these fountains today, and find comfort and consolation, will you not go there after today also? Will you not more than ever before taste and see how good the Lord is? I hope you will do this. But if you will make the Word of God henceforth your daily study, it will bring you unspeakable blessings. It will increase and preserve your faith. It will give you hope. It will turn you from the allurements of the world to the one thing needful, to Christ and his righteousness. Is this not good? Is it not a mark of God's love? With this before you, can you not find comfort in the abiding love of God? Can you not, though it be with streaming eyes, say: 

>Nothing, I am certain, can turn God's love away from me, and now I pray God, that this affliction may not turn my love away from him, but may lead me to cling to him with childlike confidence." 

– From "The Abiding Love Of God As The True Source Of Comfort In Our Sorrows" By Rev. J. H. Schneider

# Contents of the Book (357 pages)

- Copyright Information
- Part I
    - 1. God’s Will For Our Little Ones By Rev. Prof. R. C. H. Lenski, D. D.
    - 2. The Heavenly Father’s Way With The Little Children By Rev. Walter E. Tressel, A. M.
    - 3. Saving Life By Taking Life By Rev. C. B. Gohdes
    - 4. It Is Well With The Child By Prof. J. N. Kildahl
    - 5. Jesus And The Children By Rev. Albert T. W. Steinhaeuser, D. D.
    - 6. Jesus’ Love For Children By Rev. G. J. Troutman
    - 7. “It Is Well” By Rev. J. W. Schillinger
    - 8. The Two Sides Of God’s Providence By Rev. L. H. Schuh, Ph. D.
    - 9. Death And Sleep – A Comparison And A Contrast By Rev. George J. Gongoware
    - 10. God Is Love By Rev. W. E. Schramm
    - 11. The Savior’s Word Of Comfort To Sorrowing Parents By Rev. H. J. Schuh
    - 12. Jesus Loveth Little Children By Rev. M. R. Walter
    - 13. The Sorrow Of Christian Parents Over The Death Of A Beloved Child By Rev. H. J. Schuh
- Part II
    - 14. Jesus Christ Is The Conqueror Of Death By Rev. L. H. Schuh, Ph. D.
    - 15. Jesus Is Lord Of Our Dead By Prof. G. J. Zeilinger
    - 16. Jehovah Hath Put A New Song In My Mouth By Rev. Walter E. Tressel, A. M.
    - 17. A Group Of Godly Men As Mourners By Rev. W. E. Schramm
    - 18. Come Unto Me By Rev. Walter E. Tressel, A. M.
    - 19. The Way To A Happy Home By Rev. H. P. Dannecker
    - 20. The Silver Cord Is Loosed, The Golden Bowl Is Broken By Rev. J. H. Kuhlman
    - 21. Be Thou Faithful Unto Death By Rev. Walter E. Tressel, A. M.
- Part III
    - 22. The Comfort Of Christian Mourners In The Hour Of Affliction By Rev. H. P. Dannecker
    - 23. I Am The Resurrection And The Life By Rev. Walter E. Tressel, A. M.
    - 24. What Comfort Do The Scriptures Give Us Concerning Our Dead? By Rev. L. H. Schuh, Ph. D.
    - 25. The Abiding Love Of God As The True Source Of Comfort In Our Sorrows By Rev. J. H. Schneider
    - 26. God’s Standard Of Greatness By Rev. C. B. Gohdes
    - 27. Thus Saith The Lord By Rev. W. E. Schramm
    - 28. Our Conversation By Rev. Simon Peter Long, D. D.
    - 29. A Vision Of Heaven By Rev. G. J. Troutman.
    - 30. Who Will Enter The Kingdom Of Heaven? By Rev. G. J. Troutman.
    - 31. The Path Of Life By Rev. W. R. Walter
    - 32. Faithfulness Is The Crowning Glory Of The Lord’s Servants By Rev. L. H. Schuh, Ph. D.
    - 33. The Illumination Of Death By Rev. Prof. David H. Bauslin, D. D.
    - 34. The Mystery Of Death Is Solved By Rev. J. W. Schillinger
    - 35. The Word Of God As The Only Source Of True Comport In Affliction By Rev. H. J. Schuh
    - 36. What Must I Do To Be Saved? By Rev. L. H. Schuh, Ph. D.
    - 37. The Heavenly Emigrant By Rev. Frederick B. Clausen
    - 38. The Lively Hope By Rev. Prof. George Rygh
    - 39. The Christian’s Knowledge Inadequate To Solve Every Problem By Rev. G. J. Troutman
- Part IV
    - 40. Life Here And Hereafter By Rev. Wm. Brenner
    - 41. I Know That My Redeemer Liveth By Rev. Walter E. Tressel, A. M.
    - 42. Behold, The Bridegroom Cometh! By Rev. W. Hoppe, D. D.
    - 43. The Christian’s Comfort In View Of Death By Prof. J. Stump, D. D.
    - 44. The Nation’s Duty At Its Chieftain’s Bier By Rev. W. N. Harley
    - 45. The Blessedness Of Those Who Die In The Lord By Rev. W. N. Harley
    - 46. The Death Of A Christian Mother By Rev. H. J. Schuh
    - 47. Stones Rolled Away By Rev. A. K. Bell
    - 48. Faith Beholds The Glory Of God By Rev. H. J. Schuh
    - 49. The Blessings Of God In The Life Of The Departed By Rev. H. J. Schuh
    - 50. What Lightens The Farewell Of A Christian Father From His Loved Ones In The Hour Of Death? By Rev. H. J. Schuh
    - 51. A Joyous Cry At The Approach Of Death By Rev. W. E. Tressel
    - 52. What Makes The Christian Willing To Depart? By Rev. L. H. Schuh, Ph. D.
    - 53. Our Departure By Rev. M. K. Hartmann
    - 54. Ripening For God’s Garner By Rev. C. K. Solberg 
    - 55. The Christian’s Comfort In The Hour Of Death By Rev. L. H. Schuh, Ph. D.
    - 56. A Soft And Downy Pillow For Our Dying Bed By Rev. R. C. H. Lenski
    - 57. A Prayer And A Promise For Old Age By Rev. J. Sittler


# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)

![book cover](/img/posts/188wd-schuh-funeral-sermons-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/188wd-schuh-funeral-sermons.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 188wd&body=Please send 188wd-schuh-funeral-sermons.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 188wd&body=Please send 188wd-schuh-funeral-sermons.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}
[^aBY]: [source](https://www.findagrave.com/memorial/108507163/john-henry-harms)