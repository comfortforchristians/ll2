---
date: 2019-08-06 05:00:35-04:00
publishDate: 2019-08-06 05:00:35-04:00
title: "No Hierarchy In The Ministerial Office by D. Worley"
slug: "s87-worley-no-hierarchy-in-ministerial-office"
categories: ["Lutheran Library Publications"]
tags: ["Worley", "Ministry", "Short Books", "Evangelical Review"]
authors: ["Worley, D."]
titles: ["No Hierarchy in the Ministerial Office"]
origpublishers: ["Evangelical Review"]
origdates: ["1860"]
synods: ["General Synod"]
---

"We have had occasion several times, in self-defense, to declare our conscientious difference from the brethren of the Missouri as well as from those of the Buffalo Synod, on the doctrine of the Ministerial office; by some whom we think a little sensitive in the matter, we have been soundly berated, both privately and publicly, for having done so. We feel it, therefore, due to ourselves, and to the cause of truth, to present a calm expression of truth as we believe it to be found in the Holy Scriptures. – D. Worley

# Book Contents

- The Ministerial Office, Distinct From The General Priesthood, But No Hierarchy.
- This Discussion By The Light Of Scripture
- The General Idea of the Church
- The Preaching Office Precedes The Church
- The Church Is Visibly Presented By The Word And Sacraments
- The Individual Congregation Has No Authority To Act For Other Churches
- 1. The Ministerial Office is directly appointed of God, and is essential to the organization of the Church.
- 2. There is a twofold call to the ministry
- 3. The general call to the ministerial office is ordinarily in the hands of those who already hold the office
- 4. The call to particular administrations proceeds sometimes from the Church and sometimes from the individual congregation
- 5. The general call to the ministry is not and cannot be in the congregation.

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/s87-worley-no-hierarchy-in-ministerial-office-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/s87-worley-no-hierarchy-in-ministerial-office.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request s87-worley-no-hierarchy-in-ministerial-office&body=Please send s87-worley-no-hierarchy-in-ministerial-office.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request s87-worley-no-hierarchy-in-ministerial-office&body=Please send s87-worley-no-hierarchy-in-ministerial-office.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2019-08-06
- _Version 4 update_:  2019-08-06
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
