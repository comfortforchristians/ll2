---
date: 2018-11-06 12:00:00
title: "The Blessing of Cheerfulness by James Russell Miller"
slug: "303-miller-blessing-of-cheerfulness"
categories: ["Lutheran Library Publications"]
tags: ["Short Books", "Russell", "Sanctification"]
authors: ["Miller, James Russell"]
titles: ["The Blessing of Cheerfulness"]
origpublishers: ["Thomas Y. Crowell & Company"]
origdates: ["1895"]
synods: ["Presbyterian"]

--- 
"We are set in this world to be happy. We should not falter in our great task of happiness, nor move ever among our fellows with shadows on our face when we ought to have sunlight.

"We have a mission to others — to add to their cheer. This we cannot do unless we have first learned the lesson of cheerfulness ourselves. We cannot teach what we do not know. We cannot give what we do not have.

"In this little book a lesson is set for you, my reader. It may seem a hard lesson to learn; nevertheless, it is one you want to learn, and one you can learn, if you will surrender your life wholly to the great Teacher.

– From the Preface by James Russell Miller

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)

![book cover](/img/posts/303-miller-blessing-of-cheerfulness-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/303-miller-blessing-of-cheerfulness.pdf)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 303&body=Please send 303-miller-blessing-of-cheerfulness.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}
[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 303&body=Please send 303-miller-blessing-of-cheerfulness.epub)

