---
date: 2018-05-03 12:00:00
title: "Sayings of Charles Porterfield Krauth"
slug: "s01-krauth-sayings"
categories: ["Lutheran Library Publications"]
tags: ["Krauth", "Short Books"]
authors: ["Krauth, Charles Porterfield"]
titles: ["Sayings of Charles Porterfield Krauth"]
origpublishers: ["The Christian Literature Company"]
origdates: ["1898"]
synods: ["General Council"]

--- 
This little book consists of pithy selections from the biography of Charles Krauth.  "Every sincere Christian should know Krauth."

This Lutheran Library "short" is taken from the two volume biography of Charles Krauth published by Adolph Spaeth.  Spaeth includes in his Preface to that work the following:

"The Motto chosen for this Memoir is Dr. Krauth's description of Martin Luther, in the biography of the great Reformer which he undertook shortly before his death – "Faithful to the Truth, and true to the Faith." It may be properly applied to Dr. Krauth himself. It represents his own religious and theological development. Faithful to the truth of God's everlasting Word, he became ever more true to the Faith of the Church of his Fathers, and in the end its most consistent, learned, and eloquent witness in the English language. If we mistake not, there are not a few in our American Lutheran Church who, under the influence of their early training, still have their difficulties with that faith of the fathers, but are earnestly endeavoring to overcome them. We trust that this Memoir may be of special service to all such honest inquirers."[^aRB]

[^aRB]: Spaeth. Life of Krauth. New York: Christian Literature Company, 1898. Lutheran Library edition in preparation. 

 
{{% toc %}}

# About the Author

[Charles Porterfield Krauth](/charles-porterfield-krauth/), D.D., LL. D., was born March 17, 1823, at Martinsburg, Va., son of Charles Philip K. and his wife, Catharine Susan Heiskell, of Staunton, Va. He was educated at Pennsylvania College and the theological seminary in Gettysburg. Having been licensed by the Synod of Maryland, in 1841, he took charge of the mission station at Canton, near Baltimore. In 1842 he became pastor of the Lombard Str. Church in Baltimore; 1847, at Shepherdstown and Martinsburg; 1848, in Winchester. On account of the ill-health of his wife he spent the winter 1852 to 1853 i n the West Indies, serving the Dutch Reformed congregation at St. Thomas', during the absence of its pastor. In 1855 he became pastor of the first English Lutheran Church in Pittsburgh, Pa., and in 1859 pastor of St. Mark's, Philadelphia. Later on he served the mission churches of St. Peter's and St. Stephen's, in Philadelphia. In 1861 he resigned the pastorate of St. Mark's in order to devote his whole strength to the editorship of The Lutheran, which in his hands became the strongest weapon in the conflict against the shallow, unprincipled " American Lutheranism " which ruled our English Lutheran Church of that time. He was pre-eminently fitted to transplant the spirit of true, historical, conservative Lutheranism into the sphere of the English language, and there to reproduce and establish it on such a basis, that its future should be secure. When the theological seminary at Philadelphia was founded, in 1864, he was appointed Norton professor of dogmatic theology, and at the installation of the first faculty he delivered the inaugural address, defining the theological position represented by that institution. 

In the establishment of the General Council he took an active and prominent part, being the author of the Fundamental Articles of Faith and Church Polity, adopted by the preliminary convention at Reading, 1866; of the constitution for congregations, adopted in 1880, and of the theses on pulpit and altar fellowship, presented in 1877. He was also actively engaged in the liturgical work of the Church, resulting in the publication of the Church Book. From 1870 to 1880 he was president of the General Council. In 1868 he was appointed professor of mental and moral philosophy at the University of Pennsylvania. From 1873 he held the position of vice-provost, and after the resignation of Provost Stills he carried the burden of this office for many months. After a journey to Europe which was undertaken, in 1880, not only for his own recuperation but chiefly in the interest of the Luther Biography with which the Ministerium of Pennsylvania had charged him, the chair of history at the University of Pennsylvania was given him in addition to all his other duties. But the burden proved too heavy. In the winter 1881-82, his work in the seminary was frequently interrupted through bodily weakness. He died January 2, 1883. 

He was one of the most prolific and brilliant writers of our English Lutheran Church.[^afZ]

[^afZ]: The Lutheran Cyclopedia. 1899

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)

![book cover](/img/posts/s01-krauth-sayings-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/s01-krauth-sayings.pdf)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request s01&body=Please send s01-krauth-sayings.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}
[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request s01&body=Please send s01-krauth-sayings.epub)

