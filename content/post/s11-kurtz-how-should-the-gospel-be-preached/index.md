---
date: 2019-08-02 05:44:00-04:00
publishDate: 2019-08-02 05:44:00-04:00
title: "How Should The Gospel Be Preached by Benjamin Kurtz"
slug: "s11-kurtz-how-should-the-gospel-be-preached"
categories: ["Lutheran Library Publications"]
tags: ["Kurtz", "Preaching", "Evangelical Review", "Short Books"]
authors: ["Kurtz, Benjamin"]
titles: ["How Should The Gospel Be Preached"]
origpublishers: ["Evangelical Review"]
origdates: ["1850"]
synods: ["General Synod"]
---
"There is a strait gait of knowledge through which [everyone] must pass on entering the kingdom, and many of the results of his reasonings must be abandoned at that entrance, while he confesses himself a mere disciple all the way in his progress." 

"The man of the mightiest genius or the most accomplished intellect, must become a docile child, as well as the most uncultivated sinner and the rudest savage — or never be spiritually renovated. 

"Does not the word of God address all men as Vandals? Does it not find them all alike in the same condition, needing the same spiritual regimen? Is it not designed for the poor and ignorant and outcast, as well as for the more favored classes, the learned and cultivated of this world? Or are the latter above the need of its helps, and capable of attaining the same ends in another way? – Benjamin Kurtz


# Book Contents
- How Should The Gospel Be Preached?
- I. Plainness.
- II. Fervor.
- III. Prayerful Dependence On God’s Spirit.

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/s11-kurtz-how-should-the-gospel-be-preached-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/s11-kurtz-how-should-the-gospel-be-preached.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request s11-kurtz-how-should-the-gospel-be-preached&body=Please send s11-kurtz-how-should-the-gospel-be-preached.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request s11-kurtz-how-should-the-gospel-be-preached&body=Please send s11-kurtz-how-should-the-gospel-be-preached.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2019-08-02
- _Version 4 update_:  2019-08-02
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
