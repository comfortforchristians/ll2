---
date: 2018-11-21 12:00:00
title: "Catechizations on Luther's Small Catechism by Henry Jacob Schuh"
slug: "282-schuh-small-catechism"
categories: ["Lutheran Library Publications"]
tags: ["Catechism", "Luther", "Schuh"]
authors: ["Schuh, Henry Jacob"]
titles: ["Catechizations on Luther's Small Catechism"]
origpublishers: ["Lutheran Book Concern"]
origdates: ["1915"]
synods: ["Ohio Synod"]

--- 
"Luther's Smaller Catechism is the best text book for religious instruction that has yet been offered to the church. For nearly 400 years it has held this place in our church and millions have drank this 'milk of the Gospel' as the very best food for spiritual babes. 

{{% toc %}}

Catechism is not only for children!  Schuh's guide to Luther's [Small Catechism](/302-jacobs-luther-small-catechism/) is an excellent resource on all the fundamentals of the Christian faith.  
 

Rev. Schuh writes,

"One of the weaknesses of our Sunday-school teaching has hitherto been that it made too little use of this _Layman's Bible_. Luther placed over each one of the chief parts the words: 'As the head of the family should teach them in all simplicity to his household.'

If the head of the average family is expected to teach the catechism it is certainly not asking too much of the average Sunday-school teacher to teach it. What more important things can we teach children than the Ten Commandments, the Creed, the Lord's Prayer and the Sacraments?

---

Be sure also to download [Luther's Small Catechism](/302-jacobs-luther-small-catechism/)

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)

![book cover](/img/posts/282-schuh-small-catechism-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/282-schuh-small-catechism.pdf)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 282&body=Please send 282-schuh-small-catechism.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}
[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 282&body=Please send 282-schuh-small-catechism.epub)

"Luther's _Smaller Catechism_ is the best text book for religious instruction that has yet been offered to the church. For nearly 400 years it has held this place in our church and millions have drank this "milk of the Gospel" as the very best food for spiritual babes. 



# Contents

- Introductory Questions.
    - Lesson 1. Religion.
    - Lesson 2. The Catechism.
    - Lesson 3. The Holy Scriptures.
- Part 1. The Holy Ten Commandments.
    - Lesson 4. The Law.
    - Lesson 5. The First Table Of The Law.
    - Lesson 6. The Preface To The First Commandment.
    - Lesson 7. What Is Forbidden In The First Commandment.
    - Lesson 8. What Is Commanded In The First Commandment.
    - Lesson 9. What Is Meant By The Name Of God.
    - Lesson 10. What Is Forbidden In The Second Commandment.
    - Lesson 11. What Is Required In The Second Commandment.
    - Lesson 12. The Lord’s Day.
    - Lesson 13. Keeping The Lord’s Day.
    - Lesson 14. The Second Table Of The Law.
    - Lesson 15. What Is Forbidden In The Fourth Commandment.
    - Lesson 16. What Is Commanded In The Fourth Commandment.
    - Lesson 17 What Is Forbidden In The Fifth Commandment.
    - Lesson 18. What Is Commanded In The Fifth Commandment.
    - Lesson 19. What Is Forbidden In The Sixth Commandment.
    - Lesson 20. What Is Commanded In The Sixth Commandment.
    - Lesson 21. Marriage.
    - Lesson 22. What Is Forbidden In The Seventh Commandment.
    - Lesson 23. What Is Commanded In The Seventh Commandment.
    - Lesson 24. What Is Forbidden In The Eighth Commandment.
    - Lesson 25. What Is Commanded In The Eighth Commandment.
    - Lesson 26. What Is Forbidden In The Ninth Commandment.
    - Lesson 27. What Is Commanded In The Ninth Commandment.
    - Lesson 28. What Is Forbidden In The Tenth Commandment.
    - Lesson 29. What Is Commanded In The Tenth Commandment.
    - Lesson 30. The Conclusion, The Earnest Threat.
    - Lesson 31. The Gracious Promise.
    - Lesson 32. The Use Of The Law.
    - Lesson 33. Sin.
    - Lesson 34. Original Sin.
    - Lesson 35. Actual Sin.
    - Lesson 36. The Consequence Of Sin.
- Part 2. The Three Articles of the Holy Christian Faith
    - Lesson 37. The Creed.
    - Lesson 38. The Gospel.
    - Lesson 39. The Difference Between Law And Gospel.
    - Lesson 40. The Being Of God.
    - Lesson 41. The Attributes Of God.
    - Lesson 42. The Trinity.
    - Lesson 43. The Will Of God.
- The First Article
    - Lesson 44. Faith.
    - Lesson 45. Creation.
    - Lesson 46. Good Angels.
    - Lesson 47. Bad Angels.
    - Lesson 48. The Creation Of Man And The Image Of God.
    - Lesson 49. Providence.
- The Second Article:
    - 1. The Person Of Christ
    - Lesson 50. The Names Of The Savior.
    - Lesson 51. The Divinity Of Christ.
    - Lesson 52. The Humanity Of Christ.
    - Lesson 53. The Two Natures Of Christ.
    - 2. Of The Office Of Christ
    - Lesson 54. Christ, Our Prophet.
    - Lesson 55. Christ, Our High Priest.
    - Lesson 56. Christ, Our King.
- The Two States Of Christ
    - 1. The State Of Humiliation
    - Lesson 57. Wherein It Consists.
    - Lesson 58. The Object Of Christ’s Humiliation.
    - 2. The State Of Exaltation
    - Lesson 59. The Descent Into Hell.
    - Lesson 60. The Resurrection Of Christ.
    - Lesson 61. Christ’s Ascension Into Heaven.
    - Lesson 62. Christ’s Coming To Judgment.
- The Third Article Of The Creed.
    - Lesson 63. The Person Of The Holy Ghost.
    - Lesson 64. Conversion.
    - Lesson 65. Contrition And Faith.
    - Lesson 66. The Order Of Salvation.
    - Lesson 67. Sanctification.
    - Lesson 68. Good Works.
    - Lesson 69. Preservation In The Faith.
    - Lesson 70. The Christian Church.
    - Lesson 71 The Christian Church (Continued).
    - Lesson 72. The Forgiveness Of Sin, Or Justification.
    - Lesson 73. The Resurrection Of The Body.
    - Lesson 74. Eternal Life.
    - Lesson 75. Election.
    - Lesson 76. Hell.
    - Lesson 77. Prayer.
    - Lesson 78. The Introduction To The Lord’s Prayer.
    - Lesson 79. The First Petition.
    - Lesson 80. The Second Petition.
    - Lesson 81. The Third Petition.
    - Lesson 82. The Fourth Petition.
    - Lesson 83. The Fifth Petition.
    - Lesson 84. The Sixth Petition.
    - Lesson 85. The Seventh Petition.
    - Lesson 86. The Conclusion.
    - Lesson 87. The Sacraments In General.
    - Lesson 88. What Is Baptism?
    - Lesson 89. Who Should Be Baptized?
    - Lesson 90. Sponsors And Confirmation.
    - Lesson 91. What Does Baptism Give Or Profit?
    - Lesson 92. How Can Water Do Such Great Things?
    - Lesson 93. What Does Baptism Signify?
- Confession
    - Lesson 94. Of Confession.
    - Lesson 95. What Sins Should We Confess?
    - Lesson 96. The Confessional Prayer.
- The Office of the Keys
    - Lesson 97. What Is The Office Of The Keys?
    - Lesson 98. What Do You Believe In Accordance With These Words?
- The Fifth Chief Part Of The Catechism, Or Lord’s Supper.
    - Lesson 99. What Is The Sacrament Of The Altar?
    - Lesson 100. The Earthly Elements.
    - Lesson 101. The Heavenly Elements.
    - Lesson 102. What Is The Benefit Of Such Eating And Drinking?
    - Lesson 103. How Can Bodily Eating and Drinking Do Such Great - Things?
    - Lesson 104. Who Receives The Sacrament Worthily?
    
---

