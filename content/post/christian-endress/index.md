---
date: 2019-07-14 05:32:55-04:00
publishDate: 2019-07-14 05:32:55-04:00
title: "Christian Endress: A Biographical Sketch"
slug: "christian-endress"
categories: ["Biographical Sketches"]
tags: ["Endress", "Lutheran Ministers", "Evangelical Review"]
authors: ["Endress, Christian", "Krauth, Charles Porterfield"]
synods: ["General Synod"]
---

>His life was devoted to the acquisition of knowledge.

The church has always associated with [Dr. Lochman's](/george-lochman/) name that of Dr. Endress. They were not only contemporary, but they were nearly of the same age. They commenced their career together and pursued their studies in company. They were graduated at the University of Pennsylvania and both for a season, gave instruction. They studied theology under the direction of Drs. [Helmuth](/henry-helmuth/) and Schmidt, entered the ministry the same year, were connected with the same ecclesiastical body through life, lived on the most friendly terms, and frequently labored in common efforts to promote the welfare of the church. They were called away from earth to heaven, with only a brief interval of separation. The grave had scarcely closed upon the one, before it opened for the other. _They were united in life; they were not divided in death_. The church had not yet laid aside its habiliments of mourning for one beloved son, when she was called to shed tears of sorrow over the tomb of another, upon whom she leaned for support, and whose services she highly prized. Dr. Endress had only recently officiated at the funeral obsequies of his friend; now the performance of the same sad office he himself requires. The work of life must terminate! Death, with his sickle, is always ready at the appointed time! 

>"By all of human race death is a debt \
> That must be paid: and none of mortal men \
> Knows whether till tomorrow life's short space \
> Shall be extended." 

{{% toc %}}

## Christian F. Endress - Early History

The subject of the present sketch was born in Philadelphia, in the year 1775, and died in 1827. He was, consequently, at the time of his death, in the fifty-third year of his age. At an early period in life, he commenced his studies, and was regarded as a youth of rare promise. He was graduated in 1790, at the University of Pennsylvania, in which he was engaged, for a time, in teaching. Already at this period he was distinguished for his scholarship and excellence, and afforded those presages of usefulness, which were afterwards so happily realized. His youthful piety led him to think of the sacred ministry, and soon after his graduation at college, he became a student of theology in his native city. He was licensed to preach the gospel in 1794, and immediately took charge of the congregations at Frankford, Pa., and Cohanzy, N. J. He continued for some time to reside in Philadelphia, and was employed during the week as a teacher in the English school, connected with the German Lutheran congregation of the city. Some of his pupils still remain, who retain for their old teacher a fond and grateful remembrance. 

## Easton, Pa.

It was in 1801 Dr. Endress received and accepted a call to Easton, Pa., where, with the exception of a single year, spent in the State of New York, he labored uninterruptedly, till 1815. On the death of Rev. Dr. Muhlenberg, he was elected pastor of the Lutheran congregation at Lancaster, Pa., and at once entered with diligence and ardor upon the duties of the position. Here was opened a wide field of usefulness, in which his learning, piety and signal abilities were all displayed, in a manner creditable to himself, and highly conducive to the interests of his charge. 

## Instrumental in Establishing English Language Services

Although he succeeded one of the most popular and successful preachers of the day, he enjoyed the full confidence and sincere regard of the people committed to his care. For a brief season, it is true, he was called to pass through a severe trial, and encounter serious difficulty, in consequence of his connection with the effort to introduce the English language into the exercises of public worship. The Germans regarded all attempts of the kind, as an innovation upon their rights, and almost everywhere resisted the wishes of those members in the congregation, who were anxious to make provision for the spiritual instruction of their families, unacquainted with the German language. Many injurious reports, prompted by a malignant feeling, were at this time circulated, and by some believed; but the Doctor lived sufficiently long to establish their groundlessness, and to vindicate the integrity of his character. The Germans withdrew from the church, and erected an edifice to be devoted exclusively to German services. Peace was thus secured. All opposition was soon silenced. Dr. Endress' course was subsequently approved, and the verdict of posterity has pronounced the charges preferred against a faithful pastor, as gross calumnies. He enlisted a host of ardent friends, whose attachment to him was greatly increased, in view of the fearless course he had pursued. They clung to him until the last, with the warmest and most tender affection. The effort to impair his influence and destroy his usefulness, was an entire failure. — The weapon employed proved powerless — 

>_Telum imbelle sine ictu._ 

Innocence is often most successfully vindicated by the very means employed to blast its reputation. Malicious attempts to injure character usually recoil upon the perpetrator. In the beautiful language of England's favorite bard: 

>"Virtue may be assailed, but never hurt, \
> Surprised by unjust force, but not enthrall'd, \
> Yea, even that, which mischief meant must harm. \
> Will in the happy trial prove most glory." 

## Illness

Dr. Endress died after a brief but painful illness, which was borne with great meekness and Christian fortitude. His congregation, whose interest lay near his heart, occupied most of his last thoughts, and elicited his deep solicitude. His end was calm and peaceful. He left the world sustained and cheered by the truths of the gospel, in the triumphs of a living faith, and the hope of a blessed immortality. He was buried in the old Lutheran graveyard in Lancaster, H. A. Muhlenberg, D. D., of Reading, Pa., performing the services of the occasion. Testimony to his excellencies is furnished in the following inscription, engraven upon the stone designed to perpetuate his memory: "This monument, which covers the remains of the Rev. Christian F. Endress, D. D., has been erected by his friends, as a mark of their affection and a tribute to his worth. He served this congregation, as their faithful pastor, for twelve years, and having completed thirty-three years of his ministry, and the fifty-second year of his age, he was, on the 30th of September, 1827, gathered to his fathers, a bright example of the power and confidence that spring from the faith, which he had so long and so faithfully taught." 

## Brilliant and Influential

Dr. Endress has always been regarded as a brilliant light in the Lutheran church. He was one of the ablest and most influential among our older divines. He was emphatically a strong man, whose mind was naturally capacious and comprehensive, subjected to the most careful culture, and brought under the influence of the most rigid discipline. He had, from his youth, enjoyed the best advantages, and was distinguished by the versatility of his powers, and the range of his acquirements. He was a finished classical scholar, and accomplished in almost every department of knowledge. He had the faculty of disentangling the most abstruse subjects, and presenting the truth clearly to the mind. He possessed a discriminating mind, a sound judgment, a quickness of perception, and great fluency of language. 

His life was devoted to the acquisition of knowledge, and his Theological learning was the result of deep and indefatigable study. He attained the highest honors of his profession, and from the University of Pennsylvania, he received the Doctorate of Divinity, in the year 1819. He wrote with equal facility in the German and English languages and, at the time of his death,he had in contemplation several works for the press. He had prepared for publication, a commentary on Paul's Epistle to the Romans, in reference to the merits of which Bishop White, of Pennsylvania, to whose judgment the manuscript had been submitted, expressed a most favorable opinion. We most sincerely regret that the friends of Dr. Endress have never permitted the work to see the light. 

It was the Doctor's habit to deliver lectures to his people on the different epistles of the New Testament. These, it is said, were very able, and were, no doubt, written with a view to publication. During his life, he frequently contributed to the pages of the _Lutheran Intelligencer_, and since his death, several of his sermons have appeared in the _Lutheran Preacher_ [^bFM]  

[^bFM]: In 1791 Dr. Endress published, in the German language, a duodecimo, entitled the "Kingdom of Christ not susceptible of union with temporal monarchy and aristocracy." 

>&emsp;&emsp;Ibimus, ibimus \
>Uteunque praecedes, supremum \
>Carpere iter comites parati. 

## Public Spirit

Dr. Endress was a man of public spirit. He manifested a zeal for the promotion of useful knowledge, and was interested in the elevation of the people. He was active in all the great movements of the denomination, to which he belonged, and wielded an important influence. He was recognized as a leading minister in the church, and repeatedly held the most responsible offices in the Synod, with which he stood in connection. He participated in the organization of the General Synod, and was one of its most active and devoted friends. 

Although Dr. Endress was attached to the principles of the church in which he had been reared and to whose service he had devoted his life, and yielded to none in the claim of sincerity in holding them, he was no sectarian. There was neither intolerance in his views, nor proscription in his intercourse with those, who differed from him in sentiment. He venerated the standards of the church, and defended them from misrepresentations, but he did not receive them as an absolute rule of faith. He would not permit them to supplant the Bible. He was not averse to confessions, but he proposed to rest upon human declarations of faith, only so far as they derive their light from the sacred Scriptures. His liberality of sentiment endeared him to thousands, and very much enhanced his influence. He had no sympathy with that narrow bigotry, so prevalent in the land, which is disposed to exclude from Christian interest and fellowship, everything which does not originate with itself, which sees no good in that which is not after its fashion, or which is not carried on under its own auspices. He condemned the spirit of intolerance and persecution, which is the reproach and scandal of any church in which it is found. He was not among the number of those, who are continually asserting their arrogant pretensions, and exclaiming, _the temple of the Lord, the temple of the Lord are we;_  who aim to shut out from the kingdom, all who will not endorse their sentiments and acknowledge their claims. There is common ground, on which all who love our Lord Jesus Christ in sincerity and truth can meet, and they should feel under obligations to sympathize on all those questions, which pertain to the cause of our common faith. We may differ from one another as to some subjects, which are not considered as essential, yet we can live upon amicable terms, and labor together for the advancement of Christ's kingdom! Shall not Christians dwell together in unity? Can we not discuss points of differences, in love? What excuse can be offered for the bitter spirit, petty jealousy, angry controversy, harsh expressions, and disgraceful epithets, which are sometimes employed by the members of Christ's body, and too often disgrace the Christian church? Does the gospel justify such a spirit? Does it comport with the believer's profession? 

>_Tantaene animis caelestibus irae!_

## Striking Personal Appearance

The personal appearance of Dr. Endress was rather striking. He was a man of athletic frame, six feet in stature, not corpulent, but muscular. His complexion was florid, his hair light, and his temperament sanguineo-nervous. He was distinguished for his urbanity and refined manners. He united the holy affinities of his office, and the delicate sensibilities of the finished gentleman. There was in his demeanor cheerfulness without levity, dignity without austerity, piety without pretension, religion without ostentation. In the discharge of his pastoral duties, he was most faithful. In preaching, in visiting his people, in catechizing the children and those of riper years, in relieving the poor, not only by personal efforts, but by interesting others on their behalf, and in all the multiplied and arduous labors of a devoted minister of the gospel he abounded. He will be long and gratefully remembered by the church. 

>"Peace to the just man's memory; let it grow \
> Greener with years, and blossom through the flight \
> Of ages; let the mimic canvass show \
> His calm benevolent features: let the light \
> Stream on his deeds of love that shunned the sight \
> Of all but Heaven, and in the book of fame \
> The glorious record of his virtues write. 
> And hold it up to men, and bid them claim \
> A palm like his, and catch from him the hallow'd flame." 



# Publication Information

- _Author_: __"Krauth, Charles P."__, Editor.
- _Journal_: __"The Evangelical Review."__
- _Originally Published_: 1854.
- _Lutheran Library Edition_: 2019
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

{{% alert note %}}
{{% biographical-sketch-note %}}
{{% request-pdf %}}
{{% /alert %}}
