---
publishDate: 2019-02-19 05:01:00-05:00
date: 2019-02-19 05:01:00-05:00
title: "George Henry Gerberding: A Biographical Sketch"
slug: "george-henry-gerberding"
categories: ["Biographical Sketches"]
tags: ["Gerberding", "Lutheran Ministers"]
authors: ["Gerberding, George Henry"]
synods: ["General Council"]
---

[![George Henry Gerberding](/img/posts/gerberding-george-henry.jpg)](https://www.lutheranlibrary.org/authors/gerberding-george-henry/)

# Rev. George Henry Gerberding. 

The subject of this sketch was born in Pittsburg, Pa., Aug. 21, 1847. His father, J. G. H. Gerberding, was born in Germany, but came to this country in his sixteenth year. His mother was a native of the United States. Her maiden name was Josephine Lustenberger. Her parents came from Switzerland. 

Rev Gerberding grew up on his father's little farm between Allegehany City and Perryville. There he attended a district school for a few months in each year, always ranking high in his classes. Up to his twentieth year he assisted his father on the farm. Then, for two winters he attended an academy in Pittsburg walking four miles back and forth most of the time. In the fall of 1869, he entered Thiel Hall at Phillipsburg, Beaver Co., Penn., where he enjoyed the instruction of Prof. Henry Eyster Jacobs, now Dr. Jacobs of the Philadelphia Seminary, as also of Rev. Dr. H. W. Roth, the writer of this sketch. 

In 1871, with two other classmates, he entered, _ad eundem_, the junior class of Muhlenberg College, Allentown, Pa., where he graduated in 1873. On account of the financial embarrassment of his father his college studies were frequently interrupted. During the four years' course he lost forty-three weeks, by absence, necessitated by labor at home. 

He took his theological course in the Evangelical Lutheran Seminary of Philadelphia. There he enjoyed the instruction of the sainted [Drs. Charles Porterfield Krauth](/charles-porterfield-krauth/) and Charles Frederick Schaeffer. As he paid his own expenses through the seminary, his course there was more or less irregular. During the vacations of 1874 and 1875 he assisted the [Rev. Dr. Passavant](/william-alfred-passavant/) in several churches near Pittsburg, which the Doctor was then supplying. In the latter summer, with the assistance of Dr. Passavant, he raised funds, secured a lot and had a church built in the tenth ward, Allegheny, near his father's home. In the spring of 1876 he was called as pastor to the Mount Calvary English Lutheran Church, at Chartiers, three miles below Pittsburg. He was ordained April 19, at Greenville, Pa., by the Examining Committee of the Pittsburg Synod. 

In connection with Mt. Calvary church, he labored across the river, where he had built the church during the previous summer. There he organized the English Lutheran church of Mt. Zion and built up a prosperous Sunday school and congregation. The following year he began to labor at Pine Creek, ten miles out from Mt. Zion. Here was an abandoned Lutheran field. Rev. Joseph Muhlhauser had recaptured the dilapidated old church, which had been used by various denominations, and had remodeled it for a Lutheran church. On the removal of Rev. Muhlhauser to Rochester, N. Y., Rev. Gerberding gathered together the available fragments of the former Lutheran congregation, and whatever new material could be found and organized St. John's English Lutheran church, with a live Sunday school. Ere long he began to preach in a school house in Butcher's Eun, and worked up a Lutheran Sunday school. This afterwards became Memorial Evangelical Lutheran Church. His labor on the north side of the river, along the Perrysville road, had now increased to such an extent that he found it necessary to resign Mt. Calvary, at Chartiers, and give his whole time to the three new points. A parsonage was also completed at Mt. Zion. Thus, where five years ago the Lutheran church had nothing, there were now two churches and congregations, three Sunday schools and a brick parsonage. Not a dollar had been asked or received from the mission fund, and the whole debt was $400. 

In the spring of 1881, Rev. Gerberding accepted a call to the Jewett, Ohio, charge, which he served acceptably for six years. During his pastorate the charge was divided and thus the almost abandoned church at Bowling Green was again enabled to enjoy the services of a pastor. While at Jewett he also made a beginning toward establishing an English mission in Stenbenville, Ohio. 

In the spring of 1887 he accepted an urgent call from the English Home Mission Committee of the General Council to the English mission of Fargo, N. D. At that difficult post, where there was not a single person who had ever been an English Lutheran, he has built up one of the largest Sunday schools in the city, and is slowly building up a substantial congregation in the beautiful church built by the able and self-sacrificing labors of Rev. F.W. Ulery. The influence of Rev. Gerberding has not been confined to the Fargo mission. He has won the confidence and created an interest in English work among the Germans and Scandinavians. He has called their attention to their losses, induced them to look after their children, to start English teaching in their Sunday schools and preaching in their churches. He took a hurried mission trip to the Pacific coast in the spring of 1889, and kept the General Council from losing the North West coast as a mission field. 

Rev. Gerberding was married Oct. 31, 1876, to Miss Annie E. Danver, of Allegheny City, Pa., whom he learned to know and love while attending Prof Gourley's academy in Pittsburg, Pa. Their happy union has been blessed with seven children, five of whom are living, two having preceded them to the home beyond. 

He is an earnest and forcible speaker. He aims to get the truth into the heart of the hearer, rather than to please the intellect by the beauty of the setting. He wields a strong and living pen. He is the author of "The Way of Salvation in the Lutheran Church" and "New Testament Conversions." The former has proved the most popular and successful work published in the English Lutheran church, almost 9,000 copies having been sold in three years. By giving to the church which he loves these two works, Rev. Gerberding has not only done a work which his church appreciates and which will bless it, but he has made for himself an enviable place among the authors of the Lutheran Church in America. 

{{% alert note %}}
This biography is taken from Jens Jensson's _American Lutheran Biographies_, published 1890.
{{% /alert %}}

First published online 2019 at [LutheranLibrary.org](.).