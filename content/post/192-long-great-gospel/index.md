---
date: 2018-12-14 12:00:00
title: "The Great Gospel by Simon Peter Long"
slug: "192-long-great-gospel"
categories: ["Lutheran Library Publications"]
tags: ["Long", "Sermons", "Start Here"]
authors: ["Long, Simon Peter"]
titles: ["The Great Gospel"]
origpublishers: ["Lutheran Book Concern"]
origdates: ["1904"]
synods: ["Ohio Synod"]

--- 
"Do not be like a dead fish in the stream, that always goes with the water; be a live fish and swim against the stream. Too many dead fish in the Church these days, they are anything and everything because others are.

Rev. Simon Peter Long was a faithful and beloved pastor in the Ohio Synod. These sermons were delivered live from Advent 1903 to Advent 1904.  This volume also includes his famous "Sermon on Secret Societies".

{{% toc %}}

# Be Something and Stand For It

"God's truth is too plain to have a thousand opinions about doctrine. Let us find out what God teaches, and believe it, and tell the people. I expect to get the respect of every citizen in this city, not by siding in with everybody, but by being a man – a man of God, and that is what I want you all to be. Dear young people, who have come into the Church of God, be something and stand for it, and dare to be singular, as Rahab was." – Simon Peter Long

# The Scripture-twisters 

"_A man who will turn and twist one Word of the Bible is no Lutheran_, I do not care by what name he is known. A Lutheran is a man who holds fast to the very form of God's Word, and I come to you today, and tell you to hold fast to the Lutheran Church, because the Lutheran Church stands for the form of God's Word." – From the sermon for Pentecost. 


# Contents

- __Pulpit Power__
- __A Remarkable Ride__  Matt. 21: 1-9. Advent Sunday.
- __Christmas Is Coming, And So Is Christ__  Luke 21:25-36. Second Sunday In Advent.
- __Our Christmas Catechism__  Matt. 11:2-11. Third Sunday In Advent.
- __An Advent Autobiography__  John 1: 19-28. Fourth Sunday In Advent.
- __Plain Philosophy For Poor People__  Luke 2:1-14. Christmas Morning.
- __The Christian Church__  Luke 2:33-40. First Sunday After Christmas.
- __Four Jewels From Jesus__  Luke 2:21. New Year’s Day.
- __The Christ-Child Crowned__  Matt. 2:13-23. Sunday After New Year.
- __How Heathen Reach Heaven__  Matt. 2:1-12. Epiphany.
- __The Lost Lord__  Luke 2:41-52. First Sunday After Epiphany.
- __A Marriage And A Miracle__  John 2:1-11. Second Sunday After Epiphany.
- __Which Way?__ Matt. 8:1-13. Third Sunday After Epiphany.
- __A Terrible Tempest__  Matt. 8:23-27. Fourth Sunday After Epiphany.
- __The Heavenly Harvest__  Matt. 13:24-30. Fifth Sunday After Epiphany.
- __The Transfiguration__  Matt. 17:1-9. Sixth Sunday After Epiphany.
- __God’s Gift Of Grace__  Matt. 20:1-16. Septuagesima.
- __The Royal Road To Ruin__  Luke 8:4-15. Sexagesima.
- __The Passion Proclamation__  Luke 18:31-43. Quinquagesima.
- __That Same Old Serpent__  Matt. 4:1-11. First Sunday In Lent. Invocavit.
- __A Beautiful Battle__  Matt. 15 21-28. Second Sunday In Lent.
- __Dumb Devils__  Luke 11:14-28. Third Sunday In Lent.
- __Let Nothing Be Lost__  John 6:1-15. Fourth Sunday In Lent.
- __Did Jesus Sin?__ John 8:46-59. Fifth Sunday In Lent.
- __(Reunion of Catechumens.) The Scarlet Thread.__ Joshua 2:15. Palm Sunday.
- Good Friday.
- __Resurrection Rocks__  Mark 16:1-8. Easter.
- __Sunday__  John 20:19-31. First Sunday After Easter.
- __The Heart Of History__  John 10:11-18. Second Sunday After Easter.
- __A Little While And A Long While__  John 16:16-23. Third Sunday After Easter.
- __The Truth Must Be Told__  John 16:5-15. Fourth Sunday After Easter.
- __The King’s Key__  John 16:23-30. Fifth Sunday After Easter.
- __The Celestial Counselor__  Mark 16:14-20. Ascension Day.
- __The Comforter Is Coming__  John 15:26 To 16:4. Sixth Sunday After Easter.
- __Hold Fast__  2 Tim. 1:13-14. Pentecost.
- __Who Are Heathen?__ John 3:1-15. Trinity Sunday.
- __Nine Reasons Why Rich And Poor Should Be Saved__  Luke 16:19-31. First Sunday After Trinity.
- __Divine Enthusiasm__  Luke 14:16-24. Second Sunday After Trinity.
- __Pity The Poor Pharisee__  Luke 15:1-10. Third Sunday After Trinity.
- __Going To God’s Gallery__  Luke 6:36-42.  Fourth Sunday After Trinity.
- __How The Savior Caught Simon__  Luke 5:1-11. Fifth Sunday After Trinity.
- __Admission Above__  Matt. 5:20-26. Sixth Sunday After Trinity.
- __Seven Loaves Of Bread And Seven Baskets Of Crumbs__  Mark 8:1-9. Seventh Sunday After Trinity.
- __Congregational Conscience__  Matt. 7:13-23. Eighth Sunday After Trinity.
- __Those Dirty Dollars__  Luke 16:1-9. Ninth Sunday After Trinity.
- __Why Did Jesus Cry?__ Luke 19:41-48. Tenth Sunday After Trinity.
- __The Heavenly Hunter__  Matt. 18:9-14. Eleventh Sunday After Trinity.
- __Two Sides Of Salvation__  Mark 7:31-37. Twelfth Sunday After Trinity.
- __The Lord’s Lodge__  Luke 10:23-27. Thirteenth Sunday After Trinity.
- __The Men, The Master And The Man__  Luke 17:11-19. Fourteenth Sunday After Trinity.
- __A Call For Catechumens__  Matt. 6:24-34. Fifteenth Sunday After Trinity.
- __A Rumor Of The Redeemer__  Luke 7:11-17. Sixteenth Sunday After Trinity.
- __Is It Right?__ Luke 14:1-11. Seventeenth Sunday After Trinity.
- __Silenced Sinners__  Matt. 22:34-46. Eighteenth Sunday After Trinity.
- __Sick Sinners__  Nineteenth Sunday After Trinity,  Matt. 9:1-8.
- __Speechless Sinners__  Matt. 22:1-14. Twentieth Sunday After Trinity.
- __One O’Clock__  John 4:46-54. Twenty-first Sunday After Trinity.
- __Two Things Sinners Can Not Do With Their Sins__  Matt. 18:23-35. Twenty-second Sunday After Trinity.
- __John’s Vision Of The Reformation__  Rev. 14:6-11. Reformation Sunday.
- __Young America__  Matt. 22:15-22. Twenty-third Sunday After Trinity.
- __Twice Twelve Are Twelve__  Matt. 9:18-26. Twenty-fourth Sunday After Trinity.
- __Dangerous Deceptions__  Matt. 24:15-28. Twenty-fifth Sunday After Trinity.
- __The Apostles’ Creed On The Judgment Day__  Matt. 25:31-46. Twenty-sixth Sunday After Trinity.
- __A Sermon On Secret Societies__  1 Kings 18:21. 

# That Holy Book Divine by Simon Peter Long

That Holy Book Divine. \
&emsp;So deep from first to last, \
Whole Book, each Book, each word, \
&emsp;Hold fast; hold fast; hold fast. 

The doctrines in that Book \
&emsp;Our Luther found at last \
And gave them to the world, \
&emsp;Hold fast; hold fast; hold fast. 

To Christ and not to man \
&emsp;To Christ "the first and last," \
To Him you cling for life. \
&emsp;Hold fast; hold fast; hold fast. 

The bird that has no wings \
&emsp;Must not sail on the mast; \
Nor must the human soul \
&emsp;By its own strength hold fast. 

Nor must it hold to hands \
&emsp;That pull to hell at last;  \
But cling to "that good thing," \
&emsp;Eternal life – hold fast. – Amen. 

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)

![book cover](/img/posts/192-long-great-gospel-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/192-long-great-gospel.pdf)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 192&body=Please send 192-long-great-gospel.mobi)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 192&body=Please send 192-long-great-gospel.epub)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2018-12-14
- _Version 4 update_:  
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)




