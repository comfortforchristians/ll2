---
date: 2019-01-03 12:00:00
title: "Scripture Selections for Daily Reading by Rev. Jesse Hurlbut"
slug: "274-hurlbut-scripture-selections-daily-reading"
aliases:
  - "/274-hurlbut-scripture-selections-daily-reading/"
categories: ["Lutheran Library Publications"]
tags: ["Short Books", "Devotional", "Hurlbut"]
authors: ["Hurlbut, Jesse"]
titles: ["Scripture Selections for Daily Reading"]
origpublishers: ["Hunt and Eaton"]
origdates: ["1891"]
synods: ["Methodist"]

--- 
"...A good selection of Scripture passages well suited for reading at family worship...chosen to furnish a reading for every day and to complete the bible in a year.

Rev. Jesse Hurlbut writes,

"It has seemed to me that there is need of a good selection of Scripture passages, of nearly uniform length, fitted for reading at family worship, so chosen as to furnish a reading for every day in the year and to complete the Bible in a year."

"But though all parts of the Bible are valuable, and even its driest genealogical tables have their uses to the student of history, yet all portions are not equally profitable for devotional purposes, nor adapted to reading in the family."

"In the selection of passages the endeavor has been to follow the general current of history in the Bible, choosing mainly such passages as have some plain spiritual purpose; interspersing psalms with historical portions, and placing the selections from the prophets and the epistles nearly where they belong in the historical order." 

"It is the prayer of the compiler that this little work may help to a better understanding, a deeper interest, and a richer enjoyment in the Word of Life. – Jesse Hurlbut, New York, September 9, 1890.

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)

![book cover](/img/posts/274-hurlbut-scripture-selections-for-daily-reading-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/274-hurlbut-scripture-selections-for-daily-reading.pdf)

---