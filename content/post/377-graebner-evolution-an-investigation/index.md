---
date: 2019-01-23 09:05:47 
title: "Evolution by Theodore Graebner"
slug: "377-graebner-evolution-an-investigation"
categories: ["Lutheran Library Publications"]
tags: ["Graebner", "Evolution"]
authors: ["Graebner, Theodore"]
titles: ["Evolution"]
origpublishers: ["Northwestern Publishing House"]
origdates: ["1922"]
synods: ["Missouri Synod"]
---
Theodore Graebner was a Missouri Synod professor at Concordia Seminary in St. Louis, and defender of the Six Day Creation account of Genesis.  Though he wrote _Evolution_ many years ago, the points he makes are still valid.  This small book can be very helpful to Christians who wish to "detox" from the [religion of Scientism](https://www.alecsatin.com/what-is-scientism/).


{{% toc %}}

# Evolution in Text Books

"It is only in high-school texts...that the evolutionary theory as propounded by Darwin is still treated as if it enjoyed among scientific men the same respect as the multiplication table.  Speaking in the Darwinian dialect we should say that the authors of these school-texts constitute a case of "arrested development." – Theodore Graebner, from _Evolution: An Investigation and A Criticism_

# Evolution and the Historical Fact of Christ's Resurrection

"The resurrection of Jesus Christ is the central fact of our Christian belief and it is, rightly understood, the all-sufficient answer to the theory of evolution. 

"Christ's resurrection is an historical fact fully as much as the defeat of Xerxes at Salamis in 480 B. C, the discovery of America by Columbus in 1492, and the peace of Versailles of 1919 are historical facts, proven by the word and record of contemporary witnesses. 

"But if Christ was raised then we have proof for the following tenets, all contradicting evolutionary speculation at so many vital points: 

"1) The _existence of a personal God_ who is concerned with human affairs; 

"2) The _reality of miraculous interference_ with natural forces; 

"3) The _truth of atonement_ and the redemption, and 

"4) The _inspiration_ of the Old Testament Scriptures (hence also _of the creation account in Genesis_). 

"...a little patient study will bring to light the fact that each of these four basic ideas is dove-tailed, mortised and anchored so firmly in the fact of Christ's resurrection, that you can get rid of them all only by denying that fact. 


# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/377-graebner-evolution-an-investigation-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/377-graebner-evolution-an-investigation.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 377-graebner-evolution-an-investigation&body=Please send 377-graebner-evolution-an-investigation.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 377-graebner-evolution-an-investigation&body=Please send 377-graebner-evolution-an-investigation.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}

