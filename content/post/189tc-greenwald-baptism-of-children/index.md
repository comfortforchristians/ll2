---
date: 2018-02-22 12:00:00
title: "The Baptism of Children by Emanuel Greenwald"
slug: "189tc-greenwald-baptism-of-children"
categories: ["Lutheran Library Publications"]
tags: ["Greenwald", "Baptism"]
authors: ["Greenwald, Emanuel"]
titles: ["The Baptism of Children"]
origpublishers: ["Lutheran Book Store"]
origdates: ["1872"]
synods: ["Ohio Synod"]

---
Rev. Greenwald was born near Frederick, Maryland, Jan. 13, 1811, and was, like the prophet Samuel of old, dedicated by his pious parents to the holy office from his earliest infancy. His theological studies were pursued under the private supervision of Rev. David F. Schaeffer, who similarly prepared no less than fourteen other young men, in his own parsonage, for the work of the ministry. 

{{% toc %}}

# The Importance of Infant Baptism

"The Lutheran Church attaches much importance to Infant Baptism. The Augsburg Confession, Article 9, expressly teaches 'that children are to be baptized;' declares that 'by Baptism they are offered to God,' and asserts that thereby they 'are received into God's favor.' It also expressly 'condemns the Anabaptists who allow not the Baptism of children.'" 

"In many localities our members live in the midst of those denominations who oppose the Baptism of children, and they need aid to enable them to defend themselves from attack, as well as to strengthen their own convictions of truth and duty. Such aid, it is hoped, this little manual will afford, which is prepared with special reference to their needs.

# Baptism Makes One a Church Member

"The question of the right of our children to be in the Church with their parents is scarcely inferior in importance to that of the right of the parents themselves to such a connection. _The Sacrament of Baptism is known by all to be the divinely appointed initiatory ordinance by which we are initiated into the Church as members of it. He that is baptized is a member of the Christian Church, and is constituted such by the Sacrament of Baptism. He, on the other hand, that is not baptized, is not a member of the Christian Church, but is out of it_."

 

# Contents (39 pages)

- About the Author
- Copyright Information
- Preface.
- Do children have a right under the Gospel to Church-membership by Baptism?
  - 1. The Church-membership of children was directly instituted and positively commanded by the Lord himself as far back as the time of Abraham, in a covenant made with him, called expressly the Gospel, or covenant of grace, which Church-membership was not only not revoked by Jesus Christ, but was confirmed and established by him.
  - 2. Children are included in the promise connected with that covenant.
  - 3. Children are included in Christ’s invitation, and declared to he fit subjects for the New Testament Church or kingdom.
  - 4. Children are included in Christ’s original command commissioning and authorizing his ministers to administer the Sacrament of Baptism.
  - 5. Children are the lambs of Christ’s flock, as their parents are the sheep of his fold, and, therefore, their relation both to the Shepherd and to the flock forbids that they should be excluded from the fold.
  - 6. Children were included in the host of the Israelites as they were baptized unto Moses in their passage through the Red Sea, and which is declared to he an example or type of Christian Baptism.
  - 7. Children were included in the numerous family or household Baptisms that are recorded in the Scriptures.
  - 8. Children are included in the uniform practice of the universal Christian Church from the time of the apostles to the present time in the administration of Baptism.
- Conclusions
  - 1. All persons who were baptized in their infancy may he satisfied with their Baptism.
  - 2. It is the duty of all parents to present their children for holy Baptism.
- Upon the Consciences of Parents
  - 1. Christ’s command includes children. (Matt. 28:19)
  - 2. Baptism initiates into Christ’s Church. (John 3: 5)
  - 3. Baptism is in the place of circumcision. (Col. 2:11-13)
  - 4. Baptism is a means of regeneration.
  - 5. Baptism, like circumcision, brings the soul into covenant relation with God. (Gen. 17: 10)
  - 6. Infant Baptism must be practiced, because the promise is to the children. (Acts 2:38-39)
  - 7. Baptism is really a saving ordinance; not, of course, as a mere opus operatum, a mere manipulation of the hands. But in God’s economy it is a sacrament of saving efficacy, so says 1 Peter 3:21

# More About Emanuel Greenwald

"Dr. Greenwald's first parish was New Philadelphia, Ohio, and all the adjoining country within a radius of fifteen miles in every direction. At one time he supplied fourteen preaching points on Sundays and week-days. In 1842 he was elected as the first editor of the Lutheran Standard, and from 1848 to 1850 he was the president of the English District Synod of Ohio. 

"The years 1851 to 1854 were spent in the city of Columbus, during which time he held many responsible positions on important boards, committees, etc. In September, 1854, he accepted a call to the pastorate of Christ Church, Easton, Penn., which he faithfully served for twelve years. His fourth and last parish was Holy Trinity Church of Lancaster, Penn., in which he labored from May, 1867, up to his death in December, 1885. He began preaching every Thursday evening at a mission point in the northern part of the city, which soon developed into Grace Church, and afterwards started another mission in the western section which was the nucleus of Christ Church. An assistant being necessary for the increasing field, Rev. Charles S. Albert served in this capacity, then Rev. David H. Geissinger, then Rev. John \V. Rumple, then Rev. C. Elvin Haupt, then Rev. Ezra K. Reed, then Rev. Charles L. Fry. Long after his own generation will his memory continue to be revered as an ideal pastor and a man of pre-eminent saintliness. C. L. F.[^acK]

[^acK]: Jacobs, Henry Eyster, ed. (1899) *The Lutheran Cyclopedia*. New York: Charles Scribner's Sons 

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)

![book cover](/img/posts/189tc-greenwald-baptism-of-children-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/189tc-greenwald-baptism-of-children.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 189tc&body=Please send 189tc-greenwald-baptism-of-children.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 189tc&body=Please send 189tc-greenwald-baptism-of-children.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}
