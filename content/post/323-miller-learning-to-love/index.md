---
date: 2018-11-26 12:00:00
title: "Learning to Love by James Russell Miller"
slug: "323-miller-learning-to-love"
categories: ["Lutheran Library Publications"]
tags: ["Short Books", "Russell", "Sanctification"]
authors: ["Miller, James Russell"]
titles: ["Learning to Love"]
origpublishers: ["Thomas Y. Crowell & Co."]
origdates: ["1910"]
synods: ["Presbyterian"]

--- 
Learning to love is a long lesson. It takes all of the longest life to learn it. The most inveterate obstacle in mastering the lesson is self, which persists with an energy which nothing but divine grace can overcome. When no longer we seek our own in any of our relations with others, we have learned to love. Until then we still need to stay in Christ's school.

James Russell Miller writes in this little book:

The problem of Christian living is to keep love in the heart, year after year, even though bearing wrong and injustice continually. That was the way Jesus did. His enemies reviled him, denied him, mocked him, betrayed him, but he loved on and never grew bitter. His heart was as gentle at the last as it was at the beginning. No matter how men hurt him, the wound healed itself instantly. That is the problem of love with us. "Love endureth all things," "never faileth." "For fifteen years," one said, "I have had to bear daily outbursts of anger, with abusive words and unkind accusations, in my home; must I go on keeping sweet just as if I received only sweetness?" Well, that was Christ's way. That is what love means... 

This is a most delicate test of life. Of whom do we think first in deciding that we will, or will not do a certain thing — of ourselves or of others? Love does not ask, "What is in it for me? Will it benefit me? Will it bring me gain? Will it make me happy?" Instead of this, it asks, "Will this be a blessing to others? Will it impart happiness to them? Will it give comfort and enrichment of life?" "Love seeketh not its own." 

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)

![book cover](/img/posts/323-miller-learning-to-love-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/323-miller-learning-to-love.pdf)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 323&body=Please send 323-miller-learning-to-love.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}
[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 323&body=Please send 323-miller-learning-to-love.epub)

