---
date: 2019-05-31 04:00:28-04:00
publishDate: 2019-05-31 04:00:28-04:00
title: "Sweet First Fruits: A Tale To Muslims On The Truth And Virtue Of The Christian Religion by Sir William Muir"
slug: "452-muir-sweet-first-fruits"
categories: ["Lutheran Library Publications"]
tags: ["Muir", "Islam", "Missions"]
authors: ["Muir, William"]
titles: ["Sweet First Fruits"]
origpublishers: ["Religious Tract Society"]
origdates: ["1893"]
synods: ["Unknown"]
---

"_Sweet First Fruits_ is a delightful story primarily designed to give scope and opportunity for presenting to the Muslim reader the proofs of the Christian faith, the purity and genuineness of our Bible, its attestation by the Koran, and the consequent obligation on Muslims to obey its precepts. The argument is developed, in the dialectic style, between a party of Christian converts and their former companions.

"...The Muslim world has never, since the rise of Islam, had an appeal made to it under more favorable circumstances, nor one more likely to ensure respect, if not force conviction. Differing from all former treatises, it contains not one word offensive to the Muslim, beyond the strength and conclusiveness of the reasoning, which, indeed, is mainly drawn from the Koran itself."

# Book Contents
- Preface.
- Introduction.
- 1. The Epistle
- 2. Deep Waters.
- 3. Second Conference.
- 4. Danger Threatens.
- 5. Third Conference.
- 6. Intimidation And Alarm.
- 7. Brought Before The Court, And Sent To Prison.
- 8. Conspiracy To Entrap Omar Al Haris, Who Is Condemned And Beheaded.
- 9. Results Following The Execution — Riot In City — Friends Visit The Converts.
- 10. Conference With Cazi And Mufti On The Claims Of Christianity.
- 11. Discussion Continued Between The Cazi And Sheikh Ali.
- 12. Exile To Deyr Al Camr, And Return.
- 13. Death And Funeral Of Sheikh Ali — Conclusion.

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/452-muir-sweet-first-fruits-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/452-muir-sweet-first-fruits.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 452-muir-sweet-first-fruits&body=Please send 452-muir-sweet-first-fruits.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 452-muir-sweet-first-fruits&body=Please send 452-muir-sweet-first-fruits.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2019-05-31
- _Version 4 update_:  2019-05-31
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
