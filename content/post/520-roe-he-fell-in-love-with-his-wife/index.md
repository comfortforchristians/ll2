---
date: 2019-08-12 05:12:07-04:00
publishDate: 2019-08-12 05:12:07-04:00
title: "He Fell In Love With His Wife: a novel by Edward Roe"
slug: "520-roe-he-fell-in-love-with-his-wife"
categories: ["Lutheran Library Publications"]
tags: ["Roe", "Fiction"]
authors: ["Roe, Edward Payson"]
titles: ["He Fell In Love With His Wife"]
origpublishers: ["Dodd, Mead & Company"]
origdates: ["1886"]
synods: ["Presbyterian"]
---

This book was inspired by a newspaper account telling of a widowed farmer who visited the county poor house, looking for a good housekeeper. He is supposed to have said, "If there is a worthy woman here, I will marry her."

From the dust jacket:

"A simple, strong story of American life.

"The stern, silent hero is a farmer, a man with honest, sincere views of life, and of sufficient education to make him an alien among the other farmers. Bereft of his wife, to whom he had been sincerely attached, his home is cared for by a succession of domestics of varying degrees of inefficiency.

"At last, from a most unpromising source, comes a young woman applicant that the farmer feels is the housekeeper he needs. He decides to marry her before she enters his service to protect them both from the coarse suspicions of the villagers.

"Thus enters into the grim history of this man's life a romance as bright and delicate as a golden thread, developing on both sides a love that could surmount all difficulties and survive the censure of friends as well as the bitterness of enemies."

From a review by Julian Hawthorne (son of Nathaniel Hawthorne):

>"The more I think over the book the better I like it in all its parts. Upon the whole I think that Mr. Roe has written the best American novel that has been published this year." – New York World


# Book Contents

- Reviews
- 1. Left Alone
- 2. A Very Interested Friend
- 3. Mrs. Mumpson Negotiates and Yields
- 4. Domestic Bliss
- 5. Mrs. Mumpson Takes Up Her Burdens
- 6. A Marriage!
- 7. From Home to the Street
- 8. Holcroft’s View of Matrimony
- 9. Mrs. Mumpson Accepts Her Mission
- 10. A Night of Terror
- 11. Baffled
- 12. Jane
- 13. Not Wife, But Waif
- 14. A Pitched Battle
- 15. “What is to Become of Me?”
- 16. Mrs. Mumpson’s Vicissitudes
- 17. A Momentous Decision
- 18. Holcroft Gives His Hand
- 19. A Business Marriage
- 20. Uncle Jonathan’s Impression of the Bride
- 21. At Home
- 22. Getting Acquainted
- 23. Between the Past and Future
- 24. Given Her Own Way
- 25. A Charivari
- 26. “You Don’t Know.”
- 27. Farm and Farmer Bewitched
- 28. Another Waif
- 29. Husband and Wife in Trouble
- 30. Holcroft’s Best Hope
- 31. “Never!”
- 32. Jane Plays Mouse to the Lion
- 33. “Shrink from You?”

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/520-roe-he-fell-in-love-with-his-wife-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/520-roe-he-fell-in-love-with-his-wife.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 520-roe-he-fell-in-love-with-his-wife&body=Please send 520-roe-he-fell-in-love-with-his-wife.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 520-roe-he-fell-in-love-with-his-wife&body=Please send 520-roe-he-fell-in-love-with-his-wife.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2019-08-12
- _Version 4 update_:  2019-08-12
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
