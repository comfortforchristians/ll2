---
date: 2019-06-05 07:23:40-04:00
publishDate: 2019-06-05 07:23:40-04:00
title: "Eternal Punishment by Lewis Miller"
slug: "s31-miller-eternal-punishment"
categories: ["Lutheran Library Publications"]
tags: ["Miller", "Hell", "Evangelical Review", "Short Books"]
authors: ["Miller, Lewis G. M."]
titles: ["Eternal Punishment"]
origpublishers: ["Evangelical Review"]
origdates: ["1879"]
synods: ["General Synod"]
---
"They would have us believe that all hearts will finally be moved and melted by the love of God. The tender love of Jesus to poor, fallen man, did not move and melt the hearts of all with whom he came into contact here. The proud, hard heart of Scribe and Pharisee grew all the harder when the light and warmth of His presence fell upon them — no melting there. Hereafter they go on sinning, and go on hardening, as we see in that very passage in Luke 13, in which Canon Farrar takes one of his texts. The Lord there says: 'Depart from me, all ye workers of iniquity.' – Lewis Miller


# Book Contents

- Eternal Punishment.
- Is the Bible the Word of God?
- The Scientific Method
- Nature and the Bible Two Volumes of The Same Book
- Facts first, then Theory
- Mr. Beecher’s Sermon
- The Mystery of Iniquity
- Canon Farrar’s Statements
- “Damnation”, “Hell”, and “Everlasting”
- More Denials of Eternal Punishment by Canon Farrar
- Is Future Punishment “Purifying” and “Corrective”?
- Conclusion

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/s31-miller-eternal-punishment-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/s31-miller-eternal-punishment.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request s31-miller-eternal-punishment&body=Please send s31-miller-eternal-punishment.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request s31-miller-eternal-punishment&body=Please send s31-miller-eternal-punishment.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2019-06-05
- _Version 4 update_:  2019-06-05
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
