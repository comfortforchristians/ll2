---
date: 2017-09-17 12:00:00
title: A Concise Summary of The Lutheran Confessions by Henry Eyster Jacobs
slug: "117tc-jacobs-book-of-concord-epitome"
categories: ["Lutheran Library Publications"]
tags: ["Jacobs", "Book of Concord", "Catechism"]
authors: ["Jacobs, Henry Eyster"]
titles: ["A Concise Summary of the Lutheran Confessions"]
origpublishers: ["Lutheran Theological Seminary"]
origdates: ["1911"]
synods: ["General Council"]

---
Even 150 years ago there were Evangelical Christians who were suspicious of confessions and creeds.  Why read them at all? [Dr. Jacobs](/henry-eyster-jacobs/) opens the preface to his 1882 American translation of the *Book of Concord* with these words:

>The Church's Confessions of Faith are its authorized declarations on subjects concerning which its teaching has been misunderstood or misrepresented, or is liable to such misunderstanding and misrepresentation. 

{{% toc %}}

# The Love of the Truth

Christians with a *love of the truth* have it tough today.  Churches of every denomination are embracing practices which set rifts between congregants.  Yet the spirit of the age is said to be one of *tolerance* – which is interpreted to mean *anything you believe can be true for you, just don't try to tell me what's right or wrong for me*.  In other churches such as those of the Reformed "9-Marks" variety, a wrong use of the law causes terrible damage to the Lord's people.[^Bd]

Good and sound Biblical teaching is the answer to all of this.  Even for non-Lutheran Christians, familiarity with the most important Confession of the Reformation is a protection against all kinds of stumblings.  The battles going on today have ancient roots.  There is nothing new under the sun. 

# The Concise Summary of the Lutheran Confessions

The *Epitome* is the first part of the Formula of Concord, which is the final section of the Lutheran Confessions.  It was written by Jakob Andraea and Martin Chemnitz with help from Nikolaus Selnecker, David Chytraeus, Andreas Musculus and Christoph Körner and with input from other theologians. The clarity and specificity of the writing did away with most of the controversies which had arisen in the first decades after the adoption of the Augsburg Confession by the Evangelical Churches of the Reformation.

The Editor's hope is that reading and re-reading this little book will draw you closer to Christ and his marvelous work of salvation.  To whom be all glory and honor, now and forever.  *Amen*

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download) 

![book cover](/img/posts/117tc-jacobs-book-of-concord-epitome-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/117tc-jacobs-book-of-concord-epitome.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 117tc&body=Please send 117tc-jacobs-book-of-concord-epitome.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 117tc&body=Please send 117tc-jacobs-book-of-concord-epitome.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}
[^Bd]: "Victims of their heavy-handed, authoritarian abuses...are taking to social media in ever-increasing numbers to let their fellow pew-sitters hear about the outrageous abuses they have suffered in churches who have implemented the 9Marx manifesto."-[Todd Wilhelm](http://thewartburgwatch.com/2016/09/19/9marks-attempting-brand-enhancement-guest-post-by-todd-wilhelm/)   
