---
date: 2018-08-16 12:00:00
title: "The Passion Story as Recorded by the Four Evangelists by Solomon Erb Ochsenford"
slug: "213wd-ochsenford-the-passion-story"
categories: ["Lutheran Library Publications"]
tags: ["Ochsenford"]
authors: ["Ochsenford, Solomon Erb"]
titles: ["The Passion Story as Recorded by the Four Evangelists"]
origpublishers: ["General Council Publication Board"]
origdates: ["1908"]
synods: ["General Council"]

--- 
"This little book aims at the presentation of a connected narrative of the interesting and important events connected with the passion, death, resurrection and ascension of the Lord Jesus Christ, as recorded by the four Evangelists – Matthew, Mark, Luke and John. 

"It is a compilation of the record presented in the Gospels and is here presented in the words of Holy Scripture. The best Harmonies of the Gospel have been consulted with great care and the order of events carefully collated, according to the days of the week, in order that the reader may be furnished with a connected and harmonious narrative of the momentous and significant events of the memorable week during which our blessed Lord suffered death for the sin of the world and worked out a perfect redemption for man. 

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)

![book cover](/img/posts/213wd-ochsenford-the-passion-story-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/213wd-ochsenford-the-passion-story.pdf)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 213wd&body=Please send 213wd-ochsenford-the-passion-story.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}
[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 213wd&body=Please send 213wd-ochsenford-the-passion-story.epub)


