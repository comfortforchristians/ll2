---
date: 2019-04-29 19:29:14-04:00
publishDate: 2019-04-29 19:29:14-04:00
title: What's Wrong With The World? by George H. Gerberding
slug: "111ln-gerberding-whats-wrong-with-the-world"
categories: ["Lutheran Library Publications"]
tags: ["History", "Philosophy", "War", "Gerberding", "Liberalism", "Apostasy"]
authors: ["Gerberding, George Henry"]
titles: ["What's Wrong With The World?"]
origpublishers: ["Lutheran Theological Seminary, Maywood, IL"]
origdates: ["1919"]
synods: ["General Council"]

---
"Unbelieving and unrighteous men do hate the old Church doctrines. Why? Because these old teachings as to sin, guilt, retribution, the fact and need of a divine-human vicarious atonement the need of sovereign grace, the need of the divinely instituted means and all that these fundamental teachings imply – these teachings are unwelcome to the reason of the natural man. They are not the teachings that unaided reason would or could originate. They claim and proclaim an authority superior to reason, an authority that is ultimate and absolute. They boldly declare the insufficiency, the nothingness of reason in dealing with the things that pertain to God and His Kingdom. They demand that natural, human reason must bow, must abdicate before the reasoning of God."

In this small volume, Dr. Gerberding presents one of the clearest explanations available of the underlying philosophies which led directly to the wars of the Twentieth Century and the stupefaction of today.

{{% toc %}}

# Chapters

- 1. Introduction
- 2. What’s Wrong With Germany?
- 3. German Rationalism
- 4. Rationalism’s Roots
- 5. Germany’s Pantheism
- 6. Pantheistic State Craft
- 7. Materialism. Determinism. Socialistic Atheism.
- 8. Militarism. School Craft. Intolerance.
- 9. Pessimism. Bitter Fruit.
- 10. Goethe. Schleiermacher. Claus Harms. Ritschl. Eucken.
- 11. A Plea For American Fair Play And Christian Charity
- 12. France. Religion And Literature
- 13. French Revolution. French Philosophy.
- 14. What’s Wrong With England? Some Sore Spots.
- 15. Apostasy. Darwinism.
- 16. England’s Debasing Literature
- 17. America. Good Beginnings. Later Apostasy.
- 18. Social And Spiritual Dangers
- 19. The Peace Treaty
- 20. The Church’s Part In Righting The Wrong
- 21. The Church’s Part In Righting The Wrong
- 22. The Church’s Part In Righting The Wrong
- Forward to the Original Edition

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)

![book cover](/img/posts/111ln-gerberding-whats-wrong-with-the-world-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/111ln-gerberding-whats-wrong-with-the-world.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 111ln&body=Please send 111ln-gerberding-whats-wrong-with-the-world.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 111ln&body=Please send 111ln-gerberding-whats-wrong-with-the-world.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2017-09-11 
- _Version 4 update_: 2019-04-29
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
