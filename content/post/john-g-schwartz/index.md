---
date: 2019-08-25 05:10:14-04:00
publishDate: 2019-08-25 05:10:14-04:00
title: "John G. Schwartz: A Biographical Sketch"
slug: "john-g-schwartz"
categories: ["Biographical Sketches"]
tags: ["Schwartz", "Lutheran Ministers", "Evangelical Review"]
authors: ["Schwartz, John", "Krauth, Charles Porterfield"]
synods: ["Synod of South Carolina"]
---
It is seldom that the church is called to mourn the premature death of one, so highly gifted as the subject of the present sketch. He was a young man of rare attainments and extraordinary promise. Endeared to all by his talents, his virtues and his piety, he was taken away in the morning of life, and from a scene of active and useful exertion. 

>_Semper honos nomenque tuum, laudesque manebunt_. \
>[Name and praise will remain.]

{{% toc %}}

## John G. Schwartz.

In him were united qualities, which seemed, in a peculiar manner, to lit him for the position, to which he had been elected. He had just commenced his career under circumstances the most auspicious. The most sanguine expectations had been excited. Everything conspired to produce the impression, that this servant of God would become one of the greatest ornaments, which have adorned the church of Christ in this country. Had he lived, he would certainly have left his mark high on the scroll of history. But his destiny was soon fulfilled, and his work accomplished. He was translated from the field of his earthly labors and honors; his Master required him for another and higher sphere. 

>"Death loves a shining mark, a signal blow! \
>He calls for victims from the fairest fold, \
>And sheathes his shaft in all the pride of life." 

The solace of the church under the afflictive dispensation was, that it was the stroke of our Heavenly Parent, who is infinitely wise and good — who doeth all things well, and is never found to deny comfort to those who ask reverently that _His will, not theirs, be done_. God took him, and God loveth Zion!  "Even so, Father, for so it seemed good in thy sight." 


## Early Life and Education

Professor Schwartz was born in the city of Charleston, S. C., July 6th, 1807. His parents were both pious and communicant members of the Lutheran church, under the pastoral care of Rev. Dr. Bachman. They were deeply interested in the spiritual welfare of their son, and at an early period, instructed him in the principles of virtue and religion. Although he was deprived of his father when only twelve years of age, the influence of his exemplary life and pious example remained. The impressions received were indelible. The seed sown produced abundant fruit. His mother's watchful care and affectionate counsels, too, were not without their appropriate effect. The early incentives to virtue and goodness, which she furnished, were never lost upon the son. He was restrained from wandering into forbidden paths. His morals were protected from the dangers to which they were exposed. Parental fidelity was rewarded. The child was prepared for usefulness in this life, and trained for happiness in the skies. The father had dedicated his son, when quite young, to God, and before his death, fondly cherished the hope that he might be fitted for the Christian ministry. When near the close of his earthly pilgrimage, he called his pastor[^bGE] to his bedside and said: "This boy has given me such proofs of possessing talents — he seems so religiously inclined, that I thought if I should live, I would try to give him an education, so that, by God's blessing, he might become a minister of the gospel, but as I fear I am only a short time for this world, I will take it kind in you, if you will encourage him, should he continue to feel so disposed, and I trust God will bless you for this act of kindness." The request of the dying man was not disregarded. 

[^bGE]:  Rev. John Bachman. D D., LL D of Charleston, S. C.. 

The grief occasioned by his bereavement had, however, scarcely subsided, when young Schwartz visited his pastor for the purpose of obtaining counsel in reference to his studies, as he was desirous, in accordance with the wishes of his father, of devoting himself to the sacred office. The Doctor gave him advice appropriate to his tender years, but fearing lest his resolutions had been hastily adopted and in a measure influenced by the suggestions of friends, he told the lad to weigh the question carefully for one year, and if at the expiration of that period, his inclinations continued, he would direct him further. They met frequently during the year, but there was never any allusion made to the topic, which had been the subject of inquiry, although the young man was exceedingly correct in his deportment, and his studious habits had attracted attention. "Our conversation," says his pastor, "had almost escaped my recollection, when he one day presented himself before me. It appeared an unusual visit at an unusual hour, for it was early in the morning on a rainy day. He told me he had come to remind me of his promise to give me the result of another year's reflection; that it was that day a year, since I had encouraged him to call, and he had come punctually, to say that his feelings and wishes were still the same, and that his resolution to devote himself to the service of the church, remained unchanged." 

From this period, the subject of our narrative appeared to feel that he was called to preach the gospel, and at once commenced his studies with this end in view. Dr. Bachman, in whose family he now spent much of his time, became warmly attached to him; a friendship sprang up, by which they were for years intimately united, and which was dissolved only by death. He took a deep interest in his friend's progress, and devoted several hours every Saturday to giving him instruction. For some time young Schwartz was a regular pupil of Dr. Jones, but the principal part of his Academical education he received at the school of the German Friendly Society. He entered with ardor upon the pursuit of knowledge, and soon became distinguished as a scholar, occupying a prominent rank among his associates. 

In the fall of 1824 he entered the Junior class of the South Carolina College at Columbia, and in 1826 was graduated with the highest honors of his class. His amiable disposition and marked abilities had won ail hearts, and had rendered him, whilst at college, the favorite of students and instructors. One of his professors at the time wrote: "He is not only among the best scholars, but one of the best young men the institution has, for several years, graduated." 

It was in the year 1824, before leaving home for college, that he made a profession of religion, and was confirmed according to the usages of the Lutheran church, although his actual conversion long preceded this public testimony of his faith in Jesus. The principles he professed, he always carried into practice, and adorned the doctrines of the gospel. He never forgot that his solemn vows were upon him. To the means of grace and the ordinances of the sanctuary, he faithfully attended, and seemed to enjoy communion with God's people. He was deeply interested in the prosperity of Zion. For her welfare his prayers ascended; to her he was willing his cares and toils should be given; her heavenly ways he prized beyond his highest joy. He loved, too, the church in which he had been reared. Only a short time before his death he remarked, "The more I study the principles of our church, the more am I convinced that they contain the true doctrines of the Bible; and the more they are studied, the more will they be admired." Yet he was free from all sectarian feeling. He honored the conscientious convictions of those, who differed from him, and was disposed to cooperate in works of benevolence with all who loved the Lord Jesus. 

## Theological Training and Missionary Tour

During the Senior year of his collegiate course, in addition to the studies of the class, Mr. Schwartz turned his attention to the study of Theology, the prosecution of which he continued after his graduation, under the direction of Dr. Bachman. In the summer of 1827, before he had reached his twentieth year, he preached, with great acceptance, his first sermon in the Lutheran church of his native city and, during the absence of the pastor, for several weeks supplied the pulpit twice every Lord's Day, and also attended, during the week, to other religious exercises, in addition to his daily labors as teacher of a Grammar school in the city. The same year he was licensed to preach the gospel by the Synod of South Carolina, and immediately engaged in itinerant missionary service, visiting nearly all the middle and upper districts of the State, and frequently officiating every day in the week. He preached in the humblest and most destitute places, adapting his language and manners to the minds, that required the plainest kind of instruction, and laboring to build up the kingdom of the Redeemer. 

The performance of this itinerant service was not without the happiest effect upon the young missionary himself, his Christian experience was strengthened, and his own heart was disciplined; he became acquainted with the wants of the church, and learned to sympathize more deeply with the desolation that prevailed. It might be a question, whether such labor ought not to be exacted from every candidate before ordination. The report, which Mr. Schwartz presented respecting the condition and wants of our people in the regions he visited, stirred up our church in the State, and led to renewed efforts to supply the vast destitution. Means were devised by the Synod for gathering our scattered members into congregations, and furnishing them with the preached word. 

## Assistant Professor of Ancient Languages

On his return from his missionary tour, Mr. Schwartz accepted the appointment of Assistant Professor of Ancient Languages in the Charleston College. He was induced to take this step, chiefly from a desire to pursue his Theological course still further. His duties in this field of labor he performed with great satisfaction to the Trustees, and to the young men entrusted to his care. He soon, however, tendered his resignation to the Board, as he discovered that the faithful discharge of his collegiate duties afforded him little leisure for Theological study. He felt that he was called to devote himself to the active duties of the ministry, and that he could not appropriate his time to any object, which would lead him aside from his peculiar work. He therefore relinquished his present situation, with all its advantages and literary prospects, and after a trip to the North for the restoration of his impaired health, he resumed his missionary labors with increased interest and concentrated energy. 

## Charge of Four Mission Congregations

From this time commences the most active and interesting period of his life, which although brief, was eminently owned and blessed of God, and secured the affections of all who witnessed his laborious and self-denying exertions. He was disposed to make any sacrifices, and to embark his all in the cause of the gospel, frequently declaring that he could never be happy in the pursuit of any other course, lie was located in a district of country regarded, for many years, as unhealthful, having the charge of four congregations. His labors were arduous, and he was exposed to frequent attacks of disease; and although he received the most eligible proposals to locate elsewhere, nothing could induce him to surrender a field of labor, in which he was doing good. The number of his hearers greatly increased. New members were added to the church, and the congregations begged the "Society for the Promotion of Religion," from which he had received his appointment, that the services of the missionary might be continued, in the expectation that they could themselves raise for him an adequate support. 

## Need for Ministers in South Carolina 

As early as the year 1829, preparatory measures had been taken for the establishment of a Theological Seminary by the Synod of South Carolina, in consequence of the insufficiency of ministerial supplies. The great obstacle to the progress of our church in the South, was the want of clergymen. Our doctrines were approved, new congregations were organized, but the number of pastors did not meet the wants of the church. It was found impossible to procure a supply from the North. Said the Synod, 

>"We have applied in vain for aid. So wide a field is opened to our sister Synods in the North and West, that they have no ministers to send us, and it is believed our only permanent dependence, under the blessing of God, will be upon pious individuals, who will hereafter be educated for our church; who are natives of the State within the bounds of our Synod, and who are attached to our institutions, and accustomed to our climate." 

The establishment of a school of the prophets in the South, was regarded by many of the brethren as essential to the preservation of the church, and therefore, in humble reliance on the divine blessing, the enterprise was commenced, under the auspices of the Synod of South Carolina. But it encountered, at first, much opposition.. Many doubted the feasibility of the project. There was a strong prejudice in many of the churches against all institutions of the kind. These difficulties were, however, gradually removed, a zeal was awakened in the effort, and funds were raised for the purpose; it was therefore determined that the institution should go, at once, into operation. 

The first thing which then engaged their attention, was the election of a Professor for the important and responsible position. The eyes of all were immediately turned to Mr. Schwartz, although only twenty-three years of age. His piety, his talents, his education, the high estimation in which he was held by the church, directed attention to him as the proper incumbent for the office. He received the unanimous vote of the Synod. Says one who was present on the occasion: 

>"After his election there was a pause of many minutes, when he arose to address us. For a time his feelings almost prevented the power of utterance. He at length proceeded to thank us for our favorable opinion — stated his sense of his incapacity to discharge the duties of the station to which he had been elected — pointed out its difficulties; but signified his willingness to undertake it by the help of God, and entreated our prayers and intercessions, and those of all Christians, in his behalf. The youth of the individual, the occasion, the importance of the subject, and the feeling and eloquent address, melted the whole audience into tears, and I am sure that few, who were there present, will ever forget the impressive scene."

The Professor soon after entered upon his arduous duties. As circumstances prevented the immediate location of the seminary, and his congregations in Newberry and Lexington seemed so desirous of retaining his services for the year, he was permitted to continue among them, and to receive, for the present, such students as might offer, at his residence in Newberry. Several young men soon presented themselves for instruction, and to the work assigned him he began to devote himself with his whole strength. He appeared deeply impressed with the responsibilities of his position, and his dependence upon the Great Head of the church for success. He writes to a ministerial brother:

>"I have taken on myself a burden of responsibility almost greater than I can bear, yet God's grace is sufficient for me, and I trust that with his blessing, I shall at least perform my duty faithfully and conscientiously. I stand, however, in need of the prayers of my friends, and I call upon those at whose request I consented to accept the situation; I call upon my brethren in the ministry to aid me by their prayers and their counsels."

From the following extract taken from one of his letters written at this time, it will be seen that he entertained correct views concerning the sacred office. 

>"All the young men," 

says he, 

>"now with me, are promising, and if their hearts be right in the sight of God, I have no doubt they will prove a blessing to our church. The heart is known, however, only to God — we can judge only by the outward appearance; but did I think that any of these students were deficient in proper views of religion, and of the ministerial office, I should feel it my duty to advise them not to enter the institution. I dread the idea of being instrumental in educating any one for the holy office of the ministry, who, through a want of personal religion, may bring disgrace upon our sacred calling. Whilst I can testify to the consolations and encouragements which the Christian minister will receive at the hands of God, in the midst of the peculiar discouragements and difficulties, which belong to his profession, I believe those difficulties and discouragements to be of such a character as to drive any one from his office who does not feel the supporting comfort of God's presence. I could not, therefore, advise any person to enter upon this work, without being convinced that he experiences religion in his own soul, and the importance of that duty, that commands him to preach this religion to others." 

But just as the prospects of the Seminary were brightening, and the Professor was growing upon the affections of the church, the sanguine hopes of our Southern friends were crushed — their expectations were disappointed. 

>&emsp;&emsp;"On earth \
>There is no certainty, no stable hope." 

## Illness 

During the summer months the District, in which Professor Schwartz lived, was generally sickly, and he had proposed to transfer the institution, for a season, to a more healthful location, but as there was then in this congregation an unusual attention to serious subjects, he thought he could not desert his people. In reply to the remonstrances of his friends, he says, 

>"I am incurring some risk by remaining I know, but am I not in the hands of God? Has he not hitherto helped me? If it please him to remove me, by any means, from the church, will lie, who is the Head of the church, permit it to suffer thereby? I would not be presumptuous in my confidence, but am I not authorized to commit myself and all my concerns into the hands of him who hath said, 'Lo I am with you always!' Happy shall I be, if I be the humblest instrument of glorifying his Almighty name! I feel that I am in the hands of Providence, and I find the more I can realize my dependence upon God, the more cheerful, contented and happy I am. If God shall see fit to remove me, more will be accomplished by my death than could be by my life." 

Soon after he was seized with a violent attack of fever, which at first seemed to yield to the influence of remedial agencies, but the disease returned with increased severity and, on the 26th of August, 1831, terminated his valuable life, in the twenty-fourth year of his age. 

His last moments were such as might have been expected. _Constans et libens fatum excepit_. He suffered pain, but no murmur escaped his lips. His mind was calm and comfortable. His faith was unclouded. Conscious of his approaching end, he was troubled with no fears or doubts. His soul was sustained by that precious word of God, which he had treasured up in childhood. To one of his Physicians he said, 

>"See, Doctor, how much better it is to make our peace with God in time of health, than to wait until we are laid on a bed of sickness, for repentance in a dying hour is seldom of any avail." 

To a dear friend he remarked, 

>"Be not distressed on my account, for whether I live or die, all will be well." 

Just before he expired he exclaimed, 

>"I shall soon enjoy the glorious light of heaven, happiness and immortality. I am not afraid to die, for I know that my Redeemer liveth." 

He passed away without a struggle or a groan, having the faculties of mind and of speech till the last. As he drew near the grave, his face wore the expression of calm submission; the bright anticipations of his soul shone forth in the lineaments of his countenance. 

>"The room I well remember; and the bed \
>On which he lay; and all the faces too, \
>That crowded dark and mournfully around. \
>This I remember well; but better still \
>I do remember, and will ne'er forget \
>The dying eye — that eye alone was bright, \
>And brighter grew, as nearer death approached." 

His remains were buried in the cemetery of the Bethlehem church of Newberry District. The largest concourse that ever assembled in that part of the country, gathered around his grave. It was said that language could not describe the feelings of the community on the sad occasion. No one could have died more generally beloved or more sincerely lamented. No one could have possessed a stronger hold on the affections of the people, or enjoyed public confidence in a higher degree. All classes, rich and poor, old and young, white and black, bore testimony to the worth of the deceased. Long will his virtues abide in memory, "despite the ruins of the tomb." In addition to the religious exercises, conducted by several of our ministers at the time of the funeral, the occasion was still further improved, and a most eloquent and impressive discourse delivered, in the city of Charleston, by his former pastor, Rev. Dr. Bachman, from the words: "Be thou faithful until death, and I will give thee a crown of life." 

## His Character

In Professor Schwartz were united qualities of the highest character. His intellect was of the first order, and had been cultivated in the first schools. His perceptions were clear and accurate, his mind remarkably well balanced. He had practical good sense and a discriminating judgment. He was an accomplished scholar, and his authority in classical literature was acknowledged by all. His attainments in Hebraistic studies were considerable, and it is said he was a proficient in the German and French languages. He was also well read in Theology for one of his age. He loved to study, and it was his high resolve to extend and amplify his stores of knowledge. Had his life been spared, he would undoubtedly have exerted a decided influence upon the church. He was regarded as a most interesting and impressive preacher. Scriptural truth was always distinctly exhibited. All who listened to him were struck with the fervor of his eloquence, and the deep piety which pervaded his discourses. The arrangement of his thoughts was lucid, the construction of his argument logical, and his diction full and appropriate. In all his religious opinions he was thoroughly and decidedly evangelical. His convictions of sin were deep and abiding. His faith in the atonement of Christ, as the only remedy for our fallen nature, grew daily stronger and deeper until the last, when faith was lost in light, and hope in full fruition. 

His piety was perhaps the most striking feature of his character. He seemed habitually to walk with God. He was earnestly conscientious and faithful to his convictions. He seemed never to lose the sense of the Divine presence. He started out in life with the feeling, that no man liveth to himself, and that it was his duly to exert all his powers to do good. He left his peaceful home, and the attractions of society, and retired into a sickly part of the State, and thence he writes: 

>"Here in the woods of Carolina I suspect my lot is cast — here I shall live, and here shall I die. To be instrumental in doing good and enlarging the Redeemers kingdom, is all I ask."

His talents, his influence, his affections, were all consecrated to Christ, and laid at the foot of the cross. From a boy he was active in the Sabbath School, in the circulation of the sacred volume, in the advocacy of the Temperance Reform, and in all philanthropic and Christian efforts. In all the relations of society he was exemplary and faithful. In his intercourse he never forgot his office or his responsibility. Religion formed a frequent topic of remark, even when he was engaged in cheerful conversation, and his correspondence often took a serious turn. His thoughts seemed detached from the world and directed to heaven. All things were to him full of God, and he loved to speak of his goodness and mercy. He was a man of amiable temper, conciliatory spirit, warm sympathy, and great kindness of heart. He was free from all affectation. In everything that he did, he was perfectly natural. Sincerity was a prominent characteristic. The words he uttered came from his inmost soul. He did nothing for gaining popularity. He never appeared to have any selfish ends to gratify, or any hidden schemes to produce future results. He manifested all that he felt. Nor was he one of those, who conclude that nothing is well done, the paternity of which they cannot claim. He was neither elated by success nor depressed by failure. In adversity he was hopeful, in prosperity humble. He was unaffected by any change in his condition or by reverse of circumstance. His mind, so well balanced, was always prepared for the vicissitudes of fortune. 

>_Sperat infestis, metuit secundis_ \
>_Alteram sortem bene praeparatum_ \
>_Pectus_. 

Inflexible integrity and uniform consistency animated all his conduct. His course of life was unique, bearing upon one point, and in accordance with the laws of that higher nature, whose rightful supremacy he recognized, and whose dictates lie strove to obey. 

>"Early had he learned \
>To reverence the volume that displays \
>The mystery, the life which cannot die: \
>What wonder if his being thus became \
>Sublime and comprehensive! Low desires. \
>Low thoughts, there had no place, yet was his heart \
>Lowly; for he was meek in gratitude, \
>Oft as he called those ecstasies to mind, \
>And whence they flowed." 

## From A Letter Rev. Schwartz Wrote To A Friend

We cannot, perhaps, more appropriately conclude our sketch of Professor Schwartz, than by giving an extract from one of his last letters, written to a young friend, in whose spiritual good he was warmly interested. The spirit which it breathes, and the counsel it contains, cannot fail to increase our admiration of its author. The lessons, it suggests, may prove profitable to the reader. 

>"1st. Never forget that you have a soul, that must live after the body is dead — that is capable of eternal happiness at God's right hand, or may be banished forever from the presence of God, and consigned to darkness and everlasting despair. The thought of this will help you to deny yourself sinful gratification and sensual indulgence. 

>2nd. Endeavor to keep the fear of God constantly before your eyes. Remember that the Searcher of hearts is always looking down upon you; that you are in his hands, and that he is able to raise you to heaven, or sink you down to hell. Remember that His eyes are always upon you, and you will 'learn to do well and fear to do evil.' 

>3rd. Make it a rule, wherever you are, to let. nothing keep you from the house of God on the Sabbath, except it be actual sickness. When we neglect the church our souls begin to be in danger. 

>4th. Make it a rule never to lie down at night, nor to commence the labors of the day, without thanking God for his mercies, and praying to him for his protection and favor. This will be of immense advantage to you in assisting you to do good, and in helping you to avoid sins. 

>5th. Keep out of the way of temptation. It is the part of a wise man to keep at a distance from danger. We are so weak, that if we give the least opportunity to our besetting sins, they soon get the better of us. Always recollect then to avoid that kind of company and those places, in which you know there is danger. 

>6th. Seek good company, and avoid the society of such as show themselves to be the enemies of God by profanity, desecrating God's Holy Sabbath, and by other immoral practices. 

>Lastly. Think always that you, as well as all men, are a fallen creature, a rebel against God, and that you can be saved only through the merits of that Savior who loved us, and gave himself for us. Oh! never forget that salvation is by the cross of Christ. Pray to God to help you to believe in Jesus, and give your heart to him, to be renewed and to be sanctified."

# Publication Information

- _Author_: __"Krauth, Charles P."__, Editor.
- _Journal_: __"The Evangelical Review."__
- _Originally Published_: 1854.
- _Lutheran Library Edition_: 2019
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

{{% alert note %}}
{{% biographical-sketch-note %}}
{{% request-pdf %}}
{{% /alert %}}
