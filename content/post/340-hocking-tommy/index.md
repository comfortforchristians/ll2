---
date: 2019-02-27 11:46:43-05:00
publishDate: 2019-02-27 11:46:43-05:00
title: "Tommy by Joseph Hocking"
slug: "340-hocking-tommy"
categories: ["Lutheran Library Publications"]
tags: ["Hocking", "War", "Fiction" ]
authors: ["Hocking, Joseph"]
titles: ["Tommy"]
origpublishers: ["Hodder and Stoughton, Ltd."]
origdates: ["1916"]
synods: ["Methodist"]
---

"Only a small part of this story is imagination. Nearly every incident in the book was told me by "Tommy" himself, and while the setting of my simple tale is fiction, the tale itself is fact.

"My only qualification for writing this simple story of "Tommy" is that I have tried to know him, and that I greatly admire him. I met him before he joined the army, when for more than six months I addressed recruiting meetings. I have also been with him in training camps, and spent many hours talking with him. It was during those hours that he opened his heart to me and showed me the kind of man he is. 

"Since then I have visited him in France and Flanders. I have been with him down near La Bassée, and Neuve Chapelle. I have talked with him while great guns were booming as well as during his hours of well-earned rest, when he was in a garrulous mood, and was glad to crack a joke "wi' a man wearin' a black coat." 

"I have also been with him up at Ypres, when the shells were shrieking over our heads, and the "pep, pep, pep" of machine guns heralded the messengers of death. We stood side by side in the front trenches, less than a hundred yards from the German sand-bags, when to lift one's head meant a Hun's bullet through one's brain, and when "woolly bears" were common. 

"That is why I hope the story of "Tommy" will not only be read by thousands of men in khaki, but by their fathers and mothers and loved ones who bade them go to the Front, and who earnestly pray for their speedy and victorious return, even as I do. – Joseph Hocking

# About the Author

Joseph Hocking was a faithful Welsh minister.  A prolific and popular writer in his lifetime, Rev. Hocking considered the novel an ideal platform for exploring Christian spirituality and the deeper aspects of life.  His books deal with trials, difficulties and issues of faith.

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/340-hocking-tommy-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/340-hocking-tommy.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 340-hocking-tommy&body=Please send 340-hocking-tommy.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 340-hocking-tommy&body=Please send 340-hocking-tommy.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}

