---
date: 2018-10-28 12:00:00
title: The Autobiography of Rev. Joseph Hamilton Fesperman
slug: "141lb-fesperman-life-of-a-sufferer"
categories: ["Lutheran Library Publications"]
tags: ["Biography", "Fesperman", "Illness"]
authors: ["Fesperman, Joseph"]
titles: ["The Autobiography of Rev. Joseph Hamilton Fesperman"]
origpublishers: ["The Young Lutheran Company"]
origdates: ["1892"]
synods: ["Synod of the South"]

---

Those who struggle with the loneliness of dark days or the isolating pain of chronic illness may find gentle solace in the the thoughts of this dear brother in Christ.

# Solitude in Suffering

"As I sit here in pain, without a single token of sympathy from the human race, I am profoundly grateful to God for my precious Savior. That Christ came to save me, reconciles me to the numerous and depressing afflictions of my life. Amid them all, my world is strictly within myself, and its openings look out on immortality. Amid the vexations and disquieting scenes of my earthly suffering, I forget not the song of my pilgrimage, "I live, yet not I, but Christ liveth in me." This thought illumes the darkness without and hallows all within. – From Chapter 5

# "I live, yet not I, but Christ liveth in me"

'Amid the vexations and disquieting scenes of my earthly suffering, I forget not the song of my pilgrimage, "I live, yet not I, but Christ liveth in me." This thought illumes the darkness without and hallows all within.

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download) 

![book cover](/img/posts/141lb-fesperman-life-of-a-sufferer-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/141lb-fesperman-life-of-a-sufferer.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 141lb&body=Please send 141lb-fesperman-life-of-a-sufferer.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 141lb&body=Please send 141lb-fesperman-life-of-a-sufferer.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}
[^acK]: Jacobs, Henry Eyster, ed. (1899) *The Lutheran Cyclopedia*. New York: Charles Scribner's Sons 