---
date: 2019-06-07 03:37:20-04:00
publishDate: 2019-06-07 03:37:20-04:00
title: "The Doctrine of Justification by Matthias Loy"
slug: "171tc-loy-doctrine-of-justification"
categories: ["Lutheran Library Publications"]
tags: ["Loy", "Justification", "Faith","Start Here"]
authors: ["Loy, Matthias"]
titles: ["The Doctrine of Justification"]
origpublishers: ["The Lutheran Book Concern"]
origdates: ["1882"]
synods: ["Ohio Synod"]

---
"Human reason and inclination are always in their natural state averse to the doctrine of __Justification by faith__. Hence it is no wonder that earth and hell combine in persistent efforts to banish it from the Church and from the world.

"This great doctrine of the sinner's justification by faith in the Redeemer of the world, who lived and suffered and died to save our lost race, is the very soul of the supernatural revelation given in Holy Scripture. But it is, therefore, also the doctrine against which the attacks of Satan are mainly directed, and against which the world and the flesh most obstinately array themselves.

"Satan in his malice hates the Saviour, as he hates the salvation of man, whose ruin he has compassed; and man in his pride despises the gracious plan which divine wisdom has formed for his deliverance, because that plan gives no credit to his genius for devising nor to his power for executing it. </p><p>– Matthias Loy</p>

{{% toc %}}

# Peace is not proof of Justification

"The peace which we feel in believing must not be made the ground or condition of our justification. It would seem superfluous to add this caution against a procedure that is so absurd, were it not that many are actually guilty of the strange absurdity, and thus rob themselves of all peace by leaving it without a foundation. They saw off the limb upon which they sit. Supposing themselves justified because they have peace, and considering this the only satisfactory reason for thinking themselves justified at all, they set aside the only evidence upon which the soul can have assurance – namely, that of the Word – and trust in effects, which are not uniform, and the testimony of which is therefore precarious. – Matthias Loy. Chapter 5

# If the Apostle Paul is rejected, Faith Cannot Exist

"There can be no true faith where the supremacy of God's Word is not recognized, and where assent is not given to every declaration as of divine authority, to which the whole soul must be in subjection. The believer stands in awe of the great King's words, "_bringing into captivity every thought to the obedience of Christ._" (2 Cor. 10: 5.) Where one word of the Lord, being known as such, is rejected, faith cannot exist; because the authority on which all rests is repudiated." – From Chapter 4

# Contents (152 pages)

- About the Author – Matthias Loy
- Copyright Information
- Preface To The First Edition
- Preface To The Second Edition
- Introduction
- Chapter 1. The Nature Of Justification
  -  Justification Not A Declaration Defining The Sinner’s Moral Condition.
  -  Justification Not A Divine Act Making The Sinner Just.
  -  Justification A Divine Declaration Changing The Sinner’s Relation To God.
- Chapter 2. The Ground Of Justification
  -  The Ground Of Justification Not Man’s Natural Worthiness.
  -  The Ground Of Justification Not Any Human Acquirement
  -  The Ground Of Justification Is The Grace Of God And The Merits Of Christ.
- Chapter 3. The Means Of Its Bestowal
  -  Justification Requires Means To Bestow It
  -  The Word Of God
  -  The Holy Sacraments
  -  The Divine Bestowal Not The Human Possession
- Chapter 4. The Means Of Its Reception
  -  No Means Of Reception Besides Faith
  -  Faith The Designated Means Of Reception
  -  No Condition To Be Fulfilled Before Faith Avails
  -  The Nature Of Justifying Faith
  -  How Faith Justifies
  -  Degrees In Faith, But Not In Justification
- Chapter 5. Its Effects
  -  It Gives The Conscience Peace
  -  It Secures Sanctification.
  -  It Renders Glory To God.
  -  Conclusion

# About the Author – Matthias Loy

"Matthias Loy was born March 17, 1828 in Pennsylvania.  After a boyhood of poverty, he was apprenticed as a printer in Harrisburg in 1847, was treat­ed well, read some English classics, learned Latin and Greek, and was con­firmed by Charles W. Schaeffer.

"In 1847, Loy went west for his health and was persuaded by Rev. J. Roof to become a beneficiary student at the seminary of the Joint Synod of Ohio at Columbus, where his teachers included Christian Spielmann and Wilhelm Lehmann.  He was strongly influenced at this time by the works of C. F. W. Walther and by several friends who were ministers in the Missouri Synod.  In 1849 he became pastor at Delaware, Ohio where he served until 1865.

"On Christmas Day, 1853, Loy married Mary Willey.  Despite his lifelong frailty and illness, Loy accomplished much.  He was President of the Joint Synod (1860-78, 1880-94), editor of the _Lutheran Standard_ (1864-91), Professor of Theology at Capital University (1865-1902), and President of the University (1881-90).  During his lifetime and under his direction, the Synod grew to have a national influence.  He was a zealous supporter of the Lutheran Confessions.

"In 1867 he refused to let the Joint Synod become a member of the General Council, and framed his objections in the form of "four points": Chialism, altar fellowship, pulpit fellowship, and secret societies.  In 1871, Loy led the Joint Synod into the Synodical Conference.  In 1881 he rejected Walther's doctrine of predestination, founded the _Columbus Theological Magazine_ (1881-1888) to combat it, and withdrew the Joint Synod from the Synodical Conference.

"Loy was forced to retire for health reasons in 1902.  He went to be with the Lord on January 26, 1915.

From: "Matthias Loy". Retrieved 2017-11-26 from cyberhymnal.org/bio/l/o/loy_m.htm

"O Christ, Thou Lamb of God that takest away the sins of the world, grant us Thy peace. – Matthias Loy 

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)

![book cover](/img/posts/171tc-loy-doctrine-of-justification-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/171tc-loy-doctrine-of-justification.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 171tc&body=Please send 171tc-loy-doctrine-of-justification.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 171tc&body=Please send 171tc-loy-doctrine-of-justification.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2018-02-22
- _Version 4 update_: 2019-06-07 
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
