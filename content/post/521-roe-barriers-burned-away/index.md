---
date: 2019-08-19 05:21:38-04:00
publishDate: 2019-08-19 05:21:38-04:00
title: "The Great Chicago Fire: Barriers Burned Away by Edward Roe"
slug: "521-roe-barriers-burned-away"
categories: ["Lutheran Library Publications"]
tags: ["Roe", "Fiction"]
authors: ["Roe, Edward Payson"]
titles: ["Barriers Burned Away"]
origpublishers: ["Dodd, Mead & Company"]
origdates: ["1872"]
synods: ["Presbyterian"]
---

"Barriers Burned Away, a story of the Great Chicago Fire, was first published serially in a magazine, the _New York Evangelist_. In 1872, when it came out in book form, it shortly became the most popular book of the year. When his next two novels achieved similar success, Roe decided to leave the ministry and give full time to writing. He believed he could reach more people with the message of Christianity through his writing than through preaching. He eventually would write about a dozen more novels, as well as a number of popular short stories. He also wrote a non-fiction book, Success with Small Fruits, primarily about the cultivation of strawberries." – Bob Sander-Cederlof

# Book Contents

- Reviews
- Preface To The First Edition
- 1. Love Unknown
- 2. Love Known
- 3. Launched
- 4. Cold Water
- 5. A Hornet’s Nest
- 6. “Starve Then!”
- 7. A Good Samaritan
- 8. Yahcob Bunk
- 9. Land At Last
- 10. The New Broom
- 11. Too Much Alike
- 12. Blue Blood
- 13. Very Cold
- 14. She Speaks To Him
- 15. Promoted
- 16. Just In Time
- 17. Rescued
- 18. Miss Ludolph Makes A Discovery
- 19. What Is The Matter With Him?
- 20. Is He A Gentleman?
- 21. Christine’s Idea Of Christians
- 22. Equal To An Emergency
- 23. The Revelation
- 24. Night Thoughts
- 25. Darkness
- 26. Miss Ludolph Commits A Theft
- 27. A Miserable Triumph
- 28. Life Without Love
- 29. Dennis’s Love Put To Practical Use
- 30. The Two Heights
- 31. Beguiled
- 32. Bitter Disappointment
- 33. The Two Pictures
- 34. Regret
- 35. Remorse
- 36. An Apparition
- 37. If He Knew!
- 38. The Gates Open
- 39. Susie Winthrop Appears Again
- 40. Suggestive Pictures And A Prize
- 41. Fire! Fire!
- 42. Baron Ludolph Learns The Truth
- 43. “Christine, Awake! For Your Life!”
- 44. On The Beach
- 45. “Prayer Is Mighty”–Christine A Christian
- 46. Christine’s Grave
- 47. Susie Winthrop
- 48. Doctor Arten Struck By Lightning
- 49. Bill Cronk’s Toast
- 50. Every Barrier Burned Away

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/521-roe-barriers-burned-away-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/521-roe-barriers-burned-away.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 521-roe-barriers-burned-away&body=Please send 521-roe-barriers-burned-away.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 521-roe-barriers-burned-away&body=Please send 521-roe-barriers-burned-away.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2019-08-19
- _Version 4 update_: 2019-08-19 
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
