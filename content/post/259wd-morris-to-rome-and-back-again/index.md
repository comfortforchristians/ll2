---
date: 2018-08-16 12:00:00
title: "To Rome and Back Again: The Story of Two Proselytes by John G. Morris"
slug: "259wd-morris-to-rome-and-back-again"
categories: ["Lutheran Library Publications"]
tags: ["Fiction", "Roman Catholicism", "Morris"]
authors: ["Morris, John Gottlieb"]
titles: ["To Rome and Back Again: The Story of Two Proselytes"]
origpublishers: ["T. Newton Kurtz"]
origdates: ["1856"]
synods: ["General Synod"]

--- 
More than twenty years ago, the substance of this book was published under another title, when it was highly recommended by many divines of our own and other churches. Judicious friends have advised me to prepare a new edition, and I have accordingly rewritten a large portion of it, added new characters, and transferred the whole scene of the story to this country. – [John Gottlieb Morris](/john-gottlieb-morris/)

# Some excerpts

"This is the principal distinction between the Evangelical and Catholic worship: – that we do not ascribe to our worship any supernatural effect on God, but only a moral effect on men, and we arrange and conduct it accordingly. 

"Our worship is intended to enlighten the understanding, to incline the will to the practice of Christian virtue, and to purify and sanctify the heart. Hence, the preaching of the divine word, in connection with singing and prayer, is with us the principal matter. 

"The Catholic worship, as a sacerdotal one, is intended to operate on the invisible world, – on God, – and to move him to absolve you from punishment and to exercise grace toward you. Hence, preaching is... a subordinate service; at every time of worship there is required a sacrifice, and this is performed in the priest's celebrating the Lord's supper for himself, and thus a continual sacrifice is offered to God." 

 
# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)

![book cover](/img/posts/259wd-morris-to-rome-and-back-again-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/259wd-morris-to-rome-and-back-again.pdf)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 259wd&body=Please send 259wd-morris-to-rome-and-back-again.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}
[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 259wd&body=Please send 259wd-morris-to-rome-and-back-again.epub)


