---
publishDate: 2019-02-20 05:01:00-05:00
date: 2019-02-20 05:01:00-05:00
title: "Charles Philip Krauth Sr.: A Biographical Sketch"
slug: "charles-philip-krauth"
categories: ["Biographical Sketches"]
tags: ["Krauth", "Lutheran Ministers"]
authors: ["Krauth, Charles Philip"]
synods: ["General Synod"]
---

[![Charles Philip Krauth Sr.](/img/posts/krauth-charles-philip.jpg)](https://www.lutheranlibrary.org/authors/krauth-charles-philip/)

# Rev. Charles Philip Krauth, Sr., D.D. 

The older Dr. Krauth was born in Montgomery county, Pa., May 7, 1797. His father was a native of Germany, and came to this country as a young man, in the capacity of a school teacher and a church organist. His mother was a Pennsylvanian. They lived in New York, Pennsylvania, and in Baltimore, Md., also for many years in Virginia, highly respected and enjoying the confideLce of their neighbors. Of his early life comparatively little is known in consequence of his singular and habitual reticence with regard to himself. He seems to have been from, a youth of an enquiring turn of mind and fond of books. He early evinced a decided taste for linguistic studies, and, in the prosecution of the Latin, Greek, and French, won for himself high credit. Having selected medicine as his profession, he commenced its study when about eighteen years of age, under the direction of Dr. Selden, of Norfolk, Va., and subsequently attended a course of lectures in the University of Maryland. But his funds having become exhausted, he visited Frederick, Md., with the view of procuring pecuniary aid from an uncle, the organist of the Lutheran church, or of negotiating a loan for the completion of his medical studies. During a visit to Rev. D. F. Schaeffer, of Frederick, his mind was led to the conclusion that the ministry was the work to which God had called him. He very soon commenced his theological studies under the instructions of Rev. Dr. Schaeffer, and, at every step of his progress, was the more strongly convinced that he was acting in accordance with the Divine will. 

Whilst he was engaged at Frederick in the prosecution of his studies, in the year 1818, Rev. Abraham Reck, of Winchester, Va., who was in feeble health, wrote to Dr. Schaeffer, inquiring if he could not send him a theological student to aid him in the discharge of his laborious duties. In compliance with his request, Dr. Schaeffer sent young Mr. Krauth, who continued his studies under the direction of Pastor Reck, and assisted him in preaching the gospel and performing other pastoral labor. He studied under Mr. Reck one year, and the testimony of his preceptor is that he showed great comprehension of mind, and was a most successful student. 

Mr. Krauth was licensed to preach the gospel by the Synod of Pennsylvania, at its meeting in Baltimore in 1819. His first pastoral charge embraced the united churches of Martinsburg and Shepherdstown, Va., where he labored for several years most efficiently and successfully. It was at a district conference, held in the church at Martinsburg, whilst Mr. Krauth was pastor, that the enterprise of a theological seminary, in connection with the General Synod, originated, and the first funds towards the object contributed. He was, in 1826, elected a member of its first Board of Directors. In 1827 he received and accepted a call to St. Matthew's congregation, recently organized in Philadelphia. 

The removal of Mr. Krauth to Philadelphia, in 1827, marks a new epoch, not only in the history of our English Lutheran interests in that city, but of his own life. Brought into new associations, surrounded by active, earnest, living men, with large libraries at his command, the best books on all subjects accessible, new powers seemed to be awakened within him, new energies were developed. As a scholar, a theologian, and a preacher, he rapidly advanced, and made a deep impression upon the community. At first he encountered some opposition from the German churches in the prejudices which existed, even at that day, against the introduction of the English language into the services of the sanctuary, but this all vanished when his character and object were better understood. Dr. Krauth remained in Philadelphia six years, and during the whole period, enjoyed the highest reputation as a pastor and a preacher, gathering around him a large and devoted congregation, and accomplishing an amount of good that can scarcely be estimated. 

In the year 1833, when Dr. Hazelius resigned his professorship in the Theological Seminary at Gettysburg, the attention of the Board of Directors was at once turned to Mr. Krauth as the man best qualified for the position. As a Hebraeist he had not at the time, in the Church, his superior, the result of his own earnest indefatigable application. He was unanimously chosen Professor of Biblical and Oriental Literature. It was agreed that part of his time should be devoted to instruction in Pennsylvania College, with the understanding that so soon as the proper arrangements could be made his duties should be entirely confined to the Theological Seminary. 

Professor Krauth was unanimously elected President of Pennsylvania College in the spring of 1834 The duties of this office he faithfully performed for nearly seventeen years, during most of the time also giving instruction in the Theological Seminary. 

In the autumn of 1850, yet in the vigor of manhood, he relinquished with great satisfaction, the anxious, toilsome, and often ungrateful work of the College Presidency, for the more quiet, congenial and pleasant duties of theological instruction. For five years, during his connection with the seminary, he also served with great acceptance as pastor of the congregation with which the institutions are united. He continued his duties in the Theological Seminary until the close of life, delivering his last lecture to the senior class within ten days of his death, the subject, by a singular and interesting co-incidence, being the Resurrection. He died May 30, 1867, in the 71st year of his age, and the 49th of his ministry. The honorary degree of D. D. was, by a unanimous vote, conferred upon him by the University of Pennsylvania in 1837. 

His published writings are: 

- "Works of Melanchthon" 
- "The General Synod" 
- "Early History of the Lutheran Church" 
- "Schmidt's Dogmatic" 
- "The Lutheran Church in the United States" 
- "Present Position of the Lutheran Church" 
- "Contributions to the History of Church"
- "Luther and Melanchthon" 
- "German Language" 
- "Henry Clay" 
- "Baptism." 

– by [Morris](/authors/morris-john-gottlieb/). 

{{% alert note %}}
This biography is taken from Jens Jensson's _American Lutheran Biographies_, published 1890.
{{% /alert %}}

First published online 2019 at [LutheranLibrary.org](.).