---
date: 2019-06-28 05:42:27-04:00
publishDate: 2019-06-28 05:42:27-04:00
title: "Lutheran Library Updates – Summer 2019"
slug: "2019-summer-lutheran-library-updates"
categories: ["Lutheran Library Newsletters"]
---

In this issue: __Weekly Message on the Short Catechism__, __Reminiscences of Lutheran Clergymen__, __Short Books__, __Email Updates__.

Dear Friends of LutheranLibrary.org,

Here in New York City the weather has been beautiful.  I hope that wherever you are you are able to enjoy the wonders of God's creation.  

One of the greatest things the Holy Spirit brings us is a "Love of the Truth" (2 Thes. 2:10).  Wherever we might find ourselves - in Lutheran churches, Reformed, Baptist, or something else, we can follow the Apostle John's urging to, "Be watchful, and strengthen the things that remain (Rev. 2:3).  LutheranLibrary.org is here to help you to do just that.  All the materials are provided to you to grow your faith, to encourage you, and to provide resources for private study groups, catechism or seminary classes.  Please do pray for this ministry.  If we can pray for you, [let us know](https://www.lutheranlibrary.org/#contact).

You may have noticed some new features on the website.  

# 1.  [This Week With Luther's Small Catechism](https://www.lutheranlibrary.org/#weekly-catechism)

Each week on Wednesday a new message is being posted on the basics of the Christian Faith.  Traditional Pastor Robert Golladay is the author.  They have been a great blessing to many believers in the past.  Our prayer is that they may be so to you.  You may find it helpful to print out the messages and to use them as part of your daily or weekly devotions.  PDF links should be provided, or you can always request a printable PDF for any post.

- [[A1] Our Lutheran Catechism](/a1-our-lutheran-catechism/)
- [[A2] Our Christian Foundation. Christ's Answer to Man's Question 'What is Truth'](/a2-our-christian-foundation.-christs-answer-to-mans-question-what-is-truth/)

# 2. [Reminiscences of Lutheran Clergymen](https://www.lutheranlibrary.org/#lutheran-ministers)

[Charles Krauth](/charles-porterfield-krauth/) was the editor of the Lutheran Journal _The Evangelical Review_.  Here is what Dr. Krauth said of the series:

>It is the design of the writer to present to the renders of the _Review_, in a series of sketches, reminiscences of deceased Lutheran ministers, who were eminent in our earlier history, and distinguished for their zeal in advancing the interests of our Zion. 

>It is a pious duty to rescue from oblivion the memory of the great and good, and to transmit their virtues and services to posterity…  much interesting and valuable material… has already been lost. It is our design, in the work which we have assigned ourselves, to gather from those who still survive, as links connecting us with a former generation, and other available sources, such facts as may be worthy of preservation. 

What a blessing it is to read this material that was so carefully and painstakingly collected by Prof. Krauth.  Each week, on Sunday, a new biography will, Lord willing, be posted.  The stories of these great American ministers can inspire us each week to walk in those good works which are _in front of us_ to do (Eph. 2:10).

# 3. [Short Books](/tags/short-books/)

While still releasing full-length classic books, we have been steadily releasing "short books".  These are mostly essay-length works taken from _The Evangelical Review_ and other sources, by traditional Bible believing authors.  The content of these forgotten titles is excellent and unavailable elsewhere.  The length is optimal for e-book readers.

- [The Lutheran Confessions: A Brief Introduction by Henry Eyster Jacobs](/s04-jacobs-the-lutheran-confessions-a-brief-introduction/)
- [The Church and Antisemitism by Walter Horton](/s45-horton-church-and-anti-semitism/)
- [The Burning Question: The Predestination Controversy in the American Lutheran Church by Matthias Loy](/s43-loy-burning-question/)

# 4. [Email updates](/subscribe/)

These are coming out now on Fridays.  The format has been changed to include links to all material released during the previous week.  

The [RSS link](/feed.xml) will remain active if you prefer immediate updates.

That's it for now.  As always, may God richly bless you.

– Your Lutheran Library editor.


## About The Lutheran Library

The goal of the Lutheran Library is to re-release well-written and readable books from sound, faithful American Lutherans of the past for the enjoyment and edification of a new generation.  All books are available at [lutheranlibrary.org](/) for free download in a variety of formats for Kindle, Apple, and other devices. Your help is appreciated in spreading the word of our ministry.
