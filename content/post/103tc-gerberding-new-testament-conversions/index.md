---
date: 2019-04-19 19:22:17-04:00
publishDate: 2019-04-19 19:22:17-04:00
title: New Testament Conversions by George H. Gerberding
slug: "103tc-gerberding-new-testament-conversions"
categories: ["Lutheran Library Publications"]
tags: ["Gerberding", "Evangelism", "Conversion", "Justification", "Sanctification", "Assurance", "Baptism"]
authors: ["Gerberding, George Henry"]
titles: ["New Testament Conversions"]
origpublishers: ["Lutheran Publication Society"]
origdates: ["1889"]
synods: ["General Council"]

---
The only way to be certain that we understand conversion and salvation correctly is to go to the Bible.  This is what pastor Gerberding does in this excellent book.

{{% toc %}}

 

# Fanaticism

"Why, then, a new book of sermons on conversion? 

"...here is a whole host of would-be evangelizers. They seem to consider it their special mission and commission to "convert sinners."...They can bring about hundreds of conversions in an evening. They get up a revival in the home church, or start out to revive a town or city. We have heard some of them assert how they have converted whole communities, and how they were going to "capture" such a town or city "for Jesus!" 

"With them, conversion is a rousing of the feelings, a wave of emotion, a burst of excitement. While they will speak in thunder tones of the necessity of conversion and of the damnation of the unconverted, they rarely even attempt to explain the nature of conversion. Ask them what it is, and they can give at best very vague and unsatisfactory answers. Ask them how it is brought about, what its agencies and instrumentalities are, and they don't know. Ask them what its evidences are, and they don't tell you. They are full of pious phrases, and earnest exhortations, and touching stories, and tearful pleadings. But the teaching of the divine Word on this all-important subject they know not.

# Coldness

"There is still another class in the Church who need to give renewed attention to this subject. Repelled by the fanaticism and the vagaries of the aforenamed class, they have gone to the other extreme. While the former make a hobby of the subject, these latter almost ignore it. They don't preach much conversion. They seem to be almost afraid of the term. They speak much of truth, and Grace, and faith, and righteousness. And against all this we would be the last to say one word. But to neglect or ignore the subject of conversion is certainly a very grievous and dangerous mistake. It may result in a false security in the unconverted of whom there are certainly many among the hearers of every preacher. It may result in the loss of souls, which will be required at the pastor's hand. 

– From *New Testament Conversions* by George Gerberding. "Introduction"

# Book Chapters

- 1. Conversion: Its Nature, Necessity, and Efficient Agencies – Acts 3:19
- 2. The Conversion of The Woman of Samaria – John 4:28-29
- 3. The Conversion Of The Prodigal Son – Luke 15:17-20
- 4. The Conversion of the Publican – Luke 18:13
- 5. The Conversion of Zaccheus – Luke 19:2-9
- 6. The Fall and Re-conversion of Peter – Matt. 26:69-75
- 7. The Conversion of the Dying Thief – Luke 23:39-44
- 8. The Tests and Fruits of a True Conversion as Seen in Peter’s Reinstatement Into the Apostleship – John 21:15-20
- 9. The Conversion of the Three Thousand – Acts 2:37-42
- 10. The Conversion of the Ethiopian Eunuch – Acts 8:35-39
- 11. The Conversion of Paul – Acts 9:1-9, 17-18
- 12. The Conversion of Cornelius – Acts 10:1-6
- 13. The Conversion of Sergius Paulus – Acts 13:6-12
- 14. The Conversion of Lydia – Acts 16:13-16
- 15. The Conversion of the Philippian Jailer – Acts 16:25-34
- 16. A Spurious Conversion – Acts 8:9-14, 18-25
- 17. Almost Converted – Acts 24:24-25

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)

![book cover](/img/posts/103tc-gerberding-new-testament-conversions-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/103tc-gerberding-new-testament-conversions.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 103tc&body=Please send 103tc-gerberding-new-testament-conversions.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 103tc&body=Please send 103tc-gerberding-new-testament-conversions.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}



# Publication Information

- _Lutheran Library edition first published_: 2017-09-03
- _Version 4 update_: 2019-04-19 
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

