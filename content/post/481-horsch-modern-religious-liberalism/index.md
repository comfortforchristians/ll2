---
date: 2019-07-18 08:47:33-04:00
publishDate: 2019-07-18 08:47:33-04:00
title: "Modern Religious Liberalism by John Horsch"
slug: "481-horsch-modern-religious-liberalism"
categories: ["Lutheran Library Publications"]
tags: ["Horsch"]
authors: ["Horsch, John"]
titles: ["Modern Religious Liberalism"]
origpublishers: ["The Bible Institute Colportage Association"]
origdates: ["1938"]
synods: ["None"]
---

"Modern religious liberalism has really only __one dogma__ and consequently knows just one heresy… From this viewpoint __a person is heretical to the extent that he may believe that there is absolute religious truth__. – John Horsch

{{% toc %}}

# Book Contents

- Introduction
- 1. A Religious Revolution
- 2. The Inspiration And Authority Of The Holy Scriptures
- 3. Christian Experience
- 4. Religious Certainty Considered From The Point Of View Of Modernism
- 5. Philosophy And Theology
- 6. The Fathers Of Liberalistic Theology
- 7. The Modern Doctrine Of Divine Immanence
- 8. The Biblical Versus The Modern View Of Prayer
- 9. The Deity Of Christ Vs. The Modern Doctrine Of The Divinity Of Man
- 10. Sin And Salvation
- 11. Two Types Of Modern Theology Compared
- 12. The Ethical Interpretation Of Religion — The Liberalistic Morality
- 13. The Social Gospel
- 14. Religious Democracy, The Denial Of God’s Sovereignty
- 15. The New View Of Religious Education
- 16. The Modernist View Of Missions
- 17. Modern Religious Unionism
- 18. Church Discipline Versus Persecution
- 19. Historical Falsehoods — Contrasts Between Freedom And Anarchy
- 20. Immortality
- 21. Science
- 22. Evolutionism
- 23. What Ails Our Colleges And Seminaries?
- 24. Communism
- 25. The Immorality Of Theological Counterfeiting
- 26. Modern Theology In The Light Of The World War
- 27. The Failure Of Religious Liberalism
- 28. The Failure Of Unitarianism
- 29. The Chasm Between The Old And The New Theology
- Index

# Social Improvement Instead of Personal Salvation

"…Modernism overlooks the fact that personal salvation is for the individual a far more important matter than the privilege to live in a socially improved society. It is more important to have the victory of the spirit through a personal relationship to God than to have one's social and political and economic desires satisfied.

"And the thought that the world may be regenerated through human instrumentality, or in other words, that conditions on earth may be improved to such extent that men are no longer born in sin and do no longer need personal salvation through Jesus Christ — this thought is utterly fallacious. If individual reformation does not change the heart of the one who reforms, neither will improvement of social conditions break the organized power of evil that is manifest in the world. 


# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/481-horsch-modern-religious-liberalism-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/481-horsch-modern-religious-liberalism.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 481-horsch-modern-religious-liberalism&body=Please send 481-horsch-modern-religious-liberalism.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 481-horsch-modern-religious-liberalism&body=Please send 481-horsch-modern-religious-liberalism.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2019-07-18
- _Version 4 update_:  2019-07-18
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
