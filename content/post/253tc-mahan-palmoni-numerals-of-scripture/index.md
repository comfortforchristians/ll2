---
date: 2018-08-16 12:00:00
title: "Palmoni: The Numerals of Scripture by Milo Mahan"
slug: "253tc-mahan-palmoni-numerals-of-scripture"
categories: ["Lutheran Library Publications"]
tags: ["Milo", "Efficacy of the Word"]
authors: ["Mahan, Milo"]
titles: ["Palmoni: The Numerals of Scripture"]
origpublishers: ["D. Appleton and Company"]
origdates: ["1863"]
synods: ["Anglican"]
--- 

"The Bible to the early church was not a book merely; it was a grand zoön, a _living creature_, a thing instinct in every part with a mysterious, manifold and superabundant life. And in every part of it they expected to find the evidences of that life. 

And as a Divine work differs from a human chiefly in those _minutiae_ which lie beneath the surface; as a photograph, for example, painted by the sunbeam, though it be but a dot to the naked eye, will yet reveal to the microscope what no human skill can rival: so, the early Christians argued, it must be with the Divine Word. They confidently expected to see "wondrous things" in it. And the more closely they scrutinized it, the more marks of the supernatural they professed to see therein.

Hence to them the "history" of the Bible was not mere history; its "facts," not mere facts; its "arithmetic," not mere arithmetic. Each word, each letter, each cipher appeared to them transfigured in an atmosphere of the supernatural, the center of a vast throng of spiritual associations.

 
# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)

![book cover](/img/posts/253tc-mahan-palmoni-numerals-of-scripture-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/253tc-mahan-palmoni-numerals-of-scripture.pdf)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 253tc&body=Please send 253tc-mahan-palmoni-numerals-of-scripture.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}
[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 253tc&body=Please send 253tc-mahan-palmoni-numerals-of-scripture.epub)


