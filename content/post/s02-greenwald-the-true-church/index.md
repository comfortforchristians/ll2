---
date: 2018-05-12 12:00:00
title: "The True Church by Emanuel Greenwald"
slug: "s02-greenwald-the-true-church"
categories: ["Lutheran Library Publications"]
tags: ["Greenwald", "Short Books"]
authors: ["Greenwald, Emanuel"]
titles: ["The True Church"]
origpublishers: ["The Lutheran Book Store"]
origdates: ["1876"]
synods: ["Ohio Synod"]

--- 
This Lutheran Library "short" is beloved pastor Emanuel Greenwald's guide on how to recognize a true Christian Church.

Rev. Greenwald begins with these words:

"Much stress is laid by the members of the Church of Rome upon the question of the True Church, and very properly, too, for the question is one of great importance. It is not a matter of indifference whether we belong to the True Church, or not. Every one is interested in learning the marks of the True Church, and none should rest satisfied to be in any other than the True Church.
 
{{% toc %}}

# About Emanuel Greenwald

"Emanuel Greenwald was born near Frederick, Maryland, Jan. 13, 1811, and was, like the prophet Samuel of old, dedicated by his pious parents to the holy office from his earliest infancy. His theological studies were pursued under the private supervision of Rev. David F. Schaeffer, who similarly prepared no less than fourteen other young men, in his own parsonage, for the work of the ministry. 

"Dr. Greenwald's first parish was New Philadelphia, Ohio, and all the adjoining country within a radius of fifteen miles in every direction. At one time he supplied fourteen preaching points on Sundays and week-days. In 1842 he was elected as the first editor of the Lutheran Standard, and from 1848 to 1850 he was the president of the English District Synod of Ohio. 

"The years 1851 to 1854 were spent in the city of Columbus, during which time he held many responsible positions on important boards, committees, etc. In September, 1854, he accepted a call to the pastorate of Christ Church, Easton, Penn., which he faithfully served for twelve years. His fourth and last parish was Holy Trinity Church of Lancaster, Penn., in which he labored from May, 1867, up to his death in December, 1885. He began preaching every Thursday evening at a mission point in the northern part of the city, which soon developed into Grace Church, and afterwards started another mission in the western section which was the nucleus of Christ Church. An assistant being necessary for the increasing field, Rev. Charles S. Albert served in this capacity, then Rev. David H. Geissinger, then Rev. John Rumple, then Rev. C. Elvin Haupt, then Rev. Ezra K. Reed, then Rev. Charles L. Fry. Long after his own generation will his memory continue to be revered as an ideal pastor and a man of pre-eminent saintliness. C. L. F.[^acK]

[^acK]: Jacobs, Henry Eyster, ed. (1899) *The Lutheran Cyclopedia*. New York: Charles Scribner's Sons 

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)

![book cover](/img/posts/s02-greenwald-the-true-church-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/s02-greenwald-the-true-church.pdf)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request s02&body=Please send s02-greenwald-the-true-church.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}
[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request s02&body=Please send s02-greenwald-the-true-church.epub)

