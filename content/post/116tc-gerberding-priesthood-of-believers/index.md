---
date: 2017-09-16 12:00:00
title: The Priesthood of Believers by George H. Gerberding
slug: "116tc-gerberding-priesthood-of-believers"
aliases: 
  /116tc-tract-gerberding-priesthood-of-believers/
categories: ["Lutheran Library Publications"]
tags: ["Justification", "Short Books", "Gerberding"]
authors: ["Gerberding, George Henry"]
titles: ["The Priesthood of Believers"]
origpublishers: ["United Lutheran Church in America"]
origdates: ["1920"]
synods: ["General Council"]

---
"Luther taught that every true believer is a spiritual priest, is as near to the great Head of the Church as any official priest can be, and is equally entitled to the promise and gift of the Holy Ghost. He may exercise priestly functions among his fellow men and ought to do what he can to promote their well-being. 

"This teaching of Luther was calculated to awaken a joyous consciousness and energy in the common Christian calling. 

– Pastor Gerberding, from Chapter 2

# Contents

- 1. The Doctrine
- 2. Some Implications of This Doctrine
- 3. The Sad Neglect of This Doctrine
- 4. New Opportunities are Open
- 5. The Spiritual Priesthood Actualized In The Church
- 6. The Brotherhood

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)

![book cover](/img/posts/116tc-gerberding-priesthood-of-believers-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/116tc-gerberding-priesthood-of-believers.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 116tc&body=Please send 116tc-gerberding-priesthood-of-believers.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 116tc&body=Please send 116tc-gerberding-priesthood-of-believers.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}
