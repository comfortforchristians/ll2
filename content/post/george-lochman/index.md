---
date: 2019-07-07 05:54:30-04:00
publishDate: 2019-07-07 05:54:30-04:00
title: "George Lochman: A Biographical Sketch"
slug: "george-lochman"
categories: ["Biographical Sketches"]
tags: ["Lochman", "Lutheran Ministers", "Evangelical Review"]
authors: ["Lochman, George", "Krauth, Charles Porterfield"]
synods: ["Pennsylvania Synod", "General Synod"]
---

"Dr. Lochman, so widely and favorably known in the Lutheran church, was born in the city of Philadelphia, December 2, 1773. His parents had immigrated into this country at an early period and, although in humble circumstances, were distinguished for their probity and piety. Their son George, when yet a boy, seemed to promise much, and awakened high expectations. He developed, in his childhood, a remarkable fondness for reading. Whilst his companions were engaged with their sports, he was interested in his books, over whose pages he poured with fixed attention and the greatest delight. He also comprehended and retained what he read. At school he soon gave proof of more than ordinary mental capacity. His perception was quick, his memory retentive, and his intellect susceptible of great improvement. His rapid progress in study attracted the attention of his teachers, who rejoiced in the success of their pupil. In his youth he was deeply exercised upon the subject of religion. His convictions were very pungent, and he passed through severe internal struggles, and various mutations, before he experienced the quickening power of the Divine Spirit, and was brought to see the mercy of God, and to own and love his Savior. During his attendance upon the catechetical instruction of the church, he won the heart of his pastor, and the promptness with which he answered the questions, excited the hope that he might be inclined to the work of the ministry. The opportunity of directing the young man's thoughts to the subject, and of urging its importance upon his attention, was not disregarded by Dr. Helmuth. 

{{% toc %}}

## An Aside: The Rev. James Patterson

"In this connection, we are disposed to inquire, whether Christian pastors are not sometimes remiss in duty, and unmindful of their obligations to the church? Is it an object dear to their heart, to seek out young men of suitable qualifications, in their congregations, for the sacred office, and to press upon their consideration the great work of preaching the gospel? We are not only to pray that the field, which is white for the harvest, may be furnished with laborers, but we are to put forth corresponding efforts. The harvest truly is plenteous, hut the laborers are few. In the Lutheran church, especially, the destitution is very great. We need many men of fervent piety and the requisite talents, to supply the numerous waste places, scattered ail over our Zion. In almost every pastoral district, there are those of promising abilities, who might be disposed to turn their attention to this important question, if it were only suggested. Young men have often cherished the thought and been self-moved to the work, but have never ventured to name the promptings of their hearts, because there was no encouragement offered by those who sustained to them the relation of spiritual guides. Who can tell how many gifted minds have thus been lost to the ministry? It is said of the late Rev. James Patterson, of Philadelphia, a most excellent man of the Presbyterian church, that he was the means of introducing not less than sixty young men into the gospel ministry. It is impossible to estimate the influence such an individual exerts. The amount of his usefulness, and the extent of his labors cannot be measured. Eternity alone can reveal the results. He may set in motion a train of causes, which shall continue to operate long after he is laid in the silent tomb; he may have been instrumental in placing into stations of influence and power, those who shall act on the destinies of others, when his own name is forgotten! 

## "Lord, what wilt thou have me to do?"

"When the subject of the ministry was proposed to young Lochman, it received his cordial approval. With him the inquiry was, _Lord, what wilt thou have me do_? He had consecrated himself to the Savior, and was ready to engage in any service to which he was adapted; it was the governing object of his life to do good, and to glorify God. The mother also favored the project, but the father at first made numerous objections. He was desirous, that his son should aid him in the business in which he was employed; besides, his income was so limited, that he could not possibly furnish him with the means for obtaining a collegiate education. The father did not wish him to enter upon the work without being thoroughly qualified for its duties. All opposition was, however afterwards withdrawn, when it was ascertained that the son's desire in this direction was so strong, and could not easily be subdued. The influence of the pastor in the family, was likewise exerted, in convincing the parent of his duty, and leading him to arrive at proper conclusions in reference to the matter. 

![George J. Lochman, D.D.](/img/posts/lochman-j-george.jpg)

## The University of Pennsylvania and First Call

"After acquiring the preliminary knowledge, Dr. Lochman entered the University of Pennsylvania, at which he was graduated in the year 1789, and from which institution he subsequently received the honorary degree of Doctor of Divinity. Having completed his collegiate course, he taught for a season, prosecuting at the same time, his Theological studies, under the direction of Rev. Dr. Helmuth, with whom he continued, until his licensure, in the year 1794, by the Synod of Pennsylvania. Soon after, he accepted a call to Lebanon, Pa., where he remained for the space of twenty-one years, laboring with great fidelity, and the most satisfactory results. 

They were years of long and patient toil. The charge embraced not only Lebanon, but a considerable circuit in the vicinity. Our ministers, at that day, were few, and the field was extensive. Our members were scattered, and pastoral duty was necessarily onerous. He did not, however, labor in vain. Most precious fruit accompanied his efforts. His influence was felt far and wide. He had been repeatedly invited and earnestly solicited to 'pull up stakes,' and 'pitch his tent elsewhere, yet so much attached was he to his people, that he could not for years, feel that it was his duty to dissolve his connection with them. 

## Harrisburg

In 1815, when he was elected pastor of the Lutheran church at Harrisburg, Pa., then a comparatively new and inexperienced charge, and struggling under difficulties, his convictions of obligation were so strong, that there seemed to his mind no other alternative than to accept the appointment, although he had previously refused several more eligible offers. The voice of conscience urged him, and he concluded it was God's will that he should go. The call was short but pertinent, and closed with the following language: 

>'The Lord incline your heart to us, as our hearts have been inclined to you.' 

His introductory sermon was preached on the 3rd of September, from the text: _Whom we preach, warning every man and teaching every man, in all wisdom, that we may present every man perfect in Christ Jesus_ — in which the speaker furnished a clear and impressive exhibition of the object and duties of the Christian ministry, together with the obligations resting upon the people. So favorably was the discourse regarded, that the vestry resolved to have it printed for gratuitous distribution, copies of which are still extant. During his residence at Harrisburg, which was until death terminated his labors, he sustained the character of a faithful and zealous messenger of God. A fresh impulse was given to the church; the congregation prospered, and the membership was increased. During each of the eleven years of his administration, the average number of additions to the church was twenty-eight. 

## Failing Health

"Owing to the extent of Dr. Lochman's ministerial labors, his constitution began gradually to decline. He had been overtasked in public efforts, to which he had devoted himself with so much earnestness, during an active service of thirty-two years in the ministry. The infirmities of age prematurely increased upon him and, ere long, disease prostrated his strength. During his protracted confinement, his sufferings were most severe, yet he bore them with patience and Christian submission. Neither a murmur nor complaint was heard. The time was profitably occupied by him in concern for the church, in setting his house in order, and completing the preparation already made for his latter end. His faith in God was firm and unshaken; he relied for acceptance upon the blood of the atonement, and calmly awaited the summons. To the Rev. Dr. De Witt, who visited him in his dying chamber, he remarked: 

>'What should I now do, if I had not an Almighty Savior, upon whom to depend?' 

The serenity which beamed upon his countenance, and the expressions of joy and peace which fell from his dying lips, spoke of heaven. On the 10th of July, 1826, in the fifty-third year of his age, he laid down his office in the church upon the earth, to enter upon _the general assembly and the church of the first born, whose names are written in heaven_. On the following day they carried his body to the grave, attended by an immense number of sorrowing friends, who grieved that they should no longer see his face and listen to his words of affection and paternal counsel. The exercises of the occasion were conducted by Rev. Dr. Endress, of Lancaster, Pa., who preached from the words of the aged Simeon: _Lord, now lettest thou thy servant depart in peace, according to thy word; for mine eyes have seen thy salvation_. 

## A Faithful and Successful Pastor

"From what has already been said, it may be naturally inferred, that Dr. Lochman was a faithful and most successful pastor. He was abundant in labors, fervent in spirit, and valuable in counsel. He was loved and revered by his congregation, as their spiritual father. In the hour of darkness and adversity he was with them, soothing their sorrows and ministering unto them consolation and instruction. He was frequent in his visits to the house of mourning, and the chamber of the sick. He was humane and charitable, and without any ostentation bestowed his alms, and relieved the wants of the unfortunate. He possessed an amiable character, a cheerful temper, a generous nature, a warm heart, and an aptitude for making friends and securing confidence. In his deportment he was plain and unassuming, in his intercourse accessible and conciliatory, affable and courteous, attentive to all the proprieties of life. He was regarded by some, as rather punctilious, and too particular in the observance of all the forms of politeness. It was his uniform practice to take off his hat to every one, whom he met on the street. He was careful in his expressions, discreet in his actions, charitable in judgment, and slow to ascribe an improper motive to an individual's conduct without sufficient reason, or when a good one could be assigned. 

"Although nearly thirty years have elapsed since his death, his praise is still on the lips of his parishioners, who yet survive. They all unite in bearing testimony to the suavity of his manners and dignified bearing. They represent him as being most particular in his care and attentions to children, who were "allured by his gentle and affectionate manner. He devoted a part of every Lord's day he spent in town, to the instruction of the young, generally known among the Germans as _Kinderlehr_. His engagements in the surrounding country, however, rendered it impracticable for him to meet the children every Sabbath. 

It may be interesting, in this connection, to state that the congregation at Harrisburg, whilst Dr. Lochman was their pastor, claim the credit of having removed the restriction which excluded the children of the wealthy from the advantages of the Sunday School. When this institution was first established, its excellent founder proposed to benefit only the poorer classes, who were without any instruction during the week. The original plan of Raikes has been entirely changed. The idea of admitting all classes was entirely new to this community. — If practiced anywhere else, it was not borrowed, for the fact was not known to them. On a certain occasion, in the year 1819, several of the young members of the church met at the house of the pastor. In the course of a conversation, some one proposed _to start a Sabbath School_, another replied, _where will you get the children?_ The answer was, _let us commence upon the principle of receiving children of all classes, rich and poor, without distinction, and we shall have scholars enough!_ The suggestion was adopted, the enterprise commenced, and the success of the effort surpassed their most sanguine expectations.[^bFL] 

[^bFL]:  Rev. C. W. Schaeffer's discourse, delivered on the fiftieth anniversary of the English Lutheran Church at Harrisburg. 

"Dr. Lochman was an able and popular preacher. His style was solemn and impressive, kind and persuasive, marked by much feeling and great earnestness. The truths of God's word were proclaimed with amazing simplicity, meekness and power. There was nothing denunciatory in his discourses, no anathemas were launched from the pulpit; he seemed desirous of winning souls to Christ, of persuading men to be reconciled to their Father in Heaven. His preaching was deeply imbued with the doctrines of the cross; it was eminently practical and instructive. Many, by his affectionate and kind manner, were induced to examine the question of eternity, to ponder their ways, and to flee for refuge to the hope presented in the gospel. The careless were awakened, the weak were strengthened, the crushed and broken-hearted were bound up, the wanderer reclaimed; saints were edified and souls saved. He trained — 

>&emsp;&emsp;&emsp;'By every rule \
>Of holy discipline to glorious war \
>The Sacramental host of God's elect.' 

"Many of our candidates for (he sacred office resorted to him for Theological instruction. Among the number whose names occur to us are, Rev. Dr. Kurtz, Rev. Messrs. Reimensnyder, Schindle, Schnee, Steelier, Stroh, Bahl, F. Ruthrauff, J. Ruthrauff, E. Keller, D. Eyster, Shirer and A. H. Lochman. 

## Reputation and Influence

"By the church at large, Dr. Lochman was held in high estimation. He was interested in its general welfare, and labored diligently for its elevation. He was disposed to identify himself with every effort, intended to advance its best interests. The records of the Synod, with which he stood connected, show how much he was valued by his brethren in the ministry, and the influence he exercised over them. He was the early, zealous, and devoted friend of the General Synod, which has been such a blessing to the church. He presided over its first convention, assembled at Frederick in 1821. His ministerial labors so absorbed all his time, that he found little leisure for authorship. He wrote a work on the history, doctrine and discipline of the Lutheran church. Also, since his death, a volume of sermons of a devotional character, for circulation among the people, has been published. 

"Dr. Lochman exercised an unbounded influence. All sects and classes in the community were much attached to him, whose gratitude and love he enjoyed in an eminent degree. His opinions carried with them great weight; his views upon any subject always commanded attention. His life was a beautiful illustration of the reverence all feel for exalted piety and active benevolence. His death created a void in the church which could not be easily filled. Many a heart was struck with grief. The people of both charges, to whom he had dispensed the word of life, mourned his loss. His brethren in the ministry, especially those who composed the school of the prophets, at his own house, and were trained by him for the sacred ministry, greatly sorrowed. All felt that a good man had fallen. Hundreds gathered around his grave to pay their last tribute of love to departed worth; to do honor to the memory of one, whose virtues and labors were indelibly impressed upon their minds. Such a man needs no monumental stone or towering height to perpetuate his name. He has himself erected a monument more enduring than tablets of brass or marble — 

>"Aere perennius, 
>"Regalique situ pyramidum altius. 

"He will live in the affections of the people, and his excellencies will be transmitted, with unimpaired vigor, to posterity. His influence remains, the remembrance of his life is sweet, his rest is sacred! Blessed are the dead who die in the Lord, from henceforth: Yea. saith the Spirit, that they may rest from their labors, and their works do follow them. 


## Publication Information

- _Author_: __"Krauth, Charles P."__, Editor.
- _Journal_: __"The Evangelical Review"__, Vol. 6.
- _Originally Published_: 1854.
- _Lutheran Library Edition_: 2019
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

{{% alert note %}}
{{% biographical-sketch-note %}}
{{% request-pdf %}}
{{% /alert %}}
