---
date: 2017-10-30 12:00:00
title: Church History For Lutheran Young People by Peer Olsen Stromme
slug: "139tc-stromme-church-history"
categories: ["Lutheran Library Publications"]
aliases: /139tc-stromme-church-history/
tags: ["Church History", "Short Books", "Stromme"]
authors: ["Stromme, Peer Olsen"]
titles: ["Church History for Lutheran Young People"]
origpublishers: ["John Anderson Publishing"]
origdates: ["1902"]
synods: ["Norwegian American Lutheran Church"]

excerpt: "The Christian Church is in reality as old as the world itself. It has existed ever since the creation of man; for there always have been true believers, who have done God's will on earth, and who have gone to heaven when they died. And all these have been saved through faith in Christ. The church history of the time before the coming of Christ is the history of the Jews, God's chosen people, as recorded in the Old Testament." 
---
>The Christian Church is in reality as old as the world itself. It has existed ever since the creation of man; for there always have been true believers, who have done God's will on earth, and who have gone to heaven when they died. And all these have been saved through faith in Christ. The church history of the time before the coming of Christ is the history of the Jews, God's chosen people, as recorded in the Old Testament. The true believers of that day were saved by faith in the Savior whom God had promised to send when he said: "The seed of the woman shall bruise the head of the serpent." They truly believed in Christ, and were members of his church; for it was true then, as now, that "there is no salvation in any other; for there is none other name under heaven given among men, whereby we must be saved."' – From the Introduction

 
{{% toc %}}

# Contents of Church History For Lutheran Young People

Foreword
Introduction
Part I. The Early Church
-  The Apostles
-  The Early Christian Martyrs
-  Constantine The Great And Julian The Apostate
-  The Religious Services And Customs Of The Early Church
-  The Church Fathers
-  Arianism And Athanasius
-  Augustine And The Pelagian Controversy
-  Monachism And Asceticism

Part II. The Church During The Middle Ages
-  The False Prophet Mohammed
-  Christianity Victorious Throughout Europe
-  Popery
-  Superstitions And Heresies Of The Roman Church
-  The Crusades
-  The Monastic Orders
-  The General Church Councils
-  The Reformers Before The Reformation

Part III. The Reformation and After
-  The Church Reformation By Martin Luther
-  Ulrich Zwingli And John Calvin
-  The Early Struggles And Triumphs Of The Lutheran Church
-  The Growth Of The Reformed Church
-  The Jesuits
-  Doctrinal Controversies In The Lutheran Church

# About Peer Stromme

Peer Olsen Stromme was born September 15, 1856 in Winchester, Wisconsin to Norwegian immigrant parents.   Stromme graduated from Luther College (Iowa), attended Concordia Theological Seminary, and was ordained a Lutheran minister in 1878. Stromme taught at St. Olaf College, and was editor of a Norwegian language paper in Chicago. Additionally he was the founding editor of the Norwegian language newspaper Dagbladet. He also translated books for the John Anderson Publishing Company in Chicago and the Lutheran Publishing House of Decorah, Iowa.  Stromme fell asleep in he Lord on September 15, 1921.[^afI]

[^afI]: Wikipedia. "Peer Stromme". Retrieved 2017-10-20.

# From the Introduction
>It has seemed to me that a Compend of Church History, in handy form and in the English language, would fill a want among our Lutheran young people. For years I have therefore had it in my mind to prepare such a book, hoping that it might be read with interest and profit in the homes, and that it might be found useful in connection with the work done by our Young People’s Societies and our Sunday Schools. I may add that while the plan and arrangement of the book are my own, I have, of course, consulted other Church Histories, and made use of that which seemed best. – Peer O. Stromme., Madison, Wis., 1902

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download) 

![book cover](/img/posts/139tc-stromme-church-history-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/139tc-stromme-church-history.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 139tc&body=Please send 139tc-stromme-church-history.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 139tc&body=Please send 139tc-stromme-church-history.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}
[^acK]: Jacobs, Henry Eyster, ed. (1899) *The Lutheran Cyclopedia*. New York: Charles Scribner's Sons 