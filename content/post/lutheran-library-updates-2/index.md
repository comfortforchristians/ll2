---
date: 2018-05-31 12:00:00
title: "Lutheran Library Updates – June 2018"
slug: "lutheran-library-updates-2"
categories: ["Lutheran Library Newsletters"]
---
May God bless you and keep you, help you, defend you, and lead you to know the depth of His love...
 
{{% toc %}}

![update 2](/img/posts/lutheran-library-updates-2.jpg)

# New Releases

The Lutheran Library published 5 books in May: [The Sayings of Charles Krauth](/s01-krauth-sayings/), [A Selection of Sermons by Samuel Laird](/156wd-laird-selection-of-sermons/), [The True Church by Emanuel Greenwald](/s02-greenwald-the-true-church/), [Devotional Readings from Luther's Works For Every Day of the Year](/249lu-sander-devotional-readings-luthers-works/), and [Funeral Sermons by Lutheran Divines edited by Rev. Lewis Herman Schuh](/188wd-schuh-funeral-sermons/).  If you missed any of these, they are still available for download by clicking the links.

# Format Updates

As of today, 23 Lutheran Library titles are available in version 2 (v2) format.  If you acquired any of these ebooks more than a month or two ago, you might want to request an update.   Here are the authors and titles now released in v2:

### Dau, William Herman Theodore, (1864-1944)

Luther Examined and Reexamined

### Greenwald, Emanuel, (1811-1885)

Baptism of Children

The True Church

### Horine, Mahlon C., (1838-1917)

Practical Reflections on the Book of Ruth

### Jacobs, Henry Eyster, (1844-1932)

Summary of the Christian Faith

### Keyser, Leander Sylvester, (1856-1937) 

The Conflict of Fundamentalism and Modernism

### Krauth, Charles Porterfield, (1823-1883)

The Sayings of Krauth

### Laird, Samuel, (1835-1913)

Selection of Sermons

### Lehmanowsky, John Jacob, (1773-1858)
    
Under Two Captains – The Autobiography of John Jacob Lehmanowsky

### Lichtenstein, Isaac, (1824-1908)

An Appeal to the Jewish People

### Loy, Matthias, (1828-1915)

Doctrine of Justification

### Long, Simon Peter, (1860-1929)

The Way Made Plain

Prepare to Meet Thy God

### Sander, John (1850-1937)

Devotional Readings from Luther's Works

### Schmauk, Theodore Emanuel, (1860-1920)

The Confessional Principle and The Confessions of The Lutheran Church As Embodying The Evangelical Confession of The Christian Church

### Schuh, Lewis Herman, (1858-1936)

Funeral Sermons from Lutheran Divines

### Scriver, Christian, (1629-1693)

Gotthold's Emblems

### Whitteker, John Edwin, (1851-1925)

Gospel Truths

### Carr, Walter E.

The Story of Five Dogs

### Jackson, Gabrielle Emilie, 

The Adventures Of Tommy Postoffice: The True Story Of A Cat by Gabrielle Emilie Jackson

### Tappan, Eva March, (1854-1930)

Dixie Kitten

### Young, Egerton Ryerson, (1840-1909)

My Dogs in the Northland

### Anonymous

Lutheran Treasury of Prayers

# Thank you

Thank you for your interest in the books of these heroic Christian men.  Read them, love them, enjoy them.  

And thank you for your prayers for Christ's church, for the young people, and for us.  May God continue to save and take care of His sheep.

The goal of the Lutheran Library is to re-release well-written and readable books from sound, faithful American Lutherans of the past for the enjoyment and edification of a new generation.  All books are available at [lutheranlibrary.org](https://www.lutheranlibrary.org/) for free download in a variety of formats for Kindle, Apple, and other devices.

Your help is appreciated in spreading the word as often and in as many ways as you feel is appropriate.

May God bless you and keep you, help you, defend you, and lead you to know the depth of His love.  *Amen*