---
date: 2019-02-01 00:18:00
publishDate: 2019-02-01 00:18:00-05:00
title: "Benjamin Kurtz: A Biographical Sketch"
slug: "benjamin-kurtz"
categories: ["Biographical Sketches"]
tags: ["Kurtz", "Lutheran Ministers"]
authors: ["Kurtz, Benjamin"]
synods: ["Independent"]
---

[![Benjamin Kurtz](/img/posts/kurtz-benjamin.jpg)](https://www.lutheranlibrary.org/authors/kurtz-benjamin/)


# About Rev. Benjamin Kurtz, D.D., LL.D. 


Dr. Benjamin Kurtz came to Baltimore in August, 1833, to assume the editorial charge of the _Lutheran Observer_. He was at this time a widower and not in vigorous health. He had little experience in writing, and he had some difficulty in pruning his superfluous verbiage; but he acquired a vigorous, if not ornate style, and rendered invaluable service to the church in this position. He had no other employment and was ambitious of success. He was not under the control of any Synod or Board, and pursued his own independent way. He maintained this position by himself for about fifteen years, until the establishment of the book and publishing office, principally through his own agency. He superintended that institution with great ability and success, for he had eminent business capacity. 

In 1826 Dr. Kurtz went to Germany to solicit donations of money and books for the theological seminary about to be established at Gettysburg. He remained absent nearly two years, and brought home about $10,000 in money and a large number of books. Whilst in Germany he received many courtesies from all classes of men, and secured extensive popularity as a plain and impressive preacher. Immense crowds everywhere attended the churches in which he officiated. He went a second time to Europe in 1846 to attend the Evangelical Alliance in London, in August of that year, and for recreation. During the time when mesmerism was rampant in this country he became a believer in the system; not in its lower, but in its higher manifestations, and was president of a society of intelligent gentlemen who prosecuted the subject as a matter of metaphysical research. 

In 1834 he was elected Professor of History and German Literature in Pennsylvania College, and Professor in the Theological Seminary, both of which he declined. He loved his work on the _Observer_ too well to give it up for any other. 

Dr. Kurtz stoutly maintained what was called the Evangelical stand-point, and was an ardent advocate of what, in his day, were called new measures. He was not what we call a learned man or a profound theologian. He had no college training in early life, but he was uncommonly intelligent in all the ordinary affairs of life and achieved more good in the ministry than many of far greater attainments. 

The degree of D. D. was conferred upon him by the Washington College, and that of LL. D. by the Wittenberg College. He died in Baltimore, Dec. 29, 1865.— John Morris. 

{{% alert note %}}
This biography is taken from Jens Jensson's _American Lutheran Biographies_, published 1890.
{{% /alert %}}

First published online 2019 at [LutheranLibrary.org](.).