---
date: 2019-05-07 19:26:51-04:00
publishDate: 2019-05-07 19:26:51-04:00
title: "Devotional Readings from Luther's Works For Every Day of the Year"
slug: "249lu-sander-devotional-readings-luthers-works"
categories: ["Lutheran Library Publications"]
tags: ["Devotional", "Luther", "Start Here"]
authors: ["Luther, Martin", "Sander, John"]
titles: ["Devotional Readings from Luther's Works For Every Day of the Year"]
origpublishers: ["Augustana Book Concern"]
origdates: ["1915"]
synods: ["Augustana Synod"]

---
"America does not realize what an inheritance she has received from Luther and the Lutheran Reformation. The best way to understand Luther is to have Luther himself speak. His writings are so extensive that there is no trouble in finding something profitable for almost every occasion and condition in life. – Rev. John Sander

{{% toc %}}

# Luther At His Best In His Sermons

If Luther is mentioned positively at all, it's usually in the context of _The Bondage of the Will_.  This is sad.  The true glory and wisdom of Martin Luther is most evident in his sermons, which he strove to make accessible to everyone.

Dr. Lenker, of blessed memory, who did so much to make Luther's sermons available in English rightly said, "Read Luther! The Faith of the Fathers in the Language of the Children!"  

# Dedication

This Lutheran Library republication of _Devotional Readings from Luther's Works_ is dedicated to R. K., without whom this book might well have been forgotten.


# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)

![book cover](/img/posts/249lu-sander-devotional-readings-luthers-works-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/249lu-sander-devotional-readings-luthers-works.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 249lu&body=Please send 249lu-sander-devotional-readings-luthers-works.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 249lu&body=Please send 249lu-sander-devotional-readings-luthers-works.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2018-05-17 
- _Version 4 update_: 2019-05-07 
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
