---
date: 2019-08-18 05:09:37-04:00
publishDate: 2019-08-18 05:09:37-04:00
title: "John W. Richards: A Biographical Sketch"
slug: "john-w-richards"
categories: ["Biographical Sketches"]
tags: ["Richards", "Lutheran Ministers", "Evangelical Review"]
authors: ["Richards, John", "Krauth, Charles Porterfield"]
synods: ["Pennsylvania Synod"]
---

January 27th, 1854, will long be remembered in the city of Reading. It was the day when nearly the whole community crowded to the sanctuary, not to listen to the voice of the pastor of the church, but to gaze for the last time on his lifeless remains, and to pay the last tribute of respect to one who was highly esteemed in life. His voice was silent in death, but his virtues were still fresh in the remembrance of the people. All felt that a good man had been taken away, that his place could not be easily supplied. 

{{% toc %}}


## John W. Richards, D. D. 

As the tidings of Dr. Richards' death spread over the land, a sensation of deep and general regret was experienced. The church was struck with simultaneous sorrow. The ministry, in whose ranks a void had been created, earnestly looked for one upon whom the mantle of the ascending spirit might fall. The excellence of his character, the importance of his services, the value of his counsels, and the weight of his influence, all conspired to render the event most afflicting. In the prime of manhood, and the meridian of his usefulness, the summons came, the call must be obeyed! The congregation to which he ministered, presented, at the time, a spectacle of interest and progress. The Synod, of which he was a valued member, turned to him, as one whose counsels could be safely followed. Every object, with which he was connected, indicated the presence of his spirit, and a future of the highest promise. His home was the abode of affection, piety and happiness. All things under his care, appeared prosperous and successful. He was actively and zealously engaged in his Master's service. It was then the message reached him! In a few moments, _the silver cord was loosed, the golden bowl broken at the fountain_. The struggle was over, the battle fought, the victory won! His mission was done, his journey finished! His spirit had returned to God, who gave it. He was translated from the scenes of his earthly efforts, to a higher sphere, and a more exalted position. He passed at once from his labors to his reward. Of him, it might be truly said, that he walked with God, and he was not, for God took him. 

## What Can We Make of Premature Death?

In considering the bereavement with which the church is visited, in the premature death of one of her watchmen, under circumstances peculiarly afflicting, the question naturally arises, why are the good, the pure, the useful and the faithful, taken away from us in the midst of their activity? Human reason cannot answer the inquiry. Revelation alone gives the assurance, that God ordereth all things well. Though sight cannot perceive, nor reason unfathom the inscrutable ways of Providence, we are told, by the word of inspiration, that that, which is at present dark and mysterious, will become light and clear, will result in illustrating God's character, and in rendering his benevolence most glorious. In heaven we shall perfectly comprehend that which seemed to us here quite strange. Many a doubt will there be fully solved, many a perplexity entirely removed, many a mystery satisfactorily explained. We shall be led to adore the Divine goodness, and magnify infinite wisdom. 

>"God nothing does, nor suffers to be done, \
>But thou wouldst do thyself, couldst thou but see \
>The end of all events as well as He." 

## Early History

The subject of our sketch was born in Reading, Pa., April 18th, 1803. He was the son of Matthias Richards, for many years an Associate Judge of the Courts in Berks County, and grandson of Henry Melchior Muhlenberg, D. D., the apostle of Lutheranism on this Western continent. He too, was blessed with pious parents, who early instructed him in the principles of piety, and restrained him from outward acts of immorality. In 1819, when in the sixteenth year of his age, he made a profession of religion, by the principles of which, his whole subsequent career was eminently controlled. He united with Trinity church, Reading — of which H. A. Muhlenberg, D. D., was at the time pastor. His classical studies were principally pursued under the direction of John Grier, D. D., who then had charge of the Academy, in his native place. On the completion of his Academic course, being deeply impressed with the idea that he was called to the ministry of reconciliation. lie at once, in the year 1821, commenced his Theological reading with his pastor, Rev. Dr. Muhlenberg, whose instructions he continued to receive, until the fall of 1824, when he applied to the Synod of Pennsylvania for license to preach the gospel. After a satisfactory examination, on the subjects required, conducted by Rev. Drs. Ernst and Miller, he was solemnly set apart to the ministry, the Rev. Dr. Endress officiating on the occasion. With this ecclesiastical body he remained connected until the time of his death, enjoying the confidence and esteem of the members, and repeatedly holding appointments of honor and trust in the Synod. 

## Start of Ministry

Dr. Richards' first charge embraced the church at New Holland, Lancaster County, and four other congregations in the vicinity. In the Spring of 1834 he resigned and removed to the Trappe, Montgomery County, which had been the scene of his grandfather's early labors. In 1830 he received and accepted a call to Germantown, Pa., where he continued to officiate until the autumn of 1845, when he became pastor of St. John's church, Easton Pa. In this field, as in all his previous charges, he preached in the English and German languages, and labored successfully in building up the church, "cheered," in his own words, "by the kindness and cooperation of his people." During his residence here, he held, by the appointment of the Trustees, the Professorship of German Language and Literature in Lafayette College. His connection with the congregation at Easton, he most reluctantly relinquished. 

Influenced, however, by the advice of many of his ministerial brethren, and impelled by what he considered the leadings of Providence, he consented, in the Spring of 1851, to take charge of Trinity church, Reading, in which he had been brought up, and which had become vacant by the death of Jacob Miller, D. D. It was considered a difficult station to fill. Dr. Richards, in the estimation of all, seemed admirably adapted to this field of labor. His kind, conciliatory disposition, the influence he possessed over the German, as well as the English community, the position he occupied in his Synod, designated him as the individual for the situation. He had just commenced his career under the most favorable auspices. His prospects were encouraging. The obstacles in the way of his success were vanishing. The church was flourishing. The schools and societies were in a healthful condition. He possessed the affection of his people and the confidence of the public. Every thing in the future was bright and full of promise. But God's ways are not as our ways, nor his thoughts as our thoughts. The deceased had suffered, on various occasions, from an affection of the heart, but his general health was good. No dangerous consequences were anticipated. He had risen, the morning of his death, as well as usual, and had just committed to the grave, one of his own flock. During the exercises he felt pain, and at the conclusion of the service immediately repaired to his residence. He complained of suffering and was assisted to bed. Medical aid was summoned, but before it could reach him, he was a corpse. He expired in less than fifteen minutes after his return home. Without a sigh or a groan, he closed his eyes on earth, and opened them in heaven. In the trying hour he was sustained by the strong arm of Jehovah. As he walked through the valley of the shadow of death, he feared no evil, for God was with him. He died January 24th, 1854, in the fifty-first year of his age. On the following Friday his body was borne to the grave. The funeral services were conducted by Rev. G. A. Wenzel, J. C. Baker, D. D., and C. R. Demme, D. D. Dr. Baker delivered a sermon in the English language from the words: _Set thine house in order, for thou shalt die and not live_; and Dr. Demme in the German, from the text: _Blessed are the dead, who die in the Lord from henceforth: yea, saith the Spirit, that they may rest from their labors; and their works do follow them_. In the Charles Evans Cemetery the remains of this servant of God lie buried. A neat stone marks the spot, with the following simple inscription: 

> Rev. JOHN W. RICHARDS, D. D. 

>__Born April 18, 1803 — Died January 24, 1854__. 

>_Remember the words I spake unto you_. 

## A Useful and Faithful Man

Although Dr. Richards' life is unmarked by any striking occurrences, and presents little of stirring incident to diversify its course, he was a most useful man, and evidences of his pastoral fidelity, zeal, and efficiency, are to be found in every community in which he was called to labor. His duties were uniform, and generally of an onerous nature. During the course of his ministry, embracing a period of thirty years, he received into the church 1292 persons, baptized 2362, married 631, and buried 951. He was always much devoted to the people of his charge, and labored in every way to promote their highest good. He took pains to become acquainted with them all, and in his intercourse, exhibited the character of a faithful minister, and of the affectionate, warm-hearted, sympathizing friend. His whole deportment was courteous and affable, so that even the most diffident and timid felt no embarrassment in his presence. The cordiality with which he met them, at once inspired confidence, and opened the way for the most unrestrained approach. No one in affliction or distress, could go to him, without meeting generous sympathy and kind encouragement. His active benevolence and philanthropic spirit, made him an object of affection and gratitude. His congregations appeared to appreciate the interest he manifested in their welfare, and to reciprocate the esteem he cherished for them. And we doubt not that since "his spirit" has  "passed away from the checkered scenes of life, and the turf" has grown "green o'er his grave," in accordance with the wish expressed only a short time before his departure, his labors do "still speak of his affectionate regard for his people, and perpetuate in their hearts the memory of their pastor." 

## Good Works

His efforts to do good were not confined to his labors in the pulpit. With those with whom he was ecclesiastically connected, he labored harmoniously to promote the general interests of the church. In associations for religious and benevolent objects, he was an active and efficient member. He was the warm friend and zealous supporter of every project for doing good, and carrying on the great work of moral and intellectual improvement. Some idea of the interest which he took in his people, and the teachings he inculcated, may be gathered from the following exhortation, extracted from his valedictory discourse, on his departure from Easton: 

>"To perform your duty, in view of your great accountability, you must _not be weary in well doing_. Be fervent in your religious exercises, and zealous in the aid of your religious societies. Let the Sunday Schools, the Missionary and Education cause, the Benevolent association, also the Bible, Tract, Colportage, Temperance, and kindred causes, ever lie near to your heart. Let your pastor be very dear to you, and encourage him in his arduous labors. Be regular in your attendance on divine service, and frequently and worthily partake of the Lord's Supper. Search carefully the Scriptures; watch and pray without ceasing; guard well your hearts, and abstain from the very appearance of evil. Suffer not your children to grow up without baptism. Send them faithfully to the Sunday School, and bring them with you, when of a suitable age, to the sanctuary. By all means regard it as your most sacred duty to have them instructed and confirmed in the Christian religion, according to the doctrines and usage of the church." 

## Loved the Lutheran Church

He loved his church, the church in which he had been reared. He was attached to its doctrines and usages, its institutions and its benevolent efforts. He was not illiberal in feeling, or proscriptive in action, he was willing to unite with Christians of every name, in efforts to do good; yet he had little sympathy with those, who could abandon the communion of their fathers, and forsake the sphere of labor in which Providence seemed to call them, and which afforded opportunities of usefulness. In a printed sermon,[^bGy] lying on our table, we find the following sentiments on this subject. After urging parents to train up their children in the church of their fathers, he continues: 

>"We have always lived in peace with our evangelical sister churches, among whom we number many kind and dear brethren; while we say, therefore, live in charity with them, we nevertheless add, only never forget your solemn obligations to your own Zion, and let your children participate with you in its privileges, for you will find no other church more scriptural in doctrine. The children need not be ashamed of the church in which their fathers gloried; they need not fear to be lost in the communion, in which their kindred were saved. Let the Colleges and Seminaries of the church, her religious publications, her benevolent enterprises, her foreign and domestic missions, her education cause; in short, let every thing connected with the Evangelical Lutheran church awaken a deep interest in you; and let your exertions to promote her prosperity, be a light to guide others in the same glorious path." 

In another discourse,[^bGz] we find the following paragraph, indicating most clearly his strong church feeling: 

>"I am none of those, who consider my own church as the only one within whose pale salvation can be found: nor do I condemn and denounce all other churches, merely because they differ in nonessentials from mine. God forbid! My church and my Bible have not so taught me Christ. But on the contrary, neither am I one of those, who do not think my own church as good as the best of any other denomination." 

For the patriarchs of our church he had the most exalted regard.[^bGA] 

>"Our fathers," 

says he, 

>"undoubtedly deserve, in many instances, a high eulogy. _There were giants in the land in those days_. They built fine churches and parsonages, and erected schools (a distinguishing trait of Lutheranism) under most straitened circumstances. They sustained those churches and schools under very trying difficulties. They adhered faithfully to the doctrines and usages of the Reformation, amid powerful temptations; they attended the worship of their own sanctuary, though far removed from it, when it would have been easier to have framed excuses for neglecting it, than many find at present. They instructed their children carefully in the creed of their church, and united them with its fellowship. They introduced a most scriptural discipline, and impartially enforced it. They elevated high the standard of piety, and above all, they glorified their faith with a holy walk, and departed this life in its blissful triumphs. In these respects they lose nothing in comparison with any of the people of God; alas! alas! how fallen many of their offspring." 

[^bGy]:  Sermon delivered at the close of his ministry, Easton, Pa., March 9th, 1851. By the pastor, Rev. John W. Richards. 

[^bGz]: Centenary Jubilee of the Evangelical Lutheran church, Trappe, Pa. Sermon by Rev. J. W. Richards, Germantown, Pa. page 32. 

[^bGA]:  _Ib_. p. 35. 


## Strictly Orthodox

In his theological creed, Dr. Richards was strictly orthodox. Whilst he regarded "the word of God as the only and sufficient rule of faith," he entertained a most profound veneration for the standards of the church. The Augsburg Confession he cordially adopted, as a summary of the fundamental doctrines of the Bible, "not to supersede," to use his own language, "but to elucidate the word of God, or rather to arrange its doctrines methodically, for the sake of perspicuity." He calls it a glorious creed,[^bGB] and asks us to look at some of its fundamental doctrines. 

>"They are," 

he observes, 

>"the Trinity; the entire depravity of our fallen nature; the Deity and incarnation of Christ; the divinity and personality of the Holy Ghost; the atonement for sin through the sufferings and death of Christ, and the merits of his blood; the necessity of regeneration through the influences of the Holy Ghost; Justification by faith; the obligation of the moral law, _viz_: good works, including purity of heart and life, the parity in the ministerial office; the means of grace; the resurrection of the dead; the final judgment, and the eternity of future rewards and punishments. Who will not acknowledge these to be truly scriptural?" 

Dr. Richards was not, by any means, a brilliant preacher, yet he was instructive and evangelical. His discourses were simple and scriptural. The subjects he discussed, were the common, but important doctrines of the gospel. Their truths were enforced and commended to his audiences with much tenderness and earnestness. The services of the sanctuary he always conducted with great dignity and solemnity. 

[^bGB]:  Centenary Jubilee, page 30. 

## Translator of The Halle Reports (_Hallische Nachrichten_)

His numerous pastoral duties prevented Dr. Richards from leaving behind him any important literary monument. He did sometimes allow a sermon to be printed, and occasionally contributed to the pages of the _Evangelical Review_. He also commenced the translation of the _Hallische Nachrichten_, in which he had, at the time of his death, made considerable progress. This is a volume of fifteen hundred and eighteen pages, and contains a narrative of the establishment and early progress of the American Lutheran church, prepared principally by Drs. Muhlenberg, Brunholtz, Kunze and Helmuth. It is an exceedingly important work, and we trust that some one, competent to the task, may be disposed to take charge of Dr. Richards' manuscripts, and complete the undertaking. 

Our General Synod, at its convention in Charlestown, S. C., in 1851, by a unanimous resolution, expressed a deep interest in Dr. R's. labors, and commended the enterprise to the attention of our members. The subject of our narrative was honored with the degree of D. D., from Jefferson College, Canonsburg, Pa., at its annual commencement in 1852. 

## Sincere and Consistent Faith

Dr. Richards was a man of unfeigned piety. Religion, with him, seemed to be a fixed principle, and to predominate in his character, as a controlling agency. It was not feverish and inconsistent. It did not go and come by fits and starts. It was not confined to favorable junctures or circumstances, but its steady light shone forth at all seasons, and in all places, and burned with a pure and steady flame. He never assumed an appearance, which did not correspond with his habitual principles. There was a beautiful symmetry in his character. He was always the same spiritual, active, and devoted minister of the Lord Jesus, the same burning and shining light in the church of God. He never forgot his position as an ambassador of Christ. He expected to be justified by faith alone, yet not by a faith unattended by good works. He depended on the gracious influences of the Holy Spirit, for aid in the performance of every duty, yet he diligently made use of the means afforded for his spiritual progress. 

In all the various relations of private and social life, he uniformly evinced that conscientious fidelity, that honesty of purpose and singleness of aim, which the rules of the gospel prescribe, and the grace of the gospel inspires. He sometimes encountered opposition, yet his course was such as frequently to disarm hostility, and conciliate, where others would only have strengthened prejudices, and increased opposition. He was of a quiet, retiring, and unobtrusive spirit, of mild, and pleasant manners, of a confiding nature, great kindness and warm sympathy. Ill health had produced a tendency to a gentle melancholy, which often stole over him, and gave a tinge to his character. He may have made mistakes, for who that is human, can lay claim to infallibility? 

>_Nam vitiis nemo sine nascitur, optimus ille est_ \ 
>_Qui minimis urgetur_. 

>[In fact, no one is born without vices and, happy is he that has the fewest.]

He may sometimes have exhibited infirmities, which will cling to us, while we abide in the flesh, yet he was generally careful and judicious, and always sincere and conscientious. He was cautious, and disposed to sacrifice much for peace, yet he was independent and bold in the discharge of duty. The fear of God, and the approbation of his own heart, he made the rule of his life, and the standard of his actions. He was domestic in his feelings, and very attentive to his family. He enjoyed the retirement of home, and bestowed great care upon his children. In all his habits he was extremely neat and methodical. In every article of his dress, in all the furniture of his house, in the arrangement of his papers and books, the most remarkable order was observed. His manuscripts were all most carefully written and his accounts most accurately kept. His entries in the church record, were made with the greatest precision, so as frequently to elicit the highest praise. Every thing was done by him with exact rule. He had a wonderful fondness for collecting statistical and analogous treasures. He had a profound regard for time-honored customs, and a deep reverence for sacred places. He was industrious, and permitted no day to pass without accomplishing something. He took a deep interest in any thing which engaged his attention. He was disposed to act on Shakespeare's principle — "No profit grows, where is no pleasure taken." He enjoyed the luxury of doing good, and he had the satisfaction of knowing that he lived to some purpose. He might readily have answered to the description given by the author of the Task: 

>"I would express him simple, grave, sincere, \
>In doctrine uncorrupt, in language plain, \
>And plain in manner; decent, solemn, chaste. \
>And natural in gesture; much impressed \
>Himself, as conscious of his awful charge, \
>And anxious mainly that the flock he fed \
>Might feel it too; affectionate in look, \
>And tender in address, as well becomes \
>A messenger of grace to guilty men."

## His Character 

Writes one[^bGC]  who was long associated with him in the sacred office: 

>"That Dr. Richards was a truly pious and most amiable man, all acknowledge who had any acquaintance with him; and it is also known that all the congregations of which he was the pastor, prospered under him." 

Another[^bGD] who knew him well, thus testifies: 

>"My own intercourse with Dr. Richards was most delightful. I never knew a more consistent, trustworthy man. He was prudent, thoughtful and conscientious, active and zealous in every good work, and what I greatly admired, was the harmony in his character; he was never found wanting, but was never guilty of ultraism in deed or word — exceedingly firm in matters of principle, but winning in social life." 

[^bGC]:  Rev. Dr. Baker, of Philadelphia. 


The memory of such a man cannot die. The influence of character cannot be destroyed by death. It survives the dissolution of the body, and continues unfading and immortal. It lingers among us after the "sunset of the tomb," to shed light, and to diffuse a rich fragrance upon those who still remain. The memory of the just still lives. The righteous shall be had in everlasting remembrance. Our friend and brother has gone to his rest! We should not murmur or repine. Let us rather be thankful, that God spared him thus long to the church. Our loss is his gain. There is a pleasure in the thought that his redeemed spirit, freed forever from the cares, the troubles, the conflicts, the turmoils, and the sorrows of earth, has gone —

>&emsp;&emsp;"To repose, deep repose, \
>Far from the unquietness of life, from noise \
>And tumult far — beyond the flying clouds, \
>Beyond the stars and all this passing scene, \
>Where change shall cease, and time shall be no more." 

[^bGD]: Rev. Dr. Schaeffer, of Easton, Pa, 



# Publication Information

- _Author_: __"Krauth, Charles P."__, Editor.
- _Journal_: __"The Evangelical Review."__
- _Originally Published_: 1854.
- _Lutheran Library Edition_: 2019
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

{{% alert note %}}
{{% biographical-sketch-note %}}
{{% request-pdf %}}
{{% /alert %}}
