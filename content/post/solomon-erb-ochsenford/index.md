---
publishDate: 2019-02-13 05:01:00-05:00
date: 2019-02-13 05:01:00-05:00
title: "Solomon Erb Ochsenford: A Biographical Sketch"
slug: "solomon-erb-ochsenford"
categories: ["Biographical Sketches"]
tags: ["Ochsenford", "Lutheran Ministers"]
authors: ["Ochsenford, Solomon Erb"]
synods: ["General Council", "Pennsylvania Synod"]
---


Solomon Erb Ochsenford, son of Jesse and Mary Ochsenford, was born in Douglass Township, Montgomery county, Pennsylvania, on November 8, 1855. His earlier educational advantages were limited, owing to the straitened circumstances of his parents; hence the years of childhood and youth were spent in the country, near Falkner Swamp, one of the earliest German settlements in the state of Pennsylvania. The public schools afforded the advantage of acquiring the rudiments of an education. He received his preparatory training in Mount Pleasant Seminary, Boyertown, Pennsylvania, 1871-73; in the fall of the latter year he entered the Sophomore class in Muhlenberg college, Allentown, Pennsylvania, graduating in 1876. 

His theological training he received in the Lutheran Theological Seminary at Philadelphia, 1876-79, under Drs. Krauth, Mann, Charles Frederick and O. W. Schaeffer, and Spaeth. On June 9, 1879, he was ordained to the office of the ministry in the Lutheran church, by the Evangelical Lutheran Ministerium of Pennsylvania and adjacent states, the oldest Lutheran Synod in America. 

In September of the same year he became pastor of the Selinsgrove parish, consisting of the first Evangelical Lutheran church, Selinsgrove, and Zion's Lutheran church, Kratzerville. In 1888 he organized a congregation at Yerdilla, which he serves in connection with the other two. In 1884-5 a handsome church was erected in Selinsgrove, to take the place of the old log church erected in 1803-4. Although he has received a number of calls to other and more desirable parishes, yet he has preferred to remain with the people among whom he has spent his first years in the ministry. 

The subject of this sketch has always had a passion for study and early in life acquired the habit of studying with pen in hand; as a result of a habit of this kind, a number of literary efforts have been published. The following productions of his pen have been published in book form: _My First Book in Sunday School and Home_, Reading, Pennsylvania, 1883 (Second edition, 1889); _The Lutheran Church in Selinsgrove_, Selinsgrove, Pennsylvania, 1884; [_The Passion Story_](/213wd-ochsenford-the-passion-story/), Philadelphia, 1889. Among his more important published articles may be mentioned, "History of our Telugu Mission from the beginning until 1884," in _Foreign Mission_, Philadelphia, 1884-5, "Lutheran Church in America" _Lutheran Church Review_, Philadelphia, 1885; "Lutheran Doctrines", in _Church Messenger_, Allentown, Pennsylvania, 1886-88: the article "Lutherans" for _Appleton's Annual Cyclopedia_ for the years 1884-89, with the understanding that he is to furnish the same article in the future; One Hundred Sketches of Lutheran Ministers and Laymen for _Appleton's Cyclopedia of American Biography_, published in 1886-89; "Lutheran History", _Lutheran Church Review_, Philadelphia, 1888; "Salzburg and the Salzburg Lutherans," _Lutheran Church Review_, 1888; and numerous other articles in the various church periodicals, such as "_The Lutheran_", "_Church Messenger_", _Lutheran Church Review_", etc. He was statistical editor of _Church Almanac_, Philadelpia, 1883-4, 1889; editor of _The Lutheran Church Annual_, Philadelphia, since 1890. 

Since 1886 he has been devoting much of his leisure time to the study of Lutheran history and has in course of preparation an extended outline of the history of the Lutheran Church in America, and a history of the General Council, nearly completed. In the year 1889, he was elected editor of a book to be issued in the interest of Muhlenberg College, in 1892, the quarter-centennial of the establishment of the famed institution. On the 21st of October, 1890, Rev. Ochsenford was elected president of the Fifth Conference of the Ministerium of Pennsylvania. 

{{% alert note %}}
This biography is taken from Jens Jensson's _American Lutheran Biographies_, published 1890.
{{% /alert %}}

First published online 2019 at [LutheranLibrary.org](.).