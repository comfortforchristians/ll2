---
date: 2019-04-25 05:20:31-04:00
publishDate: 2019-04-25 05:20:31-04:00
title: "The Cats' Arabian Nights by Abby Morton Diaz"
slug: "e10-diaz-cats-arabian-nights"
aliases:
  - /e26-smith-story-of-zephyr-a-christmas-story/
categories: ["Lutheran Library Publications"]
tags: ["Fiction", "Animals", "Children", "Cats"]
authors: ["Diaz, Abby Morton"]
titles: ["The Cats' Arabian Nights"]
origpublishers: ["D. Lothrop Company"]
origdates: ["1881"]
synods: ["N/A"]
---
"One evening when a company of children and older people were looking at funny cat-pictures and telling cat-stories, a little ten year-old girl asked: 'Why can there not be a Cats' Arabian Nights Story Book?'"

{{% toc %}}

"There would have to be a Cat King, or Emperor, or Sultan," said her next older sister. 

"And a Cat Queen, or Empress, or Sultaness," said their cousin Joe, the sailor. 

"And she would have to go on, and on, and on, and on, and on, and on, and on, telling stories in order to save her own life," said their cousin Lucia. 

"I propose," said uncle Fred, "that cousin Lucia put together a Cats' Arabian Nights for little children, and have it ready to read to our little children when they all shall come next summer with their fathers and mothers." 

"Oh yes! Yes! Do! Pray do! Won't you do it? Say you will! Say you will!" cried many voices. 

"I think it will be fun to do it," said cousin Lucia, "if you allow me to put in some make believe and nonsense, if I want to." 

"Certainly!" was the cry. "Put in anything. Anything you please!" 

Cousin Lucia said she was willing to try, and thus it happened that the summer-children and others got a story book beginning, as all story books should begin, with – once upon a time. 

![Balance](/img/posts/i00-balance.jpg)

# Chapters

- How It Happened.
- King Grimalkum And Pussyanita; Or, The Cats’ Arabian Nights.
- The Story Of Pinky-White.
- The Story Of Black Velvet
- What Snowball Told
- Madame Pussy Hunters Story
- The Spry White Kitten’s Story
- Mrs. Beulah Black’s Story
- David’s Story
- The Kittywinkses
- Tweedledum And Tweedledee
- Tabby Furpurr
- Story Of Mistress Tabby Furpurr
- The Story Of The Feeble Cat And Her Nine Lives
- The Story Of The Two Charcoals And The Four Spekkums
- The Story Of The Janjibo, And Of The Frog And The Rat
- A Spinning Story
- The Blind Mice Story
- The Air-ball Story

# Download the eBook 

Use the following links to download:

[<i class="fas fa-arrow-circle-down"></i>](#download) 

![book cover](/img/posts/e10-diaz-cats-arabian-nights-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/e10-diaz-cats-arabian-nights.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request e10&body=Please send e10-diaz-cats-arabian-nights.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request e10&body=Please send e10-diaz-cats-arabian-nights.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2018-06-19
- _Version 4 update_: 2019-04-23 
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

