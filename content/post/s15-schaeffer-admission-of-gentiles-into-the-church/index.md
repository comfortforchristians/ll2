---
date: 2019-05-07 19:32:08-04:00
publishDate: 2019-05-07 19:32:08-04:00
title: "When Gentiles First Entered The Church by Charles William Schaeffer"
slug: "s15-schaeffer-admission-of-gentiles-into-the-church"
categories: ["Lutheran Library Publications"]
tags: ["Schaeffer","Short Books","Evangelical Review"]
authors: ["Schaeffer, Charles William"]
titles: ["When Gentiles First Entered The Church"]
origpublishers: ["Evangelical Review"]
origdates: ["1850"]
synods: ["N/A"]
---
"The prophecies of the Old Testament concerning the Gentiles had been so numerous and diversified that nothing but the partial blindness of Israel could have prevented them from entertaining a general expectation of the speedy accession of all nations to the kingdom of God...

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/s15-schaeffer-admission-of-gentiles-into-the-church-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/s15-schaeffer-admission-of-gentiles-into-the-church.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request s15-schaeffer-admission-of-gentiles-into-the-church&body=Please send s15-schaeffer-admission-of-gentiles-into-the-church.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request s15-schaeffer-admission-of-gentiles-into-the-church&body=Please send s15-schaeffer-admission-of-gentiles-into-the-church.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2019-05-07
- _Version 4 update_: 2019-05-07 
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
