---
date: 2019-04-11 10:16:02-04:00
publishDate: 2019-04-11 10:16:07-04:00
title: "The Child, The Wise Man, and The Devil by Coulson Kernahan"
slug: "s18-kernahan-child-wise-man-and-devil"
categories: ["Lutheran Library Publications"]
tags: ["Short Books", "Kernahan", "Fiction", "Gospel"]
authors: ["Kernahan, Coulson"]
titles: ["The Child, The Wise Man, and The Devil"]
origpublishers: ["Dodd, Mead and Company"]
origdates: ["1896"]
synods: ["Anglican"]
---
"They who declared that reason would not allow them to believe that God could once become Incarnate, saw no reason to doubt the manifold Reincarnation of Man. They who complained that they found the straight and level highway of Christianity too difficult a road for them to follow, or that there was no sure foothold therein, were content to lose themselves among the mazes of Superstition, or to flounder and stumble among the stony wastes of Unbelief. And many I saw who wandered backward and forward aimlessly, as if seeking for something which they found not. And ever and anon one would cry out, 'Lo, I have it!' and the others would cease their search, and run with gladness to hear him. But so often as one thus called out, so often would they who ran return whence they came, unsatisfied and unfilled, until not a few ceased to give ear at all."


# About the Author

From [Wikipedia](https://en.wikipedia.org/wiki/Coulson_Kernahan):

"John Coulson Kernahan was born in Ilfracombe, Devon to Rev. James Kernahan, M.A., F.G.S., and his wife Comfort...Kernahan was educated privately by his father and at St Albans School. James had intended for young Coulson to enter the church, but his son's 'intentions were towards literature.'...Most of Kernahan's books and works received positive reviews and recognition."

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/s18-kernahan-child-wise-man-and-devil-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/s18-kernahan-child-wise-man-and-devil.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request s18-kernahan-child-wise-man-and-devil&body=Please send s18-kernahan-child-wise-man-and-devil.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request s18-kernahan-child-wise-man-and-devil&body=Please send s18-kernahan-child-wise-man-and-devil.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}

