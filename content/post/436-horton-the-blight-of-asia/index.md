---
date: 2019-05-08 19:40:54-04:00
publishDate: 2019-05-08 19:40:54-04:00
title: "The Blight Of Asia. An Account Of The Systematic Extermination Of Christian Populations By Muslims And Of The Culpability Of Certain Great Powers. With A True Story Of The Burning Of Smyrna by George Horton"
slug: "436-horton-the-blight-of-asia"
categories: ["Lutheran Library Publications"]
tags: ["Horton", "Islam", "Martyrs", "Armenian Genocide"]
authors: ["Horton, George"]
titles: ["The Blight of Asia"]
origpublishers: ["The Bobbs-Merrill Company Publishers"]
origdates: ["1926"]
synods: ["Unknown"]
---

"The last act in the fearful drama of the extermination of Christianity in the Byzantine Empire was the burning of Smyrna by the troops of Mustapha Khemah. The murder of the Armenian race had been practically consummated during the years 1915-1916, and the prosperous and populous Greek colonies, with the exception of Smyrna itself, had been ferociously destroyed. 

"The destruction of Smyrna happened, however, in 1922, and no act ever perpetrated by the Turkish race in all its bloodstained history, has been characterized by more brutal and lustful features, nor more productive of the worst forms of human sufferings inflicted on the defenseless and unarmed. It was a fittingly lurid and Satanic finale to the whole dreadful tragedy. 

"The torch was applied to that ill-fated city and it was all systematically burned by the soldiers of Mustapha Khemal in order to exterminate Christianity in Asia Minor and to render it impossible for the Christians to return. 

"I have been cognizant of what was going on for a number of years and when I came back to America after the Smyrna tragedy and saw the prosperous people crowded in their snug warm churches, I could hardly restrain myself from rising to my feet and shouting, "For every convert that you make here, a Christian throat is being cut over there; while your creed is losing ground in Europe and America, Mohammed is forging ahead in Africa and the Near East with torch and scimitar." – From _The Blight of Asia_ by George Horton

{{% toc %}}

# Life Under Islamic Law

"One incident will be sufficient to illustrate the sort of thing that was smarting in the memory of the Christian peasantry: A small farmer with a large family had planted a field of beans for food for his wife and children — beans being one of the principal articles of food for these people. A Turkish officer staked out his horse in this field, whereupon the farmer asked him if he might not put the animal in a grass plot, where was excellent pasturage. The reply was a horse-whipping, accompanied by abusive and contemptuous epithets in the presence of his family and the village, by the officer. This is a mild incident illustrative of the general conduct of the Turks toward the Christians. It is given because it came within my personal observation, and I knew the farmer, who was a very worthy and self-respecting man. 

# What happened to Dr. Murphy

Doctor Murphy was a retired army surgeon who had been in the British Indian service. He was living with his two daughters on pension at Bournabat, an aged man with a high record. Sir Harry related that Turks had entered the Murphy home and told the doctor not to be frightened, as they meant harm to no one. They had simply come to violate the women. His daughters, fortunately, had hidden themselves in a room upstairs, but the eyes of the Turks fell upon a young and pretty servant. They attempted to seize her, when she fell on her knees and threw her arms about the legs of the aged doctor and begged him to save her. The old hero tried to protect the girl in so far as his feeble strength would allow, but he was beaten over the head with muskets, kicked, and the girl torn from him by the Turks. They then proceeded to accomplish their foul purpose. Sir Harry added that the doctor was in a desperate state and the women nearly dying from fright. The automobiles were sent and the Murphys brought down. The doctor died of his injuries. 

# The Martyred City 

    Glory and Queen of the Inland Sea 
        Was Smyrna, the beautiful city, 
    And fairest pearl of the Orient she — 
        O Smyrna, the beautiful city! 
    Heiress of countless storied ages. 
    Mother of poets, saints and sages 
        Was Smyrna, the beautiful city! 
    
    One of the ancient, glorious Seven 
        Was Smyrna, the sacred city. 
    Whose candles all were alight in Heaven — 
        O Smyrna, the sacred city! 
    One of the Seven hopes and desires. 
    One of the Seven Holy Fires 
        Was Smyrna, the Sacred City. 
    
    And six flared out in the long ago — 
        O Smyrna, the Christian city! 
    But hers shone on with a constant glow — 
        O Smyrna, the Christian city! 
    The others died down and passed away, 
    But hers gleamed on until yesterday — 
        O Smyrna, the Christian city! 
    
    Silent and dead are the church bell ringers 
        Of Smyrna, the Christian city, 
    The music silent and dead the singers 
        Of Smyrna, the happy city; 
    And her maidens, pearls of the Inland seas 
    Are gone from the marble palaces 
        Of Smyrna, enchanting city! 
    
    She is dead and rots by the Orient’s gate, 
        Does Smyrna, the murdered city. 
    Her artisans gone, her streets desolate — 
        O Smyrna, the murdered city! 
    Her children made orphans, widows her wives 
    While under her stones the foul rat thrives — 
        O Smyrna, the murdered city! 
    
    They crowned with a halo her bishop there, 
        In Smyrna, the martyred city. 
    Though dabbled with blood was his long white hair — 
        O Smyrna, the martyred city! 
    So she kept the faith in Christendom 
    From Polycarp to St. Chrysostom,
        Did Smyrna, the glorified city! 

_Note: The Chrysostom mentioned in these lines was martyred at Smyrna, September, 1922._


# Book Contents

- List of Illustrations
- The Martyred City
- Foreword by James Gerard, Former US Ambassador to Germany
- Introduction
- 1. Turkish Massacres (1822-1909)
- 2. Gladstone And The Bulgarian Atrocities
- 3. First Step In Young Turks’ Program (1908-1911)
- 4. The Last Great Selamlik (1911)
- 5. Persecution Of Christians In Smyrna District (1911-1914)
- 6. The Massacre of Phocea (1914)
- 7. New Light On The Armenian Massacres (1914-1915)
- 8. Story Of Walter M. Geddes
- 9. Information From Other Sources
- 10. The Greek Landing At Smyrna (May, 1919)
- 11. The Hellenic Administration In Smyrna (May 15, 1919 — September 9, 1922)
- 12. The Greek Retreat (1922)
- 13. Smyrna As It Was
- 14. The Destruction of Smyrna (September, 1922)
- 15. First Disquieting Rumors
- 16. The Turks Arrive
- 17. Where And When The Fires Were Lighted
- 18. The Arrival at Athens
- 19. Added Details Learned After The Tragedy
- 20. Historical Importance of the Destruction of Smyrna
- 21. Number Done To Death
- 22. Efficiency Of Our Navy In Saving Lives
- 23. Responsibility Of The Western World
- 24. Italy’s Designs On Smyrna
- 25. France and the Khemalists
- 26. Massacre Of The French Garrison At Urfa
- 27. The British Contribution
- 28. Turkish Interpretation Of America’s Attitude
- 29. The Making Of Mustapha Khemal
- 30. Our Missionary Institutions In Turkey
- 31. American Institutions Under Turkish Rule
- 32. The Reverend Ralph Harlow On The Lausanne Treaty
- 33. Islam And Christianity
- 34. The Koran And The Bible
- 35. The Example Of Mohammed
- 36. The “50-50” Theory
- 37. Asia Minor, The Graveyard Of Greek Cities
- 38. Echoes From Smyrna
- 39. Conclusion
- Appendix

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/436-horton-the-blight-of-asia-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/436-horton-the-blight-of-asia.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 436-horton-the-blight-of-asia&body=Please send 436-horton-the-blight-of-asia.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 436-horton-the-blight-of-asia&body=Please send 436-horton-the-blight-of-asia.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2019-05-08
- _Version 4 update_:  2019-05-08
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
