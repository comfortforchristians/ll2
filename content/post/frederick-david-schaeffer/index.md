---
date: 2019-08-04 05:08:31-04:00
publishDate: 2019-08-04 05:08:31-04:00
title: "Frederick David Schaeffer: A Biographical Sketch"
slug: "frederick-david-schaeffer"
categories: ["Biographical Sketches"]
tags: ["Schaeffer", "Lutheran Ministers", "Evangelical Review"]
authors: ["Schaeffer, Frederick David", "Krauth, Charles Porterfield"]
synods: ["Pennsylvania Synod"]
---

The character and ministry of this venerable man are worthy of a permanent record in the history of our earlier ministers. His life was emphatically a life of severe and constant labor. He was distinguished for his learning and piety, and after having faithfully served his day and generation, he peacefully passed away, leaving to his children and the church, the precious legacy of a good name. 


>"While the soft memory of his virtues yet \
> Lingers, like twilight hues, when the bright sun is set." 

{{% toc %}}

## An Orphan Immigrant

Dr. Schaeffer was a native German, and immigrated to this country in 1776. He was an orphan, and came in company with an uncle, who appears soon after his arrival to have died. The youth was thus left, at an early age, in a strange land, destitute and friendless, without a protector or a home. But in his loneliness and desolation he was not forsaken! That covenant-keeping God, to whom he had been dedicated in infancy, was exercising over him a watchful care, and preparing him, by a course of discipline, more effectually for the work which had been appointed him. _God's thoughts are not as our thoughts, neither are his ways as our ways._ How constantly can the Christian trace the finger of God, and recognize his guardian care and superintending guidance, in all the occurrences of life! He is often led by a path of which he knew not at the time. The dispensations of Providence may sometimes seem mysterious, altogether inexplicable to human reason, yet we do know that if we love God, all things shall work for our good. One of the most comforting doctrines of divine revelation to the believer is, that he is under the administration of an infinitely good, wise and perfect Being. Thus he gains strength for the future, and girds up his loins to the work. 

## Birth and Early Training

The subject of our narrative, the son of John Jacob Schaeffer, and his wife Susanna Maria, was born in Frankfort on the Main, November 15th, 1760. On the third day after his birth he was given to God in baptism. His parents were both pious, and although in early life he was deprived of them, their sacred,influence over him was never lost. The foundations of his character were deeply laid in the dispositions and habits, he at this time acquired; to the pure atmosphere, he then breathed, must be ascribed the strength and the vigor of his moral constitution. The religious principles they inculcated were never effaced from his mind. In after life he retained an indelible impression of the scenes of his childhood, and cherished for the memory of his devout mother, who died when he was only twelve years of age, a most tender affection. He often spoke of the counsels she gave him, and the fervent prayers, with which they were enforced. To their power, in connection with the divine blessing, he was accustomed to ascribe his subsequent change, the surrender of his heart to God. With how many illustrations are we furnished of the strength of youthful impressions, of the deep and lasting influence, which early religious instruction exerts, of encouragement to fidelity in the training of the young. 

>_Quo semel est imbuta recens, servabit odorem_ \
>_Testa diu_. 

The character is most generally formed in youth, and if you secure it in time, you may mold it into any form, making it productive of the highest good in this life, and fitting it for happiness in the skies. At this interesting and critical period, you may imprint upon the child your own soul, you may give it a direction which no later effort may change. There are no inveterate habits to destroy, no strong prejudices to eradicate, no perplexing cares to disturb. There are the fewest obstacles to spiritual progress, the least opposition to embarrass. Unsuspicious and unbiased, the youthful mind drinks in instruction, and under the influence of the Spirit, is transformed into the image of the Savior, and becomes conformed to his will. Even if our efforts should not be immediately crowned with success, there is no reason for despair. We are commanded in the morning to sow the seed, and in the evening to withhold not our hand, for we know not whether shall prosper either this or that, or whether they both shall be alike good. If we go forth in our Master's strength, relying upon the promised aid, and expecting the blessing, we shall not be disappointed. Although the seed may seem to have died, yet in God's own time it will spring up and bear fruit unto eternal life. _Train up a child in the way in which he should go, and when he is old he will not depart from it._ Those that he planted in the house of the Lord, shall flourish in the courts of our God. The promise is to us and to our children. Blessed are the children of pious parents, who are conscientious and faithful to the little ones heaven has committed to their training! 

In 1768, young Schaeffer was sent to the Gymnasium in Hanau, to be educated. In this school he remained for six years, till his father's death, which occurred in 1774. He was in his fourteenth year when he left the Gymnasium and found a home in the family of his grandmother, a wealthy widow. His education was then, for a season, conducted by his uncle, Superintendent General at Rodheim, in the kingdom of Wittenberg, by whom he was, in the year 1774, received into the church by the rite of confirmation. The fallowing year his grandmother dying, his studies were again interrupted, and his plans for the future changed. The homestead was broken up, the patrimony divided, and the family separated. One of the uncles determined to visit America; and carried with him the subject of our sketch. This was the origin of his settling in the United Stales. 

## Teacher and Student in York, Pennsylvania

Losing his guardian soon after his arrival, the first knowledge we have of the young man, is in the capacity of a teacher in York County, Pennsylvania. As his education had been carefully conducted in his youth, he was well qualified for giving instruction, and acquired a reputation for skill in teaching. We have recently conversed with an aged friend, acquainted with some of his operations, who spoke of the favorable impressions which young Schaeffer left upon the community in which he, at this period, labored. 

Whilst engaged here in the business of teaching, he was brought under the influence of that excellent man, [Rev. Jacob Goering](/jacob-goering/), who became interested in his welfare, sympathized with him in his difficulties, and gave him consolation, encouragement and support. He received him as a student of Divinity, taught him the Hebrew, and superintended his theological studies, according to the usages of the times, and prepared him for the ministry of reconciliation. This important subject had often engaged the attention of the young student before his departure from his native land. His own inclinations had long led him to think of the same office. His choice of this profession accorded with the wishes of his father. He knew too, that his mother had consecrated him in infancy to the work of the ministry. This fact affected him deeply. Its influence was irresistible. His mind was satisfied as to the course he ought to pursue. The path of duly was made plain. The prayers of his mother were answered. 

## Licensed To Preach

He was licensed to preach the gospel in 1786, by the Synod of Pennsylvania. He received his ordination October 1st, 1788. As a candidate, he took charge of the Lutheran church at Carlisle, and preached also to several other congregations in Cumberland and York Counties. In those days, there was a great want of ministers in the Lutheran church, and our pastors usually had an extensive field to cultivate. A charge enclosed a large number of congregations, and covered considerable ground. 

## A Happy Marriage

During the autumn of 1786, Mr. Schaeffer was united in marriage to Rosina, a daughter of Lewis Rosenmiller, of York County. She was a woman of humble piety, great discretion, and active sympathy, of strong mind and great energy of character, who was fond of reading, and had devoted much time to the culture of her mind. She was such a gift as God bestows only on the most highly favored. She was her husband's counselor, comforter and cherished companion, with whom he shared the joys and sorrows of earth for half a century, and whose death he scarcely a year survived. It may be said with safety that no man was ever more favored in such a connection — a union of unclouded harmony and unbroken felicity, encircled with heaven's choicest blessings. Their life was a beautiful exemplification of the strength of conjugal attachment, and of the influence which this relation, when properly entered into and faithfully discharged, exerts for usefulness and happiness. 

From this marriage there were eight children — two died in infancy — four sons became ministers of the gospel, David F. Schaeffer, D. D., who for so long a period had charge of the Lutheran congregation in Frederick, Md., Solomon Schaeffer, who was pastor of the church in Hagerstown, Md., and whose son is C. W. Schaeffer, D. D., of Germantown, Pa., Christian Schaeffer, D. D., who officiated for some time as pastor of the English Lutheran congregation in New York city, and Charles F. Schaeffer, D. D., of Easton, Pa. The fourth son married a daughter of J. Daniel Kurtz, D. D., Baltimore, Md. Two children survive, Rev. Dr. Schaeffer, of Easton, and his sister, the wife of Rev. Dr. Demme, of Philadelphia. Who can calculate the amount of good this family of children, in the providence of God, may have been permitted to exercise, or adequately estimate the power, which pious and faithful parents may wield, even to the remotest generations? When dead, they may still speak in the character and life of those, who were brought under their teachings, and experienced the influence of their holy example. The blessings of piety continue to descend in the lineage of the righteous. 

## Labor in the "Germantown" district and in Philadelphia

In 1790 Dr. Schaeffer took charge of the then extensive Germantown district. Here he labored for twenty-two years with great acceptance and with manifest seals to his ministry. In 1812 he removed to Philadelphia, having received a call from St. Michael's and Zion's churches, as colleague pastor of Rev. Dr. Helmuth, and successor to Rev. Dr. Schmidt. In this charge he also continued for twenty-two years, exhibiting the same interest for the spiritual improvement of his flock, the same zeal and devotion, which characterized his former career. 

## German Language Controversy

He occupied this field of labor, during part of the time, when the unfortunate controversy existed, occasioned by the proposition to introduce English into the services of public worship. The contest was protracted and bitter; the discussions warm and acrimonious; the excitement most intense and fierce. Bad feeling and angry strife for a long time prevailed. It was at this period, that a German, a man of intelligence and 
influence, in the course of a speech made at a public meeting, called for the purpose of considering the propriety of supplying with English preaching those families in the church, whose children were growing up with a limited acquaintance with the German, remarked, "that the time had come when it would be necessary to shed blood in support of their rights; that at all hazards, the German language and German interests must be upheld." To us, at the present day, such a state of things seems almost incredible, and yet the fact has reached us from the most reliable source. Well may the Christian, when his holy religion is thus degraded, and a reproach brought upon his profession, exclaim — 

>_Quis talia fando_ \
>_Temperet a lacrimis!_ 

This zealous and angry controversy operated as a hindrance to the truth; these animosities and distinctions were fatal to the progress of piety, and proved almost ruinous to the prospects of our church in Philadelphia. Dr. Schaeffer's heart was nearly broken by the sad condition of things. Although he was himself a German, and never spoke English, except from necessity, yet he had not the inveterate prejudices of many of his German contemporaries. He thought that the German language ought to be upheld, and the interests of his German brethren protected, but he entertained Dr. Kunze's views respecting the introduction of the English into the exercises of the pulpit, and thought the German congregation ought to make provision for those, who understood only the one language. From all that we can learn, his course was most reasonable. He sympathized with those, whose preferences were for the church of their birth, but whose ignorance of the German debarred them from the enjoyment of the privileges of the sanctuary. He thought the views of both parties should be respected. 

>_Tros Tyriusque nullo discrimine mild agetur_. 

He knew the folly of attempting to perpetuate the German to the exclusion of the English, and felt that our church must in time become extinct in this country, if its services were confined to its vernacular tongue. He regarded the prevailing tendencies as most disastrous. He witnessed with deep sorrow and painful emotion the state of affairs. But the current against him was too strong — the opposition was most violent. Salutary measures were with pertinacity rejected, and better counsels repelled. 

He was naturally of a timid disposition, and saw that his efforts must be futile. If he would have done more to favor the English interests, he would have lost all his influence, and most probably could not have retained his position. If he could, however, have controlled matters, they would have taken a different turn. The evils, that ensued, would never have occurred. If it could have been in his power to prevent the difficulties, our church in Philadelphia might have been saved, and at the present time a different aspect presented — 

>_&emsp;&emsp;Si Pergama dextra_ \
>_Defendi possent, etiam hac defendere fuerint._


## Retirement From Ministry

In 1834, in consequence of the failure of health, and the increasing infirmities of age, he relinquished the active duties of the ministry, and removed to Frederick, Md., to spend the remainder of his days with his eldest son. But his earthly pilgrimage soon terminated. The summons reached him January 27th, 1836, in the 76th year of his age. 

His last moments were in unison with those of his whole life. He was sustained to the last by a cheerful reliance on the mercy of God through Jesus Christ. He gave unwavering testimony to the truth and power of the religion he professed, and was gathered to his fathers, like a shock of corn fully ripe for the heavenly garner. He was buried in the Lutheran cemetery at Frederick, Md. On the following Sabbath, a sermon appropriate to the occasion, and commemorative of the virtues of the deceased, was delivered by Rev. Dr. Schmucker, of Gettysburg, from the words: _Blessed are the dead, who die in the Lord from henceforth: yea! saith the Spirit, that they may rest from their labors: and their works do follow them._ 

The council of the Lutheran church in Frederick, and the corporation of the German churches in Philadelphia, so long served by Dr. Schaeffer, adopted resolutions of genuine sorrow and expressive of the deep sense of the loss the church had sustained in the death of this good man. As a further mark of respect, both these churches were enshrouded in mourning, and in the place of his decease, the bells of all the protestant churches were tolled in testimony of his worth and the profound regard and warm affection entertained for him by all classes of the community, and all denominations of Christians. 

## Solid Abilities and Studious Habits

Dr. Schaeffer was a man of solid abilities and of studious habits. He was a close student, and carefully read the Hebrew Bible and the Septuagint. No day passed without the deliberate perusal of the sacred original. His intellect had been invigorated and enriched by earnest effort and constant diligence. It was single in its aims, and more effective than many a mind of greater brilliancy. From the University of Pennsylvania, in 1813, he received the honorary degree of D. D. As an author he did little. The only work he prepared for the press was a _Reply to a Defense of the Methodists_.[^bFR] Our earlier ministers, although so abundantly able, in consequence of their numerous and arduous pastoral labors, found no time for authorship. Dr. Schaeffer was particularly interested in Geographical studies, and had accumulated a large collection of maps. After his professional studies, this seems to have been his favorite pursuit. He was also enthusiastic in his love of music, and from this source frequently sought recreation. It was his usual practice, every night before retiring, to play on the piano, and sing a few choice stanzas. He had likewise a poetic talent, which in earlier life he was disposed to cultivate. He composed quite a number of hymns. In later years he does not seem to have exercised this gift. 

Although there were no striking incidents in the life of Dr. Schaeffer, it was distinguished by excellencies, which any of Christ's ambassadors might desire to attain. He was a most faithful servant of his Master, and, in the performance of his ministerial labors, persevering and indefatigable. He was wholly given up to the work. With it no other aims or cares were permitted to interfere. Every other object was made subsidiary to his vocation. He was active and zealous, and ever ready to discharge the duties of his office. It was his constant endeavor to win souls to the Savior, and to take care of the flock over which Christ had made him overseer. His visits to the sick and poor were uninterrupted. He walked in the footsteps of the divine Redeemer, who went about doing good. He appeared to live but 

>"To lure to brighter worlds and lead the way." 

## As a Preacher

As a preacher, Dr. Schaeffer was plain and unostentatious, but instructive and experimental. His views on all subjects of  Christian faith were evangelical. The teachings of divine revelation he implicitly received. After the sacred scriptures he revered the volume of our Symbolical books, a Latin copy of which he always read. These, in his judgment, contained a summary of Christian doctrine, the truths of God's word. He never, however, exalted them above the Bible. He was tolerant in his views, liberal in his spirit, and conciliatory in his intercourse. His sermons were eminently practical, designed to reach the heart and affect the conduct. His partialities were all in favor of the Arndt and Spener school. These predilections, which he acquired in his youth, he retained through life. Their influence was plainly perceptible in his preaching. He gave his cordial support to all evangelical labors, and was deeply interested in every cause, which had for its object the promotion of God's glory and the advancement of his kingdom. As an evidence of his spirit, we give the following passage from a letter,[^bFS] written by him at a time when an interest was awakened in our church in this country, upon the subject of missions and beneficiary education: 

>"It has pleased a kind providence that, for many years past, I should be employed as a minister of the gospel of a crucified Savior, in the evangelical Lutheran church. Ready to employ the remainder of my strength and time, so long as God shall please to continue the same to me, in his holy service, I need not assure you that your missionary institution is an object of deep interest to my heart. And while I am sensible of the honor you have conferred upon me, I have to regret that I cannot be a more active member of your body. I am encouraged, however, to pray for a continual blessing upon the pious efforts of your society, and shall take pleasure in contemplating your progress in a good and great work; for I see that the good seed sown in the evangelical Lutheran church in the United States of North America, is daily springing up more and more — hindrances which were not under our control are lessening, and many able men are engaged in the field, profiting by the good example of those who have gone before them, or who have trained them up for the sacred employment. The church is in the hands of the Lord, who is God over all. To him, our all-gracious Savior, let us look with humility and faith, seeking his glory, and he will bless us as instruments in his hands." 

## "A Good Man"

Dr. Schaeffer was a man of ardent piety. All who came in contact with him, were impressed with the conviction that he was a good man. He was conscientious, serious and devout. He lived near his God, and seemed to enjoy communion with his own heart. He was a man of prayer. He, at all times, maintained his Christian integrity and remembered his high calling. His character was free from reproach, it was above suspicion. It was transparent, simple and guileless. He was remarkable for his meekness, candor and forbearance. He possessed a gentle disposition, childlike simplicity and vast benevolence. He was quiet, modest, and unpretending, and seemed to set too slight a value upon his attainments; he was opposed to everything like display, and refrained from what might have been regarded as a reasonable show of learning. In all that he did, he appeared to be actuated by principle, by a desire to answer the great object of his existence. He was a pattern of every Christian virtue. His life was gentle, his end was peace! He went down to the grave calmly and without a fear. When the damp of death collected upon his brow, and the hand of the destroyer was uplifted to strike him down, he could review the past with satisfaction, and see those, whose hearts he had gladdened, and whose lives he had cheered. He approached the dark chambers of death — 

>&emsp;&emsp;"Sustained and soothed \
>By an unfaltering trust,… \
>Like one who wraps the drapery of his couch \
>About him, and lies down to pleasant dreams!" 

[^bFR]:  Anlwort auf eine Vertheidigung der Methodisten, Germantown, 1S0G. 

[^bFS]:  In reply to a communication informing him of his appointment as an honorary vice-president of the Missionary and Education Society of the Lutheran church in the State of New, York. 

He could look forward with joy and humble hope, to those bright and beautiful mansions prepared for him on high, to that city, which hath foundations, whose maker and builder is God. When his heart fainted and his strength failed, God was the strength of his heart and his portion forever. 

## Extract From Prof. Schmucker's Obituary Discourse

We cannot, perhaps, more appropriately conclude our brief sketch of this devoted servant of God, than by presenting an extract from the obituary discourse, delivered by Professor Schmucker. It contains some reference to the domestic character of Dr. Schaeffer, and an interesting allusion to the death of the partner of his life, from whom he was, for only a short time, separated: 

>"'Tis but a few months since ye saw the grave open, to receive into its cold embrace, the friend and companion in life of him, whose departure has convened us today. Then he stood among the mourners, he mingled his tears with yours, and felt more keenly than you all the wound inflicted by the fatal dart. You mourned the departure of a venerable matron, whose life beautifully illustrated the milder virtues of the religion she professed: or you mourned over the lifeless clay of an affectionate mother, who had watched over your infant hours with the fond solicitude of maternal tenderness, and had early instilled into your minds the benign truths of our holy religion. You participated deeply in the sufferings of the aged holy man of God, bending under the weight of three score years and ten, and tottering on the brink of the grave. But his sufferings were still keener, for he buried part of himself. He resigned into the hands of him, who gave her, the friend of his youth, the partner of his long and checkered life, his solace in affliction, his consolation in declining years. Yet with that spirit of resignation which characterized his life, he bowed in holy submission to the hand divine that smote him. He said the Lord gave, the Lord hath taken away, blessed be the name of the Lord! 

>Now he too has been cut down; his lifeless remains have been deposited at her side, and both slumber together in death. Be it so! It is the Lord's doing: and He hath done all things well. They have lived long, a rare example of conjugal affection, of Christian fidelity and ministerial usefulness. In sickness and in health, in prosperity and adversity they were of one heart, one mind, one hand; and they have together gone to that Savior, whom they together loved and served. With Israel's king let us exclaim: _They were lovely and pleasant in life, and in death they were not long divided._" 

## Common Themes Amongst These Early American Lutheran Ministers

Our earlier ministers, we have found, as we have passed along, were men not only of devoted piety, but of enlarged intellectual culture. Their piety, because of their attainments, was not less active or less efficient. The servant of God, we believe, will have an influence upon the church and the world wide and enduring, as his zeal is accompanied with learning; or the extent and permanency of ministerial influence is, under God, proportionate to its intellectual power.

## The Need For An Educated Clergy

Education gives dignity and value to ministerial action, and increases an individual's ability to do good. It is a gratifying fact, that among different denominations of Christians, there is felt an increasing interest upon this subject. No one now rejoices that in his youth, he did not enjoy the advantages of a classical education. All seem to feel the importance of a thoroughly educated ministry. The conviction is gaining ground, that mere piety uneducated will not do, just as education without piety, will fail of the desired object. It will not answer to introduce into the sacred office the halt, the blind and the maimed, to keep the people always limping: 

>_Non tali auxilio, nec defensoribus istis_ \
>_Tempus eget._ 

The church needs men of a different stamp, those who are competent to feed the flock of Christ with knowledge and understanding, rightly to divide the word of truth. The age demands an educated ministry. The minister of the gospel should be a ready scribe, instructed unto the kingdom of heaven, and like a good householder, able to bring out of his treasure things new and old. He should challenge respect, and be prepared to grapple with error, in whatever form it maybe presented.

If a man thinks he is called to the ministry, let him study and qualify himself thoroughly for the responsible work. The mental discipline, which Paul received at the feet of Gamaliel, and the stores of knowledge he acquired in the schools, did not diminish his power to do good. His wonderful acquisitions, while they fitted him the better to combat with error, and to baffle his subtle and learned adversaries, to detect their sophistries, and expose their absurdities, also rendered his teachings to the ignorant and the simple more effective and successful. Luther, Calvin, Zwingli and Wesley were men of profound erudition, deeply versed in ancient wisdom. Their varied attainments contributed largely to the astonishing results they achieved. The Divine Being could carry forward his plans independently of any human agency, but he has seen fit graciously to employ the instrumentality of man, for the accomplishment of his glorious purposes. The enlightening and renovating influences of the Holy Spirit, we regard as indispensable, but we must not think that when God calls men to this sacred work, he sends them forth, as the fabled Minerva proceeded from the brain of Jove, fully grown, and completely equipped for the service, without the necessity of further preparation. God works not without means.

In apostolic times, when illiterate men were chosen, he qualified them himself for the office. They were miraculously furnished with the requisite learning, before they were sent forth to preach. Knowledge is necessary to aid in explaining, illustrating, defending and enforcing the truth. 

>"It requires," 

says a gifted author, 

>"no small learning to be correct, not a little study to be simple, and great command of language to be plain. It is with rare exceptions your uneducated or half-educated men, that confound their audience with great swelling polysyllables of vanity, imperfect definitions, which are fruitful of error, and thoughts perhaps good in themselves, but with as little arrangement as chaos. The thoroughly educated preacher alone is simple, lucid and intelligible, because his words are well chosen, his scheme preconceived, and his logic exact. Little do the people know what years of patient study were spent over the yellow pages of classic lore, to make the sermon so plain, that the child bears it home and fancies he could have preached it himself." 

Ignorant ministers will have ignorant congregations. Like priest like people. The ministry is brought into contempt, the church is crippled in its power, and religion is degraded. Would that, in our Christian churches throughout the land, we had men planned of the proper qualifications, wholly given up to the work, to which they have consecrated themselves! We need educated men, men too, who will go forth in the spirit of their Master, in humble reliance upon his strength, to spread the triumphs of the cross, to build up the desolations of Zion, and cause the waste places to flourish like cedars in the courts of the Lord. How important, how responsible is the sacred office! Careless hands should be laid on no man. The herald of the gospel is commissioned by God to make known to men, 

>"The eternal counsels: in his Master's name \
>To treat with them of everlasting things. \
>Of life, death, bliss and woe." 

What a power does the pulpit possess, how stupendous its influence! It furnishes means of doing good, afforded by no other human agency, of overthrowing vice and upholding morality, to an extent which nothing else can boast. The day, the place, the theme, the circumstances, the purposes, the credentials, give the messenger of God advantages for this end, unrivaled and unexampled. 

>&emsp;&emsp;"The pulpit, in the sober sense \
>Of its legitimate, peculiar powers, \
>Must stand acknowledged while the world shall stand, \
>The most important and effectual guard, \
>Support and ornament of virtue's cause." 

# Publication Information

- _Author_: __"Krauth, Charles P."__, Editor.
- _Journal_: __"The Evangelical Review."__
- _Originally Published_: 1854.
- _Lutheran Library Edition_: 2019
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

{{% alert note %}}
{{% biographical-sketch-note %}}
{{% request-pdf %}}
{{% /alert %}}
