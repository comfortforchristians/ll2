---
publishDate: 2019-02-08 01:02:16-05:00
title: "Weapons of Mystery: A Novel by Joseph Hocking"
slug: "334-hocking-weapons-of-mystery"
categories: ["Lutheran Library Publications"]
tags: ["Hocking", "Fiction", "Occult", "New Age"]
authors: ["Hocking, Joseph"]
titles: ["Weapons of Mystery"]
origpublishers: ["Ward, Lock & Company"]
origdates: ["1890"]
synods: ["Methodist"]
---

"Weapons of Mystery" is a singularly powerful story of occult influences and of their execution for evil purposes. Like all Mr Hocking's novels, "Weapons of Mystery" has an underlying religious and moral purpose, but merely as a story, and quite apart from the purpose which was in the mind of the author, the tale has a curious fascination for the reader. The cleverly conceived plot, and the strange experience of the hero and heroine make "Weapons of Mystery" a story which it is not easy to put down when once commenced. – From the book cover.




# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/334-hocking-weapons-of-mystery-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/334-hocking-weapons-of-mystery.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 334-hocking-weapons-of-mystery&body=Please send 334-hocking-weapons-of-mystery.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 334-hocking-weapons-of-mystery&body=Please send 334-hocking-weapons-of-mystery.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}

