---
date: 2018-05-04 12:00:00

title: "Lutheran Library Updates – May 2018"
slug: "lutheran-library-updates-2"
categories: ["Lutheran Library Newsletters"]

---
Welcome – Format Updates – Recreational Reading – Dixie Kitten – The Adventures of Tommy Postoffice – Dogs in the Northland

{{% toc %}}

![update 1](/img/posts/lutheran-library-updates-1.jpg)

# Welcome

Hello to friends of the Lutheran Library Publishing Ministry.  Since our establishment last summer, 69 books have been completed.  All republished books are available at [lutheranlibrary.org](https://www.lutheranlibrary.org/) for free download in a variety of formats for Kindle, Apple, and other devices.

The goal of the Lutheran Library is to re-release well-written and readable books from sound, faithful American Lutherans of the past for the enjoyment and edification of a new generation. 

# Format Updates

Ebooks are created with something called Cascading Style Sheets (CSS).  Old-school computer programmers claim CSS is not really a language - but it does have a sort of logic to it.  After a period of study with [O'Reilly's CSS guide](http://shop.oreilly.com/product/0636920012726.do), your editor has updated the Lutheran Library CSS files.  You will be able to see an immediate improvement in all newly released ebooks.  Over time, all of the back titles will be updated.

As always, if you notice any typographical errors or strange behavior in an ebook, let us know so that it may be corrected in future versions.

You can find the version number of any Lutheran Library title at the bottom of the copyright page.  Updated versions begin with <code>v2</code>. 

# Recreational Reading

Every once in a while we come across books which are not religious in any way, but are fun to read.  You can find these under the ["Extras" tag](/tags/extras/).  Here are a few you might enjoy:

### Dixie Kitten by Eva March Tappan

![Dixie Kitten](/img/posts/e28-tappan-dixie-kitten-1024.jpg)

"The first thing that Dixie could remember was of being cuddled up to some one who was soft and comfortable and gave her sweet warm milk to drink. Somehow, she knew that this was her mother, and that her mother would feed her when she was hungry and keep her warm and take care of her and not let anything hurt her.

"Their home was a nest of soft hay, so deep in the pile that when Dixie was at the farther end, she could not see out at all. After a while, however, she crept out to the light now and then, and here were so many interesting things that her eyes grew bigger and bigger the longer she looked. There were piles of hay and straw, there were bags of grain, there were rakes and spades and wheelbarrows, there was a carriage, and there was a sleigh. Dixie climbed up one of the shafts of the sleigh and stretched out her paw to touch a bell. She only wanted to see what it was..."

### The Adventures of Tommy Postoffice by Gabrielle Jackson

![Tommy Postoffice](/img/posts/e22-jackson-adv-tommy-postoffice-1024.jpg)

"The first sound came from a dark corner in the cellar of the R——— postoffice, far in behind some stored-away mail-pouches, and the second was its answer, as a fine black-and-white cat made her way daintily across the dusty pouches, shaking first one foot and then another, to free them from any dirt which might adhere to them, before entering the corner from which the first cry proceeded. And, indeed, she had reason to guard those snowy stockings she wore, for she had taken infinite pains to wash them until they were immaculate..."

### My Dogs in the Northland by Egerton Young

This title is by a missionary to the Canadian wilderness.  These true stories were an inspiration to Jack London, who used them as source material for his book, _The Call of the Wild_.

![My Dogs](/img/posts/225ms-young-my-dogs-in-the-northland-1024.jpg)

"For years, with great dogs, I toiled and often with them was in great perils. Much of my work was accomplished by their aid. So I believe in dogs, and here in this book I have written of some of them and their deeds." – Rev. Egerton R. Young, from the Introduction.

# Thank you

Thank you for your prayers and - most of all - for your interest in the books.  Read them, love them, enjoy them.  

Your help is appreciated in spreading the word as often and in as many ways as you feel is appropriate.