---
date: 2019-05-06 09:32:37-04:00
publishDate: 2019-05-06 09:32:37-04:00
title: "Infant Baptism And Infant Salvation In The Calvinistic System – A Review Of Dr. Hodge's Systematic Theology by Charles Krauth"
slug: "110tc-krauth-calvinist-infant-baptism"
categories: ["Lutheran Library Publications"]
tags: ["Calvinism", "Hodge", "Baptism", "Krauth", "Covenant", "Salvation"]
authors: ["Krauth, Charles Porterfield"]
titles: ["Infant Baptism and Infant Salvation in Calvinism"]
origpublishers: ["Lutheran Book Store"]
origdates: ["1874"]
synods: ["General Council"]
---

"What happens to young children who die? If you've ever wondered about the so-called "Age of Accountability", or how "Original Sin" relates to infants, this small book can help."
 
>...Many of the greatest of the Reformed divines have been overwhelmed with what they grant is on this point – the objective force of Baptism – the faith of all the fathers and of the entire Christian Church through all the ages before the rise of Calvinism. – From Chapter 43. 

{{% toc %}}

# What happens to young children who die?  

[Dr Krauth](/charles-porterfield-krauth/) answers this question by a thorough review of the theology of Reformed scholar Dr. Charles Hodge.  If you've ever wondered about the *age of accountability*, or how *original sin* relates to infants, this small book can help.

>There are but two developed systems in the world that claim with any show of probability to be purely Biblical. These systems are the Lutheran and the Calvinistic. They possess a common basis in their recognition of the same rule of faith; their profession of the Old Catholic faith as set forth in the three General Creeds; in their acknowledgment of the doctrine of justification by faith and of its great associated doctrines; and they have vast interests, great stakes, mighty bonds of sympathy in common. No two bodies of Christians have more reason for thoroughly understanding each other than Calvinists and Lutherans have, and no two parts of Christendom are closer together in some vital respects than consistent Calvinism and consistent Lutheranism. It is well worth their while to compare views.

# Topics Covered

- Infants, Infant Baptism, and Infant Salvation In The Calvinistic System
- The Westminster Confession And Elect Infants
- How Are Confessions To Be Interpreted?
- The Salvation Of Infants Dependent On Absolute Personal Election
- Infants Elect And Reprobate
- Infants Worthy Of Perdition
- Actual Perdition Of Infants According To Calvinism
- Presumption And Assurance In Regard To Infants
- The Election of Children and their Death
- Hereditary Rights Of Infants
- Hereditary Exemption From The Common Lot
- Judaizing View
- Cutting Off Of Infants From The Covenant
- Elect Parents And Elect Infants
- A Pious Fiction
- Reserve
- Baptism And Anabaptism
- Grace Before Baptism.
- Baptism Without Objective Force
- Definition Of Baptism
- Baptism Of Non-elect Infants
- Infants Outside Of The Church
- Calvinism And Anabaptism
- Children Of Unbelievers – Reprobate Infants
- The Secret Impediment
- Non-elect Infants Have No Right To Baptism
- Calvinism Without A Logical Argument Against Anabaptism
- The Means Of Grace In Their Relation To Infants
- Calvinistic Doctrine Of The Church In Its Bearing On Infant Salvation
- Calvinism And Romanism On Infant Salvation
- Calvinism And Pelagianism
- Calvinism And Arminianism, On The Election And Reprobation Of Infants, And The Insane
- The Synod Of Dort
- Severity Of The Calvinistic Spirit
- The Confession And Apology Of The Arminians And The Calvinistic Censure
- The Great Calvinistic Divines Against The Arminians
- The Arminians Against The Calvinists
- The Westminster Assembly (1643–45)
- Attempts At Mitigation Of The Calvinistic Doctrine Of Infant Damnation
- (1) Limbus Infantum
- (2) Infant Annihilation
- (3) Mediating Tendency
- (4) Lutheranizing Tendency
- (5) Reformed Liturgies
- Outline And General Estimate Of Dr. Hodge’s Systematic Theology


# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)

![book cover](/img/posts/110tc-krauth-calvinist-infant-baptism-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/110tc-krauth-calvinist-infant-baptism.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 110tc&body=Please send 110tc-krauth-calvinist-infant-baptism.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 110tc&body=Please send 110tc-krauth-calvinist-infant-baptism.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2017-09-10 
- _Version 4 update_:  2019-05-06
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
