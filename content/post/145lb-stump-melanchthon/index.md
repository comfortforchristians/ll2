---
date: 2019-07-04 05:44:07-04:00
publishDate: 2019-07-04 05:44:07-04:00
title: The Life of Philip Melanchthon by Joseph Stump
slug: "145lb-stump-melanchthon"
categories: ["Lutheran Library Publications"]
tags: ["Biography", "The Reformation", "Confessions", "Book of Concord", "Stump", "Start Here"]
authors: ["Stump, Joseph"]
titles: ["The Life of Philip Melanchthon"]
origpublishers: ["Pilger Publishing House"]
origdates: ["1897"]
synods: ["Ohio Synod", "General Council"]

---

"The life of so distinguished a servant of God as Melanchthon deserves to be better known to the general reader than it actually is. In the great Reformation of the sixteenth century, his work stands second to that of Luther alone. Yet his life is comparatively unknown to many intelligent Christians. 

"In the preparation of this book, the author has made use of a number of biographies of Melanchthon by German authors…His aim has been to prepare a brief but sufficiently comprehensive life of Melanchthon, in such a form as would interest the people… That these pages may, in some measure at least, accomplish their purpose, and make the Christian reader more familiar with the work and merit of the man of God whom they endeavor to portray, is the sincere wish of the author.  – Joseph Stump.

{{% toc %}}

# Book Contents 

 - Preface 
 - Introduction
 - 1. His Birth And Parentage
 - 2. His Childhood. 1504-1509.
 - 3. At The University. 1509-1516.
 - 4. The Call And Removal To Wittenberg. 1518.
 - 5. At Wittenberg.
 - 6. Early Conflicts.
 - 7. Melanchthon’s Marriage And Domestic Life.
 - 8. Melanchthon During Luther’s Absence From Wittenberg. 1521-1522.
 - 9. New Labors. A Visit To Bretten. Melanchthon And Erasmus. 1522-1524.
 - 10. The Peasants’ War. Luther’s Marriage. The Saxon Visitation. 1525-1527.
 - 11. The Second Diet Of Speyer. The Marburg Colloquy. 1529.
 - 12. The Diet Of Augsburg. 1530.
 - 13. The Schmalkald League. The Religious Peace Of Nuremberg. Melanchthon Invited To France And England. 1531-1535.
 - 14. The Wittenberg Form Of Concord. Journey To Tuebingen. Accused Of Heresy.
 - 15. The Convention At Schmalkald. Attacks Upon Melanchthon. 1537-1539.
 - 16. The Frankfort Suspension. Labors In Ducal Saxony And Brandenburg. Second Convention At Schmalkald. The Landgrave’s Bigamy. Melanchthon At Death’s Door. 1539-1540.
 - 17. The Religious Colloquy At Worms. The Diet At Ratisbon. 1540-1541.
 - 18. The Bishopric Of Naumberg. The Reformation At Cologne. A Year Of Suffering For Melanchthon. 1541-1544.
 - 19. The Diet At Worms. 1545. The Diet At Ratisbon. 1546. Luther’s Death.
 - 20. The Schmalkald War. The Dissolution And Restoration Of The University. 1546-1547.
 - 21. The Augsburg Interim. The Leipzig Interim. Controversies. 1548-1550.
 - 22. The Osiandrian And Majoristic Controversies. The Religious Peace Of Augsburg 1550-1556.
 - 23. The Crypto-Calvinistic Controversy. Negotiations With Flacius. 1556-1557.
 - 24. The Religious Conference At Worms. 1557.
 - 25. His Last Years And Death. 1558-1560.
 - 26. His Character And Services.


# Melanchthon's Brilliant Intellectual Gifts

"The brilliant intellectual gifts of Melanchthon elicited the unqualified admiration of Luther, Erasmus, and in fact of all his contemporaries. Endowed by nature with an extraordinary memory, and possessed of unwearied industry in the pursuit of knowledge, he became in almost every branch of learning the peer and frequently the superior of those who made these branches a special study…

"His strength lay… in the power to develop, expand, clear up, define, and defend evangelical truth. The old comparison, that Luther found the deep-lying veins of ore and brought the precious metal to light, while Melanchthon coined it and set it to circulating, contains much of truth. He was gifted with rare powers of lucid expression, and wrote a beautiful style. On this account he became the scribe of the Reformation. He not only wrote those two greatest confessions of Protestantism, The Augsburg Confession and The Apology, but whenever the Wittenberg theologians were called upon for an opinion, it was almost invariably Melanchthon upon whom the task of preparing it devolved.

"He possessed in an eminent degree the very talents which were needed to make him an invaluable assistant to Luther. A wise and beneficent Providence decreed that these two, the one warlike, aggressive, bold, the other peaceful, cautious, apprehensive, should labor side by side for the same great cause; and that, linked together by the same devotion to God and the truth, they should supplement one another's work by the very diversity of their talents and temperaments…


# About Joseph Stump

"Joseph Stump was born on October 6, 1866 in Marietta, Pennsylvania.  He received his education at Capital University, Columbus, Ohio, and The Lutheran Theological Seminary at Philadelphia.  From 1887-1915 he served pastorates in Pennsylvania and New Jersey. Stump was professor at Chicago Lutheran Theological Seminary in Maywood, Illinois from 1915–1920, and at Chicago Lutheran Divinity School from 1920–1921, and served as president and professor at Northwestern Lutheran Theological Seminary from 1921 until his death in 1935. His works include _An Explanation of Luther's Small Catechism; The Christian Life; A Handbook of Christian Ethics; Life of Philip Melanchthon; The Christian Faith;_ and _A System of Christian Dogmatics._

[From: Lutheran Cyclopedia. "Joseph Stump". Retrieved 2017-10-29 from cyclopedia dot lcms dot org]


# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download) 

![book cover](/img/posts/145lb-stump-melanchthon-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/145lb-stump-melanchthon.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 145lb&body=Please send 145lb-stump-melanchthon.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 145lb&body=Please send 145lb-stump-melanchthon.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}



# Publication Information

- _Lutheran Library edition first published_: 2017-10-31
- _Version 4 update_: 2019-07-04 
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
