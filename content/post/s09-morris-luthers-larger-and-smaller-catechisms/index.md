---
publishDate: 2019-01-31 12:49:04-05:00
title: A Concise Introduction to Luther's Larger and Smaller Catechism by John Morris
slug: "s09-morris-luthers-larger-and-smaller-catechisms"
categories: ["Lutheran Library Publications"]
tags: ["Short Books", "Confessions", "Book of Concord", "Catechism", "Morris","Evangelical Review"]
authors: ["Morris, John Gottlieb, (1803-1895)"]
titles: ["A Concise Introduction to Luther's Larger and Smaller Catechism"]
origpublishers: ["Evangelical Review"]
origdates: ["1849"]
synods: ["Maryland Synod", "General Synod"]
---
"We want no broader line than the catechism draws; but then we do not want that line whitewashed out by a diluted and false liberalism, so as nearly to obliterate it. We desire to see it remain in its original breadth and depth, so that we may consistently and honestly reply to the query: 'What are the distinctive doctrines of your church?' 'You will find an epitome of them in Luther's Smaller Catechism.'"

{{% toc %}}

# The Impact of Luther's Smaller and Large Catechisms on Protestantism

"Next to Luther's translation of the Scriptures, none of his books exerted so extensive and wholesome an influence on the Protestant cause, as his Larger and Smaller Catechisms. The latter, particularly, was translated into all the modern languages of Europe, and into Hebrew, Greek, Arabic, and Latin. Even the Malabars of India, and other eastern nations, read it in their own tongue. 

- It has been illustrated in many a ponderous tome — 
- It has been made the basis of sermons by the most celebrated preachers of the church — 
- Learned professors have used it as the ground of their doctrinal lectures — 
- Historians have made it the theme of many a curious and elaborate volume — 
- Commentators in scores have expended their strength upon it, and 
- The enemies of the Reformation have assailed it with demoniacal rancor and hate. 

"So much did the Jesuits of a later day fear its overwhelming influence, that they committed in relation to it one of the most villainous frauds that is to be found in literary history. They concocted a catechism, by perverting Luther's words, so as to make it appear a defense of popery, and published it as his own. 

"Even poets, scholastics, astronomers, grammarians, and astrologers not excluded, have expended much ink and paper in versifying and explaining it on the principles of their respective theories.

# Luther's Larger Catechism Ought To Be Better Known

"The _Larger Catechism_ is not so well known to pastors and catechumens in this country, even to those who can read it in the original language, as it deserves to be. We have never met with an English translation of it, and that accounts, to some extent, for its limited circulation among us. It is not often seen printed by itself, and the Book of Concord, in which it is contained, has not, heretofore, been so widely distributed as it should be. We are, however, glad to observe strong symptoms of a _revival_ of symbolic theology among our ministers. Every one of us will be the better in many respects, from studying that immortal work. 

# About Rev. John Gottlieb Morris

"For many years he has been prominently connected with all the great movements in the Church, and is, perhaps, more widely known than any living minister connected with the General Synod. Besides his literary and theological labors. Dr. Morris has, from the beginning of his ministry, given considerable attention to the study of Natural History, and has attained to an enviable position among the naturalists of this country. Two of his works on Natural History have been published by the Smithsonian Institute of Washington, D. C."  [Read more...](/john-gottlieb-morris/)

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/s09-morris-luthers-larger-and-smaller-catechisms-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/s09-morris-luthers-larger-and-smaller-catechisms.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request s09-morris-luthers-larger-and-smaller-catechisms&body=Please send s09-morris-luthers-larger-and-smaller-catechisms.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request s09-morris-luthers-larger-and-smaller-catechisms&body=Please send s09-morris-luthers-larger-and-smaller-catechisms.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}

