---
date: 2019-05-28 09:41:15-04:00
publishDate: 2019-05-28 09:41:15-04:00
title: "Paul Gerhardt: A Short Biography of the Hymn Writer by John Gottlieb Morris"
slug: "s14-morris-paul-gerhardt"
categories: ["Lutheran Library Publications"]
tags: ["Gerhardt","Hymns","Biography","Short Books","Evangelical Review"]
authors: ["Morris, John Gottlieb"]
titles: ["Paul Gerhardt: A Short Biography of the Hymn Writer"]
origpublishers: ["Evangelical Review"]
origdates: ["1850"]
synods: ["General Synod", "Maryland Synod"]
---

"Never were pious resignation to God's will — complete subjection to His sovereignty – perfect patience under disappointment and sorrow more beautifully and impressively uttered... We do not think it possible for human language to express a more thorough acquiescence in the decrees of Providence. This was the character of Gerhardt's piety, and to be in all things of the same mind with God, is the perfection of piety." – John Morris.



# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/s14-morris-paul-gerhardt-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/s14-morris-paul-gerhardt.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request s14-morris-paul-gerhardt&body=Please send s14-morris-paul-gerhardt.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request s14-morris-paul-gerhardt&body=Please send s14-morris-paul-gerhardt.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2019-02-06
- _Version 4 update_: 2019-05-28 
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
