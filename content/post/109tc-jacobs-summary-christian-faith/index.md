---
date: 2019-04-22 15:05:59-04:00
publishDate: 2019-04-22 15:05:59-04:00
title: A Summary of the Christian Faith by Henry Eyster Jacobs
slug: "109tc-jacobs-summary-christian-faith"
categories: ["Lutheran Library Publications"]
tags: ["Catechism", "Book of Concord", "Jacobs", "Justification", "Election", "Predestination", "Start Here"]
authors: ["Jacobs, Henry Eyster"]
titles: ["A Summary of the Christian Faith"]
origpublishers: ["General Council Publication House"]
origdates: ["1905"]
synods: ["General Council"]
---

Henry Eyster Jacobs was one of the clearest and best teachers American Lutheranism has produced.  _The Summary_ has been beloved by Christians for its easy to use question and answer format and is coverage of all the essentials of the Christian faith.  

He includes two essays on the subject of election and predestination.  The second, *Luther on Speculations Concerning Predestination*, is particularly useful.  

# Topics Covered

- The Being And Attributes Of God
- The Trinity
- Creation
- Providence
- Of Angels
- Man As Created
- Sin
- The Grace Of God Towards Fallen Men
- The Preparation Of Redemption
- The Person Of Christ
- The States of Christ
- The Offices Of Christ, Christ As Prophet
- Christ As Priest
- Christ As King
- The Mission Of The Holy Ghost
- Faith in Christ
- Justification
- The Gospel Call
- Illumination
- Regeneration
- The Mystical Union
- Renovation Or Sanctification
- The Word As The Means Of Grace
- The Law And The Gospel
- The Sacraments
- Of Holy Baptism
- Of The Holy Supper
- The Church
- The Ministry
- The Church’s Confessions
- Church Discipline
- The Christian Family
- The State
- Life After Death
- The Resurrection Of The Body
- The Return Of Christ
- The General Judgment
- Eternal Death
- Eternal Life
- The Divine Purpose As Interpreted By Its Contents And Results


# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)

![book cover](/img/posts/109tc-jacobs-summary-christian-faith-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/109tc-jacobs-summary-christian-faith.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 109tc&body=Please send 109tc-jacobs-summary-christian-faith.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 109tc&body=Please send 109tc-jacobs-summary-christian-faith.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}

# Publication Information

- _Lutheran Library edition first published_: 2018-09-09
- _Version 4 update_: 2019-04-22 
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)



