---
date: 2019-04-23 09:31:20-04:00
publishDate: 2019-04-23 09:31:20-04:00
title: "Great Cats I Have Met by William Thomson"
slug: "e23-thomson-great-cats-i-have-met"
categories: ["Lutheran Library Publications"]
tags: ["Cats", "Animals"]
authors: ["Thomson, William"]
titles: ["Great Cats I Have Met"]
origpublishers: ["Alpha Publishing Company"]
origdates: ["1896"]
synods: ["N/A"]

---

"In throwing these various adventures together, I have followed as nearly as possible the actual course of my journeying from one country to another, so as to form the whole into a sort of continuous narrative, though in one case I jump from Brazil to Texas in order to bring in some relevant incidents which occurred long after the one related in the first part of the Brazilian story.

"Lacking, on the day I write these lines, but one month of completing my seventy-second year, it may well be that I shall not live to see this book published; yet I submit it to a generous public without fear of hostile criticism, because, from its utterly unpretentious character, it is not open to such. – William Thomson


# Chapters

- Author’s Note.
- 1. My First Great Cat.
- 2. A Bobcat And A Pigeon-pie.
- 3. Little Manuel And The Jaguar.
- 4. The “Little Spotted Tiger.”
- 5. A Black Lion.
- 6. A Day In A Tree.
- 7. My First Lion-hunt.
- 8. A South African Leopard.
- 9. A Family Of Tigers.
- 10. How I Met The True Panther.
- 11. A Black Leopard.
- 12. Two Girls And A Tortoise-shell Tiger.

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download) 

![book cover](/img/posts/e23-thomson-great-cats-i-have-met-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/e23-thomson-great-cats-i-have-met.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request e23&body=Please send e23-thomson-great-cats-i-have-met.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request e23&body=Please send e23-thomson-great-cats-i-have-met.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2017-08-08 
- _Version 4 update_: 2019-04-23
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
