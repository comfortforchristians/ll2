---
publishDate: 2019-01-31 12:02:21-05:00
title: "John Gottlieb Morris: A Biographical Sketch"
slug: "john-gottlieb-morris"
categories: ["Biographical Sketches"]
tags: ["Morris", "Lutheran Ministers"]
authors: ["Morris, John Gottlieb"]
synods: ["General Synod"]
---

[![John Gottlieb Morris](/img/posts/morris-john-gottlieb.jpg)](https://www.lutheranlibrary.org/authors/morris-john-gottlieb/)

# About John Gottlieb Morris


Rev. John Gottlieb Morris, D.D., LL. D., was born in York, Pa., Nov. 14, 1803. He was graduated at Dickinson College in 1823, studied theology at Princeton Theological Seminary in 1823-26, and at Gettysburg Seminary in 1827, being a member of the first class in the latter institution, and was licensed to preach in 1827. He received the degree of D. D. in 1839, and that of LL. D. in 1873, both from Pennsylvania College, Gettysburg. Dr. Morris was the founder of Trinity English Lutheran Church, Baltimore, Md., and its pastor in 1827-60, librarian of Peabody Institute, Baltimore, in 1860-65; pastor of the Third English Lutheran Church, Baltimore, in 1864-73, and since 1874 of a congregation at Lutherville, Md. He has been lecturer on Natural History in Pennsylvania College, Gettysburg, since 1834; on pulpit eloquence and the relation of science and revelation in the theological seminary there since 1874, and has delivered lectures in Smithsonian Institute, Washington, D. C. He was secretary of the General Synod in 1839, and president of the same body in 1843 and 1883. and President of the first Lutheran Church Diet in Philadelphia in 1877. 

He has been a trustee of Pennsylvania College and director of the theological seminary at Gettysburg for many years. With his brother he founded Lutherville Ladies' Seminary. In science he has devoted himself specially to entomology and microscopy. He has been elected to membership in many scientific societies in this country and has been chairman of the entomological section of the American Association for the Advancement of Science. He is president of the Maryland Bible Society and the Maryland Historical Society. During the year 1846 he traveled extensively in Europe, and in the same year he aided in establishing the Evangelical Alliance at London. He founded the Lutheran Observer in 1831, was its editor until 1833, and since then has been one of its contributors. He is the leader of the conservative party in the General Synod, and its ablest representative. 

Besides many translations of works, addresses; review and magazine articles, tracts, and scientific papers, he has published "Catechumen's and Communicant's Companion" (Baltimore, 1831); "Henry and Antonio," of Brettschneider, translated from the German (Philadelphia, 1831; altered edition, entitled, "To Rome and Back Again," 1853); "Catechetical Exercises on Luther's Catechism," altered from the German (Baltimore, 1832); Von Leonard's "Lectures OD Geology," translated from the German (1839); "Popular Exposition of the Gospels," (2 vol., 1840); "Life of John Arndt" (1853); -'Life of Martin Behaim, the Gerijian Cosmographer" (1856); "Life of Catherine de Bora" (1856); "The Blind Girl of Wittenberg"; (Philadelphia, 1856); "Quaint Sayings and Doings Concerning Luther" (1859); Catalogue of the Lepidoptera of North America" (1860); "Synopsis of the Diurnal Lepidoptera of the U. S." (Washington, 1862); "The Lords Baltimore" (Baltimore, 1874); "Bibliotheka Lutherana" (Philadelphia, 1876); "Fifty Years in the Lutheran Ministry" (1878); "A day in Capernaum," translated from Franz Delitzsch (1879); "The Diet of Augsburg," (1879); "Augsburg Confession and the Thirty-nine Articles" (1879); "Journeys of Luther" (1880); "Luther at Wartburg and Coburg" (1882); "Life of Luther," translated from Kostlin (1883); "Lutheran Doctrine of the Lord's Supper" (1884); and Memoirs of the Stork Family (1886). 

For many years he has been prominently connected with all the great movements in the Church, and is, perhaps, more widely known than any living minister connected with the General Synod. Besides his literary and theological labors. Dr. Morris has, from the beginning of his ministry, given considerable attention to the study of Natural History, and has attained to an enviable position among the naturalists of this country. Two of his works on Natural History have been published by the Smithsonian Institute of Washington, D. C, namely. Catalogue of the Described Lepidoptera of the United States and Synopsis of the Described Lepidoptera of the United States. He has also been a frequent contributor to scientific journals, both American and foreign, and is a member of two foreign learned societies, — one in Denmark, The Eoyal Anti-Columbian Society of Northern Antiquaries, Copenhagen, and one in Germany, Die Naturhistorische Gesellschaft zu Nuernberg, Bavaria, and also a member of the American Scientific Association, and of over a dozen other Literary and Scientific societies. 


{{% alert note %}}
This biography is taken from Jens Jensson's _American Lutheran Biographies_, published 1890.
{{% /alert %}}

First published online 2019 at [LutheranLibrary.org](.).