---
date: 2019-04-23 09:34:38-04:00
publishDate: 2019-04-23 09:34:38-04:00
title: "Crow's language lessons and other stories of birds and animals by Julia Darrow Cowles"
slug: "e25-cowles-crows-language-lessons"
categories: ["Lutheran Library Publications"]
tags: [ "Cats", "Animals"]
authors: ["Cowles, Julia Darrow"]
titles: ["Crow's Language Lessons"]
origpublishers: ["Thomas Y. Crowell & Company"]
origdates: ["1903"]
synods: ["N/A"]

---
"...Did you ever hear of a cat taking in boarders? I never did till I heard of Peter. But he was a real cat, and this is a true story I am going to tell you... – From "Peter's Boarding House"

# Stories in this Volume

- Crow’s Language Lesson.
- A Saucy Thief.
- Mrs. Wiggins’ Sweet Peas.
- Tippie's Visit.
- Ponce’s Vacation.
- Brave Trix.
- Peter’s Boarding-house.
- How Muff Won Her Way.
- How The Kittens Were Named.
- Old Woolly Stockings.
- The Rescue Of Mother Hen’s Family.
- A Restaurant For Birds.
- A Saucy Band Of Robbers.
- A Race.
- A Winter Walk.
- The Dog That Telegraphed.
- What Grandma Sent.
- An Unexpected Parade.
- The Rooster’s Joke.
- The Bird That Sang In The Night.
- Lutheran Library Publishing Ministry Catalog

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download) 

![book cover](/img/posts/e25-cowles-crows-language-lessons-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/e25-cowles-crows-language-lessons.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request e25&body=Please send e25-cowles-crows-language-lessons.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request e25&body=Please send e25-cowles-crows-language-lessons.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2017-08-09 
- _Version 4 update_: 2019-04-23
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
