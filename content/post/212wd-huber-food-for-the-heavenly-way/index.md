---
date: 2018-08-16 12:00:00
title: "Food for the Heavenly Way: Words of Counsel to Beginners in the Christian Life by Eli Huber"
slug: "212wd-huber-food-for-the-heavenly-way"
categories: ["Lutheran Library Publications"]
tags: ["Huber", "Catechism"]
authors: ["Huber, Eli"]
titles: ["Food for the Heavenly Way: Words of Counsel to Beginners in the Christian Life"]
origpublishers: ["Lutheran Publication Society"]
origdates: ["1885"]
synods: ["Unknown"]

--- 
"A good deal is said in these days about how to preach. In the days of Christ and Paul, what to preach seemed of vastly more importance. How to listen, what preparation of mind and heart is needful, what attitude toward the truth, what appreciation of the truth, these are more important questions than extempore or written preaching. Take heed how ye hear, is a divine injunction; take heed how ye preach, is a human command. The soil needs preparation quite as much as the sower and the seed."

"You want to learn yet more minutely and distinctly what God requires of you, and what through his grace he will do for you. To this end you will need to engage in a life long study of God's Word. To direct and encourage you in this blessed work, is the object of the following address to young Christians. That you may read it carefully and devoutly, is the desire, counsel, and prayer of...Your Pastor, Eli Huber 


# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)

![book cover](/img/posts/212wd-huber-food-for-the-heavenly-way-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/212wd-huber-food-for-the-heavenly-way.pdf)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 212wd&body=Please send 212wd-huber-food-for-the-heavenly-way.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}
[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 212wd&body=Please send 212wd-huber-food-for-the-heavenly-way.epub)


