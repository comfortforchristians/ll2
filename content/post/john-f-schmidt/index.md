---
date: 2019-06-30 05:35:23-04:00
publishDate: 2019-06-30 05:35:23-04:00
title: "John F. Schmidt: A Biographical Sketch"
categories: ["Biographical Sketches"]
tags: ["Schmidt", "Lutheran Ministers", "Evangelical Review"]
authors: ["Schmidt, John F.", "Krauth, Charles Porterfield"]
synods: ["Pennsylvania Synod"]
---

The transition from [Dr. Helmuth](/henry-helmuth/) to his intimate friend and colleague is very natural. Dr. Schmidt would, perhaps, have never abandoned the country of his birth, had it not been for his fond devotion to the friend of his youth, _animae dimidium suce_, separation from whom seemed so painful and almost insupportable. Such instances of friendship are rare, and yet how beautiful, how honorable to humanity! A well tried friend, one of kindred spirit and congenial tastes, cannot be too highly valued. 

{{% toc %}}

>_Nil ego contulerim jucundo sanus amico._ 

How greatly may he add to our joy, and alleviate our sorrow, lighten adversity, and render prosperity yet brighter. _Nam et secundas res splendidiores facit amicitia, et adversas, partiens communicansque, leviores_. As we tread the pathway of life, strewn with so many thorns, and beset with numerous difficulties, our nature loves sympathy; we seek for one, in whom we may confide, in whose presence we may think aloud, unbosom our cares, and reveal the secrets of the heart. _Quid dulcius quam habere, quicum omnia audeas sic loqui, ut tecum_. 


The attachment of Drs. Helmuth and Schmidt commenced in youth. It continued unbroken and unaltered through life, and terminated only in death. Although they occupied a position, in which defects in each other's character could be readily discerned, and the infirmities of their common nature noticed, yet we never learned that their friendship experienced any change, their affection suffered any diminution; that anything occurred to awaken suspicion, or to mar pleasant intercourse. Their intimacy was of the most close and endearing character. It was deep, intense devotion. They lived in harmony all the time, and labored together faithfully for the good of the people, over whom they had been placed as spiritual guides, and for the extension of their Master's kingdom. 

## John F. Schmidt, D. D. 

The subject of the present sketch was born in 1746. His parents resided in a rural district, the village of Froshe, near Aschersleben, and were engaged in agricultural pursuits. His father was a man of more than ordinary intelligence, and was deeply interested in the education of his children. Discovering that his son John possessed talents of a high order, he resolved to furnish him with the best advantages for mental culture, and to send him to the celebrated Orphan House at Halle, at the time under the care of that eminent man of God, Augustus Hermann Francke. The funds expended for this object were not misapplied. The expectations of the friends were fully realized — the son soon became distinguished as a diligent, persevering and successful student. His progress in the acquisition of the Ancient languages, as well as in the study of the Natural Sciences, was very rapid. In the year 1765 he was regarded as sufficiently qualified for admission into the University. Here he continued to sustain the reputation, as a scholar, which he had previously enjoyed. He engaged with great zeal in the study of divinity, and devoted considerable time to the Hebrew, Syriac and Arabic languages. His clear and acute mind also found much pleasure in philosophical investigations. As a mathematician, he was distinguished. He was fond of Astronomy, and in the accuracy of his historical knowledge, particularly of ecclesiastical history, he had scarcely a superior. During his connection with the University, he was appointed a teacher in the Orphan School, and for two years gave instruction in the mathematics, as well as the Latin and Greek. He was considered so good an Arithmatician, that to him the first class in the school in that branch was assigned. 

## Helmuth's Call to America

It was in 1788 Dr. Helmuth received a call to America, to preach the gospel. He immediately communicated the fact to his friend Schmidt, who was greatly distressed at the idea of parting with one he so tenderly loved. Soon after, in the course of a conversation with Dr. Francke on the subject of his mission, the Doctor expressed his regret to young Helmuth, that he must undertake the voyage alone, and wished that there was some one to accompany him in the enterprise in, which he was about to embark. Helmuth replied that he thought Schmidt would not be averse to going with him, and suggested that the matter should be presented to his consideration. Schmidt at once cordially acceded to the proposition, provided it met with the approbation of his father, from whom having, in a few days, received a favorable response, he determined to give himself to the work. 

## "Friends could not give him up"

The young men, in company, started on their journey, and proceeded to the residence of Mr. Schmidt's parents, for the purpose of bidding adieu to the scenes of his childhood. On their arrival at the house, they found the whole family were at church. The father, on his return, gave them a cordial reception, but seemed to be much affected, when he ascertained that they were already on their way to the New World. Presently the mother and the rest of the household reached the dwelling, when quite a scene occurred. The grief was intense, and exhibited itself in sobs and tears. By this time the strange news had spread through the neighborhood, and the room was thronged with inquiring and sympathizing friends. The excitement increased, and the deepest feeling prevailed. The beloved son, who was the occasion of this anxious interest, remained calm and self-composed. He uttered not a word. His friends felt as if they could not give him up; it seemed to them, as if one so lovely and interesting must not be torn from the arms of their embrace. In the midst of this state of things, Mr. Helmuth begged all present to be quiet for a few minutes, as he desired to say something to them. He took from his pocket his favorite book, Bogatzki's _Schatz-Küstlein_, and on opening it his eyes fell upon a passage adapted to the occasion, which he read. He then offered a fervent prayer to God. The influence was most happy; all became immediately consoled, the parents were reconciled and acquiesced in the son's decision, whilst the father, extending his hand to the travelers, said: "Go in the name of the Lord and, if it should be necessary, even to Turkey — the Lord be with you." The father appeared so well satisfied, that he followed them, so as to be present at the ordination, which was to take place a few days afterwards at Wernigerode. 

## Intense self-doubts

Thence the pilgrims went to Hamburg, from which point it was proposed to embark for London. Here young Schmidt was called to pass through a most severe conflict. After the excitement connected with the separation from his parents had subsided, he experienced a reaction in his feelings. Doubts and difficulties perplexed and embarrassed his mind. His confidence wavered, and his courage began to fail. The Lord tried his faith. He came forth from the struggle strengthened. His heart was encouraged. He felt firmly convinced that he was in the path of duty, that it was the Lord's will he should labor on these Western shores. 

The young men were detained longer at Hamburg than they expected. Their passage had been secured, and their baggage already conveyed on board, but on account of some unforeseen difficulties arising, they were disappointed, and obliged to remain. Their goods were consequently removed from the vessel, and they awaited another opportunity, which they supposed would soon offer. This detention saved their life. The ship, in which they had intended to sail, was wrecked by the way. It seemed a divine interposition. It was regarded by them as a proof of God's special care over them. They felt grateful. They knew that they were not forsaken. They were assured that He, in whom they trusted, and who was mightier than their enemies, was with them, and that no weapon formed against them could prosper. 

In the month of January, 1769, they sailed from London, and reached Philadelphia the following April. During part of the voyage, young Schmidt suffered considerably in health, so that serious apprehensions were excited with regard to the result. He, however, recovered, very much to the joy of his friend Helmuth, who was deeply concerned in reference to him, as he had been chiefly instrumental in inducing him to leave home and undertake the journey. 

## Received by Dr. Muhlenberg

Mr. Schmidt, with his friend, was kindly received by Dr. Muhlenberg, the apostle of Lutheranism in this country. He enjoyed the hospitalities of his home for several months, until he accepted a call to Germantown, Penn. This congregation he served with great fidelity for seventeen years. He was greatly beloved by his people, and his labors appreciated. He was pastor there during our Revolutionary war, and as he was a strenuous Whig, and disposed to take a decided stand in favor of the patriotic efforts, which were made for independence, he was compelled to flee, whilst the town was occupied by the enemy. Not very far from the spot, on which the old church was erected, is still standing the building in which the British took refuge on the occasion of that memorable battle, which was so disastrous to the American arms. Pastor Schmidt returned to his charge as soon as it was thought that his life would no longer be in jeopardy. 

## Return to Philadelphia

In the year 1785 he removed to Philadelphia, and became the colleague of Dr. Helmuth. His departure from Germantown, and his acceptance of this situation, was no doubt influenced by the desire to be more intimately associated with his old friend. This position he held until his death, in the language of his bereaved colleague, "faithfully discharging its duties, and enjoying the respect and affection of all." Whilst here he passed through the furnace of affliction, burying in rapid succession seven children, all in the bloom of life, and soon after following to the grave the beloved partner of his life. He was also himself attacked with yellow fever, during the fearful ravages of this dreadful epidemic in the year 1793. _Many are the afflictions of the righteous, but the Lord delivereth him out of them all_. Having suffered himself, he could feel for the suffering. He could truly say: 

>_Non ignarus mali, miseris succurrere disco._ 

He did sympathize with the afflicted and distressed; he was always ready to minister to their comfort, and bring the relief in his power; the sick and the aged found in him a devoted friend — 

>&emsp;&emsp;&emsp;&emsp;&emsp;"Needy poor \
>And dying men, like music heard his feet \
>Approach their beds; and guilty wretches took \
>New hope, and in his prayers wept and smiled, \
>And blessed him, as they died forgiven." 

Even when in feeble health, and sickness had prostrated his strength, he was visited by the members of his church, and performed for them pastoral service. He was wont to say:— "I will labor as long as I can, and I will not spare myself, even if I should sink under the weight of my burdens."


It pleased the Lord to remove him from this world, after a protracted and painful illness, May 16th, 1812, in the sixty-seventh year of his age. He endured his sufferings without a murmur, with remarkable fortitude and Christian resignation. His cheerful submission to the divine will was striking, and profitable  all who visited his sick chamber. His remains were followed to the grave by a large concourse of sorrowing friends. His surviving and mourning associate, Dr. Helmuth, delivered a brief and suitable address on the occasion, from the words: "I am distressed for thee, my brother Jonathan: very pleasant hast thou been to me: thy love to me was wonderful, passing the love of women." The corpse, after the delivery of the address, was interred in the vault in front of the altar of St. Michael's church, corner of Fifth and Cherry, Philadelphia. A fortnight afterwards, the occasion was still further improved by a discourse, which Dr. Helmuth preached, from the text: "For I am not ashamed of the, Gospel of Christ; for it is the power of God unto salvation to every one that believeth; to the Jew first and also to the Greek" — which text, said the speaker, contained the sum and substance of all the deceased's sermons, for he esteemed the doctrine of the atonement as precious above every thing else. 

![John F. Schmidt](/img/posts/schmidt-john-f.jpg)

## Dr. Schmidt's Character

Dr. Schmidt possessed a clear, acute and vigorous intellect. He was a deep and an original thinker. His attainments were varied and extensive. He was generally regarded as a fine scholar, and from the University of Pennsylvania he received the highest literary testimonials. He never made any parade of his knowledge, but was characteristically modest, retiring and unassuming. In the pulpit he was practical and instructive. He is described in the _Hallische Nachrichten_, as a plain and pious preacher, whose aim it was to lead the impenitent to God, and to present before his hearers Jesus Christ and him crucified. He was considered by all who knew him, a sincere, upright, and devoted Christian, fearing God and eschewing evil, whose constant endeavor it was to do his Master's will, preaching the truth by example as well as by precept, and laboring systematically, and with unwearied patience, for the good of souls. His actions corresponded with the words he presented from the sacred desk. His life was blameless and unsullied, a beautiful exemplification of the power of religion. He did not labor in vain. His efforts to do good were signally blessed. Through his instrumentality souls were awakened, and brought to a saving acquaintance with the Redeemer. When such a one is removed, and the church deprived of his labors, it is not surprising that his loss should be felt, and deep sorrow evinced! We may truly exclaim:— Help Lord, for the godly man ceaseth.for the faithful has failed among the children of men. 

# Publication Information

- _Author_: __"Krauth, Charles P."__, Editor.
- _Journal_: __"The Evangelical Review."__
- _Originally Published_: 1854.
- _Lutheran Library Edition_: 2019
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

{{% alert note %}}
{{% biographical-sketch-note %}}
{{% /alert %}}
