---
date: 2018-07-02 12:00:00
title: "John Burns: The Hero of Gettysburg by Edmund Jacob Wolf"
slug: "s03-wolf-john-burns-hero-gettysburg"
categories: ["Lutheran Library Publications"]
tags: ["Wolf", "Short Books"]
authors: ["Wolf, Edmund Jacob"]
titles: ["John Burns: The Hero of Gettysburg"]
origpublishers: ["The Lutheran Publication Society"]
origdates: ["1905"]
synods: ["General Synod"]

--- 
"Let us see to it by the spirit of eternal vigilance that America continue to produce a race of men like John Burns, and our place in the forefront of the great world powers will be held as long as the granite and bronze of this monument, here dedicated to personal heroism and valor. 

"And we do well, fellow citizens, in rendering here, on the anniversary of his daring feat, this final tribute to the memory of our townsman, who so surprisingly and justly so, became one of the most famous characters of the war of the Union. Who can estimate the debt which our nation owes to such a spirit of self-sacrifice and unmeasured devotion, what strength it derives from this species of moral fiber, what independence and security, what majestic and glory accrue to the Republic from a citizenship which in any crisis and at any cost springs to its defense."
 
# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)

![book cover](/img/posts/s03-wolf-john-burns-hero-gettysburg-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/s03-wolf-john-burns-hero-gettysburg.pdf)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request s03&body=Please send s03-wolf-john-burns-hero-gettysburg.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}
[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request s03&body=Please send s03-wolf-john-burns-hero-gettysburg.epub)

