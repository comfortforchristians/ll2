---
date: 2019-06-23 05:55:31-04:00
publishDate: 2019-06-23 05:55:31-04:00
title: "Henry Helmuth, D.D.: A Biographical Sketch"
slug: "henry-helmuth"
categories: ["Biographical Sketches"]
tags: ["Helmuth", "Lutheran Ministers", "Evangelical Review"]
authors: ["Helmuth, Henry", "Krauth, Charles Porterfield"]
synods: ["Pennsylvania Synod"]
---

The memory of those who have been eminently useful in the church of God, should be cherished, and their virtues transmitted to posterity. They are worthy of grateful remembrance and respectful imitation. Their services should be embalmed for future generations. The language found in the burial service of the Church of England, is exceedingly beautiful, and has often been much admired: 

>"We give thee hearty thanks for the good examples of all these thy servants, who having finished their course in faith, do now rest from their labors." 

## Reminiscences of Lutheran Ministers

The record of a good man's life should be kept, so that, though dead, he may yet speak, that his history may be not only a memorial of his fidelity and zeal, but an example to others, urging them on to increased fidelity in their Master's service, and prompting them to go forward with greater diligence in their work of faith and love. 

In the death of every good man, we have additional evidence afforded us of the power of religion; we are impressed with the sentiment that the word of the Lord is true, that in his service there is rich reward, that Godliness is profitable unto all things, having the promise of this life and of that which is to come. No one can peruse the memoirs of the pious, and contemplate their character, without profit to his soul. Besides, the praise we render to departed worth, is testimony in honor of truth and virtue — testimony which cannot fail to exert a salutary influence upon the living. It is seldom we find one so abandoned as to desire to leave behind him a tarnished name or a sullied reputation, to hand down to survivors an immortality covered with infamy and shame. Many minds that would otherwise be excluded from our teachings, may thus be reached. 

{{% toc %}}

## J. C. Henry Helmuth, D.D.

{{% alert note %}}
[Download a printable PDF](/pdf/henry-helmuth.pdf)
{{% /alert %}}

![Henry Helmuth](/img/posts/helmuth-henry.jpg)

Among the most faithful, zealous and successful of our earlier ministers, J. C. Henry Helmuth, D. D., holds a high rank. He was born at Helmstadt, in the Duchy of Brunswick, in the year 1745. His father died when he was yet a boy. He immediately left home without the knowledge of any of the family, and was overtaken on the highway by a nobleman in his carriage, who entered into a conversation with him, and inquired whither he was going. The lad informed him that he had left home, because he was angry with God, having prayed earnestly to him during his father's illness, for his restoration to health, but God had not answered his petition. Interested in the artless reply of the innocent boy, and commiserating with his sad condition, the nobleman took him into the carriage, and afterwards sent him to Halle at his expense, to be educated. He was in the fourteenth year of his age, when he entered the Orphan House, and after having passed over the prescribed course of study, he became a member of the University, the Alma Mater of Dr. Muhlenberg, and other pioneers of the Lutheran church in the United States. To this institution our congregations usually looked, in our earlier history, when in want of a pastor, relying with confidence upon the Theological Faculty in the selection of the candidate, and seldom were the expectations of the people disappointed. 

## Request for a Preacher To Go To America

When the request for a preacher was on this occasion made, the attention of Dr. Francke was immediately directed to young Helmuth, who was in the twenty-fourth year of his age, and at the time engaged as a preceptor in the Orphan School. His position here furnished him with experience, and more fully prepared him for the office for which he was, in the Providence of God, destined. It also gave those, into whose hands the appointment had been committed, an opportunity to ascertain the peculiar fitness of Mr. Helmuth for the Missionary work in this Western land. They wisely judged that a man, not only truly pious, but so fluent in speech, would be just the one to send to America. The Faculty had also been most favorably impressed with the first attempts of young Helmuth at preaching. His first sermon was delivered in the Hall of the Orphan House, used for divine service, and the celebrated Bogatzky, the author of the _Schatz-Kästlein_[^bEJ] was present, sitting in an alcove under the pulpit, concealed from the notice of the speaker. After the exercises, Bogatzky expressed his approbation of the performance, and offered him some encouragement. This was, no doubt, one reason why Dr. Helmuth retained, in after life, so much affection for Bogatzky, and regularly, every morning, read a passage in the _Schatz-Kästlein_. 

## A Borrowed Wig

As an illustration of the facility with which he spoke in public, and of the pulpit talent he evinced, at the very commencement of his career, we are told that he and another candidate were invited by a neighboring clergyman to preach in his church, the one in the morning, and the other in the afternoon. In those days it was customary to wear wigs in the pulpit, and it would have been regarded as extremely indecorous for a clergyman to appear in the sacred desk without one. Dr. Helmuth succeeded in borrowing a wig, and preached in the morning; but his friend, failing to procure the essential, could not officiate. Dr. Helmuth, with little or no preparation, again ascended the pulpit, and acquitted himself most creditably, and much to the satisfaction of the audience assembled. 

## Pastor at Lancaster, Pennsylvania

When the call to this Western world was first presented to Dr. Helmuth, he hesitated in reference to its acceptance. Some of his friends attempted to dissuade him from the enterprise, but in spite of all opposition he determined to go. His doubts were removed, the path of duty became clear, and he felt that if he refused the invitation, he would do violence to conscience, and resist the will of God. Like his predecessors, he was ordained by the _Consistorium_ at Wernigerode, and after making a visit to his widowed mother, at Hanover, he journeyed to England, whence he embarked for this country. He reached Philadelphia in the spring of 1769, and was soon after elected pastor of the Lutheran church at Lancaster, Pa., where he, for ten years, labored to great acceptance, and enjoyed the respect and confidence of the people. 

## Professor at The University of Pennsylvania

In 1779 he resigned this charge, having received a unanimous call to Philadelphia. Here he spent the remainder of his life, discharging his duties with great ability and faithfulness, as long as his physical strength permitted him. His pastoral relations were continued until the autumn of 1820, when the growing infirmities of age compelled him to relinquish the station. He passed his time in retirement, engaged in meditation, and waiting for the coming of the Lord. He died February 5th, 1825, in the eightieth year of his age. His funeral sermon was preached by Rev. Dr. Demme, from the words: Remember them which have the rule over you, who have spoken unto you the word of God: whose faith follow, considering the end of their conversation. 

Dr. Helmuth was a man of acknowledged ability, which may be inferred from the fact, that he held the appointment of Professor of German and Oriental languages in the University of Pennsylvania, for the space of eighteen years, from which institution he received, in 1780, the honorary degree of A. M., and in 1785 that of D. D. In 1785, he, with his colleague, Dr. Schmidt, commenced a private seminary for the instruction of candidates for the Lutheran ministry; they continued the work for twenty years, so long as their numerous duties would allow. Among their students we find the names of Drs. Lochman, Endress, Schmucker Sr., Miller, Baker, Messrs. Goering, Batis, Ulrich, Jaeger, Hecht, and other ministers of our church. 

## Influence of Dr. Helmuth

Dr. Helmuth exercised an influence which is rarely possessed. In the ecclesiastical body of which he was a member, he was frequently elected to offices of honor and trust. In the city in which nearly a half century of his life was passed, he was identified with many of the public institutions of the day, and frequently occupied responsible positions. He was also favorably known as an author. In 1793 he published a work on Baptism and the Sacred Scriptures, an octavo volume of three hundred and thirty-six pages. He also wrote a practical treatise entitled Communion with God. He composed numerous pious works for children, and a volume of hymns, many of which have been introduced into the German collection, published under the auspices of the Pennsylvania Synod. He was likewise editor of the Evangelical Magazine, printed for some years in the German language in Philadelphia. 

As a preacher, Dr. Helmuth had more than ordinary power. It is the testimony of all who ever heard him, that he was able and eloquent. His manner was natural and impressive, and characterized by overwhelming pathos and great unction. He seemed deeply interested in the truth he was presenting, and produced upon the mind the conviction that he was in earnest, that he felt the importance of what he uttered, that he was actuated by an anxious concern for the temporal and eternal welfare of those, whom he addressed. In listening to his pulpit discourses, you were forcibly reminded of the truth of the rule laid down by an ancient master: 

>Si vis me flere, dolendum est primum ipsi tibi. \
>[If you would have me weep, you must first feel grief yourself. – Horace]

and led to the conclusion that earnestness is a necessary element in eloquence, that the great secret for moving the passions is to be moved ourselves. 

## With Muskets In Their Hands

As an evidence of Dr. Helmuth's power over an audience, we give on reliable authority, the following incident in his life. Having been invited to pay a visit to one of our country pastors,[^bEK]  who had formerly studied under his care, and who lived in a small village in the interior of Pennsylvania, he accompanied him on a certain occasion to a small church, situated upon the brow of a hill, where, in olden times, for fear of the savages, the fathers attended the sanctuary with their muskets in their hands as sentinels, whilst the others worshipped their God. The audience at this time was unusually large, and the venerable Doctor, in his usual affectionate manner, and with all that simplicity of style for which he was distinguished, presented the word of God's grace. The effect was astonishing; the attention of the audience was fixed, their feelings seemed to be at the speaker's control. Every eye was moist, every soul moved; and when, in order to impress more forcibly the solemn truths he was urging upon the fathers, he said, "_Nun stellt euch jetzt vor_" [^bEL] to his surprise, the old men came out of their seats, and all stood around the altar. The Doctor, not at all disconcerted, with his customary felicitous manner of turning all things to a good account, addressed and exhorted them,and then told them to take their seats. By this time the feeling of the congregation was indescribable. Not willing to let so favorable an opportunity of doing good pass, he next addressed the mothers, _Jetzt ihr Mutter stellt euch vor_ — then the young men, _Stellt euch vor_, and afterwards the young women, most powerfully and affectionately pressing the truths home to their hearts. 

## Strength in the Pulpit

In the year 1805 he preached the Synodical sermon at Lebanon, and such an impression did it produce on the audience, that it was remembered for years, and frequent reference was afterwards made to its power. He was exceedingly fluent in the pulpit, expressing himself with readiness and correctness. He did not write out his discourses, but usually spoke from a skeleton carefully studied. His mind was so well disciplined, and he possessed in so high a degree, the gift of speech, that he could discuss almost any question, with profit and interest to his hearers. In the winter of 1811, on a very inclement Sabbath, he gathered the few persons that were present around the stove, and delivered, it is said, a most powerful and edifying sermon on the subject of the weather. With great fidelity he preached the gospel, fearlessly declaring the whole counsel of God, and constantly having in view the great object of his calling, Christ and Him crucified. In the discharge of his pastoral duties he was faithful, and labored with untiring zeal for the spiritual welfare of his charge. 

## Catechization

He was deeply interested in the rising generation, and devoted much time to their improvement. He took great delight in instructing the children of the church, and indoctrinating them in the principles of the Christian religion. His catechizations were very interesting and instructive. The Lutheran church, in its early history, was distinguished for the provision she made for the thorough religious education of the youth of the church, and pastors laid themselves out for the work. It is one of the peculiarities of our denomination, and ought not to be neglected. Although catechetical instruction has sometimes failed to secure the design intended, and has been the occasion of animadversion, it is, nevertheless,a valuable means of grace; it has been owned of God, and blessed to the salvation of precious souls. As early as 1804 there was a flourishing Sabbath School connected with his church, embracing two hundred scholars and forty teachers, which may be regarded as an additional proof of his desire to advance the cause of religion among his people. 

## Prayer

He was a man of prayer, and the friend of spiritual religion. It was his constant aim to promote among his people vital godliness. Prayer-meetings were regularly held in his congregation, which he approved and countenanced. Whilst pastor in Lancaster, in 1773, in a communication to the _Hallische Nachrichten_ [Halle Reports], on the state of religion and his charge, he says:[^bEM] 


>"Twice or thrice a week, meetings were held in the evening, at different places, by the subjects of a work of grace, and the time spent in singing, in praying, in reading a chapter of the word of God, or of Arndt's _True Christianity_, and if no prayer-meeting was held on Sabbath evening in the church, the substance of the sermon was discussed. In some houses the number was rather large, there being sometimes as many as forty persons assembled at one place. The children of this world several times attempted to disturb their worship, by standing at the windows listening, and by throwing against the doors. But by grace they were able to bear it without any resistance, and even when on their way home, they were assailed on the streets, and stigmatized with harsh epithets, but they answered not a word. Some of their persecutors also, when they heard these men sing and pray with so much fervor and sincerity, not only ceased their opposition, but induced others to do the same."

On another occasion he writes:—[^bEN] 


>"As to the spiritual condition of our church, there is at present an unusually blessed state of revival. Aged, dead sinners have been brought to life, and cried out weeping for mercy. Sinners whose case I had often regarded as hopeless, are powerfully affected, and many of them truly converted to Christ. How frequently has my despondent mind been cheered, and my sluggish heart been roused, especially during the past weeks. I published a Sacramental season, and in order that I might have an opportunity to probe the hearts of my dear people, I gave them an invitation to call on me from eight to twelve o'clock, a. m., every day, for two weeks. I thus had an opportunity to converse with each one separately, and to learn the extent and depth of the work of grace in their souls."

## Yellow Fever

He was very attentive in his visitations upon the sick, and administered to them counsel and instruction, comfort or warning, as their case required. During the terrible ravages of the yellow fever, which spread its deadly contagion over Philadelphia in 1793 and 1800, and swept away thousands of its inhabitants, most of the pastors forsook their congregations; all, who could escape the devouring pestilence, fled. Few were left to attend to the sick and the dying, and to bury the dead. Dr. Helmuth remained with his flock, at the imminent risk of his life. Inspired with the courage which faith gives, he looked death in the face. He went to the house of mourning. Like an angel of mercy, he visited the sick, and bent over the dying, imparting the consolations of the gospel. Hundreds of our Lutheran friends fell victims to this fearful epidemic, and six hundred and twenty-five of his members he buried. On one occasion, from the pulpit, he remarked, "look upon me as a dead man," and then, in the spirit of his Master, he departed to the abode of suffering and distress. He counted not his life dear, so that he might finish his course with joy, and the ministry which he had received of the Lord Jesus, to testify the gospel of the grace of God. 

## English Language

Dr. Helmuth preached exclusively in the German language. He, as well as his colleague, Dr. Schmidt, were very determined in their opposition to the introduction of the English into the services of the sanctuary. They resisted with considerable feeling the attempt to establish an English Lutheran church in the city of Philadelphia, and were exceedingly bitter towards those who differed from them in sentiment, as to the propriety of the measure. In 1791 a pamphlet was issued, written at the request of the vestry, and signed by the pastors of the German churches, addressed to the Lutherans of Philadelphia, on the signal evidences of the divine goodness and mercy to them, urging them to acknowledge and evince their gratitude, by upholding their German religious institutions and language. In a Liturgy published in 1786, the following petition is to be found: "That the Germans of our land might never dishonor their nation, or disown their ancestry, and that the German churches and German schools might be sustained and perpetuated here."[^bEO] 

They were sincere in the course they pursued, but mistaken. The policy was almost suicidal to our church, and we shall never recover the ground we have lost in our large cities. If provision had been made for preaching the gospel in English to those, who did not understand the German, the Lutheran church in Philadelphia and New York might, at the present time, be as numerous as any other. The action of these excellent men, upon this subject, is an illustration of the strength of prejudice, and shows how far individuals may be carried, when their passions are excited, and their interests and sympathies enlisted in a particular direction. Instead of subjecting them to our censure, we ought, perhaps, to make some allowance for their conduct, and to feel that under similar circumstances, we might have been tempted to occupy the same position. 

## Personal Difficulties

The latter days of Dr. Helmuth were clouded by domestic troubles. His connection with some pecuniary transactions, involved him in difficulty, and seemed for a time to implicate the integrity of his character. But those who knew him best were satisfied of his innocence; they felt that he had been wronged. He had trusted too implicitly to those, whom he thought worthy of his confidence, and believed their representations. His whole life was a refutation of the charge. 

Integer vitae scelerisque purus \
Non eget Mauris jaculis nec arcu.

He was a good man, and could not designedly have countenanced that which was improper, or sanctioned, in the most remote way, even the appearance of evil. We cannot more appropriately conclude our sketch of this venerable man, than by presenting a passage from the sermon preached on the occasion of his death, by his successor in office, when calling upon the congregation to keep in affectionate recollection their former pastor, who for more than forty years had dispensed unto them the word of life: 

>Every heart must acknowledge that a grateful remembrance is his due. What teacher, that spends a single year among a people, and is faithful with the talent entrusted to him by God, will not gain many a heart to himself? But he has lived so long among you, has labored among you so long, that he might have said, I have labored more abundantly than they all. 

>How immense, therefore, is the debt of gratitude you owe to your teacher! how large is the number of those who, by his instrumentality, have been enlightened and brought to the truth, who have been renovated and gained to virtue, who have been comforted by him, and through his instrumentality have obtained peace with God, through the word of reconciliation! how great the number of those, who have to acknowledge after his departure, that they owe him much, yea all, inasmuch as the hour for the kingdom of heaven struck under his instruction! Many a soul will he already have met in the realms of bliss, to whom he was here the instrument, in the hands of the Lord, to obtain that happiness, but surely there are many here, here in this edifice, who are the epistle of Christ ministered by him, written not with ink, but with the spirit of the living God. 

>Come then, render to him the sacrifice of your love, pay him the last honor, by preserving for him a grateful remembrance in your hearts! And especially ye, whose love is wont to endure, whom he received when infants, instructed when children, whom he dedicated to God at his altar, and whose covenant of matrimonial fidelity and love he blessed, ye his small congregation, within the circle of the more extensive one, ye who have so frequently celebrated his birth-day with pious congratulations, celebrate now, as often, the day of his departure with pious gratitude!" 

[^bEJ]:  Casket of Precious Treasures. This book bas been translated into English, and published by the American Tract Society. Its title-page reads thus: A Golden Treasury for the Children of God, whose Treasure is in Heaven: consisting of Devotional and Practical Observations on select passages of Scripture, for every day in the year: By C. H. V. Bogatzky. [Note by Krauth. "Evangelical Review"] 

[^bEK]:  Rev. Dr Lochman, at the time, pastor of the Lutheran congregation in Lebanon, Pa. 

[^bEL]:  Now place the matter before your mind, consider, but literally, now come forward. 

[^bEM]:  _Hall. Nach_., p. 1351. 
[^bEN]:  _Hall. Nach_., p. 1344. 

[^bEO]:  Vide Kirchen. ende der Evansrelisch-Lutherischen Vereinisrten Gomeinden in Nord America. Philadelphia, gedruckt bei Melchior Srciner, in der Reesstrasse, 1786. 

# Publication Information

- _Author_: __"Krauth, Charles P."__, Editor.
- _Journal_: __"The Evangelical Review."__
- _Originally Published_: 1854.
- _Lutheran Library Edition_: 2019
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

{{% alert note %}}
{{% biographical-sketch-note %}}

[Download a printable PDF](/pdf/henry-helmuth.pdf)
{{% /alert %}}