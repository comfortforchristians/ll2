---
date: 2019-05-17 03:00:00-04:00
publishDate: 2019-05-17 03:00:00-04:00
title: "The Law of Apostasy in Islam by Samuel Zwemer"
slug: "438-zwemer-the-law-of-apostasy-in-islam"
categories: ["Lutheran Library Publications"]
tags: ["Zwemer"]
authors: ["Zwemer, Samuel Marinus"]
titles: ["The Law of Apostasy in Islam"]
origpublishers: ["Marshall Brothers, Ltd."]
origdates: ["1925"]
synods: ["Presbyterian", "Reformed"]
---
"The idea of personal liberty – freedom of conscience – has no place in Moslem law, whether religious or civil. . . The law of apostasy is known to all Moslems from their youth up, if not in its detail of legal penalties, yet in its power of producing an attitude bitterly hostile toward converts to Christianity. What else could such a law produce except a fanatic attitude toward all who are not Moslems? The more Muslim a country or a community, the more does it despise the Christian.

"The catalogue of tortures endured because of faith in God, given in the eleventh chapter of the epistle to the Hebrews, could be paralleled in the lives of those who have suffered for Christ because they were apostates from Islam. Every one who makes the choice faces the possibilities of loneliness, disinheritance, persecution and even death. It is the same story in Arabia, Turkey, Afghanistan, Persia, Algiers, India – no mercy for the Apostate and no equality or liberty for Christian minorities.

{{% toc %}}

# Book Contents
- List of Illustrations
- Preface.
- 1. Why So Few Moslem Converts?
- 2. The Law Of Apostasy.
- 3. How It Works.
- 4. Centuries Of Intolerance And Persecution.
- 5. Hidden Disciples.
- 6. The Dawn Of A New Era
- Bibliography

# Our Common Brotherhood in Christ

"As we look back upon these centuries of persecution of our fellow Christians the Nestorians, the Armenians, the Greeks and the Copts we realize the truth of our unity in Christ, and come to a similar conclusion as that reached by Adrian Fortescue, the Roman Catholic historian: 

"In a land ruled by Moslems there is at bottom an essential solidarity between all Christians. These other Christians too are children of God, baptized as we are. Their venerable hierarchies descend unbroken from the old Eastern Fathers, who are our Fathers too. When they stand at their liturgies they adore the same sacred Presence which sanctifies our altars, in their Communions they receive the Gift that we receive. And at least for one thing we must envy them, for the glory of that martyr's crown they have worn for over a thousand years. We can never forget that. 

During all those dark centuries there was not a Copt nor a Jacobite, not a Nestorian nor an Armenian, who could not have bought relief, ease, comfort, by denying Christ and turning Turk. I can think of nothing else like it in the world. These poor forgotten rayahs in their pathetic schisms for thirteen hundred years of often ghastly persecution kept their loyalty to Christ. [^brM]

[^brM]: _The Lesser Eastern Churches_, by Adrian Fortescue (London, 1913). 
# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/438-zwemer-the-law-of-apostasy-in-islam-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/438-zwemer-the-law-of-apostasy-in-islam.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 438-zwemer-the-law-of-apostasy-in-islam&body=Please send 438-zwemer-the-law-of-apostasy-in-islam.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 438-zwemer-the-law-of-apostasy-in-islam&body=Please send 438-zwemer-the-law-of-apostasy-in-islam.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}

# Letter from an Egyptian Convert

"The following letter, written by a Moslem convert in Cairo, shows better than any argument could do the conditions that obtained in Egypt in 1878. It was written on January 21st of that year. The writer afterwards escaped from Egypt, received a medical education in Scotland, and has had a remarkable career as a medical missionary in China: 

"To Her Highness, The Maharanee Dulup Singh. 

"As your Highness is a convert of the American Mission School in Cairo, and as you have much interest in all who love the Lord Jesus Christ in this city and in this land, I wish to take the liberty of telling you of my persecutions since I became a Christian five months ago. I am an Egyptian, and was a pupil in the American School five years, and also a teacher the last two years. My father is a strict Muslim, but when I was teaching and reading the Bible I found that the Muslim religion is not the true one. I searched many months for the true religion of God, and read the Bible very much, and some other books; and when I found that Christianity is the true faith, I rejected my father's religion. 

"Fearing that my father and relations would murder me, I intended to fly away from their faces; but when I consulted Dr. Lansing and Dr. Watson, the two missionaries in Cairo, they persuaded me that Cairo would be safer for me than any other place. So it was arranged that I should come to Dr. Lansing's house for protection. I sent letters to my father and brothers about the reason for my leaving home and embracing Christianity. I wished very much to show my love to Christ and to profess His name, and so I was soon baptized in the Mission Chapel by my name Ahmed. 

"My brothers and friends and sheikhs and learned men came often to see me and made much controversy with me, but by the help of God I was always victorious, which made them very angry. For fear of them, I never went out excepting to teach in the school, which is only a few steps from Dr. Lansing's house, and in a very public place. They had spies watching me for several days, and after five weeks, on coming home one afternoon, I was surrounded by ten persons, three of them being my brothers. They caught me, and putting their hands on my mouth and eyes, thrust me in a closed carriage in a very violent manner. 

"There was a café very near, and when some men saw this they came forward to stop the horses from going and to help me; but my uncle, who was standing near, called out Let them alone; this is by the order of the Government. They took me to my father's house, assuring me that if I did not tell him that I was a Muslim when he asked me, he would kill me. I did tell him, however, that I was a Christian, and he brought the most learned philosopher in Cairo and a very learned man, and with many others present they talked with me very hotly eight hours, until I was sick and vomited. 

"After three days of continued controversy, seeing that I would not yield, they then threatened me with immediate death according to their law, and in such a way I was certain it would be done. Now the great trial had come, and I began to feel a little weak. They wrote a paper saying that I had returned home of my own will and also to Islam, and forced me to put my name to it. They next took me to the police house and compelled me to write with my own hand to the same effect. After this they took me to the English Consulate, where I was again forced to say the same thing, as my brothers were secretly armed to kill me or any one who would defend me if I did not do so. Although after all this had been done they knew I was still a Christian at heart, it was proclaimed that I had returned to Islam, and they made a great feast to deceive and to take away the disgrace of the family. The controversy still continued, and after a month, when I wished to have my freedom and go to teach in the school they refused, I showed them even more strongly that I am still a Christian, and insisted upon my rights. But knowing the danger that I was now in, the Lord helped me to escape out of their hands; when I again sought refuge at Dr. Lansing's house, to whom I am certainly indebted for his kindness because of his giving me to eat and treatment as his own beloved son. 

"Now I wish to tell your Highness that I am again a prisoner, unable to go out at all or even to step on the balcony; because they are so excited and watching me night and day, desiring to quench their thirst with my blood, the blood of the helpless young Christian. My brothers, according to their law, often assured me that if they murdered me they would be martyrs for doing so. I thank God who delivered me out of the hands of my Government, which I fully believe is watching me and allowing my relatives to do whatever they please and wish, so that I may be destroyed. Oh, would that God would bring freedom and justice here very soon. How dreadful is such injustice and oppression. How freedomless is this miserable country. How many persecutions for embracing God's true religion I have suffered I cannot tell, and how many troubles I have endured. As I have no freedom and no prospect of liberty or safety, may I ask your Highness to have compassion on me; and, for the sake of Christ and of Justice, to help me and deliver me out of the hands of such wicked and barbarous people. 

"I hope your Highness will excuse me for troubling you so much; but you will see that I am in great distress and need help. I know that you love Christ very much, and also all the people who suffer for His sake. As you are a friend of Her Majesty, the Good Queen of England, would you do me the great favor to beseech her to use her exalted power to help me, as I believe nothing else will avail. I wish her to know, also, that I not only ask her help for myself, but for many others who wish to embrace Christianity, but cannot for fear of persecution and death. I am very anxious to study the Holy Bible in the theological school, that I may, with the help of God, preach to the ignorant people in this land. I do not wish the Government to hear of this letter of your servant, lest it should tear me into pieces. I wish your Highness to pray for me that I may be strong and endure much, and all this help I ask for the sake of the Lord Jesus, for Whose name I have suffered much. 

"I am your Highness most obedient and most humble servant, etc., 

<p class="author">"A. F.</p> 

"P.S. Since writing the above this morning I have received a secret visit from a true friend of my family, whom I can trust, begging me not to leave this house, assuring me that my life will not be spared. My father has given orders to my brothers and all to kill me if they meet me and they are watching me constantly. You thus see my perilous state. May God help me, and shield me from the power of my many enemies. "A. F. 

"Sent Jan. 21st, 1878." 

# Publication Information

- _Lutheran Library edition first published_: 2019-05-17
- _Version 4 update_: 2019-05-17
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
