---
date: 2019-04-22 09:27:13-04:00
publishDate: 2019-04-22 09:27:13-04:00
title: The Confessional Principle by Theodore E. Schmauk
slug: "104tc-schmauk-confessional-principle"
categories: ["Lutheran Library Publications"]
tags: ["Schmauk", "Confessions", "Book of Concord", "Start Here"]
authors: ["Schmauk, Theodore Emanuel", "Benze, C. Theodore"]
titles: ["The Confessional Principle and The Confessions of The Lutheran Church As Embodying The Evangelical Confession of The Christian Church"]
origpublishers: ["General Council Publication Board"]
origdates: ["1911"]
synods: ["General Council"]

---

This is Dr. Schmauk's magnum opus on Christian Confessionalism, a treasure of approachable, Biblically Conservative scholarship.   

# "No Creed but Christ"

>If you simply content yourself with the assertion, "The Bible is my creed," you are leaving unanswered many of the most important and vital questions of faith and life. When you refuse to take a definite stand on vital issues in the Christian Faith, but say, "The Bible is my creed," are you really confessing Christ?'

{{% toc %}}

# "Ten Thousand Questions"

>The Bible raises ten thousand questions. If you answer any one of them in your own way only, and without looking farther, and say, "This is what I believe," you are setting up a personal creed of your own. If you simply content yourself with the assertion, "The Bible is my creed," you are leaving unanswered many of the most important and vital questions of faith and life. And a Church's answer, more than your own, must be ample to meet all questions. When you refuse to take a definite stand on vital issues in the Christian Faith, but say, "The Bible is my creed," are you really confessing Christ, or are you taking the problems of religious life easy, and evading the unpleasant but important doctrines which the Spirit of God has brought to an issue in the development of the Faith and His Church in history? – Theodore Schmauk. *The Confessional Principle* 


# Section I. Historical Introduction

# Section II. The Christian Confessional Principle

1. The Question of a Confessional Foundation: What is the Question?

2. How Is The Question To Be Discussed?

3. What Are Confessions? Definitions.

4. Does The Church Need Confessions?

5. Do Confessions Constrict, Or Do They Conserve?

6. Should Confessions Condemn and exclude?

7. What Gives The Confession Validity?

8. Do Confessions Bind?

# Section III. Its History and Development in Christianity

9. The Rise of the Confessional Principle in the Church

10. The Development of The Confessional Principle in The Church

11. The Confessional Principle In The Augsburg Confession

12. The History and Tendency of The Confessional Principle in The Church

13. The Confessional Use of The Word “Symbol”

# Section IV. The Lutheran Confessional Principle. 

14. The Lutheran Confession

15. The Origin of the Augsburg Confession. Kolde’s Introduction

16. Melanchthon’s Unsuccessful Attempts as a Diplomatist. Kolde’s Essay

17. Kolde on the First Known Draft, or Oldest Redaction of the Augsburg Confession, and its Discovery

18. The Oldest Redaction of The Augsburg Confession

19. The Hand of God in the Formation of the Augsburg Confession, as shown by the Course of Events in 1529 and 1530, and in the Letters of Luther, and of Melanchthon

20. The Augsburg Confession Remained Unaltered

21. The Augsburg Confession: The Further History of its Editions and Manuscripts. Kolde’s Essay, With A Summary of the Argument as it Bears on the Confessional Question, by T. E. Schmauk

22. Protestantism Under The Augsburg Confession To The Death of Luther

23. Protestantism From The Death of Luther To The Death of Melanchthon and to the Disintegration of Lutheranism

24. Melanchthon and The Melanchthonian Principle.

25. The Need of A Concordia Realized, and its Origin Attempted

26. The Formula of Concord: its Origin Based on Kolde’s Introduction and on the Formula in Hauck

27. The Introduction of The Concordia, and The Augustana Preserved

28. Is The Formula of Concord A Confession?

29. The Answer of a Providential Origin to the Question - Is the Formula a Confession?

30. The Answer To The Criticism Made On The Motives and Men, as Touching The Question, Is The Formula A Confession?

31. The Answer of The Formula’s Outer Form to The Question, Is The Formula A Confession?

32. The Answer of The Formula’s Subject Matter, Touching The Question, Is The Formula of Concord a Confession?

33. The Person of Christ and The Formula of Concord

34. Concordia Is The Church’s Great Confession of Christ.

35. What The Formula of Concord Accomplished As A Confession of The Lutheran Church.

36. The Book of Concord. The Facts of its Origin and Publication. Kolde’s Essay.

# Section V. In America.

37. From the Book of Concord to the Present Day

38. The Book of Concord and Historical Lutheranism In America.

39. The Confessional Principle of The Book of Concord and American Protestantism

40. The Confessional Principle of The Book of Concord and Christian Cooperation

41. The Confessional Principle of The Book of Concord and The Brotherhood of The Christian Church

42. The Confessional Principle of The Book of Concord and the Future of the Church In America.

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


<a name="download"></a><br />

![book cover](/img/posts/104tc-schmauk-confessional-principle-1024.jpg)

[PDF](/pdf/104tc-schmauk-confessional-principle.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 104tc&body=Please send 104tc-schmauk-confessional-principle.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 104tc&body=Please send 104tc-schmauk-confessional-principle.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2017-09-04
- _Version 4 update_: 2019-04-20
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)


