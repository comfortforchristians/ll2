---
date: 2019-07-08 05:38:15-04:00
publishDate: 2019-07-08 05:38:15-04:00
title: "The Nature of Fundamental Doctrines by Charles F. Schaeffer"
slug: "s33-schaeffer-nature-of-fundamental-doctrines"
categories: ["Lutheran Library Publications"]
tags: ["Schaeffer", "Confessions", "Short Books", "Evangelical Review"]
authors: ["Schaeffer, Charles Frederick"]
titles: ["The Nature of Fundamental Doctrines"]
origpublishers: ["Evangelical Review"]
origdates: ["1915"]
synods: ["General Synod"]
---

"What are 'fundamental doctrines,' or 'fundamental Articles of Faith'?… Every intelligent Christian feels competent to state the general basis of his belief, or the doctrinal foundation of his Christian character and life… When he, however, proceeds to _specify in detail_ the doctrines which essentially constitute that 'foundation,' he will no longer be surprised by the embarrassment that even distinguished divines, on attempting to furnish an answer, have candidly confessed." – Charles F. Schaeffer.

# Book Contents

- An Inquiry Into The Nature Of Fundamental Doctrines.
- Religious Terms Not Defined or Used Correctly
- Can’t We "Agree to Differ"?
- Each Religious Opinion Is Assumed To Be "Fundamental"
- What Are "Fundamental Doctrines"?
- Do "Fundamentals" Concern Differences Between "Protestants" Only?
- Elementary Doctrines As Defined In The Bible
- "Foundation" in the New Testament
- Christ as our "All"
- The Builder is "Every Professing Christian"
- "Every Man’s Work"
- All Details and Ramifications of Any Scriptural Doctrine Are Also Strictly Fundamental.
- Church Government
- Rigid Calvinistic Views
- Inspiration of Scripture
- Non Fundamentals

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/s33-schaeffer-nature-of-fundamental-doctrines-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/s33-schaeffer-nature-of-fundamental-doctrines.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request s33-schaeffer-nature-of-fundamental-doctrines&body=Please send s33-schaeffer-nature-of-fundamental-doctrines.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request s33-schaeffer-nature-of-fundamental-doctrines&body=Please send s33-schaeffer-nature-of-fundamental-doctrines.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 
- _Version 4 update_:  
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
