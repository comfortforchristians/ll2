---
date: 2018-12-17 10:22:00
title: "New Books | End of the Year Round Up | Upcoming in 2019"
slug: "updates-announcements"
categories: ["Lutheran Library Newsletters"]
---

New Books, what's coming in 2019, and with best wishes to you for a very happy and healthy Christmas and New Year's from the Lutheran Library.

{{% toc %}}

# Website

Early in 2019 you can expect a completely redesigned site.

Until then, you should notice faster loading times for the website.  The cover images have been reduced in size, and other changes have been made to make it easier to browse whether you are using a computer or some other device.  


If you are on a cellphone or in an otherwise low-bandwidth situation, you can always access the [text-only authors page](/authors/) or the [text-only catalog](/titles/).  Both are listed at the top of the menu on the upper left side of the page.  These may be handy if you are out and about and want to recommend a book to a friend.

# Six Dozen Books!

There are now 72 Lutheran Library titles available.  These cover the range of Christian books including daily and weekly devotionals, sermons, Q and A and catechismal materials, biographies, etc.  Both "milk" and "meat".  But even the theological titles have been carefully chosen to be both orthodox _and_ readable. 

# Format Updates

All ebooks have been updated with new formatting.  Feel free to re-download your PDF files or request updated versions of EPUB or Kindle MOBI files.  

_What's changed?_  The embedded images in the books will display better on e-readers.  There have also been improvements made to the line spacing and typefaces.  

# Novels and Christian Fiction

While [books like The Shack](https://www.alecsatin.com/the-shack-william-paul-young-and-aleister-crowley-video/) have given "Christian Fiction" a bad name, there are well-written novels in the genre which glorify _the One True God_ and can encourage faith.  An author of such books who deserves to be better known is the Rev. Joseph Hocking.

Joseph Hocking was a faithful Nonconformist minister in Wales who had nearly 100 books published in his lifetime.  Over the next year, the Lutheran Library plans to release many of these in ebook editions.

- [Joseph Hocking](/authors/hocking-joseph/)

# Church Schools and Seminaries

All of the books put out by the Lutheran Library may be freely shared and copied.  If you know of or are affiliated with a Church School or Seminary, consider using LutheranLibrary.org materials which may be translated into other languages and/or printed without incurring extra charges or dues for rights.

May the Lord bless the next generation with faithful teachers and good material.

# Upcoming in 2019

![Upcoming Lutheran Library Covers](/img/posts/2018-12-17-updates-announcements.jpg)

Some of the books to be released in the next year include the following:

- _The Life of Charles Porterfield Krauth_, by A. Spaeth
- _Life and Times of Henry Melchior Muhlenberg_ by W. Mann
- _The Planting of the Lutheran Church in America_ (also known as _The Halle Reports_)
- _Morning Exercises_ by Rev. William Jay
- _The Child's Book_ by Emanuel Greenwald
- _Weapons of Mystery_ by Joseph Hocking

And more!

# A Personal Note

My heart breaks for the younger generations, who in many cases don’t even realize they are starving for truth, _or even believe that it exists or can be known_.

A major hope for starting the Lutheran Library has been to find and make available Biblically sound, readable books from a time when the American Lutheran Church was in a better place (the late 1800’s to early 1900’s).

Please pray that these books may encourage some to understand and believe the gospel, and more than that, to know why the Lutheran Confessions are an accurate and trustworthy explanation of the teaching of Scripture.

In searching out these books, it has been quite a revelation to me to discover that a large part of the conservative Lutheran tradition was apart from the Synodical Conference. __Simon Peter Long, Theodore Schmauk, Emanuel Greenwald, Henry Eyster Jacobs, Charles Krauth, and Matthias Loy__ are all my friends and brothers now through their writings. Nothing would make me happier than for them to be as well-known and appreciated as they were not so many years ago.

My own history is a story for another time. In brief, like __Lehmanovsky, Lichtenstein, and Einspruch__ (titles by each available in Lutheran Library), I am a "branch grafted back in". By God’s grace I heard and believed the Gospel as a teenager. After college I attended seminary for a time, but family difficulties made it impossible for me to continue. Eventually I fell into so-called “spiritual but not religious” (really pagan) practices for about 20 years. One day I saw my Bible on the shelf, took it down and by God’s grace remembered my faith. Now a much humbler man, I depend only on His goodness through Jesus Christ as the sole means by which I have any hope for eternal life. 

To God be the glory, great things He hath done.  _Amen_

Merry Christmas from the Lutheran Library.

---

The Lutheran Library

The goal of the Lutheran Library is to re-release well-written and readable books from sound, faithful American Lutherans of the past for the enjoyment and edification of a new generation.  All books are available at [lutheranlibrary.org](/) for free download in a variety of formats for Kindle, Apple, and other devices.

Your help is appreciated in spreading the word as often and in as many ways as you feel is appropriate.

May God bless you and keep you, help you, defend you, and lead you to know the depth of His love.  *Amen*