---
date: 2019-07-29 05:27:04-04:00 
publishDate: 2019-07-29 05:27:04-04:00 
title: "Catechization by John Morris"
slug: "s12-morris-catechization"
categories: ["Lutheran Library Publications"]
tags: ["Morris", "Catechism", "Evangelical Review", "Short Books"]
authors: ["Morris, John Gottlieb"]
titles: ["Catechization"]
origpublishers: ["Evangelical Review"]
origdates: ["1850"]
synods: ["General Synod", "Maryland Synod"]
---

"In the primitive church there was a private and public catechization. The private was practiced by parents according to Eph. 6:4… The public was held in schools, churches, and other places, and the pupils were called catechumens, from κατγχουμενοι, learners, the word that is used in the New Testament passages before quoted.

"In the course of ages, as the church became more corrupt, the practice fell into disuse, or sadly degenerated. Yet the councils recommended and enjoined it, but the duty, where attended to all, was wretchedly performed.

"The Reformation revived the ancient system of catechizing, and if it had done nothing more, it would still be a glorious event. The Reformers immediately prepared catechisms for the young and ignorant, and all the preachers regarded this as one of the most important functions of their office. So it should be considered in every age. 

# Book Contents

- What We Mean By “Catechizing”
- Catechetical Instruction Practiced In Every Church Age
- Christ’s Use Of Catechism
- The Apostles’ Use of Catechism
- Cave and Grotius
- Church Fathers
- The Reformation and Catechization
- In The Roman Catholic Church
- Those Who Speak Lightly of This Pastoral Duty
- Slovenly Catechistics
- Benefits
- Need to Understand The Rules of Teaching
- Frequency of Catechetical Classes

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/s12-morris-catechization-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/s12-morris-catechization.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request s12-morris-catechization&body=Please send s12-morris-catechization.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request s12-morris-catechization&body=Please send s12-morris-catechization.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2019-07-18
- _Version 4 update_:  2019-07-18
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
