---
date: 2019-06-27 05:03:08-04:00
publishDate: 2019-06-27 05:03:08-04:00
title: "The Lutheran Confessions: A Brief Introduction by Henry Eyster Jacobs"
slug: "s04-jacobs-the-lutheran-confessions-a-brief-introduction"
categories: ["Lutheran Library Publications"]
tags: ["Jacobs", "Confessions", "Short Books"]
authors: ["Jacobs, Henry Eyster"]
titles: ["The Lutheran Confessions A Brief Introduction"]
origpublishers: ["Evangelical Review"]
origdates: ["1881"]
synods: ["General Synod", "General Council"]
---

"There are points in the Church’s history, years, months, days, in which all the evil that has ever assailed the Church, seems brought to a focus, and to overcome it, the Holy Ghost, who never deserts his charge, concentrates against it not only the sum of all the experience of the Church of the past, but also the endowments of new, fuller, richer unfoldings of the sense and power of God’s Word.

"The Book of Concord of 1580, beginning with the Apostles’ Creed and ending with the Formula of Concord, is entitled to this respect as a memorial of a number of such most important epochs; so that the history of the Book of Concord may be considered in a certain sense as almost epitomizing the entire history of the Church. – Henry Eyster Jacobs

{{% toc %}}

# When Your Experience Corresponds

"… the real interest which is felt in the Confessions must be in proportion to the degree in which the experience portrayed in the Confessions corresponds with our own. As we read in them the doubts and difficulties that have had an existence in our own inner life, and recognize the solutions there presented, as those also which God’s Spirit in his Word has given us, the Confessions become as dear to us as our own Christian experience, and we can no more disown them, or fail to acknowledge and defend them, than we can deny our Christian life, and all upon which that life depends. – Henry Eyster Jacobs


# Book Contents

- A Brief Introduction to the Lutheran Confessions.
- Jubilees Of The Confessions.
- Relation Of Christian Life Of The Present To That Of The Past.
- Crises In Christian Life, The Origin Of The Confessions.
- The Truths Of The Confessions, Living Truth.
- The Subject Matter Of The Confessions.
- The Ecumenical Creeds.
- The Augsburg Confession.
- Shall There Be Any Confessions Beyond The Augustana.
- The Apology Of The Augsburg Confession.
- The Smalcald Articles.
- The Catechisms.
- The Formula Of Concord.
- The Chief Ground of Attack on The Formula.
- The Book Of Concord.
- The Principle Of Confessional Development.

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/s04-jacobs-the-lutheran-confessions-a-brief-introduction-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/s04-jacobs-the-lutheran-confessions-a-brief-introduction.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request s04-jacobs-the-lutheran-confessions-a-brief-introduction&body=Please send s04-jacobs-the-lutheran-confessions-a-brief-introduction.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request s04-jacobs-the-lutheran-confessions-a-brief-introduction&body=Please send s04-jacobs-the-lutheran-confessions-a-brief-introduction.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2019-06-27
- _Version 4 update_: 2019-06-27 
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
