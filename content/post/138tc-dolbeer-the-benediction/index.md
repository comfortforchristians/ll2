---
date: 2017-10-29 12:00:00
title: The Benediction - by William H. Dolbeer
slug: "138tc-dolbeer-the-benediction"
categories: ["Lutheran Library Publications"]
tags: ["Dolbeer", "Liturgy", "Pastoral"]
authors: ["Dolbeer, William", "Bauslin, David Henry"]
titles: ["The Benediction"]
origpublishers: ["Lutheran Publication Society"]
origdates: ["1907"]
synods: ["General Synod"]

---
"When the minister comes from the altar, he must not forget that he now bears a message from the Lord to the waiting congregation. It is not a prayer; it is not man's word; it is in no sense a subordinate act. It is the Lord's own explicit word, and man is the entrusted messenger."

{{% toc %}}

# The Meaning and Force of the Benediction

>The meaning and force of the benediction has been generally overlooked; and as a consequence this important ministerial duty has frequently been indifferently and carelessly performed. When the minister comes from the altar, he must not forget that he now bears a message from the Lord to the waiting congregation. It is not a prayer; it is not man's word; it is in no sense a subordinate act. It is the Lord's own explicit word, and man is the entrusted messenger. It is the end sought in worship, namely, the Lord's salvation and blessing. – From Chapter 5

 
# Contents

- Introduction
- Chapter 1 – Definition
- Chapter 2 – Patriarchal Blessings
- Chapter 3 – The Aaronic Benediction
- Chapter 4 – The Significance Given To The Act Of Blessing By The Lord’s Words
- Chapter 5 – The Epistolary Salutations
- Chapter 6 – The Pauline Or Apostolic Benediction
- Chapter 7 – The Aaronic And Apostolic Benedictions Compared
- Chapter 8 – The Apostolic Benediction And Epistolary Salutations Compared
- Chapter 9 – The Official Character Of The Act Under The Gospel
- Chapter 10 – Importance Of Act
- Chapter 11 – Manner Of Pronouncing And Receiving The Benediction
- Chapter 12 – Where And When Proper To Make Use Of Benedictions
- Chapter 13 – Mistakes Corrected

# About William H. Dolbeer

William H Dolbeer was born at Enon, Ohio in 1854. He graduated from Wittenberg College in 1876, and from Hamma Divinity School in 1878. Dolbeer was ordained to the Lutheran Ministry with first parish Leipsic, Ohio. Here he met Miss Eliza Euxine Walters, who was teaching in the Leipsic school. They were married in April of 1879 and had seven children.  He died July 26, 1920 in Belleville, Pennsylvania.[^afH]

# God's Paternal Care

"This thought of God's paternal care appears repeatedly in the Old Testament. It is given fullest expression in His declaration of love in such passages as Jer. 31:3: "Yea, I have loved thee with an everlasting love: therefore with lovingkindness have I drawn thee." And in Isa. 49:15, where He compares His love for His people to that of mothers for their children: "Yea, they may forget, yet will not I forget thee." These and other passages show that this paternal attitude of God towards His people is not dependent upon their attitude towards Him. – From Chapter 3

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download) 

![book cover](/img/posts/138tc-dolbeer-the-benediction-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/138tc-dolbeer-the-benediction.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 138tc&body=Please send 138tc-dolbeer-the-benediction.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 138tc&body=Please send 138tc-dolbeer-the-benediction.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}
[^afH]: Find a grave dot com. "William H Dolbeer". Retrieved 2017-10-20.