---
date: 2019-06-14 05:00:34-04:00
publishDate: 2019-06-14 05:00:34-04:00
title: "Luther Examined and Reexamined by William Dau"
slug: "185tc-dau-luther-examined-and-reexamined"
categories: ["Lutheran Library Publications"]
tags: ["Luther", "Dau", "Start Here"]
authors: ["Dau, William H. T."]
titles: ["Luther Examined and Reexamined"]
origpublishers: ["Concordia Publishing House"]
origdates: ["1917"]
synods: ["Missouri Synod"]

---

"Attacks on Luther are demanded for Roman Catholics by the law of self-preservation... Rome has never acknowledged her errors nor admitted her moral defeat. The lessons of past history are wasted upon her.

"A recent Catholic writer correctly says: 'There is no doubt that the religious problem today is still the Luther problem.' 'Almost every statement of those religious doctrines which are opposed to (Roman) Catholic moral teaching find their authorization in the theology of Martin Luther.' – William Dau


# Contents


- Preface.
- 1. Luther Worship.
- 2. Luther Hatred.
- 3. Luther Blemishes.
- 4. Luther’s Task.
- 5. The Popes in Luther’s Time.
- 6. Luther’s Birth and Parentage.
- 7. Luther’s Great Mistake.
- 8. Luther’s Failure as a Monk.
- 9. Professor Luther, D. D.
- 10. Luther’s “Discovery” of the Bible.
- 11. Rome and the Bible.
- 12. Luther’s Visit at Rome.
- 13. Pastor Luther.
- 14. The Case of Luther’s Friend Myconius.
- 15. Luther’s Faith without Works.
- 16. The Fatalist Luther.
- 17. Luther a Teacher of Lawlessness.
- 18. Luther, Repudiates the Ten Commandments?
- 19. Luther’s Invisible Church.
- 20. Luther on the God-Given Supremacy of the Pope.
- 21. Luther the Translator of the Bible.
- 22. Luther a Preacher of Violence against the Hierarchy.
- 23. Luther, Anarchist and Despot All in One.
- 24. Luther the Destroyer of Liberty of Conscience.
- 25. “The Adam and Eve of the New Gospel of Concubinage.”
- 25. “The Adam and Eve of the New Gospel of Concubinage.”
- 26. Luther an Advocate of Polygamy.
- 27. Luther Announces His Death.
- 28. Luther’s View of His Slanderers.
- Afterword

# About the Author – William Herman Theodore Dau

"W. H. T. Dau was born February 8, 1864 in Pomerania.  He emigrated to America in 1881, and graduated Concordia Seminary in St. Louis in 1886.  He first served as pastor in Memphis, TN from 1886-1892, then was president of Concordia College in Conover, NC from 1892-1899.  From 1899-1905 he served as pastor in Hammond, Indiana, then was professor at Concordia Seminary, St. Louis, from 1905-1926.  From 1926-1929 he was president of Valparaiso University in Indiana.

From "Dau, William Herman Theodore" retrieved 2018-03-16 from cyclopedia.lcms.org

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)

![book cover](/img/posts/185tc-dau-luther-examined-and-reexamined-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/185tc-dau-luther-examined-and-reexamined.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 185tc&body=Please send 185tc-dau-luther-examined-and-reexamined.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 185tc&body=Please send 185tc-dau-luther-examined-and-reexamined.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}



# Publication Information

- _Lutheran Library edition first published_: 2018-03-16
- _Version 4 update_: 2019-06-14
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
