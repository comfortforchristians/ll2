---
publishDate: 2019-02-06 01:35:27-05:00
title: "Charles William Schaeffer: A Biographical Sketch"
slug: "charles-william-schaeffer"
categories: ["Biographical Sketches"]
tags: ["Schaeffer", "Lutheran Ministers"]
authors: ["Schaeffer, Charles William"]
synods: ["Pennsylvania Synod", "General Council"]
---

[![Charles William Schaeffer](/img/posts/schaeffer-charles-william.jpg)](https://www.lutheranlibrary.org/authors/schaeffer-charles-william/)

Rev. Charles William Schaeffer, D.D., LL.D., the son of the Rev. Solomon Frederick Schaeffer, was born in Hagerstown, Md., May 5, 1813. His father was at the time the pastor of St. John's Church of that city, but when only twenty-four years of age, and the son was about one year old, he fell victim to a fever that was contracted by visiting a camp of soldiers near Hagerstown. His mother was Catharine Eliza Crever. 

After the father's death, the widow, with her infant son, returned to her father's home in Carlisle, Pa., where the subject of this sketch spent the next fourteen years of his life. 

During her residence in Carlisle, Mrs Schaeffer was married to the Rev. Benjamin Keller, the pastor of the church in that city. In 1829 Mr. Keller having received a call from St. Michael's Church, Germantown. Pa., the family left Carlisle for their new place of residence. This move brought the son, Charles, to a place which was not without its special interest to him, for here his father was born, while his grandfather, the Rev. Frederick David Schaeffer, D. D., was pastor of St. Michael's. 

The Germantown Academy, whose origin dates back to the years preceding the Revolution, offered the best educational advantages, and thither the young Charles was sent to complete his preparation for college. He next attended the University of Pennsylvania from which he graduated with honor in 1832. While going to College he made his home with his grandfather, who then was the pastor of Zion's and St. Michael's Church, Philadelphia. 

Having resolved to enter the ministry of the Church, the young man, after his graduation at the University, went to Gettysburg, and entered the Theological Seminary. While pursuing his studies there he also spent a part of his time in discharging the duties of a tutor in the college. 

In 1835 Dr. Schaeffer was licensed to preach by the Pennsylvania Synod, at its meeting in Germantown, and immediately afterwards he took up his residence at Barren Hill, Montgomery Co., Pa , where he became the pastor of St. Peter's Church, and also of the Union Church of White Marsh, in the same county. These congregations had heretofore been connected with St. Michael's Church, Germantown, but now they formed a separate charge, and Dr. Schaeffer was their first resident pastor. 

In 1840 Rev. Dr. Schaeffer was called to Harrisburg, Pa., where he resided as the pastor of what is now the mother church of all the Lutheran congregations in that city. In 1849 he resigned, and coming to Germantown, assumed charge of St. Michael's Church, which he retained until June, 1875, a period of twenty-six years. Daring the years of his ministry, in connection with St. Michael's, Dr. Schaeffer, having now received the degree of Doctor of Divinity from the University of Pennsylvania, witnessed and took a very active part in many of the most important movements of the church in latter years. 

When the Theological Seminary in Philadelphia was begun, October, 1864, Dr. Schaeffer was a member of the faculty, being Professor Extraordinary, a position he occupied until the endowment of the Burkhalter Professorship, to which he was nominated and elected in the year 1870, and the duties of which he still continues to discharge. Since the death of his uncle the Rev. Charles Frederick Schaeffer, D. D., Dr. Charles William Schaeffer has been the Chairman of the Faculty of the Seminary, and has ever shown a keen interest in its welfare. 

Dr. Schaeffer, whose abilities were recognized a few years ago by Thiel College, which conferred upon him the degree of Doctor of Laws, has contributed to the literature of the Church, as books, translations of prose and verse, and numerous articles in papers and reviews will testify. His first official position in the Church was that of Treasurer of the Pennsylvania Synod, and then in later years he was President of the same Synod and also of the General Council. 

He has published the following: History of the Lutheran Church in Harrisburg, Pa.; The General Synod; Early History of the Lutheran Church in America; Luther's Preaching; The Lord's Supper from Luther; Valedictory; Wittenberg Nightingale; Washington's Birthday; Halle Reports (trans.) — _Indicator_. 
{{% alert note %}}
This biography is taken from Jens Jensson's _American Lutheran Biographies_, published 1890.
{{% /alert %}}

First published online 2019 at [LutheranLibrary.org](.).