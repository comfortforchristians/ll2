---
date: 2019-07-03 05:24:28-04:00
publishDate: 2019-07-03 05:24:28-04:00
title: "[A3] First Table: God The Lawgiver (The Small Catechism)"
categories: ["This Week With The Small Catechism"]
tags: ["Weekly Catechism", "The Ten Commandments", "Golladay", "Catechism", "First Table"]
authors: ["Golladay, Robert Emory"]
titles: ["The Ten Commandments"]
origdates: ["1915"]
---


>"I am the Lord thy God." — Exodus 20:2.

<span class="firstwords">Our Catechism</span> is appropriately called the Layman's Bible, which means that the fundamental truths of God's Word are there made easily accessible for our people. One of the introductory questions of the catechism is: "What is in general the right use and benefit of all these chief parts?" The answer is: "That we may learn to know who we are, and how we stand in the sight of the Lord our God; who God is, and how we may become reconciled and united with Him." With the central thought here presented we now stand face to face.

In this introduction to the commandments, which applies to all the ten as well as to the first, God stands before us as the speaker. He has an epoch-making revelation to give to the children of men.

God! What a thought! In this little word the greatest problem that can ever engage the mind of man or angel is presented. God! In this little word all true philosophy, all true science, is compassed.

Man, of all earthly creatures, is distinguished by this, that he not only beholds and uses the things about him, but wants to know the causes operating back of them and through them. The animals, as their nature dictates, eat and drink, sleep and wake; when the Queen of the East arises to drive the shades of night before her, they return to their lairs or start forth to browse; they eat what they find to suit their appetites; they blink at the silent torch-bearers of the night and the resplendent mistress of the day; they receive the falling rain, endure the cold of winter and the heat of summer: but, so far as we know, there is no reflection on the how or the wherefore of all this. With man it is not so. He contemplates the changing seasons. He gathers the fruits of the earth. He passes his days amid the objects and events of this ever-changing and yet never-changing order. By the very constitution of his being and judging from all that experience and investigation have taught him, he is certain there is something back of all these phenomena of nature. He is sure there is a workman back of all this variety of splendid handiwork, a giver of all these bountiful gifts, who, in wisdom and power, must be in keeping with the results He produces.

Who is He? What is He?

Whatever the particular ideas with respect to Him may be, whatever the local or national name ascribed to Him, all the world has united in calling Him God — the Author, the Final Cause, the Sustainer of all things. Christian people are glad for any true light that philosophy or science may throw on the subject of the person and nature of God, but our source of knowledge is God's Word — His own revelation of Himself.

We are now engaged in studying the problem of God's person and nature with special reference to its relation to the Law, the holy Ten Commandments, therefore we ask your devout, prayerful attention for the study of this all-important subject, 

## I. God The Lawgiver

Let us consider, first of all, the Nature and Attributes of God. What does God say about Himself in His Word? What do we find in that other volume of His revelation, the book of nature, to illustrate or corroborate this?

Is it worthwhile spending any time on this subject? There are some who say God is unknown because unknowable. There are comparatively few atheists in the world, men who positively deny the existence of God; but there are a good many agnostics, those who deny that we can know anything definite about God: whether He is or is not, what He is or what He is not.

We readily grant that there is a sense in which God is not known, and cannot be known to us. We can never with these limited, sin-weakened minds of ours grasp all that God is or has done or can do. There are things of which men can say, I have analyzed this; I have resolved it into its parts, I know its constituent elements, the principles of its construction, the laws of its operation. In this sense we can never speak of knowing God. That would be to make God as small as the compass of our circumscribed human minds.

Let me give you a little illustration of the impossibility of the human mind fully to comprehend God. Most of us feel that this earth is a rather large place, and, in a way, it is. In reality, however, and by comparison, it is only a little speck of dust in this great universe. If this earth were snuffed out of existence, it would not mean nearly so much, so far as the material universe is concerned, as if one of the many, many millions of stars which twinkle in the heavens on a starry night were removed. We think this earth, with its twenty-five thousand miles circumference, rather large. And when they speak of the sun being more than ninety million miles distant it begins to stagger us. But they tell us that the nearest of those fixed stars that look down on us from the heavens is twenty billions of miles away. Light travels at the almost inconceivable velocity of one hundred and eighty-six thousand miles in one second. In the time it takes me to raise my hand it would encircle this globe more than seven times. But, though light travels at the rate of eleven million one hundred and sixty thousand miles in one minute, astronomers tell us that the average distance of those fixed stars that we see in the sky with the naked eye, is so far away that it would take a ray of light from one of them a hundred and twenty years to reach the earth. They further tell us that powerful instruments have discovered stars so far distant that it would take from the time of Abraham, four thousand years ago, to the present day for their light to reach the earth. And there are other magnitudes of which men have caught glimpses that are still more wonderful.

I am not giving you these statements as a lesson in astronomy, but to emphasize the statement that no human mind is capable of fully appreciating many of the fairly well established facts concerning this material universe. They can be stated in words. They can be set down in figures. They can be demonstrated by the fixed rules of geometry. But the human mind is incapable of grasping their full meaning. We can follow them a certain distance, but after that the mind wanders off into a vague dream in which there are no clearly defined ideas.

And now, remember that back of all this incomprehensible immensity and grandeur, is the author of it all, the ruler of it all — God. He it is, as the prophet Isaiah says, "who hath measured the waters in the hollow of His hand, and meted out the heavens with the span, and comprehended the dust of the earth in a measure, and weighed the mountains in scales, and the hills in a balance" (40:12).

Is it strange that we cannot fully comprehend Him? The Psalmist was not speaking from the low level of human ignorance, but from the height of wisdom when he said concerning God and His ways: "Such knowledge is too wonderful for me; it is high, I cannot attain unto it" (139:6). And Isaiah was but voicing the thought of God after Him when he declared: "As the heavens are higher than the earth, so are my ways higher than your ways, and my thoughts than your thoughts" (55:9). 

There is a sense, then, in which God is unknowable; but also a sense in which He is knowable. We cannot weigh, measure, or analyze Him. But we do know that He exists and much of what He has done and is doing. No man, as yet, has been able to define magnetism, gravitation, light, electricity, or life; but we know that they exist and a good many things about their uses. So it is with God. We cannot take into our minds all that God is or has done, but we can take in all that is necessary for our needs. This wonderful God, in the person of His dear Son Jesus, deigns to come and dwell in our hearts and, by sweet heart-fellowship to make up the deficiency of our minds. The character of our knowledge of God and our experience of God determines the character of our religion and our life.

In our study of God we Christians do not begin with nature. The heathen do so, and St. Paul tells us that this revelation of His eternal power and Godhead is so clear that they are without excuse for their godlessness (Romans 1:20). But we have a much clearer light. We come to our knowledge of God from the quiet and sublime height of God's own revelation of Himself in His Word.

And now, what kind of God is He who has here revealed Himself? One of the first and most important points is that God makes Himself known to us as a personal being. 

He is not a mere force.

He is not a mere life-principle pervading all things.

He is not a mere abstract intelligence or universal mind.

Personality is not easy to define. But you have a fair idea of what is meant when we speak of a human being as a person. You have a conception of what is meant when we speak of the person of Jesus Christ. Now we must not lose sight of this when we think or speak of God. He is a being, the original one, who can say "I — I am the Lord thy God." He is possessed of self-consciousness and a will. He knows Himself to be God; all His actions are self-directed; He is a God who is one and undivided, but, mystery of mysteries! existing in three persons.

Here now are some of the attributes, or qualities, that the Scriptures clearly reveal as inherent in the person of God. He is an eternal Spirit, the original, uncreated, unchangeable, self-existent being. All else besides Him are creatures. He is the Creator. All creatures depend on Him, He depends on nothing. God is all-powerful; there is nothing impossible to Him except to do wrong, the very perfection of His being precludes this. He is everywhere present. He is here with each one of us in a personal relation; and ten thousand times farther away than any man-made telescope has ever penetrated or any dream of man has ever reached, there God is, — not with a part of His being, but with His whole Divine personality. 

God knows all things: our inmost thoughts, the falling of a hair, the death of a sparrow. He knows every part, as well as the entire working of this sublimely great and intricate universe, the very smallest part of which only has perhaps come within the scope of human understanding. All this is part of the every day knowledge of God.

He is holiness itself, and no unrighteousness is possible with Him.

He is truthful, perfectly so. Indeed, when the converging lines of truth which thread this great universe are followed to their point of radiation, they are all found to center in God. And, more wonderful still, this great Being, only the outline of whose transcendent character we are capable of bringing within the purview of our understanding, is infinitely condescending, tender and merciful to us, the frail, sinful children of men.

These are but scattered signposts along the way to the knowledge of God, into which knowledge the very angels seek to penetrate.

No wonder that the Psalmist, meditating on the immeasurable greatness of God and His works and the littleness of man, exclaimed: "When I consider thy heavens, the work of thy fingers, the moon and the stars, which thou hast ordained, what is man, that thou art mindful of him? and the son of man that thou visitest him?" (8:3, 4).

What study is comparable to that which has as its subject God and His works? What subject ought so to hold the mind and soul of man in thrall? It is said of the pantheistic Jewish philosopher Spinoza that he was a God-intoxicated man. In a higher, nobler sense this ought to be true of every Christian.

We are told in Scripture that the very angels in heaven, as they surround the everlasting throne on high, cover their faces and fall down in adoration before God. Is this God our God? Do we prostrate ourselves in adoration before Him? Is meditation upon His person and His works a delight to us? Do we trust Him implicitly, love Him and serve Him?

## II. The Sovereignty Of God Is At All Times A Subject Of Absorbing Interest To Those Who Contemplate The Person And Works Of God.

In a special sense does it press for consideration in a study of the Ten Commandments, God's holy Law.

By the sovereignty of God we mean His absolute dominion over all things. Among men, in the affairs of nations, there is a recognized sense of authority. Whether it center in one man, as in an absolute monarchy, or whether it be vested in a sovereign people, there is a recognized source of authority. The person or the persons in whom this authority is centered may not be able, often, indeed, are not able to exercise to the full the authority with which, theoretically, they are clothed. All things human have their decided limitations. But with God there are no such limitations. He is, in the highest, fullest sense of the word, an absolute monarch. There is nothing that He cannot rule, nothing that He does not rule. There is not a human being on the earth, not a sphere in the farthest stretch of space to which God does not give His Law.

This sovereignty of God is not a quality or at tribute of the nature or essence of God in addition to those previously enumerated, but a right of His, arising from the perfection of His being and His relation to the universe as its author. If God is the great, unchangeable Spirit, infinite in wisdom and power, possessing truth and righteousness as inherent attributes of His being, absolute in all His perfections, the Creator and Preserver of the universe, then He is, of right, its Lord, its absolute Ruler. All things are His by right of possession.

>"The earth is the Lord's and the fulness thereof; the world, and they that dwell therein" (Psalm 24:1).

All things are His by right of His perfect ability to control them and bring them to the end He desires. 

>"He doeth according to His will in the army of heaven, and among the inhabitants of the earth: and none can stay His hand, or say unto Him, what doest thou" (Dan. 4:35).

This is a truth established not only by the nature of things, but frequently proclaimed and insisted on in God's revealed Word. "Our God is in the heavens, He hath done whatsoever He hath pleased" (Psalm 115:3). "Of Him, and through Him, and to Him, are all things" (Romans 11:36).

The fact of God's sovereignty is indisputably established. Nature hints at it and Revelation confirms it. But like the person and nature of God there are mysteries connected with this problem of God's sovereignty which man can never dwarf to the diminutive size of his own intellect. There are, however, some things we can know because they are revealed to us, and it is well for us to keep them well in mind. For one thing, God can never be deposed from His lordship. It can neither be ignored nor rejected. It binds all human beings as inexorably as the laws of nature, which is but God's sovereignty in another sphere, bind the material universe. 

It is true, men may temporarily refuse to own God's sovereignty and trample God's laws under their feet. But in the very act of so doing they are drawing the network of God's laws about them for their own punishment. It is here just as it is when men outrage the laws of health: for the time being they seem to do it with impunity, but the inevitable day of reckoning comes. Heaven and earth shall pass away, but not one jot of God's decrees shall fail of complete fulfillment.

Another point to be remembered about God's sovereignty is that it is always exercised in perfect accordance with His infinite holiness and love. The idea of sovereignty vested in men, unless sufficiently safeguarded, is looked upon with suspicion, because so frequently abused.

God's sovereignty, although properly absolute, is the sovereignty of a Father, perfect in sympathy and love. There are many experiences in the lives of men and nations which are strange and perplexing, and some that, to our imperfect understanding, seem even to contradict the supremacy of a sovereign whose ruling principles are justice and love. But let us remember that we see only as through a veil, darkly. We must believe the Father's Word. We must accept the assurance that He who holds the reins of the supreme government, ruling and overruling all things, is guided not only by infinite wisdom, but by infinite love as well; a love which seeks not only the present temporal good of his children, but above all their spiritual and eternal good.

This is the God in whose presence we are to stand as we study His Word. Until we have an adequate idea of Him, His Law will not mean much to us. On the foundation of the person, nature and work of God Himself we must build the whole structure of our religion. Without this first step our Christianity will be a structure hanging in midair, without a support. And this doctrine of the sovereignty of God plays a very important part in the structure of this foundation. It gives us peace and repose. It makes us confident. It assures us that our lives are not the sport of circumstances, and that they are not ruled by an impersonal, unfeeling fate.

So long as our hand is in God's hand, no circumstance, no folly of man, no malice of Satan can in reality injure us.

We are in the care and keeping: of Him who is infinite in His wisdom, power and love.

This is the God who stands before us and says: "I am the Lord thy God." He is the one who gives us the Ten Commandments, the sum of the moral law. As we study these commandments, let us remember that through them God is saying to us: This is what I forbid, this is what I require of you.

Is this God your God? Is this the God to whom you have surrendered yourself? Have you enlisted in His service? Remember that it is written:

>"This is eternal life to know thee, the only true God, and Jesus Christ whom thou hast sent" (St. John 17:3).

# Publication Information

- _Author_: __"Golladay, Robert Emory"__
- _Title_: __"The Ten Commandments"__
- _Originally Published_: 1915 by Lutheran Book Concern, Columbus, Ohio.
- _Lutheran Library Edition_: 2019
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

{{% alert note %}}
{{% weekly-catechism-note %}}
{{% request-pdf %}}
{{% /alert %}}