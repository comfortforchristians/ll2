---
date: 2019-06-12 05:49:37-04:00
publishDate: 2019-06-12 05:49:37-04:00
title: "Essential Apologetics: Fundamental Truths of Christianity by Christoph Luthardt"
slug: "433-luthardt-fundamental-truths-of-christianity"
categories: ["Lutheran Library Publications"]
tags: ["Luthardt","Apologetics"]
authors: ["Luthardt, Christoph Ernst"]
titles: ["The Fundamental Truths of Christianity"]
origpublishers: ["T & T Clark"]
origdates: ["1873"]
synods: ["German Lutheran"]
---
"There is __but one objection__ at the bottom of all the different arguments which have been set up against the historical truth of the gospel narratives; and that is, __the denial of miracles — the denial of another world__.

"This is an objection arising not from historical criticism, but __from the philosophical view of the world__. They who believe in the existence of another world, and see in the person and history of Jesus Christ a revelation thereof, find this stumbling block removed, are convinced of the truth of the miracles in His history, nay, cannot but require it to contain miracles.

"We have but one condition to insist upon in the case of miracles, and that is, that they should have a moral purpose; that they should be neither arbitrary nor fantastic, but should subserve the revelation of truth and grace which appeared in Christ Jesus. And who, that knows the gospel narratives, does not perceive and acknowledge that this condition is observed?

"If we would obtain further certainty on the point, we need only compare the apocryphal gospels, and their arbitrary and tasteless narratives of miracles, devoid of any moral purpose or the series of legends which have been formed concerning Mohammed, with our gospels, to be convinced that these are as far removed from those as the heavens are from the earth, and to perceive what striking confirmation these caricatures of the evangelical narrative furnish of the works of our evangelists. – Christoph Luthardt


# Book Contents

- Preface To The First Edition.
- Preface To The Fourth Edition.
- Preface To The Fifth Edition.
- Preface To The Sixth Edition.
- Preface To The Seventh Edition.
- 1. The Antagonistic Views Of The World In Their Historical Development.
- 2. The Contradictions Of Existence.
- 3. The Personal God.
- 4. The Creation Of The World.
- 5. Man.
- 6. Religion.
- 7. Revelation.
- 8. The History Of Revelation – Heathenism And Judaism.
- 9. Christianity In History.
- 10. The Person Of Jesus Christ.

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/433-luthardt-fundamental-truths-of-christianity-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/433-luthardt-fundamental-truths-of-christianity.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 433-luthardt-fundamental-truths-of-christianity&body=Please send 433-luthardt-fundamental-truths-of-christianity.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 433-luthardt-fundamental-truths-of-christianity&body=Please send 433-luthardt-fundamental-truths-of-christianity.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2019-06-12
- _Version 4 update_:  2019-06-12
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
