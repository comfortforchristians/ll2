---
date: 2019-03-27 12:17:51-04:00
publishDate: 2019-03-27 12:17:55-04:00
title: "Reminiscent Reflections of a Youthful Octogenarian by George Henry Gerberding"
slug: "380-gerberding-reflections-youthful-octogenarian"
categories: ["Lutheran Library Publications"]
tags: ["Gerberding", "Biography"]
authors: ["Gerberding, George Henry"]
titles: ["Reminiscent Reflections of a Youthful Octogenarian"]
origpublishers: ["Augsburg Publishing House"]
origdates: ["1928"]
synods: ["General Council", "Augustana Synod"]
---
Reflections of a faithful Lutheran pastor on a life of scholarship and service.

Also by the same author:


 - <a href="/105lb-gerberding-passavant/">The Life and Letters of William A. Passavant</a>
 - <a href="/116tc-gerberding-priesthood-of-believers/">The Priesthood of Believers</a>
 - <a href="/111ln-gerberding-whats-wrong-with-the-world/">What's Wrong With The World?</a>
 - <a href="/108tc-gerberding-lutheran-church-in-the-country/">The Lutheran Church in the Country</a>
 - <a href="/103tc-gerberding-new-testament-conversions/">New Testament Conversions </a>

# Contents

- Chapter 1. Early Childhood
- Chapter 2. School Days, Companions And Teachers
- Chapter 3. War Days And Academy Experiences
- Chapter 4. Thiel Hall. Doctor Passavant
- Chapter 5. In Muhlenberg College
- Chapter 6. In The Philadelphia Seminary
- Chapter 7. In Philadelphia Seminary (Continued)
- Chapter 8. Entering Upon The Great Life Work
- Chapter 9. Work Expanding
- Chapter 10. Resignation And Removal
- Chapter 11. Experiences In Jewett Charge
- Chapter 12. Out To The Far Frontier
- Chapter 13. Life And Experiences In Fargo
- Chapter 14. A Missionary Journey And A New Synod
- Chapter 15. Beginning My Professorship
- Chapter 16. Further Seminary Experiences
- Chapter 17. A Chapter Of Tragedy
- Chapter 18. Some Backward Glances
- Chapter 19. New Beginnings

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/380-gerberding-reflections-youthful-octogenarian-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/380-gerberding-reflections-youthful-octogenarian.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 380-gerberding-reflections-youthful-octogenarian&body=Please send 380-gerberding-reflections-youthful-octogenarian.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 380-gerberding-reflections-youthful-octogenarian&body=Please send 380-gerberding-reflections-youthful-octogenarian.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}

