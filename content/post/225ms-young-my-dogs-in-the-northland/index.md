---
date: 2018-03-13 12:00:00
title: "My Dogs in the Northland by Egerton Ryerson Young"
slug: "225ms-young-my-dogs-in-the-northland"
categories: ["Lutheran Library Publications"]
aliases: 
  - /davis-the-bar-sinister/
  - /e32-bacon-true-philosopher-and-other-cat-tales/
  - /e41-carr-story-of-five-dogs/
tags: ["Missions", "Young", "Extras", "Animals"]
authors: ["Young, Egerton Ryerson"]
titles: ["My Dogs in the Northland"]
origpublishers: ["Fleming H. Revell Company"]
origdates: ["1902"]
synods: ["Methodist"]


---

"Was it not Sir Walter Scott who said, 'I hate to love a dog, he lives so short a life?' Yet Sir Walter did love dogs with rare devotion, as the traditions of Abbotsford, as well as much that he himself has written, affirm. 

{{% toc %}}

# Jack London's _Call of the Wild_

My Dogs in the Northland (1902) was used as source material by the author Jack London for his novel "The Call of the Wild_ (1903).

# From the Introduction

"When a lad I was not allowed to keep a dog. My father in his early manhood days saw a man smothered to death between two great feather beds because he was a hopeless victim of hydrophobia [rabies], caused by the bite of a mad dog. So no dog was allowed in our home...

"For years, with great dogs, I toiled and often with them was in great perils. Much of my work was accomplished by their aid. So I believe in dogs, and here in this book I have written of some of them and their deeds. – Rev. Egerton R. Young, from the Introduction



![Egerton R. Young](/img/posts/egerton-r-young.jpg)

# Contents (172 pages)

- Copyright Information
- Introduction
- I. My Eskimo Or Huskie Dogs
- II. With Wild Eskimo Dogs Under The Auroras
- III. Robber Dogs And An Indian Council
- IV. Jack, The Giant St. Bernard
- V. Jack. And Many Things Concerning Him
- VI. Jack Triumphant In The Blizzard
- VII. Jack In Civilization
- VIII. Cuffy, The Beautiful Newfoundland Dog
- IX. Voyageur, The Matchless Leader
- X. Voyageur, The Broken-hearted
- XI. Rover I, The Successful Dog Doctor
- XII. Rover II, Also Called Kimo
- XIII. Muff, The Affectionate Mother Dog
- XIV. Caesar, The Clever Rascal
- XV. Koona, The Eskimo Leader
- XVI. Traveling With Dogs In Northern Wilds
- XVII. Still On The Trail With The Dogs
- XVIII. Our Dogs In Summer Time
- XIX. Cui Bono? [For whose benefit?]
- About the Author

# About the Author – Egerton R. Young

…It was the comfort of the great apostle to the Gentiles that with a clear conscience he could say, “I have kept back nothing that was profitable for you,” and that when he knew that those among whom he had gone preaching the kingdom of God, would see his face no more, he could boldly “take them to record,” that he was pure from the blood of all men, and had “not shunned to declare all the counsel of God.” However materialists may oppose amid the thickening troubles of these ungodly days; however distasteful it may be to the carnal mind; however scorners may jest and sneer; the ambassador of God must boldly deliver unto the people, all that they have received of the Lord. – From Chapter 20.

"Egerton Ryerson Young was born at Smith's Falls, Ontario, April 7, 1840. He was educated at the Normal School of the Province of Ontario, after having taught for several years, and in 1863 entered the ministry. Four years later he was ordained, and, after being stationed at the First Methodist Episcopal Church, Hamilton, Ontario, in 1867-68, was sent as a missionary to Norway House, North-West Territory. There he worked among the Indians for five years, and in 1873 went in a similar capacity to Beren's River, Northwest Territory, where he remained three years (1873-76). In 1876 he returned to Ontario and was stationed successively at Port Perry (1876-79), Colborne (1879-82), Bowmanville (1882-85), Medford (1885-87), and St. Paul's, Brampton (1887-88). Since 1888 he has been prominent as a lecturer on work among the American Indians, and in this cause has made repeated tours of the world. He has written about a dozen books.

"This biography was written before Rev. Young's Death in 1909.[^aDh]

[^aDh]: Christian Classic Ethereal Library at Calvin College.

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)

![book cover](/img/posts/225ms-young-my-dogs-in-the-northland-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/225ms-young-my-dogs-in-the-northland.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 225ms&body=Please send 225ms-young-my-dogs-in-the-northland.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 225ms&body=Please send 225ms-young-my-dogs-in-the-northland.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}

[^aD3]: Retrieved 2018-03-04 from cyclopedia.lcms.org "Leander Sylvester Keyser"