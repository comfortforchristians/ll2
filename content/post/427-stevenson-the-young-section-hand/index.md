---
date: 2019-05-16 15:37:23-04:00
publishDate: 2019-05-16 15:37:23-04:00
title: "The Young Section Hand by Burton Egbert Stevenson"
slug: "427-stevenson-the-young-section-hand"
categories: ["Lutheran Library Publications"]
tags: ["Stevenson", "Children", "Railroads", "American West", "Fiction"]
authors: ["Stevenson, Burton Egbert"]
titles: ["The Young Section Hand"]
origpublishers: ["L. C. Page & Company"]
origdates: ["1905"]
synods: ["N/A"]
---

"Every branch of railroading fascinates the average American boy.  The shops, the telegraph and signal systems, the yard and track work, the daily life of danger which confronts every employee, whether he be the ordinary workman or the engineer of a limited express train, and the mysterious "office" which controls every branch of the work.

"Mr. Stevenson's hero is a manly lad of sixteen who is given a chance as a section hand on a big Western railroad, and whose experiences are as real as they are thrilling.  He is persecuted by the discharged employee whose place he took, and becomes involved in complications which nearly cause his undoing; but his manliness and courage are finally proven, and the reward is his for duty done at any cost.

– From the 1915 Publisher's Description

{{% toc %}}

# Book Contents
- List of Illustrations
- 1. The Bottom Round
- 2. A New Experience
- 3. An Adventure And A Story
- 4. Allan Meets An Enemy
- 5. Allan Proves His Metal
- 6. Reddy To The Rescue
- 7. The Irish Brigade
- 8. Good News And Bad
- 9. Reddy’s Exploit
- 10. A Summons In The Night
- 11. Clearing The Track
- 12. Unsung Heroes
- 13. A New Danger
- 14. Allan Makes A Discovery
- 15. A Shot From Behind
- 16. A Call To Duty
- 17. A Night Of Danger
- 18. The Signal In The Night
- 19. Reddy Redivivus
- 20. The Road’s Gratitude
- Other Books by Page & Company, Boston

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/427-stevenson-the-young-section-hand-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/427-stevenson-the-young-section-hand.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 427-stevenson-the-young-section-hand&body=Please send 427-stevenson-the-young-section-hand.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 427-stevenson-the-young-section-hand&body=Please send 427-stevenson-the-young-section-hand.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2019-05-16
- _Version 4 update_:  2019-05-16
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
