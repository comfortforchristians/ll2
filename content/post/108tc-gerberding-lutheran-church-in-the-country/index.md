---
date: 2019-04-29 19:24:17-04:00 
publishDate: 2019-04-29 19:24:17-04:00 
title: The Lutheran Church in the Country by George H. Gerberding
slug: "108tc-gerberding-lutheran-church-in-the-country"
aliases: 
  - /108tc-gerberding-lutheran-country-church/
categories: ["Lutheran Library Publications"]
tags: ["History", "Pastoral", "Gerberding"]
authors: ["Gerberding, George Henry"]
titles: ["The Lutheran Church in the Country"]
origpublishers: ["General Council Publication Board"]
origdates: ["1916"]
synods: ["General Council"]

---

"There are thousands of sincere, well meaning and earnest Christians in the Reformed churches in every section of the country. They recognize and deplore the threatening change that has come over the church life of the country. They fear the impending heathenizing. They plan and pray for a remedy..."

"They never have been clearly instructed in God's way in His sanctuary. They do not know that God has His own way of saving humanity and that His way of salvation is clearly marked out in His Word. Of this they are sadly ignorant. It has not been explained to them. This is their misfortune rather than their fault. Instead of denouncing them, we should feel sorry for them.  We should use every endeavor to show them kindly, lovingly, patiently, a more excellent way."

"In their zeal, which is not according to knowledge, these good people are ready to take up and fall in with anything that promises relief and betterment. They are often imposed upon and inveigled into the fanatical sects that make a great show of earnestness. These immersionist and revivalist and sanctificationist sects are heretics as to psychology, as to pedagogy and as to theology. They burn the country over like a forest fire."

George H. Gerberding was born in Pittsburgh in 1847.  He served as a missionary and pastor in Ohio, Pennsylvania, and Fargo, North Dakota, and as president of the Lutheran Synod of the Northwest and of the Chicago Synod.  He was a professor at the Chicago Lutheran Seminary and Northwestern Lutheran Seminary.  He died in 1927.[^BY]

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/108tc-gerberding-lutheran-church-in-the-country-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/108tc-gerberding-lutheran-church-in-the-country.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 108tc&body=Please send 108tc-gerberding-lutheran-church-in-the-country.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 108tc&body=Please send 108tc-gerberding-lutheran-church-in-the-country.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2017-09-08 
- _Version 4 update_: 2019-04-29 
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)




[^BY]: Lutheran Cyclopedia.