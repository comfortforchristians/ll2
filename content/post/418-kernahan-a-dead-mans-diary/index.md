---
date: 2019-04-11 06:32:22-04:00
publishDate: 2019-04-11 06:32:34-04:00
title: "A Dead Man's Diary: Written After His Decease by Coulson Kernahan"
slug: "418-kernahan-a-dead-mans-diary"
categories: ["Lutheran Library Publications"]
tags: ["Kernahan", "Fiction"]
authors: ["Kernahan, Coulson"]
titles: ["A Dead Man's Diary"]
origpublishers: ["Ward, Lock and Company, Ltd."]
origdates: ["1890"]
synods: ["Anglican"]
---

"There can be no doubt of the force and freshness of most of the book, of the fine literary quality of some of the chapters, and of the interest of the whole. . . . There is too many a burst of beautiful English." – Mr. Israel Zangwill, in "Ariel"

---

"Originally the Diary was published anonymously, when it attracted so much attention that dishonest claims were put in to the authorship, and one man, by representing himself as the author, induced a firm of publishers to advance money upon a book of his. His work was in the press and approaching completion before the fraud was discovered, when the entire edition was suppressed. The name of the author, Mr. Coulson Kernahan, which was disclosed in the columns of the Athenaeum, will appear for the first time on the title-page of the Fourth Edition. Like Mr. Jerome and Mr. J. M. Barrie, Mr. Kernahan is a protege of Mr. F. W. Robinson, the novelist." – The Times.

{{% toc %}}

# Chapters

- 1. Explains My Title
- 2. Deals With Death And The Dread Of it
- 3. Deals With Life And The Lust Of it, But Has No Direct Bearing On My Story
- 4. I Die
- 5. Is Of An Explanatory Nature Only
- 6. Tells Of My First Awful Awakening in Hell, And Of The Shameful Sin Which Brought Me Thither
- 7. Shows The Fitness Of My Punishment
- 8. Make An Unexpected Discovery in Hell, And Meet With An Old Acquaintance
- 9. I See Some Strange Sights in Hell, And Am Favored With Something in The - Nature Of A Sermon
- 10. A Love Story in Hell
- 11. The Mystery Of “The Dead Who Die”
- 12. I See The Brother Whom We Have All Lost
- 13. A Dream Of Eternal Rest
- 14. Hope
- 15. Heaven
- Other Books by Mr. Kernahan
- Copyright Information
- How Can You Find Peace With God?
- About the Lutheran Library Publishing Ministry
- Benediction
- You Might Also Like …

# About the Author

From [Wikipedia](https://en.wikipedia.org/wiki/Coulson_Kernahan):

"John Coulson Kernahan was born in Ilfracombe, Devon to Rev. James Kernahan, M.A., F.G.S., and his wife Comfort...Kernahan was educated privately by his father and at St Albans School. James had intended for young Coulson to enter the church, but his son's 'intentions were towards literature.'...Most of Kernahan's books and works received positive reviews and recognition."

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/418-kernahan-a-dead-mans-diary-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/418-kernahan-a-dead-mans-diary.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 418-kernahan-a-dead-mans-diary&body=Please send 418-kernahan-a-dead-mans-diary.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 418-kernahan-a-dead-mans-diary&body=Please send 418-kernahan-a-dead-mans-diary.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}

