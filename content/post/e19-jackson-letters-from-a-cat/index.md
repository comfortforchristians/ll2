---
date: 2019-04-18 09:17:42-04:00
publishDate: 2019-04-18 09:17:42-04:00
title: "Letters From A Cat by Helen Hunt Jackson"
slug: "e19-jackson-letters-from-a-cat"
categories: ["Lutheran Library Publications"]
tags: ["Extras", "Cats", "Animals", "Children", "Fiction"]
authors: ["Jackson, Helen Hunt"]
titles: ["Letters from a Cat"]
origpublishers: ["Roberts Brothers"]
origdates: ["1879"]
synods: ["N/A"]

---
"I do not feel wholly sure that my Kitty wrote these letters herself. They always came inside the letters written to me by my mama, or other friends, and I never caught Kitty writing at any time when I was at home; but the printing was pretty bad, and they were signed by Kitty’s name; and my mama always looked very mysterious when I asked about them, as if there were some very great secret about it all; so that until I grew to be a big girl, I never doubted but that Kitty printed them all alone by herself, after dark.

"They were written when I was a very little girl, and was away from home with my father on a journey. We made this journey in our own carriage, and it was one of the pleasantest things that ever happened to me. My clothes and my father’s were packed in a little leather valise which was hung by straps...

![](/img/posts/i00-colophon.jpg)

# What are Lutheran Library "Extras"?

["Extras"](/tags/extras/) are entertaining or otherwise valuable books which don't fit the regular publishing guidelines for the Lutheran Library. The editors hope you will find them worthwhile.



# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download) 

![book cover](/img/posts/e19-jackson-letters-from-a-cat-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/e19-jackson-letters-from-a-cat.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request e19&body=Please send e19-jackson-letters-from-a-cat.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request e19&body=Please send e19-jackson-letters-from-a-cat.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}

# Publication Information

- _Lutheran Library edition first published_: 2017-08-01
- _Version 4 update_: 2019-04-18 
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
