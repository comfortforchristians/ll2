---
date: 2019-05-23 12:00:18-04:00
publishDate: 2019-05-23 12:00:18-04:00
title: "A Testimony Against the False Doctrine of Predestination Recently Introduced By The Missouri Synod. Part 3 of Walther and the Predestination Controversy or The Error of Modern Missouri by Schodde et al."
slug: "242tc-lenski-the-blue-island-theses-election-and-predestination"
categories: ["Lutheran Library Publications"]
tags: ["Schodde", "Allwardt", "Lenski",  "Election", "Predestination", "Predestination Controversy", "Intuitu Fidei", "Walther", "Calvinism", "Book of Concord", "Justification"]
authors: ["Schodde, George Henry","Allwardt, Heinrich August","Lenski, Richard C. H."]
titles: ["A Testimony Against the False Doctrine of Predestination Recently Introduced By The Missouri Synod", "Walther And The Predestination Controversy","The Error of Modern Missouri"]
origpublishers: ["The Lutheran Book Concern"]
origdates: ["1892"]
synods: ["Ohio Synod", "Iowa Synod", "Missouri Synod", "General Council", "General Synod"]
--- 

The rise of the Synodical Conference under the LCMS has made Universal Objective Justification the Shibboleth of American Lutheranism.  Quite simply, if you do not accept UOJ, you are not considered a "Confessional Lutheran".  

Towards the end of his life, C. F. W. Walther brought forth a teaching of election which many Missouri and other American Lutherans could not reconcile with the Scriptures or the Lutheran Confessions.  The resulting "Predestination Controversy" split the Lutheran Church.

# The Lutheran Church Unanimous

"Since the publication of the Formula of Concord, for some 300 years, the Lutheran Church has unanimously held that the doctrine of our Confession and of the following teachers of our Church harmonized perfectly also in the article of predestination. Modern Missourians are the first "Lutherans" who assert the contrary; it is to be hoped that they will also be the last."

{{% toc %}}

# Blue Island and The Theses

"On November 16, 1881, 12 pastors and teachers, 4 representatives of congregations, and 9 guests met at Blue Island, Illinois to discuss the new doctrine of predestination the Missouri Synod had begun to teach at that time. Besides this a number of letters from pastors and laymen were sent in, heartily favoring the purpose of the meeting. 

"Most of these represented congregations...had left Missouri as a result of the issue.

"This book describes the five theses which came out of the meeting and were approved by the representatives.


1. God has irrevocably elected unto salvation before the foundation of the world all those who are saved in time.

2. Election is revealed in the Scriptures, and is therefore no more “a mystery” than any other article of faith.

3. Election is revealed in the Gospel and not in the law.

4. The Gospel directs us to Christ – God has elected in Christ.

5. Christ’s merit is considered in election not merely as obtained for us, but also as apprehended by us – God has elected in view of faith.

# Disgraceful Behavior and "Dirty Tricks"

"If there had been only personal disputes, the matter would have been different. But one of the highest rights in the Church was at stake, the right of every Christian to protest against false doctrine and to be heard at least before judgment is pronounced. 

"To Dr. W.'s great learning and eloquence I could oppose nothing except my plain testimony for the truth; standing against him I would be at disadvantage in a hundred different ways. That he was not satisfied with this preeminence, but had employed such violent measures, made me exceedingly indignant, and I was determined now to take the course which promised me the greatest success; namely, to reply publicly to the heresy that had been publicly promulgated and defended by craft and sophistry. When afterwards oral negotiations were undertaken, I was found in my place and refrained, during these negotiations as well as in my essays, from all insulting utterances; for I always entertained the hope that Dr. W. would yield. This has not occurred. 

"How our opponents in Chicago and Fort Wayne, and since then, defended their cause, yes theirs — not God's — to state this at length would require too much time and space. God willing, this shall yet be done, __in order that unprejudiced people may obtain, at least in time to come, a just opinion of the present controversy__. 

"Surely, even though it be slowly, the Church in time always gains clear insight into the controversies through which it has passed. The confusion was so great at the time of the first crypto-Calvinistic controversies that the most sincere people did not, to a large extent, know who was right. But long before the end of the century everything was as clear as the sun, and the men who had been derided as wranglers and disturbers of the peace, who had been deposed and persecuted, stood forth gloriously justified. We cannot and will not set ourselves up as their equals; but __we have learned from them that one need not despair of the victory of truth.__ 

# Why The Lutheran Attendees Thought It Essential To Publish This Book

A discussion of the assembly was written up by the attendees and published in English by Lenski and Tressel. The editors close the introduction with these words:

"Our present intention is not the defense of our good name over against the calumniations of Missouri. We cannot deny indeed, that it pains us to have so many of our former brethren and fathers in Christ look upon us now as heretics, synergists. Pelagians, arch-Pelagians, and even as pagans and Turks. Yet we have the testimony of a good conscience. And we see also to what fallacies they must resort to give any support to their accusations...

"__From conversations with many Missouri pastors since the inception of the controversy we know that it was almost impossible for them to swallow the new doctrinal propositions, and they dare not even to this day present them openly and honestly to their congregations__. 

"These are indeed miserable conditions, and we can only thank God for having preserved us and strengthened our hearts to fight against the error. __The aspersions cast upon us we can bear readily, knowing that a day of just judgment is drawing nigh__.

"Our purpose is simply to raise our united voices in warning: Beware, O Lutheran Church of America, beware! Missouri, so highly favored and blessed – Missouri with Dr. Walther at its head – has fallen into great error, into an error which affects the very foundation of our salvation – God’s eternal love for sinners.

"Missouri indeed comes with an indignant denial. And, in fact, it does not explicitly deny that God has loved all men, that the Son of God has redeemed all, and that God in a certain sense would have all men to be saved. Missouri confesses all this, and often clothes it in beautiful words, finer than we are able to produce. And yet by the side of this its teaching Missouri adheres to a doctrine of predestination which in very fact annuls the universal love of God. 

"Missouri itself confesses that apparently the doctrine of predestination contradicts the doctrine of God’s universal will of grace; it tells us that the connection between these two doctrines is a mystery; and under cover of this “mystery” it seeks to establish this doctrine in the Church.


# Summary Book Contents

- Introduction.

- III. A Testimony Against The False Doctrine Of Predestination Recently Introduced By The Missouri Synod.

    - General Introduction.
    - Introduction To The Doctrinal Discussion.
    - The Blue Island Theses.
        - Thesis 1.
            - Wider Sense
            - Confession and Universal Counsel
            - Dr. Luther’s Preface to Romans
            - Wider Sense – Gospel
        - Thesis 2.
            - Missouri – Faith in Election – Faith in Justification
            - Missouri Doctrine Of Election And The Biblical Doctrine Of Grace
            - Thoughts of a Troubled Heart
            - Chemnitz, Contingency
            - If You Are To Be Saved
            - Romans 11:33
            - Missouri Repeating Role of Old Israel
            - Election Revealed in Scripture
            - Certainty of Salvation Conditional
            - A Missourian On The Witness Stand
            - Luther To A Heart Troubled About Predestination
            - The Windy Thing on Legs
            - Dr. Walther’s Postil
            - Is It A Mystery Why God Did Not Ordain All To Eternal Life?
        - Thesis 3.
            - Election Revealed In The Gospel
            - Confession on “Election Revealed In The Gospel”
            - Words of Confession as Chief Point of Controversy
            - Missouri and The Thoughts of Reason
            - Is Redemption Only For The Elect?
            - The Eight Points
            - Missouri’s False Construction Of The Eight Points Refuted
        - Thesis 4.
            - We Place No Merit In Man
            - Dr. Walther’s Proof Of Our Synergism
            - Luther, The Confession, The Scriptures Speak As We Do
            - Baier and Huelsemann Speak As We Do
            - Quenstedt Speaks Likewise
            - Faith Impels God
        - Thesis 5.
            - The Point Of The Controversy
            - Matthew 22:1-14
            - Chemnitz on Matthew 22:1-14
            - Romans 8:28-30
            - “Foreknow”
            - Ephesians 1:3-6
            - 2 Thessalonians 2:13
            - 1 Peter 1:1-2
            - Obedience
            - Review of Scripture Passages
            - The Analogy of Faith
            - What Elect Means
            - Election and the Preacher

- The Predestination Controversy: How It Happened.
    - How the Predestination Controversy “Broke Out”

# Essays included

In three sections by Lutheran authors well respected at the time, the history of the Predestination Controversy and the points in conflict are clearly presented and compared with extracts from the writings of the framers of the Lutheran Confessions and other Church Fathers.


This book is part 3 of [Walther and the Predestination Controversy or The Error of Modern Missouri](/239-schodde-the-error-of-modern-missouri/)

- [Part 1: The Present Controversy on Predestination](https://www.lutheranlibrary.org/240tc-stellhorn-which-predestination-reformed-or-lutheran)
- [Part 2: _Intuitu Fidei_ "In Light of Faith"](https://www.lutheranlibrary.org/241tc-schmidt-intuitu-fidei)


# Beware Non-Revealed Mysteries!

"Beware, O Lutheran Church! This “apparent” contradiction is a real contradiction, a contradiction of the fundamental doctrine of the Scriptures, namely that God had such compassion upon all men as to render the salvation of all in reality possible. When our opponents speak of the universal will of grace, they still for the most part speak correctly; but when they speak of predestination, their words are false. Paul tells us that a little leaven leaveneth the whole lump. But our opponents have mixed the truth of the Scriptures not with “a little”, but with a good-sized lump of error.

"Away with non-revealed mysteries in this or in any other doctrine! If there are real mysteries, i. e. real truths which it has not pleased God to reveal to us, then they are well taken care of in God's hands, but they do not belong to Christian doctrine. If, however, there are "mysteries" which directly contradict the revealed Word, then they are lies of the devil.
 
# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)

![book cover](/img/posts/242tc-lenski-the-blue-island-theses-election-and-predestination-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/242tc-lenski-the-blue-island-theses-election-and-predestination.pdf)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 242tc&body=Please send 242tc-lenski-the-blue-island-theses-election-and-predestination.mobi)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 242tc&body=Please send 242tc-lenski-the-blue-island-theses-election-and-predestination.epub)


{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}

# Publication Information

- _Lutheran Library edition first published_: 2018-06-11
- _Version 4 update_: 2019-05-23
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
