---
date: 2018-09-05 12:00:00
title: "A Bible Year: A Course In Bible-reading, Completing The Entire Bible In One Year; With Daily Suggestions For Meditation And For Further Study by Amos Wells"
slug: "272-wells-a-bible-year"
categories: ["Lutheran Library Publications"]
aliases:
  - "/wells-a-bible-year/"
tags: ["Devotional", "Wells"]
authors: ["Wells, Amos"]
titles: ["A Bible Year"]
origpublishers: ["United Society of Christian Endeavor"]
origdates: ["1899"]
synods: ["Unknown"]

--- 
This little book pretends to be nothing more than a primer of Bible-reading.  For a year, week by week, these studies were printed in the columns of "The Christian Endeavor World".

![book cover](/img/posts/272-wells-a-bible-year-1024.jpg)

"This little book pretends to be nothing more than a primer of Bible-reading.

"For a year, week by week, these studies were printed in the columns of _The Christian Endeavor World_. A call was made for the names of all that would undertake to read the Bible through in accordance with this plan during the year; and without advertisement or urging, more than nine thousand sent in their names. Hundreds and even thousands of these expressed themselves as thankful for the plan, and said that, though they had often proposed reading the Bible through and had frequently set out upon the task, this plan had furnished just the stimulus and aid needed to spur them to its completion.

"These Bible-readers were in all parts of America and in many foreign lands. Indeed, the course was translated into several foreign tongues.

"Pastors induced their churches to take up the work, Families read the Bible thus together, Sunday-school teachers used the plan as an incentive to their classes for regular home Bible-reading and study. Some that had never read the Bible for three days in succession, completed the entire course. Some in their eagerness got far ahead, and read the Bible through twice in the year.  The plan was found useful in prisons.

"Ministers of the gospel testified that even to them it had opened up fresh gospel truth. "The Bible has become a new book to me," was a frequent message.

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)

![book cover](/img/posts/272-wells-a-bible-year-1024.jpg)

<a name="download"></a><br />
[PDF](/pdf/272-wells-a-bible-year.pdf)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 272wd&body=Please send 272-wells-a-bible-year.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}
[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 272wd&body=Please send 272-wells-a-bible-year.epub)

