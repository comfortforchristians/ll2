---
date: 2019-05-05 09:35:59-04:00
publishDate: 2019-05-05 09:35:59-04:00
title: Discourses on Romanism and the Reformation by Emanuel Greenwald
slug: "112tc-greenwald-why-the-reformation"
categories: ["Lutheran Library Publications"]
tags: ["Roman Catholicism", "The Reformation", "Greenwald", "Inquisition"]
authors: ["Greenwald, Emanuel"]
titles: ["Discourses on Romanism and the Reformation"]
origpublishers: ["John Baer's Sons"]
origdates: ["1880"]
synods: ["Ohio Synod"]

---
 
Pope Frances declared, "The Reformation is Over". Was he right? Many evangelical Christians don't see any significant differences between the "different flavors" of Christianity.  

{{% toc %}}

# A Sensation in the Town

In 1879 a series of spirited talks were given in Lancaster, PA which were sharply critical of the doctrines and character of Martin Luther and of the Reformation.  In response, Rev. Greenwald gave seven Sunday Evening Sermons addressing the points raised.  These content of these well-attended messages were published later that year under the title, *Discourses on Romanism and the Reformation*.

# Is The Reformation Over?

In 2015, Pope Frances declared, *The Reformation is Over*.[^Af]  Is the Pope right?  

Greenwald's book may be the best primer available for this purpose.  It answers:

* Was the Reformation necessary?

* What were the essential issues?  Do they still apply?

* Has the Roman Catholic Church changed in its claims?

# Partial Outline

1. St. Paul's Church of Rome
  - Who founded the Church of Rome?
  - Who was the first Bishop in Rome?
  - Was Peter ever in Rome?

2. The Papacy
  - According to the Roman Catholic Church, what is the office and jurisdiction of the Pope?
  - What is the *Power of the Keys*?
  - How did the Papacy develop over time?

3. Doctrines of the Church of Rome
  - How do the Apocryphal Books relate to the Old and New Testaments?
  - What is the role of *oral tradition*?
  - What is *original sin*?
  - Are human beings justified by faith plus works?
  - Is the Mass a sacrifice for sins?
  - Should we invoke saints in prayer?

4. Rome a Persecuting Church
  - What was St Bartholomew's Eve?
  - The Huguenots, Waldenses and Vaudois.
  - Pope Pius IX (1864) pronounces anathemas on all who maintain liberty of the press, of conscience and of free speech

5. Necessity of the Reformation
  - How the Gospel as a system of faith and salvation became corrupted.
  - The despotism of the Papacy in the Dark Ages.
  - The gross ignorance of the clergy and the people.
  - The licentiousness of the priests and monks.

6. Reform Before the Reformation
  - Strong opposition to the use and worship of images including by Charlemagne, Agobard, and Claudius, Bishop of Turin.
  - Bernard of Clairvaux's calling out of drunkenness and other vices of the Popes, bishops, monks and priests in the twelfth century.
  - Rejection of the hierarchical Papal system by the Catharoi in the eleventh century.
  - Peter de Bruys efforts against superstition and abuses from 1110 - 1130.
  - The Albigenses in the twelfth and thirteenth centuries.

  
7. Historical Sketch of the Reformation

# About the Author, Rev. Emanuel Greenwald

Emanuel Greenwald, D.D. was born in 1811 near Frederick, Maryland.  His devout parents prepared their son for the ministry from an early age.  Emanuel studied theology under the personal supervision of Rev. David F. Schaeffer.  

Rev. Greenwald's first parish in New Philadelphia, Ohio included country for fifteen miles in each direction.  At one point he supplied fourteen preaching points on Sundays and week-days.  

In 1842 Dr. Greenwald became the first editor of the *Lutheran Standard*.  He was the president of the English District Synod of Ohio from 1848 to 1850 and held many important positions in Columbus over the following years.  In 1854 he accepted a call to Christ Church, Easton, PA, where he served for 12 years.  His final parish was Holy Trinity Church of Lancaster, PA where he ministered from 1867 until his death in 1885.[^Ae]

His best-known works are *Order for Family Prayer* and its 1883 revision, *Jesus our table guest*, which includes these words:

>May Jesus be the Table Guest in every house! With fervent prayers for the divine blessing upon this humble attempt to glorify His dear name, this little volume is sent forth into our families. 

[^Ae]: *The Lutheran Cyclopedia*. Ed. Henry Eyster Jacobs, 1899.

[^Af]: http://www.catholicherald.co.uk/issues/july-24th-2015/the-popes-great-evangelical-gamble/

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)

![book cover](/img/posts/112tc-greenwald-why-the-reformation-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/112tc-greenwald-why-the-reformation.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 112tc&body=Please send 112tc-greenwald-why-the-reformation.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 112tc&body=Please send 112tc-greenwald-why-the-reformation.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2017-09-18 
- _Version 4 update_: 2019-05-05
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
