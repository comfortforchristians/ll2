---
date: 2019-06-17 15:06:54-04:00
publishDate: 2019-06-17 15:06:54-04:00
title: "The Life of Henry Clay by Charles Krauth"
slug: "s22-krauth-henry-clay"
categories: ["Lutheran Library Publications"]
tags: ["Krauth", "Short Books", "Biography"]
authors: ["Krauth, Charles Porterfield"]
titles: ["Life of Henry Clay"]
origpublishers: ["H.C. Neinstadt"]
origdates: ["1852"]
synods: ["General Synod"]
---
If it weren't for the American statesman Henry Clay, "Who can tell the evils which would have ensued?

"Would we this day be a united and happy people, prosperous beyond example, and with a most brilliant career opening before us, the envy of tyrants, and the boast of the friends of freedom the world over?

"Would we be living in peace with all men, governing ourselves, promoting by our efforts pure morality and genuine religion; sitting under our own vine and fig tree, there being none to hurt or make us afraid?

"Would education, would Christianity be receiving the attention and exercising the ever increasing influence, which, it is notorious, that they now do?

"As these and similar interrogatories are answered by us, the answers will be associated with one name, the name of Clay, whose clear intellect saw, and whose prompt invention devised, the remedy for the grievous diseases of the body politic. In view of all may we say, _Well done good and faithful servant!_  

– Charles Krauth


# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/s22-krauth-henry-clay-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/s22-krauth-henry-clay.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request s22-krauth-henry-clay&body=Please send s22-krauth-henry-clay.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request s22-krauth-henry-clay&body=Please send s22-krauth-henry-clay.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2019-06-17
- _Version 4 update_:  2019-06-17
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
