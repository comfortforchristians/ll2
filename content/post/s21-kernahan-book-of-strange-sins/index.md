---
date: 2019-04-12 15:20:24-04:00
publishDate: 2019-04-12 15:20:29-04:00
title: "A Book of Strange Sins by Coulson Kernahan"
slug: "s21-kernahan-book-of-strange-sins"
categories: ["Lutheran Library Publications"]
tags: ["Kernahan", "Fiction", "Short Books"]
authors: ["Kernahan, Coulson"]
titles: ["A Book of Strange Sins"]
origpublishers: ["Ward, Lock & Company, Ltd."]
origdates: ["1893"]
synods: ["Anglican"]
---

"A writer possessing not only a fine literary gift, and a marvelous power of intense emotional realization, but a fresh, strange, and fascinating imaginative outlook. We know of nothing published in recent years which, in lurid impressiveness and relentless veracity of rendering, is to be compared with the realization of the fatally dominant alcoholic craving in the study entitled 'A Literary Gent.' " – "_The Daily Chronicle_".

"The terrible truth which rings out in every word leaves one heart-sick, and yet thankful for the story and for the story-teller." – Lady Henry Somerset in "_Woman's Signal_". 

"I do not remember to have read for a long time a study of the deadliness to soul and body — of what I may even call the murderousness of purely sensual passion — in which the moral is so finely, and, I must use the word, awfully, conveyed. . . . One of the hits of the season. I am not surprised." – B. T. P. O'Connor, M.P., in the "_Weekly Sun_".


{{% toc %}}


# Stories 

- Preface.
- A Strange Sin.
- A Suicide.
- The Garden Of God. (a Story For Children From Eight To Eighty.)
- The Apples Of Sin.
- A Literary Gent. A Study In Vanity And Dipsomania.
- The Lonely God.

# About the Author
From [Wikipedia](https://en.wikipedia.org/wiki/Coulson_Kernahan):

"John Coulson Kernahan was born in Ilfracombe, Devon to Rev. James Kernahan, M.A., F.G.S., and his wife Comfort...Kernahan was educated privately by his father and at St Albans School. James had intended for young Coulson to enter the church, but his son's 'intentions were towards literature.'...Most of Kernahan's books and works received positive reviews and recognition."


# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/s21-kernahan-book-of-strange-sins-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/s21-kernahan-book-of-strange-sins.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request s21-kernahan-book-of-strange-sins&body=Please send s21-kernahan-book-of-strange-sins.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request s21-kernahan-book-of-strange-sins&body=Please send s21-kernahan-book-of-strange-sins.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}

