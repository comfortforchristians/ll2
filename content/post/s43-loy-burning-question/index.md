---
date: 2019-06-13 05:01:42-04:00
publishDate: 2019-06-13 05:01:42-04:00
title: "The Burning Question: The Predestination Controversy in the American Lutheran Church by Matthias Loy"
slug: "s43-loy-burning-question"
categories: ["Lutheran Library Publications"]
tags: ["Loy", "Short Books", "Election", "Predestination", "Missouri Synod", "Columbus Theological Magazine", "Predestination Controversy"]
authors: ["Loy, Matthias"]
titles: ["The Burning Question: The Predestination Controversy in the American Lutheran Church"]
origpublishers: ["Columbus Theological Magazine"]
origdates: ["1881"]
synods: ["Ohio Synod"]
---

"[Missouri's] new doctrine would have us believe that there is saving grace only for the few embraced in God's purpose of election."

"We shall, by the grace of God, be neither enticed nor driven into such folly, but shall abide by __the old and well established doctrine of the Church, that God desires with equal sincerity the salvation of all men, and that He saves, and has elected unto salvation, all those who do not obstinately resist the saving work of the Spirit__.

"This gives us the sure comfort that __God loves all of us, that He does everything necessary to save all of us,__ and that if any one is not saved it is because he would not come unto Christ that he might have life. The grace of God unto salvation that is for all men is also for me. What power could deprive me of that comfort, and what more could I want?
 –  Matthias Loy


# Book Contents

- The Burning Question: The Present Predestination Controversy in the American Lutheran Church
- The Claims of the Missouri Synod
- Established Doctrine of 300 Years
- Missouri Claims The Formula Teaches New Doctrine
- Forsaking the Old Paths On The Word of A Few Men
- The Two Opposing Doctrines
- 1. God Determined From Eternity To Save Our Lost Race Through Christ By Faith.
- 2. God Arbitrarily and Indiscriminately Drew From The Multitude of Lost Souls Those To Be Adopted As Children of God.
- Dr. A. Pfeiffer’s Presentation of The Lutheran View of Election.
- It’s Not Easy To Prove Missouri’s New Doctrine
- Dr. Walther’s Theses.
- The Trouble With Dr. Walther’s Conception of Election.
- Missouri Now Teaches Two Divine Decrees.
- Why This Teaching Must Be Exposed and Resisted.
- 1. It is an Outgrowth of Philosophical Speculation.
- 2. It Is Damaging To The Revealed Doctrine of God and His Attributes.
- 3. Its Exegetical Principles and Practices Are Problematic.
- 4. It Endangers The Central Doctrine of Justification By Faith, and Thus Threatens To Revolutionize The Lutheran Doctrinal System.
- 5. It Undermines the Precious Biblical Doctrine Of The Means of Grace.
- 6. It Is Destructive Of The Comfort The Gospel Is Designed To Bring.
- More Than 100 Good Christian Books For You To Download And Enjoy

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/s43-loy-burning-question-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/s43-loy-burning-question.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request s43-loy-burning-question&body=Please send s43-loy-burning-question.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request s43-loy-burning-question&body=Please send s43-loy-burning-question.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2019-06-13
- _Version 4 update_: 2019-06-13 
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
