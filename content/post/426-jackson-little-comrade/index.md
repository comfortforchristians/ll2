---
date: 2019-04-26 08:44:13-04:00
publishDate: 2019-04-26 08:44:13-04:00
title: "Little Comrade, The Story Of A Cat, And Other Animal Stories by Gabrielle Emilie Jackson"
slug: "426-jackson-little-comrade"
categories: ["Lutheran Library Publications"]
tags: ["Jackson", "Cats", "Animals"]
authors: ["Jackson, Gabrielle Emilie"]
titles: ["Little Comrade"]
origpublishers: ["J. F. Taylor & Company"]
origdates: ["1903"]
synods: ["N/A"]
---
These four stories originally appeared in _The American Boy_, _Our Animal Friends_, and _The New York Herald_.


# Contents of the Book

-  "Little Comrade"
   1. The Cat
   2. “Just As Easy As — As––”
   3. The Trophy
   4. The Catastrophe
   5. The Rescue
-  Ted and His “Sergeant”
   1. The Sergeant Is Introduced
   2. Disputed Ownership
   3. Company K’s Mascot
   4. Hail To The Chief!
-  A Little Derelict
   1. A Strange Voyage
   2. Not A Sparrow Shall Fall
   3. Old Dolly’s Discovery
   4. “All For Dimmie And Me”
-  Madge Harding’s “Curmudge”

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/426-jackson-little-comrade-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/426-jackson-little-comrade.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 426-jackson-little-comrade&body=Please send 426-jackson-little-comrade.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 426-jackson-little-comrade&body=Please send 426-jackson-little-comrade.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2019-04-26
- _Version 4 update_:  2019-04-26
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
