---
date: 2019-05-23 11:57:41-04:00
publishDate: 2019-05-23 11:57:41-04:00
title: "Intuitu Fidei or In View of Faith. Part 2 of Walther and the Predestination Controversy or The Error of Modern Missouri by Schodde et al."
slug: "241tc-schmidt-intuitu-fidei"
categories: ["Lutheran Library Publications"]
tags: ["Schodde", "Gohdes", "Lenski", "Schmidt", "Election", "Predestination", "Predestination Controversy", "Intuitu Fidei", "Walther", "Calvinism", "Book of Concord", "Justification"]
authors: ["Schodde, George Henry","Schmidt, Frederick Augustus","Lenski, Richard C. H.", "Ghodes, Conrad"]
titles: ["Intuitu Fidei", "The Error of Modern Missouri"]
origpublishers: ["The Lutheran Book Concern"]
origdates: ["1892"]
synods: ["Ohio Synod", "Iowa Synod", "Missouri Synod", "General Council", "General Synod"]
--- 
Calvinism's "doctrines of grace" make unconditional election the first cause of a person's salvation. In contrast, the Book of Concord places "justification by faith" at the center of orthodox Christianity.  

The rise of the Synodical Conference under the LCMS has made Universal Objective Justification the Shibboleth of American Lutheranism.  Quite simply, if you do not accept UOJ, you are not considered a "Confessional Lutheran".  

Towards the end of his life, C. F. W. Walther brought forth a teaching of election which many Missouri and other American Lutherans could not reconcile with the Scriptures or the Lutheran Confessions.  The resulting "Predestination Controversy" split the Lutheran Church.


# The Lutheran Church Unanimous

"Since the publication of the Formula of Concord, for some 300 years, the Lutheran Church has unanimously held that the doctrine of our Confession and of the following teachers of our Church harmonized perfectly also in the article of predestination. Modern Missourians are the first "Lutherans" who assert the contrary; it is to be hoped that they will also be the last."

{{% toc %}}


# Justification The Proper Center of Confessional Lutheranism

"This article [justification by faith] is, as it were, the fortress and chief bulwark of the whole Christian doctrine and religion. If this article remains inviolate, the perversions of the other articles will cease of themselves." – Chemnitz 


# Harassing Yourself With The Secret Counsel of God

"Therefore no one who would be saved, should trouble or harass himself with thoughts concerning the secret counsel of God, as to whether he also is elected and ordained to eternal life, for with these miserable Satan is accustomed to attack and annoy godly hearts. But they should hear Christ (and in Him look upon the book of life in which is written the eternal election); who testifies to all men without distinction that it is God's will that all men who labor and are heavy laden with sin should come to Him in order that He may give them rest and save them (Matt. 2:28; Decl. XI) 

But if God wills that all men should come to Christ and be saved, it surely must have been His will to elect them all in Christ unto salvation, provided they would believe. The actual "election", therefore, has taken place with strict regard to faith in Christ. All who shall believe, whether they be all men, or many, or only a few, whether they be Jews or Gentiles, these or those – all believers in Christ shall have part in the election in Christ. All non-believers, however, shall be excluded. For on one hand God has ordained in His counsel to restore all those who in true repentance and faith apprehend Jesus, unto grace, sonship and heirship and to elect them thereto. On the other hand He has ordained in His counsel also, to save no one except those who believe in His Son Jesus Christ. Hence where He did not foresee faith, He did not choose to elect; where He was to elect. He wanted to foresee faith. For it was as true then as it is today: "Without faith it is impossible to please God" and to be restored unto grace, sonship and heirship, or to be ordained thereto. 



# Summary Book Contents

- Introduction.

- II. “Intuitu Fidei”

    - Part 1. What Do The Lutheran Church Fathers Teach Regarding “God Elected In View Of Faith”?
    - Introductory Remarks.
    - 1. Over against Romanism
    - 2. Sola Fide and Intuitu Fidei
    - 3. Dr.Samuel Huber
    - 4. Correct Understanding of Testimonies
    - 5. Missouri’s New Discovery
    - 6. Missouri Holds Fast the Doctrine of Our Old Teachers and Also Rejects It
    - 7. Meeting of Synodical Conference in Chicago
    - 8. Quotations from Orthodox Publications and Teachers
        - A. Authors Of The Formula Of Concord: David Chytraeus, Jacob Andreae, Christopher Koerner, Martin Chemnitz, Selnecker
        - B. Original Subscribers And Defenders Of The Formula Of Concord: Aegidius Hunnius, The Wittenberg Faculty., The Wuertemberg Theologians., John Wigand., Matthew Vogel., G. Mylius., Stephen Gerlach., Daniel Arcularius., John George Sigwart., Luke Backmeister And Jacob Coler (and Chytraeus)., David Lobech., John Winckelmann., Adam Francisci., Polycarp Leyser., Solomon Gesner., Wolfgang Mamphrasius., John Pappus., Andrew Schaafmann., Philip Nicolai., John Habermann (Avenarius)., Matthias Hafenreffer., Luke Osiander., John Coler., Concluding Remarks.
        - C. The Immediate Pupils Of The Subscribers Of The Formula Of Concord: Introduction., Leonhard Hutter., Frederick Balduin., John Weber., David Runge., George Stampel., Joachim Zehner., Esaias Silberschlag., Wolfgang Franz., Balthasar Mentzer., Peter Piscator., John Schroeder., Luke Osiander, Jr., Albert Grauer., John Foerster., John Gerhard., Justus Feuerborn., Nicolas Hunnius., Conrad Dietrich.
    - Part 2. Do Our Lutheran Fathers Depart From The Confession When Teaching That The Election Took Place In View Of Faith?
        - Does Missouri Claim Our Fathers Departed From the Confession?
        - Did Missouri Hold To Her Present Doctrine in 1872?
        - Colloquy in Columbus 1879 and Following
        - Scriptures, Ordination to Salvation, Acta Huberiana, Wuertemberg Men
    - Part 3. Is The Doctrine That God Has Elected Men To Salvation In View Of Faith Found In Our Lutheran Confession?
        - Intuitu Fidei Found In the Book of Concord
        - The Formula of Concord
        - The Eleventh Article
        - Missouri Guilty of Misusing The Confession.


- The Predestination Controversy: How It Happened.
    - How the Predestination Controversy “Broke Out”

# Essays included

In three sections by Lutheran authors well respected at the time, the history of the Predestination Controversy and the points in conflict are clearly presented and compared with extracts from the writings of the framers of the Lutheran Confessions and other Church Fathers.

This book is part 2 of [Walther and the Predestination Controversy or The Error of Modern Missouri](/239-schodde-the-error-of-modern-missouri/)

- [Part 1: The Present Controversy on Predestination](https://www.lutheranlibrary.org/240tc-stellhorn-which-predestination-reformed-or-lutheran)
- [Part 3: A Testimony Against the False Doctrine of Predestination Recently Introduced By The Missouri Synod.](https://www.lutheranlibrary.org/242tc-lenski-the-blue-island-theses-election-and-predestination)



# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)

![book cover](/img/posts/241tc-schmidt-intuitu-fidei-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/241tc-schmidt-intuitu-fidei.pdf)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 241tc&body=Please send 241tc-schmidt-intuitu-fidei.mobi)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 241tc&body=Please send 241tc-schmidt-intuitu-fidei.epub)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}



# Publication Information

- _Lutheran Library edition first published_: 2018-04-11
- _Version 4 update_: 2019-05-23
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
