---
date: 2018-01-19 12:00:00
title: "The Way Made Plain by Simon Peter Long"
slug: "190wd-long-the-way-made-plain"
categories: ["Lutheran Library Publications"]
tags: ["Short Books", "Long", "Basic Gospel"]
authors: ["Long, Simon Peter"]
titles: ["The Way Made Plain"]
origpublishers: ["Privately Printed"]
origdates: ["1903"]
synods: ["Ohio Synod"]


---
"God is Judge, and on the great Judgment Day he will not try to save you. There is no chance there for Holy Baptism; there is no chance there for catechetical instruction; there is no chance there for deliberation about what you must do to be saved. 

"The Judgment Day will not convert people, it will only let the whole world know who was converted, who was regenerated. The Judgment Day will simply state the facts that have been long before."

{{% toc %}}

# From the Introduction by David Bauslin

"The capacity to set before the people sound and wholesome theological truths in plain unambiguous speech, is something to be coveted by every preacher of Righteousness. The man in whom there is united soundness in doctrine and lucid and homely force of statement, has within himself the possibility of unusual effectiveness as a preacher. Such a man we take the author of these sermons to be." –  David H. Bauslin

# Contents

(153 pages)

- About the Author, Simon Peter Long (1860-1929)
- Introduction
- 1. The Valley Of Decision. Joel 3:14.
- 2. Jesus Is The Way And The Only Way To The Father. The Bible Knows No Other Way.
- 3. The Law Knows No Other Way. John 14:4-6.
- 4. Faith Knows No Other Way. John 14:4-6.
- 5. Prayer Knows No Other Way. John 14:4-6.
- 6. Baptism Knows No Other Way. John 14:6.
- 7. Confession And Absolution Know No Other Way.
- 8. The Lord’s Supper Knows No Other Way.
- Question Box.

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download) 

![book cover](/img/posts/190wd-long-the-way-made-plain-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/190wd-long-the-way-made-plain.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 190wd&body=Please send 190wd-long-the-way-made-plain.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 190wd&body=Please send 190wd-long-the-way-made-plain.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}
