---
date: 2019-07-22 05:33:08-04:00 
publishDate: 2019-07-22 05:33:08-04:00 
title: "Christian Liberty by Charles Krauth"
slug: "s17-krauth-christian-liberty"
categories: ["Lutheran Library Publications"]
tags: ["Krauth", "Short Books"]
authors: ["Krauth, Charles Porterfield"]
titles: ["Christian Liberty"]
origpublishers: ["Henry B. Ashmead"]
origdates: ["1860"]
synods: ["General Synod", "General Council"]
---

"The body without the spirit is dead, but it retains for a while the form; and while the form is there, hope may sometimes lie cherished that life will yet revisit it; but when even the form is gone, and the body fallen to ashes, unless God shall speak, hope is extinct forever.


"It is a sad thing to see the form robbed of the power; but there is one stage of misery below this. It is reached when the Church becomes so careless, so indolent, that she does not even keep up the form. And this was the condition of a large part of our Church. The power had vanished, and the form went with it. We take this position and defy contradiction, that the abandonment of her ancient usages, by our Church in this country originated in her deadness and not in her spirituality. – Charles Krauth



# Book Contents

- Prefatory Note.
- Christian Liberty Maintained
    - The Essence, Manifestation, and Means
    - The Fundamental Proposition
    - Individuals and Congregations
    - The Principles Applied
    - New Testament Usage
    - Clerical Apparel
    - Decline and Revival of the American Lutheran Church
    - The General Synod Did Not Originate The Diversity
    - This Usage is Right.
- Christian Liberty Defended
    - Objection 1: There Is No Express Command.
    - Objection 2: The Usages Are Offensive To Some

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/s17-krauth-christian-liberty-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/s17-krauth-christian-liberty.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request s17-krauth-christian-liberty&body=Please send s17-krauth-christian-liberty.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request s17-krauth-christian-liberty&body=Please send s17-krauth-christian-liberty.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2019-07-22
- _Version 4 update_:  2019-07-22
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
