---
date: 2019-05-06 09:51:26-04:00
publishDate: 2019-05-06 09:51:26-04:00
title: "The Six Days Of Creation, The Fall, And The Deluge by J B Reimensnyder" 
slug: "375-reimensnyder-the-six-days-of-creation"
categories: ["Lutheran Library Publications"]
tags: ["Reimensnyder", "Creation", "Genesis", "The Fall", "Original Sin", "The Flood"]
authors: ["Reimensnyder, Junius Benjamin"]
titles: ["The Six Days of Creation"]
origpublishers: ["Lutheran Publication Society"]
origdates: ["1886"]
synods: ["General Synod"]
---
"This weightiest chapter ever penned by Inspiration yields up its lessons best when literally understood, and when explained by the laws of common sense. In this volume the author has sought to show not only how false is the assertion that modern scientific discoveries refute the Mosaic accounts of the Creation, Fall and Deluge, but that they marvelously corroborate and impregnably fortify these oldest archives of our world's history.

{{% toc %}}

# Contents of the Book

- 1. Mosaic Authorship Of The Creative Record.
- 2. Origin Of The Universe By Creation.
- 3. First Day: Creation Of Light.
- 4. Second Day: Creation Of The Firmament.
- 5. Third Day Morning: Creation Of Land And Seas.
- 6. Third Day Evening: Creation Of Nature – Grasses, Plants, Trees.
- 7. Fourth Day: Creation Of The Sun, Moon, And Stars.
- 8. Fifth Day: Creation Of Animals.
- 9. Sixth Day: Creation Of Man – Fallacy Of Evolution.
- 10. Creation And Sphere Of Woman.
- 11. Creation Of Paradise.
    - The Name Of Paradise.
    - The Locality Of Paradise.
    - The Nature Of Paradise.
    - The Occupations Of Paradise.
    - The Probation Of Paradise.
    - The Typology Of Paradise.
- 12. Institution And Obligation Of The Sabbath.
    - Rest Of Jehovah.
    - The Sabbath Of Creation.
    - The Jewish Sabbath.
    - The Christian Sabbath Or Lord’s Day.
    - The Sabbath In Accord With Nature And Science.
    - The Modern And American Sunday.
    - Sunday and the State: Sunday Laws.
    - Sunday And The Working Classes.
    - The Dangers Of Sunday Desecration.
    - Type Of The Eternal Sabbath.
- 13. The Fall — The Moral Catastrophe Of Creation.
    - Origin Of Evil.
    - The Temptation.
    - The Fall.
    - The Curse.
- 14. The Deluge — The Physical Catastrophe Of Creation.
    - The Cause Of The Deluge.
    - Noah And The Ark; Skeptical Objections Refuted.
    - The Nature Of The Deluge, Was It Partial Or Universal?
    - An Actual Historical Fact.
    - Moral Lessons Of The Deluge.
- Conclusion.

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/375-reimensnyder-the-six-days-of-creation-1024.jpg)

"Ah! it is not the Voltaires, or Paines, or Ingersolls, who are open enemies, that we have to fear; but Christianity's real danger is from her secret foes, from the wolves in sheep's clothing, of whom our Lord forewarned us, who mislead the flock into paths of destruction, and from the easy-going and indifferent sheep themselves, who often are too willing to be thus beguiled." – From the book.

<a name="download"></a><br />

[PDF](/pdf/375-reimensnyder-the-six-days-of-creation.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 375-reimensnyder-the-six-days-of-creation&body=Please send 375-reimensnyder-the-six-days-of-creation.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 375-reimensnyder-the-six-days-of-creation&body=Please send 375-reimensnyder-the-six-days-of-creation.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2019-05-06
- _Version 4 update_:  2019-05-06
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

