---
date: 2019-07-01 05:38:36-04:00
publishDate: 2019-07-01 05:38:36-04:00
title: "The Story of My Life by Matthias Loy"
slug: "180lb-loy-story-of-my-life"
categories: ["Lutheran Library Publications"]
tags: ["Biography", "Loy","Start Here"]
authors: ["Loy, Matthias"]
titles: ["The Story of My Life"]
origpublishers: ["Lutheran Book Concern"]
origdates: ["1905"]
synods: ["Ohio Synod"]

---

"The history of the Church confirms and illustrates the teachings of the Bible, that yielding little by little leads to yielding more and more, until all is in danger; and the tempter is never satisfied until all is lost. 

"It seems but a small concession that we are asked to make when an article of our confession is represented as a stumbling block to many Christians which ought therefore in charity to be removed, but surrendering that article would only lead to the surrender of another on the same ground, and that is the beginning of the end; the authority of the inspired Word of our Lord is gradually undermined. 

"There is not an article in our creed that is not an offence to somebody; there is scarcely an article that is not a stumbling block to some who still profess to be Christians. It is impossible to find a place to stop, when the concessions once begin. And the reason is manifest; the principle is wrong, and displaces a principle that is right. The one is human, the other is divine; the human opinion and sentiment is substituted as a rule and guide for the Word of God and the faith that accepts it as absolute authority. – Matthias Loy

{{% toc %}}
 
# _Lutheran Standard_ Editor for 25 Years

"The course which I pursued as editor, and which with my faith and convictions could not be otherwise, led me into many controversies. The reason for this is not that I had any special delight in polemics. I love peace and quiet, and would rather suffer wrong than fight. But what God had entrusted to my keeping I could not surrender without losing His favor and my peace. My editorship extended over a period of more than a quarter of a century, and often required me to say what I knew must be displeasing to men of other minds. I could not have been faithful to the Lord and His Church if I had not been willing to defend the truth when assaults were made upon it. The grace of God always protected me from the weakness of letting my natural love of peace overcome the love of His cause. He had taught me to contend earnestly for the faith, and sustained me in the purpose which He had given me to do His will. It was the truth, for the maintenance and defense of which the _Standard_ was set, that provoked controversy.

"It was clear to me that if the paper was properly to fulfill its mission it must do something more than to furnish church news, or even to supply its readers with brief items of light religious reading, with which an idle hour might be whiled away and which might in a certain sense be called edifying. Even my idea of edification would not permit me to adopt such an editorial management. Without a knowledge of the truth revealed in Holy Scripture and an intelligent appreciation of at least its principal doctrines there can be no solid and lasting edification, readily as it may be admitted that a sort of sentimental piety might be instilled in souls ignorant of the way of salvation and a wild and thoughtless activity might be produced through an excited "zeal without knowledge." I had ample opportunity, during my long abode in a Methodist town, to observe how such a religious training works, and I had read enough to know whither it leads when reduced to a system. 

# Contents (246 pages)

- Preface.
- 1. Childhood.
- 2. Printer.
- 3. Student.
- 4. Pastor.
- 5. Synod.
- 6. Home.
- 7. Editor.
- 8. Professor.
- 9. Author.
- Selected Bibliography
- Hymns by Matthias Loy

# About Matthias Loy

"Matthias Loy was born March 17, 1828 in Pennsylvania. After a boyhood of poverty, he was apprenticed as a printer in Harrisburg in 1847, was treat­ed well, read some English classics, learned Latin and Greek, and was con­firmed by Charles W. Schaeffer.

"In 1847, Loy went west for his health and was persuaded by Rev. J. Roof to become a beneficiary student at the seminary of the Joint Synod of Ohio at Columbus, where his teachers included Christian Spielmann and Wilhelm Lehmann. He was strongly influenced at this time by the works of C. F. W. Walther and by several friends who were ministers in the Missouri Synod. In 1849 he became pastor at Delaware, Ohio where he served until 1865.

"On Christmas Day, 1853, Loy married Mary Willey. Despite his lifelong frailty and illness, Loy accomplished much. He was President of the Joint Synod (1860-78, 1880-94), editor of the Lutheran Standard (1864-91), Professor of Theology at Capital University (1865-1902), and President of the University (1881-90). During his lifetime and under his direction, the Synod grew to have a national influence. He was a zealous supporter of the Lutheran Confessions.

"In 1867 he refused to let the Joint Synod become a member of the General Council, and framed his objections in the form of “four points”: Chiliasm, altar fellowship, pulpit fellowship, and secret societies. In 1871, Loy led the Joint Synod into the Synodical Conference. In 1881 he rejected Walther’s doctrine of predestination, founded the Columbus Theological Magazine (1881-1888) to combat it, and withdrew the Joint Synod from the Synodical Conference.

"Loy was forced to retire for health reasons in 1902. He went to be with the Lord on January 26, 1915.

From "Matthias Loy". Retrieved 2017-11-26 from cyberhymnal.org/bio/l/o/loy_m.htm

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download) 

![book cover](/img/posts/180lb-loy-story-of-my-life-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/180lb-loy-story-of-my-life.pdf)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 180lb&body=Please send 180lb-loy-story-of-my-life.mobi)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 180lb&body=Please send 180lb-loy-story-of-my-life.epub)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2018-03-20
- _Version 4 update_: 2019-07-01 
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
