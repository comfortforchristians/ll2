---
date: 2017-12-28 12:00:00
title: "Churches And Sects Of Christendom by J. L. Neve" 
slug: "176tc-neve-churches-sects-christendom"
categories: ["Lutheran Library Publications"]
tags: ["Neve", "Roman Catholicism", "Confessions"]
authors: ["Neve, Juergen Ludwig"]
titles: ["Churches And Sects Of Christendom"]
origpublishers: ["The Lutheran Literary Board"]
origdates: ["1940"]
synods: ["Iowa Synod"]

---
"The Christian religion has been called a way of life. It is indeed a way of life, but it is at the same time a way of faith and a way of thought. If it should cease to be a way of thought, it would soon cease to be a way of life. 

"The things we believe make up our creed, and our creed determines our character and our conduct. For this reason the first thing we wish to know about a man is his creed – Kerr, Dr. H.T. for the Presbyterian Board of Christian Education as a "Guide for Individual Christians or Communicant Classes," 1937. Cited by Neve. *Churches and sects of Christendom* 

{{% toc %}}

# The Difference Between a Church and a Sect

The historical churches are results of the very great, epoch-making events in the Church's history. Each of them has a theology of its own, which is marked by historicity and by a deep-going organic consistency in doctrine and practice. They claim that their baptized children are under the regenerating grace of the Holy Spirit through the influence of the Word and Sacrament. There will be cases where the necessity of a special conversion must be stressed, in the process of which the powers of regeneration will then function. But the "church" will reject the idea that the individual is supposed to have received no grace before such a "conversion" has taken place. Just as the prodigal son knew of a time when he was yet a child in his father's house, so the sermons and all the pastoral work of a church will remind men of that relation once established in holy baptism. Under normal conditions this relation simply needs to be kept up under careful nursing with the Word of God, and the result then can be expected as described in Mark 4:26-28: "_So is the kingdom of God, as if a man should cast seed into the ground; and should sleep, and rise night and day, and the seed should spring and grow up, he knoweth not how. For the earth bringeth forth fruit of herself; first the blade, then the ear, after that the full corn in the ear._" This description gives us the character of a church in distinction from that of a sect. 

# Chapters 

(698 pages)

- Foreword
- Introductory Matters
  - I. Creeds or Confessions of Faith
  - II. The Common Confessional Heritage of all Christendom. (Ecumenical Creeds)
  - III. Departure Into Denominationalism.
  - IV. Ought There Be a Distinction Between Church and Sect?
  - V. A New Method for Studying the Church Groups:
  - VII. The Quotation of Scripture.
- Chapter One – The Eastern Orthodox Churches
- Chapter Two, Part One – Roman Catholicism as an Organization.
- Chapter Two, Part Two – The Roman Catholic Dogma.
- Chapter Three – Old Catholic Churches and Relatives
- Chapter Four – The Lutheran Church
- Chapter Five – The Reformed And The Presbyterians
- Chapter Six – The Anglican And Episcopal Churches
- Chapter Seven, Part One – The Methodist Bodies
- Chapter Seven, Part Two – Bodies Related To Methodism (United Brethren, Evangelicals, Holiness and Pentecostal, Salvation Army)
- Chapter Eight – The Union Bodies (Moravians, Disciples of Christ, Community Churches, Churches of Christ)
- Chapter Nine – The Congregational Christian Church
- Chapter Ten – The Baptists, Their Predecessors And Their Relatives (Mennonites)
- Chapter Eleven – Quakerism And The Quakers (Friends)
- Chapter Twelve – The Rationalist Group (Unitarians, Universalists).
- Chapter Thirteen – The Adventist Bodies
- Chapter Fourteen – Movements And Organizations Independent And Unrelated (Plymouth Brethren, Mormons, Jehovah's Witnesses, Christian Science, Unity)
- Appendix 1. The Problem of Church Union
- Appendix 2. Modernism/Liberalism as Against Conservative Theology

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download) 

![book cover](/img/posts/176tc-neve-churches-sects-christendom-1024.jpg)
<a name="download"></a><br />

[PDF](/pdf/176tc-neve-churches-sects-christendom.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 176tc&body=Please send 176tc-neve-churches-sects-christendom.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 176tc&body=Please send 176tc-neve-churches-sects-christendom.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}
