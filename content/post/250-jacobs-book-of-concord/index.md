---
date: 2019-04-22 15:23:18-04:00
publishDate: 2019-04-22 15:23:18-04:00
title: "The Book of Concord: The Symbolical Books of the Evangelical Lutheran Church by Henry Eyster Jacobs and Charles Krauth"
slug: "250-jacobs-book-of-concord"
categories: ["Lutheran Library Publications"]
tags: ["Jacobs", "Krauth", "Confessions", "Catechism", "History","Start Here"]
authors: ["Jacobs, Henry Eyster"]
titles: ["The Book of Concord"]
origpublishers: ["United Lutheran Publication House"]
origdates: ["1911"]
synods: ["General Council", "General Synod"]
---
Here is a clear, trustworthy and easy-to-search and navigate version of the Lutheran Confessions.  This edition was prepared by Henry Eyster Jacobs for the use of all the Lutheran Churches in America and published as _The People's Edition_.

{{% toc %}}

# Summary of the Contents

- I. The General Creeds: The Apostles’ Creed. The Nicene Creed. The Athanasian Creed.
- II. The Augsburg Confession
- III. Apology Of The Augsburg Confession.
- IV. The Smalcald Articles.
- V. The Small Catechism of Martin Luther
- VI. The Large Catechism of Dr. Martin Luther.
- VII. The Formula Of Concord.
- Appendix: The Saxon Visitation Articles

# About the Translation

"The translation of the Augsburg Confession adopted in this volume is the well-known one of Dr. Charles P. Krauth, which he has kindly revised as the proof-sheets passed through his hands. 

"In the Small Catechism, the translation prepared by Dr, Charles F. Schaeffer with the co-operation of a committee of the Ministerium of Pennsylvania, and in universal use in the English churches of the General Council, is reprinted, with the addition of the formula for confession contained in the Book of Concord. 

"The Large Catechism was translated for this work by Rev. A. Martin, Professor of the German Language and Literature in Pennsylvania College, to whom the Editor is greatly indebted for assistance and advice also in other directions. 

"The Apology of the Augsburg Confession, the Smalcald Articles and the Formula of Concord were translated by Henry Eyster Jacobs. The rendering of the Apology is from the Latin, the German translation of Justus Jonas of the _Concordienbuch_ being more of a paraphrase than a translation, differing sometimes from the original by the omission, introduction and transposition of entire paragraphs, and therefore inducing the editors of some of the best German editions of the Symbolical Books to prepare fresh translations. We have, accordingly, carefully revised our translation from the Latin, by comparing it with the German translations of Schöpf, Köthe, Spieker and Bodemann.


# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/250-jacobs-book-of-concord-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/250-jacobs-book-of-concord.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 250-jacobs-book-of-concord&body=Please send 250-jacobs-book-of-concord.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 250-jacobs-book-of-concord&body=Please send 250-jacobs-book-of-concord.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 2019-04-23 
- _Version 4 update_:  2019-04-23
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
