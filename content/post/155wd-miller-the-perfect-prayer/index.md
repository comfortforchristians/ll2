---
date: 2017-12-07 12:00:00
title: "The Lord's Prayer: A Devotional Study Based on Luther's Smaller Catechism by Charles Armond Miller"
slug: "155wd-miller-the-perfect-prayer"
categories: ["Lutheran Library Publications"]
tags: ["Catechism", "Prayer", "Miller"]
authors: ["Miller, Charles Armond"]
titles: ["The Lord's Prayer: A Devotional Study Based on Luther's Smaller Catechism"]
origpublishers: ["General Council Publication House"]
origdates: ["1902"]
synods: ["General Council"]

---
"Our God says: 'I not only freely forgive, but as for your sins, I have cast them behind my back.'

{{% toc %}}

# Luther and a Friend

"Luther was once walking with a friend through a country full of beauty, and both being deeply impressed with the loveliness of nature, as her charms lay before their eyes and spoke to their appreciative hearts, the friend said, "Earth, in its beauty, is a paradise." "Yes," was the answer, "paradise enough if sin were not." There is the core and kernel of all our woe and sadness, bereavement, pain and loss. Two things we should learn: the hatefulness of sin and Satan, and the goodness of God. Trial should bring us to hate evil. And the remembrance of the mighty goodness of the Lord, which shall turn these sores and pains, and losses and griefs into means of chastening, into ways of profit, so that they shall work for us a far more exceeding and eternal weight of glory, should cause in us undying thankfulness to Him who will "deliver us from evil." 

# Vain Repetition?

One of the objections made to the use of the *Lord's Prayer* is Jesus's condemnation of the "vain repetition" of the unbelievers in Matthew 6:7-8.  Rev. Miller says this:

"It is not a question of the repeating of so many forms. Prayer is not acceptable in proportion to its length. That is a heathen idea. The prophets of Baal, crying aloud from morning till night, "O Baal, hear us!" "O Baal, hear us!" had that idea. The Hindus and Mohammedans of today have that idea. The Church of Rome, teaching her people, as a penance, to repeat so many "Ave Marias" and so many "Pater Nosters," keeping account, the while, on their string of beads, arranged for that purpose, has something very like the idea the Master rejected.       

Rather, this *perfect prayer* given by our Lord is to be used thoughtfully.  Luther, as quoted by Rev. Miller:

"The Lord's Prayer, because it arises from the Lord, is without question the highest, noblest and best. For if He had known a better, the holy faithful school-master that He was, He would have taught us that also. There is on earth no nobler prayer to be found, it is the highest under the sun. It is a wall and bulwark of the Church, a strong weapon for all godly Christians, a prayer above all prayers. I am well aware that I do not rightly know the Lord's Prayer, old and experienced Doctor that I am! Like a babe I daily draw milk from the Lord's Prayer, and cannot get enough of it; to me it surpasses even the Psalms, although I have great love for them, and it is the very best of prayers. In other prayers is not the sap, the power, the fervor and the fire, which I find in the Psalter and the Lord's Prayer. Truly He was a wise man who made it, after whom no one can do any thing! The Lord's Prayer binds the people together, so that each prays with and for the other, and it makes strong and mighty, so that it drives away even death. The Lord's Prayer is my prayer, I use it, and intermingle at times something from the Psalms. _But it is a pity above all pities that such a prayer of such a Master should be babbled and chattered without the least devotion over all the world_. 

"Many pray the Lord's Prayer many times in a year, and if they prayed in the same way a thousand years, they would never then have tasted or prayed a word or a tittle of it." – Martin Luther

# Chapters 

- The School Of Prayer
  - 1 Prayer is Indispensable to the Christian Life.
  - 2 Prayer is a Thing of Intimate, Secret Communion with God.
  - 3 Prayer Involves Trust in the Father’s Love and Wisdom.
  - 4 Prayer Should Expect and Receive an Answer.
- The Introduction
- The First Petition
- The Second Petition
- The Third Petition
- The Fourth Petition
- The Fifth Petition
- The Sixth Petition
- The Seventh Petition
- The Conclusion
- The Prayer As A Whole

# "Lord, teach us to pray."

"(According to the account in St. Luke 11:2) Jesus was asked by one of his disciples, not one of the Twelve, but a more recent follower, who had not been present at the first giving of the prayer, _"Lord, teach us to pray."_ It is a suggestive fact that the Lord did not find it necessary to give any further instruction or any substitute or improvement but repeated the answer He had already given, and thus distinctly authorized the use of what we call the Lord's Prayer, not only as a model, but as a fixed form. _"And He said unto them, When ye pray, say, 'Our Father which art in heaven.'"_ Here, once for all, is given justification, nay, even command, for the devout and fervent use of forms of prayer. 

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download) 

![book cover](/img/posts/155wd-miller-the-perfect-prayer-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/155wd-miller-the-perfect-prayer.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 155wd&body=Please send 155wd-miller-the-perfect-prayer.epub)

[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 155wd&body=Please send 155wd-miller-the-perfect-prayer.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


"'Our God says: "I not only freely forgive, but as for your sins, I have cast them behind my back."

---

