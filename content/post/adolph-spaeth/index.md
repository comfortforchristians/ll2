---
publishDate: 2019-02-18 05:01:28-05:00
date: 2019-02-18 05:01:28-05:00
title: "Adolph Spaeth: A Biographical Sketch"
slug: "adolph-spaeth"
categories: ["Biographical Sketches"]
tags: ["Spaeth", "Lutheran Ministers"]
authors: ["Spaeth, Adolph"]
synods: ["General Council"]
---

[![Adolph Spaeth](/img/posts/spaeth-adolph.jpg)](https://www.lutheranlibrary.org/authors/spaeth-adolph/)

# Rev. Prof. A. Spaeth, D.D. 

The land of Brentz and Jacob Andreae has been represented in our seminary from its very inception in the person of the eminent professor of Hebrew. The incumbent of the chair of New Testament Exegesis, the Rev. Dr. Adolph Spaeth, is also a native of the kingdom of Wuertemberg, having been born in the town of Esslingen, on the 29th of October, 1839. A very thorough course of classical training prepared him for the study of theology at the University of Tuebingen, where he was graduated in 1861. 

{{% alert note %}}
[Download a printable PDF](/pdf/adolph-spaeth.pdf)
{{% /alert %}}

His first experience in the active ministry, to which he was ordained in October, 1861, was had in the position of a Vicar, to which he was appointed on the death of a pastor, whose family shared in the benefits of this arrangement, being thus enabled to remain in their old homes and retain a portion of their former income. Previous to his departure for America, Dr. Spaeth spent some time in Scotland engaged in teaching. The Marquis of Lorne, so well known to Americans a few years ago as the Governor-General of Canada, was his pupil. This sojourn also led to his marriage to a daughter of Dr. Duncan, of Edinburgh. 

In the year 1863 a call was extended him by the congregations of Zion's and St. Michael's of Philadelphia, to aid Dr. Mann in his arduous labors. When St. John's German Lutheran church was established as an outgrowth of Zion's, Dr. Spaeth took charge of the new enterprise, and has held the pastorate ever since. It is universally acknowledged that by reason of his power of thought and his mastery of the German language, as well as his eloquence, no man in the German pulpit of this country is his superior. Another sphere of activity, implying much additional labor and great responsibility, cognate in its nature and yet although distinct from his pulpit work, was opened up when the New York Ministerium offered to endow a chair in the Seminary, whose history is just at present attracting the cordial attention of her sons and friends. Dr. Spaeth assumed the duties of his new position in the year 1873, and has been constant in his attention to the demands made upon him as professor of New Testament Exegesis and Catechetics. 

The proper discharge of all the duties gf his double calling, as pastor of a large congregation, with all its labors in and out of the pulpit, and of professor in important departments of the Seminary, would seem to be all that should be required of one individual. But, like so many of our ministers who are able and willing to work, as there seems to be hardly any limit to the work expected by the Church, one burden after another has been laid upon Dr. Spaeth's shoulders. In the year 1880 he was elected to the presidency of the General Council of the Evangelical Lutheran Church in North America, and continued to hold this distinguished place until the meeting in Minneapolis in the year 1888. But one of his predecessors in the chair of the General Council served for a greater number of years, the Rev. Dr. Charles P. Krauth, himself so closely identified with the seminary from the day of its foundation until the day of his departure from earth. 

In the year 1887 Dr. Spaeth was deputed by the General Council as delegate to the General Conference of the Lutheran Church of Germany, which met in the city of Hamburg, and where Dr. Spaeth was accorded an opportunity to present the status of the Lutheran Church of America, in the presence of the most distinguished Lutheran theologians of the fatherland. From time to time the General Council has charged Dr. Spaeth with important interests. In connection with others he has served on the committee having charge of the great work of Foreign Missions. He has been especially active in the liturgical and hymnological work of the Council, associated with the late Dr. Beale M. Schmucker, whose untiring zeal in these departments is yet fresh in our memory, and who was also identified with the work of the Seminary from its beginning until his sudden removal from our midst. 

Dr. Spaeth has shown a special interest in woman's work in the church. He has visited the institutions devoted to these labors of mercy in Germany, where this activity in its evangelical spirit originated, and he may well be termed the father of the movement in the Evangelical Lutheran Church in this country. His pen and his voice have ever been ready to promote a cause, which promises to be productive of so much good in stirring up the church to the active exercise of charity in the alleviation of suffering. The address on Phoebe, the Deaconess, published in German and English, 1885, sounded the keynote of a movement to which a layman, worthy of all honor for his kind liberality, Mr, John D. Lankenau, has consecrated thousands of dollars, in the erection of a monument that will, for years to come, remind the church and the world of the power that dwells in Christian love. 

As a writer, Dr. Spaeth has been untiringly active. Since the death of that indefatigable worker, the Rev. S. K. Brobst, another member of the noble band of brethren, who labored and prayed for the welfare of our Seminary, he has been in editorial charge of the _Jugend Freund_. With rare exceptions every weekly issue of the _Herold und Zeitschrift_ brings an article from him, rich in comments on the Gospels of the Church year. The _Lutheran Church Review_, of whose editorial corps he is a member, contains quite a number of articles from his pen. 

Dr. Spaeth's fondness of hymnology, and his admiration for the great Reformer of his Church, shows itself in his "Luther im Lied seiner Zeitgenossen," published in 1883; address before the General Council, 1888; Heimath Grusse; Lieder Lust; Hausgottesdienst; Having and not Having; in Memoriam of A H. Schnabel; Brosamen; Luther in Liedern; Antrittsrede; Phoebs: Funeral of C. S. Schaeffer, D. D.; St Paulus; Der Ruf Zum Grossen Abendmahl; The General Council; Luther der Gottes-Held; Amer. Beleuchtung; Luther our Ensample; Theses on Galesburg Declaration; Sermon on Trinity Sunday; Address at Brooklyn; Luther's Mnety-five Theses; Program of Luther Jubilee; Jugenfreund Lieder; Liturgische Andachten; Predigt ueber Math. 9; Abraham Lincoln; Gutachten ueber Gnadenwahl; Faith and Life; Die Ersten im Weinberge; Reply to Dr. Valentine; Gedaechtniss-feier William IV; The Nation and the Gospel; Christtag Andacht; Das Konigliche Hochzeitsmahl; Von d. Seele his auf Fleisch; Reformation und Reformirung. 

Dr. Spaeth has also gathered considerable material for a biographical memorial of his father-in-law. Dr. Krauth, to be entitled: _The Life, Correspondence and Works of Charles P. Krauth_. The University of Pennsylvania, in the year 1875, formally recognized his abilities by conferring upon him the degree of Doctor of Divinity. Dr. Spaeth is still in the prime of life, and celebrates his own semi-centenial, almost simultaneously with the quarter-centenial of the theological school to which he has devoted so much of his time and his strength. May he live to witness the semi-centenial celebration of our Alma Mater. — _Indicator_. 

{{% alert note %}}
This biography is taken from Jens Jensson's _American Lutheran Biographies_, published 1890.
{{% /alert %}}

First published online 2019 at [LutheranLibrary.org](.).