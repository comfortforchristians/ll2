---
date: 2019-03-04 01:02:16-05:00
publishDate: 2019-03-04 01:02:16-05:00
title: "The Trampled Cross by Joseph Hocking"
slug: "344-hocking-the-trampled-cross"
categories: ["Lutheran Library Publications"]
tags: ["Hocking", "Islam", "Faith", "Fiction"]
authors: ["Hocking, Joseph"]
titles: ["The Trampled Cross"]
origpublishers: ["Hodder and Stoughton, Ltd."]
origdates: ["1910"]
synods: ["Methodist"]
---
"A young English man in the early 20th century is captured by a fanatical tribe of Muslims and given the choice between death and trampling on the cross.  Life is more to him than religion, and he places his heel where the two sticks cross each other, and crushes them into the desert sands.

"This is the story of the consequences of this decision.  What would you have done?

# About the Author

Joseph Hocking was a faithful Welsh minister.  A prolific and popular writer in his lifetime, Rev. Hocking considered the novel an ideal platform for exploring Christian spirituality and the deeper aspects of life.  His books deal with trials, the difficulties of life, and issues of faith.

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/344-hocking-the-trampled-cross-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/344-hocking-the-trampled-cross.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 344-hocking-the-trampled-cross&body=Please send 344-hocking-the-trampled-cross.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 344-hocking-the-trampled-cross&body=Please send 344-hocking-the-trampled-cross.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}

