---
date: 2019-06-24 05:25:59-04:00 
publishDate: 2019-06-24 05:25:59-04:00 
title: "Aamon Always by Dan E. L. Patch"
slug: "479-patch-aamon-always"
categories: ["Lutheran Library Publications"]
tags: ["Patch", "Fiction", "Anti-Semitism", "War", "Orphans", "Jewish Christians", "Messianic Jews"]
authors: ["Patch, Dan E. L."]
titles: ["Aamon Always"]
origpublishers: ["Bible Institute Colportage Association Press"]
origdates: ["1940"]
synods: ["Unknown"]
---

Dan E. L. Patch, a devoted Christian and the police chief of Highland Park, Michigan, wrote this book in 1940, just before the US entered World War II.  It's a gripping story of a young man who transcends corruption and misfortune.


# Book Contents

- Foreword
- 1. Life’s Equations
- 2. Complications Develop
- 3. The Unseen Web
- 4. Defiance
- 5. Questions, Legal and Otherwise
- 6. Where the Treasure Is, There Shall the Heart be Also
- 7. A Venture in Crime
- 8. Whose Gold Is It?
- 9. Premonition of Trouble
- 10. Once a Jew, Always a Jew
- 11. Languishing in Jail
- 12. The Shadows Fall
- 13. Choice of Two Evils
- 14. The Die is Cast
- 15. The Law of Compensation
- 16. Facing Finland and Death
- 17. Future Without Promise
- 18. In Disguise
- 19. The Stowaway
- 20. The Holy Land
- 21. An Ensign for the Nations
- 22. A Man of Mystery
- 23. The Lord’s Doing
- 24. The Stumbling Block
- 25. The Great Iron Claw

# Download the eBook [<i class="fas fa-arrow-circle-down"></i>](#download)


![book cover](/img/posts/479-patch-aamon-always-1024.jpg)

<a name="download"></a><br />

[PDF](/pdf/479-patch-aamon-always.pdf)

[EPUB (Apple, Kobo)](mailto:lutheranlibrary@runbox.com?subject=Epub request 479-patch-aamon-always&body=Please send 479-patch-aamon-always.epub)


[MOBI (Kindle)](mailto:lutheranlibrary@runbox.com?subject=Kindle request 479-patch-aamon-always&body=Please send 479-patch-aamon-always.mobi)

{{% alert note %}}
{{% request-any-title %}}
{{% /alert %}}


# Publication Information

- _Lutheran Library edition first published_: 
- _Version 4 update_:  
- _Copyright_: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
