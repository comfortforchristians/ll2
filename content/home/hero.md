+++
# Hero widget.
widget = "hero"
active = true
date = 2017-10-15T00:00:00

title = "The Lutheran Library Publishing Ministry"

# Order that this section will appear in.
weight = 3

# Overlay a color or image (optional).
#   Deactivate an option by commenting out the line, prefixing it with `#`.
[header]
#  overlay_color = "#666"  # An HTML color value.
# Image path relative to your `static/img/` folder.
#  overlay_img = "header/spring1.jpg"  
overlay_img = "header/ray-hennessy-253874-unsplash.jpg"
#  overlay_img = "header/Der_Anschlag_von_Luthers_95_Thesen.jpg" 
#  overlay_filter = 0.1  # Darken the image. Value in range 0-1.

# Call to action button (optional).
#   Activate the button by specifying a URL and button label below.
#   Deactivate by commenting out parameters, prefixing lines with `#`.
#[cta]
#  url = "./post/getting-started/"
#  label = '<i class="fas fa-download"></i> Install Now'
+++

<p style="color:white"><strong><big><big>More than a hundred good Christian books for you to download and enjoy.</big></big></strong></p>

<br \><br \><br \><br \><br \>

<p style="color:white"><strong><big>A non-profit publisher of well-written, Biblically sound books.  All Kindle<sup>®</sup> ready and available at no cost and with no licensing fees.</big> <a href="/statement-of-faith/"><i class="far fa-arrow-alt-circle-right"></i> Read More...</a></strong>
