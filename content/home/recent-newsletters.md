+++
# Recent Posts widget.
# This widget displays recent posts from `content/post/`.
widget = "posts"  # Do not modify this line!
active = true  # Activate this widget? true/false
weight = 50  # Order that this section will appear.
date = 2016-03-20T00:00:00

title = "Recent Newsletters"
subtitle = ""

# Number of recent posts to list.
count = 5

# List format.
#   0 = Simple
#   1 = Detailed
#   2 = Stream
list_format = 0

# Filter posts by tag or category.
#  E.g. to only show posts tagged with `Academic`, set `filter_tag = "Academic"`
filter_tag = ""
filter_category = "Lutheran Library Newsletters"
+++

